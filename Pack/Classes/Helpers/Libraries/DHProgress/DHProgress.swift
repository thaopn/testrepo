import UIKit

class DHProgress: NSObject {

  static var isShow: Bool = false
  static var viewBackground: UIView = UIView (frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height
    ))
  static var viewFake: UIView = UIView (frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height
    ))
  static var viewMain: UIView = UIView (frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height
    ))
  static var progressView: KDCircularProgress = KDCircularProgress(frame: CGRectMake((UIScreen.mainScreen().bounds.size.width-150)/2, (UIScreen.mainScreen().bounds.size.height - (150+30+0))/2, 150, 150
    ))
  static var labelPercent: UILabel = UILabel (frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 30))
  static var labelProgress: UILabel = UILabel (frame: CGRectMake(0, progressView.frame.origin.y + progressView.frame.size.height+0, UIScreen.mainScreen().bounds.size.width, 30))

  class func showWithValue(value: Float) -> Void {

    dispatch_async(dispatch_get_main_queue(), {
      progressView.angle = Int(0 * 360)
      progressView.startAngle = -90
      progressView.progressThickness = 0.15
      progressView.trackThickness = 0.15
      progressView.clockwise = true
      progressView.gradientRotateSpeed = 3
      progressView.roundedCorners = true
      progressView.glowMode = .Forward
      progressView.glowAmount = 0.9
      progressView.setColors(UIColor.whiteColor(), UIColor.whiteColor(), UIColor.cyanColor(), UIColor.whiteColor(), UIColor.whiteColor())
      labelPercent.center = progressView.center
      labelPercent.textAlignment = NSTextAlignment.Center
      labelPercent.font = UIFont(name: nameFontBold, size: changeFontSizeWithDevice(30))
      labelPercent.textColor = UIColor.whiteColor()
      labelPercent.text = "\(Int(value*100))" + "%"
      labelProgress.textAlignment = NSTextAlignment.Center
      labelProgress.font = UIFont(name: nameFontBold, size: changeFontSizeWithDevice(17))
      labelProgress.textColor = UIColor.whiteColor()
      labelProgress.text = "Uploading ..."
      if isShow == false {
        labelProgress.alpha = 1.0
        UIView.animateWithDuration(0.89, delay:0, options: [.Repeat, .Autoreverse], animations: {
          labelProgress.alpha = 0.0
          }, completion: { (finished) -> Void in
            UIView.animateWithDuration(0.89, animations: {
              labelProgress.alpha = 1.0
            })
        })
      }
      viewBackground.backgroundColor = UIColor.clearColor()
      viewFake.backgroundColor = UIColor.blackColor()
      viewMain.backgroundColor = UIColor.clearColor()
      viewFake.alpha = 0.85
      viewBackground.addSubview(viewFake)
      viewBackground.addSubview(viewMain)
      viewMain.addSubview(labelProgress)
      viewMain.addSubview(progressView)
      viewMain.addSubview(labelPercent)
      let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
      appDelegate.window?.addSubview(viewBackground)
      viewBackground.alpha = 1.0
      progressView.angle = Int(value * 360)
      isShow = true
    })
  }

  class func hide() -> Void {

    dispatch_async(dispatch_get_main_queue(), {
      viewBackground.alpha = 0.0
      viewBackground.removeFromSuperview()
      UIView.commitAnimations()
      isShow = false
    })
  }
}
