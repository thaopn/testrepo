import UIKit

class DHCalendarData: NSObject {

  // MARK: - Calendar Current
  class func getCalendarCurrent() -> NSCalendar {
    return NSCalendar(identifier: NSCalendarIdentifierGregorian)!
  }

  // MARK: - Year
  class func getYearWithDate(date: NSDate) -> Int {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)

    return comps.year
  }

  class func getFirstDayOfYearWithDate(date: NSDate) -> NSDate {

    let comps = NSDateComponents()
    comps.day = 1
    comps.month = 1
    comps.year = self.getYearWithDate(date)

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func getEndDayOfYearWithDate(date: NSDate) -> NSDate {

    let comps = NSDateComponents()
    comps.day = 31
    comps.month = 12
    comps.year = self.getYearWithDate(date)

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func nextYearFormDate(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)

    comps.year = comps.year + 1
    return getCalendarCurrent().dateFromComponents(comps)!
  }

  // MARK: - Month
  class func numberDayOfMonthWithDate(date: NSDate) -> Int {
    let days: NSRange = self.getCalendarCurrent().rangeOfUnit(.Day, inUnit:.Month, forDate: date)

    return days.length
  }

  class func getFirstDayOfMonthWithDate(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)

    comps.day = 1
    return getCalendarCurrent().dateFromComponents(comps)!
  }

  class func getEndDayOfMonthWithDate(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)

    comps.day = self.numberDayOfMonthWithDate(date)
    return getCalendarCurrent().dateFromComponents(comps)!
  }

  class func getMonthWithDate(date: NSDate) -> Int {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)
    return comps.month
  }

  class func numberMonthFromDate(fromDate: NSDate, toDate: NSDate) -> Int {
    if fromDate.timeIntervalSinceNow > toDate.timeIntervalSinceNow {
      return 0
    }

    let monthFrom = self.getMonthWithDate(fromDate)
    let yearFrom = self.getYearWithDate(fromDate)

    let monthTo = self.getMonthWithDate(toDate)
    let yearTo = self.getYearWithDate(toDate)

    if yearFrom == yearTo {
      return monthTo - monthFrom + 1
    } else if yearFrom + 1 == yearTo {
      return (12 - monthFrom) + 1 + (monthTo - 1) + 1
    } else {
      return (12 - monthFrom) + 1 + (monthTo - 1) + 1 + (yearTo - yearFrom - 1 ) * 12
    }
  }

  class func daysInMonthWihtDate(date: NSDate) -> [NSDate] {
    var daysInMonth: [NSDate] = []
    let days = self.getCalendarCurrent().rangeOfUnit(.Day, inUnit: .Month, forDate: date)
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)

    for i in days.location ..< days.location + days.length {
      comps.day = i
      daysInMonth.append(self.getCalendarCurrent().dateFromComponents(comps)!)
    }

    return daysInMonth
  }

  // MARK: - Day
  class func getDayWithDate(date: NSDate) -> Int {
    let components = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)
    return components.day
  }

  class func getWeekDayWithDate(date: NSDate) -> Int {
    let comps = self.getCalendarCurrent().components([.Weekday], fromDate: date)

    return comps.weekday
  }

  class func daysWithStartDate(startDate: NSDate, endDate: NSDate) -> Int {
    let components = self.getCalendarCurrent().components([.Day], fromDate: startDate, toDate: endDate, options: [])

    return components.day
  }

  // MARK: - Date
  class func dateWithDay(day: Int, month: Int, year: Int) -> NSDate {
    let comps = NSDateComponents()
    comps.day = day
    comps.month = month
    comps.year = year

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func dateNextMonthFromDate(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)
    comps.month = comps.month + 1
    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func datePrevMonthFromDate(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)
    comps.month = comps.month - 1
    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func dateWithAddDay(day: Int, month: Int, year: Int, fromDate: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: fromDate)

    comps.day = comps.day + day
    comps.month = comps.month + month
    comps.year = comps.year + year

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func nextDateFrom(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)

    comps.day = comps.day + 1

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func prevDateFrom(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day], fromDate: date)

    comps.day = comps.day - 1

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func saturdayOfWeek(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day, .Weekday], fromDate: date)

    var saturdate: NSDate = date
    for _ in comps.weekday ..< 7 {
      saturdate = self.nextDateFrom(saturdate)
    }

    return saturdate
  }

  class func dateNextHourFromDate(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: date)

    comps.second = 0
    comps.minute = 0
    comps.hour = comps.hour + 1

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func dateNextHourNoTrimMinFromDate(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: date)

    comps.hour = comps.hour + 1

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func dateAddHour(hour: Int, withDate date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: date)

    comps.second = 0
    comps.minute = 0
    comps.hour = comps.hour + hour

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func trimSecondOfDate(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: date)

    comps.second = 0

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func trimTimeOfDate(date: NSDate) -> NSDate {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: date)

    comps.second = 0
    comps.minute = 0
    comps.hour = 0

    return self.getCalendarCurrent().dateFromComponents(comps)!
  }

  class func getDate(format: String, stringDate: String) -> NSDate {
    let dateformater = NSDateFormatter()
    dateformater.dateFormat = format

    if dateformater.dateFromString(stringDate) == nil {

      print("NIL DATE MAT ROI")

      return NSDate()
    } else {
      return dateformater.dateFromString(stringDate)!
    }
  }

  // MARK: - Hour
  class func getHourWithDate(date: NSDate) -> Int {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: date)
    return comps.hour
  }

  // MARK: - Minutes
  class func getMinuteWithDate(date: NSDate) -> Int {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: date)
    return comps.minute
  }

  // MARK: - Format
  class func formatStringTypeMDYWithDate(date: NSDate) -> String {
    let dateformater = NSDateFormatter()
    dateformater.dateFormat = "MM/dd/yyyy"
    return dateformater.stringFromDate(date)
  }

  class func formatStringTypeDMYWithDate(date: NSDate) -> String {
    let dateformater = NSDateFormatter()
    dateformater.dateFormat = "dd/MM/yyyy"
    return dateformater.stringFromDate(date)
  }
  class func formatStringTypeHHmmWithDate(date: NSDate) -> String {
    let dateformater = NSDateFormatter()
    dateformater.dateFormat = "HH:mm"
    return dateformater.stringFromDate(date)
  }


  class func formatStringTypeYMDWithDate(date: NSDate) -> String {
    let dateformater = NSDateFormatter()
    dateformater.dateFormat = "yyyy/MM/dd"
    return dateformater.stringFromDate(date)
  }

  class func formatStringTypeYMDLineWithDate(date: NSDate) -> String {
    let dateformater = NSDateFormatter()
    dateformater.dateFormat = "yyyy-MM-dd"
    return dateformater.stringFromDate(date)
  }

  class func formatStringTypeISO8601(date: NSDate) -> String {
    let dateformater = NSDateFormatter()
    dateformater.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    return dateformater.stringFromDate(date)
  }

  class func formatGTM() -> String {
    let dateformater = NSDateFormatter()
    dateformater.dateFormat = "Z"
    return dateformater.stringFromDate(NSDate())
  }

  class func formatStringWithFormat(format: String, withDate date: NSDate) -> String {
    let dateformater = NSDateFormatter()
    let locale = NSLocale.currentLocale()

    dateformater.locale = locale
    dateformater.dateFormat = format
    return dateformater.stringFromDate(date)
  }

  class func nameMonthWithDate(date: NSDate) -> String {
    return "\(self.getMonthWithDate(date))"
  }

  class func nameYearWithDate(date: NSDate) -> String {
    return "\(self.getYearWithDate(date))"
  }

  // MARK: - String
  class func nameDayWithNumberDay(number: Int) -> String {
    if number == 2 {
      return "Monday"
    } else if number == 3 {
      return "Tuesday"
    } else if number == 4 {
      return "Wednesday"
    } else if number == 5 {
      return "Thursday"
    } else if number == 6 {
      return "Friday"
    } else if number == 7 {
      return "Saturday"
    } else if number == 1 {
      return "Sunday"
    } else {
      return ""
    }
  }

  class func nameMonthSortWithNumberMonth(number: Int) -> String {
    if number == 1 {
      return "Jan"
    } else if number == 2 {
      return "Feb"
    } else if number == 3 {
      return "Mar"
    } else if number == 4 {
      return "Apr"
    } else if number == 5 {
      return "May"
    } else if number == 6 {
      return "Jun"
    } else if number == 7 {
      return "Jul"
    } else if number == 8 {
      return "Aug"
    } else if number == 9 {
      return "Sep"
    } else if number == 10 {
      return "Oct"
    } else if number == 11 {
      return "Nov"
    } else if number == 12 {
      return "Dec"
    } else {
      return ""
    }
  }

  class func nameMonthWithNumberMonth(number: Int) -> String {
    if number == 1 {
      return "January"
    } else if number == 2 {
      return "February"
    } else if number == 3 {
      return "March"
    } else if number == 4 {
      return "April"
    } else if number == 5 {
      return "May"
    } else if number == 6 {
      return "June"
    } else if number == 7 {
      return "July"
    } else if number == 8 {
      return "August"
    } else if number == 9 {
      return "September"
    } else if number == 10 {
      return "October"
    } else if number == 11 {
      return "November"
    } else if number == 12 {
      return "December"
    } else {
      return ""
    }
  }

  // MARK: - Compare
  class func isGreaterThanDate(date: NSDate, ToCompare: NSDate) -> Bool {
    // Declare Variables
    var isGreater = false

    // Compare Values
    if date.compare(ToCompare) == NSComparisonResult.OrderedDescending {
      isGreater = true
    }

    // Return Result
    return isGreater
  }

  class func isLessThanDate(date: NSDate, ToCompare: NSDate) -> Bool {
    // Declare Variables
    var isLess = false

    // Compare Values
    if date.compare(ToCompare) == NSComparisonResult.OrderedAscending {
      isLess = true
    }

    // Return Result
    return isLess
  }

  class func equalToDate(date: NSDate, ToCompare: NSDate) -> Bool {
    // Declare Variables
    var isEqualTo = false

    // Compare Values
    if date.compare(ToCompare) == NSComparisonResult.OrderedSame {
      isEqualTo = true
    }

    // Return Result
    return isEqualTo
  }

  class func convertToGMT0(withDate date: String) -> String {
    let currentGMT = formatGTM()

    let strDateGMT0 = "\(getDate("yyyy-MM-dd'T'HH:mm:ssZ", stringDate: date + currentGMT))"

    let array = strDateGMT0.componentsSeparatedByString(" ")

    if array.count == 3 {
      return array[0] + "T" + array[1] + array[2]
    } else {
      return date + "+0000"
    }
  }

  class func getCurrentDate() -> NSDate {
    let formatter: NSDateFormatter = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    let date = formatter.stringFromDate(NSDate())
    return DHCalendarData.getDate("yyyy-MM-dd", stringDate: date)
  }

  class func compareTwoMoment(fromDate: String, toDate: String) -> Bool {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

    let startDate: NSDate = dateFormatter.dateFromString(fromDate)!
    let endDate: NSDate = dateFormatter.dateFromString(toDate)!

    let components = durationTwoMoment(startDate, endDate: endDate, calendarUnit: .Second)

    if components.second > duration {
      return true
    }
    return false
  }

  class func durationTwoMoment (startDate: NSDate, endDate: NSDate, calendarUnit: NSCalendarUnit) -> NSDateComponents {

    let cal = NSCalendar.currentCalendar()
    let unit: NSCalendarUnit = calendarUnit
    let components = cal.components(unit, fromDate: startDate, toDate: endDate, options: [])

    return components
  }

  class func checkDateInWeekCurrent(date: NSDate) -> Bool {
    let comps = self.getCalendarCurrent().components([.Year, .Month, .Day, .Weekday], fromDate: NSDate())

    var monday: NSDate = NSDate()
    
    for _ in comps.weekday.stride(to: 0, by: -1) {
      monday = self.prevDateFrom(monday)
    }

    var IsInWeek = false

    for _ in 0 ..< 6 {
      if formatStringTypeDMYWithDate(date) ==  formatStringTypeDMYWithDate(monday) {
        IsInWeek = true

        break
      }

      monday = self.nextDateFrom(monday)

      if formatStringTypeDMYWithDate(date) ==  formatStringTypeDMYWithDate(monday) {
        IsInWeek = true

        break
      }
    }
    return IsInWeek
  }

}
