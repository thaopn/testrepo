import Foundation
/**
 * DEFINE ERROR ID NOTIFICATION
 */
let _gg_idError1: Int32 = 6969
/**
 * DEFINE ERROR MESSAGE NOTIFICATION
 */
let _gg_messageError1: String = "This is demo error."
/**
 * TIME_OUT
 */
let GGWSRequestTimeOutDefault: Float = 30
/**
 * Form Access token
 */
let Authorization: String = "Authorization"
let IfMatch: String = "If-Match"
let DeviceToken: String = "device_token"
/**
 * Authorization
 */
let AUTHEN_USERNAME: String = ""
let AUTHEN_PASSWORD: String = ""
/**
 * Define limit page default
 */
let LIMIT_PAGE_GET: Int32 = 10
/**
 * API URL(MAIN HOST)
 */
let API_URL: String        = "http://www.readthisnext.com"
let HOST_IMAGE_URL: String = ""
/**
 * API CUSTOM FUNCTTION
 */
let API_IMAGE_DEFAULT: String  = "http://i.imgur.com/6ZmWl1P.jpg"
let API_AVATAR_DEFAULT: String = "http://i.imgur.com/YVaspfB.png"
let TEXT_DEFINE: String = "GGStructureSwift awesome!"

func LINK_PHOTO(imageName: String) -> String {
  return HOST_IMAGE_URL + imageName +  ".jpg"
}

func GetHeader()->[String:String] {
  var header: [String:String] = ["":""]
  let accessToken = NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != nil ?NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant)!:""
  header = [
    Authorization : "Bearer " + accessToken
  ]
  print(header)
  return header
}
