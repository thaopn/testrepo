import Foundation

let instagramDevToken = "2093522599.3dd781b.65be4ca0c4804651ad84bcb8048f5d8d"
let IsLive = false
//Layer App ID
/**
 *let LAYER_APP_ID         = "layer:///apps/staging/8c14f066-38ec-11e6-903d-a6252f055ea0" //Dev
 *let LAYER_APP_ID         = "layer:///apps/staging/90e7d34c-38ec-11e6-8c24-def55004791f" //Test
 */
let LAYER_APP_ID           = "layer:///apps/staging/3a615336-33a6-11e6-8e94-2f691f123da9" //Amazone
/**
 *let apiHost = "http://projects.greenglobal.vn:8282" //sv Dev
 *let apiHost = "http://projects.greenglobal.vn:8283" //sv Test
 */
let apiHost = "http://ec2-52-39-2-95.us-west-2.compute.amazonaws.com" // sv Amazon

/**
 * @METHOD GET
 */
let apiGetMeInfo: String = apiHost + "/me"

func ApiGetListUser(userId: String) -> String {
  return apiHost + "/users?" + "where=fb_id%3E%22%22".encoding()
}

func apiGetUserMatching(userId: String) -> String {
  return apiHost + "/match/\(userId)"
}

func apiLikeUser() -> String {
  return apiHost + "/likes"
}

func apiDislikeUser() -> String {
  return apiHost + "/dislikes"
}

func apiGetStatusLiked(userId: String, targetUserId: String) -> String {
  return apiHost + "/likes?"+"where=%7B%22userid%22%3A%22\(targetUserId)%22%2C%22target_userid%22%3A%22\(userId)%22%7D"
}

func ApiGetUserInfo(userId: String) -> String {
  return apiHost + "/users/\(userId)"
}

func apiGetPersonalities(page: Int) -> String {
  return apiHost + "/personalities?page=\(page)"
}

func apiGetImageWithId(imageId: String) -> String {
  return apiHost + "/images/\(imageId)"
}

func apiGetImagesWithUserId(userId: String) -> String {
  return apiHost + "/users/\(userId)/images"
}

func apiGetListUserMatched(userId: String, page: Int) -> String {
  return apiHost + "/matches?page=\(page)&where=%7B%22%24or%22%20%3A%20%5B%7B%22small_userid%22%3A%20%22\(userId)%22%7D%2C%7B%22large_userid%22%3A%20%22\(userId)%22%7D%5D%2C%20%22chats%22%3A%20%7B%22%24ne%22%3A1%7D%7D"
}

func apiSearchListMatched(userId: String, textSearch: String) -> String {
  return apiHost + "/matchsearch/\(userId)/\(textSearch)"
}

/**
 * @METHOD POST
 */
let apiLoginWithFacebook: String = apiHost + "/loginfb"
let apiLogout: String = apiHost + "/auth/logout"
let apiForgotPassword: String = apiHost + "/password/forgot/request"
let apiMembershipRegistration: String = apiHost + "/memberships"
let apiAccountRegistration: String = apiHost + "/users"
let apiWriteUs: String = apiHost + "/contacts"
let apiUploadPhoto: String = apiHost + "/upload/photos"
let apiIdentityToken: String = apiHost + "/layertoken"
let apiLoginWithInstagram: String = apiHost + "/logininstagram"
let apiReportUser = apiHost + "/reports"

func apiUpImage(userId: String) -> String {
  return apiHost + "/users/\(userId)/images"
}

/**
 * @METHOD PUT
 */

/**
 * @METHOD PATCH
 */
func getApiUpdatePersonal(personalId: String) -> String {
  return apiHost + "/personalities/\(personalId)"
}

func getApiEditInfo(userId id: String) -> String {
  return apiHost + "/users/\(id)"
}

func getApiUpdateInfoUser(userId: String) -> String {
  return apiHost + "/users/\(userId)"
}

func apiGetUpdateDeviceId(deviceId: String) -> String {
  return apiHost + "/devices/\(deviceId)"
}

func getApiUpdateStatusMatch(matchId: String) -> String {
  return apiHost + "/matches/\(matchId)"
}

/**
 * @METHOD DELETE
 */

/**
 * @METHOD GET URL
 */
let pageDomain: String = "https://beta.onedoor.mc"
let urlTermsAndConditions = "http://getpackapp.com/terms"
let urlHelp = "http://getpackapp.com/help"

func getEventPurchaseUrl(eventId eventId: String) -> String {
  return pageDomain + "/event/\(eventId)/purchase"
}
