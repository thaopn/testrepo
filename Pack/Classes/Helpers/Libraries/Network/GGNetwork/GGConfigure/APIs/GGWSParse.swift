import UIKit

class GGWSParse: NSObject {
  
  //Mark: - Custom Parse Header
  class func parseDataAPIHaveDataReturn(dic: AnyObject, complete:(dicData: AnyObject?, errorParse: GGWSError?)->Void)->Void {
    
    if dic.isKindOfClass(NSArray) {
      complete(dicData: dic, errorParse:nil)
    } else {
      if dic.isKindOfClass(NSDictionary) {
        let dictionary = dic as! NSDictionary
        let errorDic = dictionary.dictionaryForKey("errors")
        let errorString = dictionary.stringForKey("error")
        if errorDic != nil && errorDic?.allKeys.count > 0 {
          let new_error: GGWSError = GGWSError()
          new_error.error_id = 1
          new_error.error_msg = GGWSParse.getStringWithErrorDic(errorDic!)
          complete(dicData:nil, errorParse:new_error)
        } else if  errorString != "" {
          let new_error: GGWSError = GGWSError()
          new_error.error_id = 1
          new_error.error_msg = errorString
          complete(dicData:nil, errorParse:new_error)
        } else {
          complete(dicData: dic as? NSDictionary, errorParse:nil)
        }
      } else {
        complete(dicData: dic as? NSNumber, errorParse:nil)
      }
    }
  }
  
  class func getStringWithErrorDic(error: NSDictionary) -> String {
    
    let keys: NSArray = error.allKeys
    for key in keys {
      let array = error.arrayForKey(key)
      if array?.count > 0 {
        return String(format:"%@ %@", key as! String, array?.objectAtIndex(0) as! String)
      } else {
        return "error"
      }
    }
    return "error"
  }
}
