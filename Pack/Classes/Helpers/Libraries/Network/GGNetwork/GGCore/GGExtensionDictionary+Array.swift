import Foundation

public extension NSMutableArray {
  
  func kRemoveObjectAtIndex(index: Int) -> Void {
    if self.count > 0 {
      if index <= self.count {
        self.removeObjectAtIndex(index)
      } else {
        print("Extension: Don't remove object")
      }
    } else {
      print("Extension: Don't remove object")
    }
  }
  
  func kReplaceObjectAtIndex(index: Int, withObject object: AnyObject?) -> Void {
    if self.count > 0 {
      if index <= self.count && object != nil {
        self.replaceObjectAtIndex(index, withObject:object!)
      } else {
        print("Extension: Don't replace object")
      }
    } else {
      print("Extension: Don't replace object")
    }
  }
  
  func kInsertObject(object: AnyObject?, atIndex index: Int) -> Void {
    if self.count > 0 {
      if index <= self.count && object != nil {
        self.insertObject(object!, atIndex: index)
      } else {
        print("Extension: Don't insert object")
      }
    } else {
      print("Extension: Don't insert object")
    }
  }
}

public extension NSArray {
  
  func isEmpty() -> Bool {
    if self.count > 0 {
      return false
    } else {
      return true
    }
  }
  
  func kObjectAtIndex(index: Int) -> AnyObject? {
    if self.isEmpty() == true {
      return nil
    } else {
      return self.objectAtIndex(index)
    }
  }
}


public extension NSDictionary {
  
  func checkNumeric(s: AnyObject) -> Bool {
    
    let sc: NSScanner = NSScanner(string: s as! String)
    if sc.scanFloat(nil) {
      return sc.atEnd
    } else {
      return false
    }
  }
  
  func parseObjectForKey(key: AnyObject) -> AnyObject {
    return self.objectForKey(key)!
  }
  
  func dictionaryForKey(key: AnyObject) -> NSDictionary? {
    if self.objectForKey(key)?.isKindOfClass(NSDictionary.classForCoder()) == true {
      return self.objectForKey(key) as? NSDictionary
    } else {
      return nil
    }
  }
  
  func arrayForKey(key: AnyObject) -> NSArray? {
    if self.objectForKey(key)?.isKindOfClass(NSArray.classForCoder()) == true {
      return self.objectForKey(key) as? NSArray
    } else {
      return nil
    }
  }
  
  func stringForKey(key: AnyObject) -> String {
    
    if  self.objectForKey(key) != nil {
      let temp: String = self.objectForKey(key) as! String
      if temp == "null" || temp == "<null>" || temp == "(null)" || temp == "" || temp.isEmpty {
        return ""
      } else {
        return NSDictionary.stringByStrippingHTML(temp)
      }
    } else {
      return ""
    }
  }
  
  func intForKey(key: AnyObject) -> Int {
    
    let temp = self.objectForKey(key)
    if temp == nil {
      return 0
    } else {
      if self.objectForKey(key)!.isKindOfClass(NSNumber) == true {
        let num: NSNumber = self.objectForKey(key) as! NSNumber
        return num.integerValue
      } else {
        return 0
      }
    }
  }
  
  func floatForKey(key: AnyObject) -> Float {
    
    let num: NSNumber = self.objectForKey(key) as! NSNumber
    let temp: String = String(format:"%f", num.floatValue)
    if  self.objectForKey(key) != nil {
      if temp == "null" || temp == "<null>" || temp == "(null)" || temp == "" || temp.isEmpty {
        return 0.0
      } else {
        return NSString(string: temp).floatValue
      }
    } else {
      return 0.0
    }
  }
  
  private class func stringByStrippingHTML(string: String) -> String {
    var r: NSRange
    var s: NSString = NSString(string: string)
    r = s.rangeOfString("<[^>]+>", options: NSStringCompareOptions.RegularExpressionSearch)
    while r.location != NSNotFound {
      s = s.stringByReplacingCharactersInRange(r, withString:"")
      r = s.rangeOfString("<[^>]+>", options: NSStringCompareOptions.RegularExpressionSearch)
    }
    return s as String
  }
}
