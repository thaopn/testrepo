import UIKit
import Alamofire
import Async

public class NetworkManager {
  
  public static let sharedInstance: Manager = {
    let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
    configuration.URLCache = nil
    configuration.URLCache?.removeAllCachedResponses()
    configuration.requestCachePolicy = .ReloadIgnoringCacheData
    configuration.HTTPAdditionalHeaders = Manager.defaultHTTPHeaders
    configuration.timeoutIntervalForResource = NSTimeInterval(GGWSRequestTimeOutDefault + 0.1)
    return Manager(configuration: configuration)
  }()
}

class GGWebservice: NSObject {
  
  static var imageDefault = UIImageJPEGRepresentation(UIImage(named:"image_default")!, 1.0)
  
  // MARK: - Call WS with request
  class func callWebServiceWithRequest(method: Alamofire.Method!, urlString: String!, param: [String : AnyObject]!, accessToken: String!, isAuthorization: Bool!, success:(value: AnyObject?, statusCode: Int?, error: GGWSError?)->Void)->Request {
    
    var currentRequest: Request
    let  dicHeader: [String:String] = GetHeader()
    NSURLCache.sharedURLCache().removeAllCachedResponses()
    currentRequest = NetworkManager.sharedInstance.request(method, urlString, parameters:param, headers: dicHeader).responseJSON { response in
      
      Async.background {
        let statusCode = response.response?.statusCode
        Async.main(block: {
          if statusCode == 403 && AccountFlowManager.currentUser.id.characters.count == 0 {
            AccountFlowManager.shareInstance.handle403Error()
            Common.showAlert(textAlertWhenBlocked)
            let new_error = GGWSError()
            new_error.error_id = 403
            new_error.error_msg = textAlertWhenBlocked
            success(value:nil, statusCode: statusCode, error:new_error)
            return
          }
          if statusCode == 401 {
            AccountFlowManager.shareInstance.handle403Error()
            return
          }
        })
        if (response.result.error != nil) {
          let new_error: GGWSError! = GGWSError()
          new_error.error_id = 1
          new_error.error_msg = (response.result.error! as NSError).localizedDescription
          
          Async.main {
            success(value:nil, statusCode: statusCode, error:new_error)
          }
        } else {
          GGWSParse.parseDataAPIHaveDataReturn(response.result.value!, complete: { (dicData, errorParse) -> Void in
            if (errorParse != nil) {
              
              Async.main {
                success(value:nil, statusCode: statusCode, error:errorParse)
              }
            } else {
              Async.main {
                success(value:dicData, statusCode: statusCode, error:nil)
              }
            }
          })
        }
      }
    }
    return currentRequest
  }
  
  // MARK: - Call WS with header
  class func callWebServiceWithHeader(method: Alamofire.Method!, urlString: String!, header: [String:String], param: [String : AnyObject]!, success:(value: AnyObject?, statusCode: Int?, error: GGWSError?)->Void)->Request {
    
    var currentRequest: Request
    let  dicHeader: [String:String] = header
    currentRequest = NetworkManager.sharedInstance.request(method, urlString, parameters:param, headers: dicHeader).responseJSON { response in
      
      Async.background {
        let statusCode = response.response?.statusCode
        Async.main(block: {
          if statusCode == 403 && AccountFlowManager.currentUser.id.characters.count == 0 {
            AccountFlowManager.shareInstance.handle403Error()
            let new_error = GGWSError()
            new_error.error_id = 403
            new_error.error_msg = textAlertWhenBlocked
            success(value:nil, statusCode: statusCode, error:new_error)
            return
          }
          if statusCode == 401 {
            AccountFlowManager.shareInstance.handle403Error()
            return
          }
        })
        if (response.result.error != nil) {
          let new_error: GGWSError! = GGWSError()
          new_error.error_id = 1
          new_error.error_msg = (response.result.error! as NSError).localizedDescription
          
          Async.main {
            success(value:nil, statusCode: statusCode, error:new_error)
          }
        } else {
          GGWSParse.parseDataAPIHaveDataReturn(response.result.value!, complete: { (dicData, errorParse) -> Void in
            if (errorParse != nil) {
              Async.main {
                success(value:nil, statusCode: statusCode, error:errorParse)
              }
            } else {
              Async.main {
                success(value:dicData, statusCode: statusCode, error:nil)
              }
            }
          })
        }
      }
    }
    return currentRequest
  }
  
  // MARK: - Return json, not to parse "Meta"
  class func callAPIExceptParseMeta(method: Alamofire.Method!, urlString: String!, param: [String : AnyObject]!, accessToken: String!, success: (value: AnyObject?, error: GGWSError?) -> Void) -> Request {
    
    var currentRequest: Request
    var dicHeader: [String: String] = ["": ""]
    if (accessToken != "" && accessToken != nil) {
      dicHeader = [
        "Authorization": "Beare " + accessToken,
        "Content-Type" : "application/x-www-form-urlencoded"
      ]
    } else {
      dicHeader = [
        "Content-Type" : "application/x-www-form-urlencoded"
      ]
    }
    currentRequest = NetworkManager.sharedInstance.request(method, urlString, parameters:param, headers: dicHeader).responseJSON() {
      (response) in
      
      if let err = response.result.error {
        let new_error = GGWSError()
        new_error.error_id = 1
        new_error.error_msg = err.localizedDescription
        success(value: nil, error: new_error)
      } else {
        if let json = response.result.value {
          success(value: json, error: nil)
        } else {
          let error = GGWSError()
          error.error_msg = "Unkown"
          success(value: nil, error: error)
        }
      }
    }
    return currentRequest
  }
  
  // MARK: - Custom Webservice
  class func downloadObjectWithStringURL(urlFile: String, indexPath: NSIndexPath!, identifer: String!, andAuthen isAuthen: Bool!, wantCache isCache: Bool?, success:(dataFile: NSData?, error: GGWSError?, indexPath: NSIndexPath!, identifer: String!) -> Void, percent:(value: Float?)->Void)->Void {
    
    Async.background {
      if urlFile == API_IMAGE_DEFAULT {
        percent(value: 1.0)
        success(dataFile:self.imageDefault!, error: nil, indexPath:indexPath, identifer:identifer)
      } else {
        var dicHeader: [String:String] = ["":""]
        if (isAuthen == true) {
          dicHeader = [
            "Authorization": "Basic "+AUTHEN_USERNAME+AUTHEN_PASSWORD,
            "Content-Type" : "application/x-www-form-urlencoded"
          ]
        } else {
          dicHeader = [
            "Content-Type" : "application/x-www-form-urlencoded"
          ]
        }
        Alamofire.request(.GET, urlFile, headers:dicHeader).progress { (bytesRead, totalBytesRead, totalBytesExpectedToRead) -> Void in
          
          Async.main {
            percent(value: Float(totalBytesRead)/Float(totalBytesExpectedToRead))
          }
          }.response { (request, response, data, error) -> Void in
            if error != nil {
              let new_error: GGWSError = GGWSError()
              new_error.error_id = 1
              new_error.error_msg = (error! as NSError).localizedDescription
              
              Async.main {
                success(dataFile: nil, error: new_error, indexPath:indexPath, identifer:identifer)
              }
            } else {
              if UIImage(data: data!) == nil {
                
                Async.main {
                  success(dataFile:self.imageDefault!, error: nil, indexPath:indexPath, identifer:identifer)
                }
              } else {
                Async.main {
                  success(dataFile: data!, error: nil, indexPath:indexPath, identifer:identifer)
                }
              }
            }
        }
      }
    }
  }
  
  // MARK: - Upload Object with string URL
  class func uploadObjectWithStringURL(urlString: String, dataFile: NSData, field: String, info: Dictionary<String, String>!, accessToken: String!, isAuthen: Bool!, success:(value: AnyObject?, statusCode: Int?, error: GGWSError?) -> Void, percent:(value: Float!) -> Void ) -> Void {
    
    func urlRequestWithComponents(urlString: String, parameters: Dictionary<String, String> = ["":""], imageData: NSData, field: String) -> (URLRequestConvertible, NSData) {
      // create url request to send
      let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
      mutableURLRequest.HTTPMethod = Method.POST.rawValue
      let boundaryConstant = "myRandomBoundary12345"
      let contentType = "multipart/form-data;boundary="+boundaryConstant
      mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
      mutableURLRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: Authorization)
      mutableURLRequest.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
      // create upload data to send
      let uploadData = NSMutableData()
      // add image
      uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
      uploadData.appendData("Content-Disposition: form-data; name=\"\(field)\"; filename=\"user_avatar.jpg\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
      uploadData.appendData("Content-Type: image/jpeg\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
      uploadData.appendData(imageData)
      // add parameters
      for (key, value) in parameters {
        uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
      }
      uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
      // return URLRequestConvertible and NSData
      return (ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0, uploadData)
    }
    
    func urlRequestWithComponentsNoParam(urlString: String, imageData: NSData, field: String) -> (URLRequestConvertible, NSData) {
      // create url request to send
      let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
      mutableURLRequest.HTTPMethod = Method.POST.rawValue
      let boundaryConstant = "myRandomBoundary12345"
      let contentType = "multipart/form-data;boundary="+boundaryConstant
      mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
      mutableURLRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: Authorization)
      mutableURLRequest.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
      // create upload data to send
      let uploadData = NSMutableData()
      // add image
      uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
      uploadData.appendData("Content-Disposition: form-data; name=\"\(field)\"; filename=\"user_avatar.jpg\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
      uploadData.appendData("Content-Type: image/jpeg\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
      uploadData.appendData(imageData)
      
      uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
      // return URLRequestConvertible and NSData
      return (ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0, uploadData)
    }
    let urlRequest: (URLRequestConvertible, NSData)
    if let info = info {
      urlRequest = urlRequestWithComponents(urlString, parameters: info, imageData: dataFile, field: field)
    } else {
      urlRequest = urlRequestWithComponentsNoParam(urlString, imageData: dataFile, field: field)
    }
    
    Alamofire.upload(urlRequest.0, data: urlRequest.1).progress { (bytesRead, totalBytesRead, totalBytesExpectedToRead) -> Void in
      
      percent(value: Float(totalBytesRead)/Float(totalBytesExpectedToRead))
      }.responseJSON() {
        (response) in
        let statusCode = response.response?.statusCode
        if let err = response.result.error {
          let new_error: GGWSError! = GGWSError()
          new_error.error_id = 1
          new_error.error_msg = err.localizedDescription
          success(value:nil, statusCode: statusCode, error:new_error)
        } else {
          if let json = response.result.value {
            GGWSParse.parseDataAPIHaveDataReturn(json, complete: { (dicData, errorParse) -> Void in
              if (errorParse != nil) {
                success(value:nil, statusCode: statusCode, error:errorParse)
              } else {
                success(value:dicData, statusCode: statusCode, error:nil)
              }
            })
          } else {
            let error = GGWSError()
            error.error_msg = "Unknown Error"
            success(value:nil, statusCode: statusCode, error:error)
          }
        }
    }
  }
}
