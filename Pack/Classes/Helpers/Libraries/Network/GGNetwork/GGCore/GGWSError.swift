import UIKit

let GGWSErrorNotification: String = "GGWSErrorNotification"

class GGWSError: NSObject {

    static var _error: GGWSError!
    var error_id: Int = 0
    var error_msg: String = ""

    // Mark: - Function share instance error
    class func errorWithMessage(msg: String) -> GGWSError {

        if _error == nil {
            _error = GGWSError()
            _error.error_id = 69
            _error.error_msg = msg
            return _error
        } else {
            _error.error_id = 69
            _error.error_msg = msg
            return _error
        }
    }
}
