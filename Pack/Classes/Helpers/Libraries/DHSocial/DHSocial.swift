import UIKit
import Foundation
import Social
import MessageUI
import FBSDKLoginKit
import FBSDKShareKit
import ObjectMapper

class DHSocial: NSObject {

  typealias completionReturn = (userInfo: FBUserProfile?, isCancelSignIn: Bool?, error: NSError?) -> Void
  typealias getPhotoCompleted = (photos: [FBImages], error: NSError?) -> ()

  class func getProfileFBWithComplete(onVC: UIViewController, completion: completionReturn) -> Void {
    let login: FBSDKLoginManager = FBSDKLoginManager()
    login.loginBehavior = .Web

    login.logOut()
    login.logInWithReadPermissions(["public_profile", "email", "user_friends", "user_photos", "user_birthday", "user_about_me", "user_education_history", "user_work_history"], fromViewController: onVC) { (result, error) -> Void in

      if error != nil {
        completion(userInfo: nil, isCancelSignIn: false, error: error)
      } else if result.isCancelled {
        completion(userInfo: nil, isCancelSignIn: true, error: nil)
      } else {
        if result.grantedPermissions.contains("email") {
          if FBSDKAccessToken.currentAccessToken() != nil {

            let token: String = FBSDKAccessToken.currentAccessToken().tokenString

            FBSDKGraphRequest(graphPath: "me", parameters:["fields": "id, name, link, first_name, last_name, picture.type(large), email, birthday, bio, location, friends, hometown, friendlists, cover, gender, education, work"]).startWithCompletionHandler({ (connection, result, error) -> Void in

              if error != nil {
                completion(userInfo: nil, isCancelSignIn: false, error: error)
                print(error)
              } else {


                print(result)
                if let userInfo = Mapper<FBUserProfile>().map(result) {
                  userInfo.fbToken = token
                  completion(userInfo: userInfo, isCancelSignIn: false, error: nil)
                } else {
                  let error = NSError(domain: "Pack", code: 1, userInfo: [NSLocalizedDescriptionKey: "Unkown Error"])
                  completion(userInfo: nil, isCancelSignIn: false, error: error)
                }
              }
            })
          }
        }
      }
    }
  }

  typealias logoutFacebookHandler = (result: Bool?) -> Void

  class func logoutWithFacebook(handleLogout: logoutFacebookHandler) {
    let deletepermission = FBSDKGraphRequest(graphPath: "me/permissions/", parameters: nil, HTTPMethod: "DELETE")
    deletepermission.startWithCompletionHandler({(connection, result, error) -> Void in
      print("the delete permission is \(result)")

    })

    FBSDKLoginManager.renewSystemCredentials { (result, error) in
      print("FBSDKLoginManager.renewSystemCredentials: \(result)")
    }

    let loginManager = FBSDKLoginManager()
    loginManager.logOut()

    if checkUserHaveLoginFacebook() {
      handleLogout(result: false)
    } else {
      handleLogout(result: true)
    }
  }

  class func checkUserHaveLoginFacebook() -> Bool {
    if let _ = FBSDKAccessToken.currentAccessToken() {
      return true
    } else {
      return false
    }
  }

  class func inviteFriends(appLinkURL: String, appInvitePreviewImageURL: String = "", vc: UIViewController, fbInviteDelegate: FBSDKAppInviteDialogDelegate) {
    let content = FBSDKAppInviteContent()
    content.appLinkURL = NSURL(string: appLinkURL)
    if appInvitePreviewImageURL != "" {
      content.appInvitePreviewImageURL = NSURL(string: appInvitePreviewImageURL)
    }
    FBSDKAppInviteDialog.showFromViewController(vc, withContent: content, delegate: fbInviteDelegate)
  }

  class func getMePhoto(completed: getPhotoCompleted) {
    if let _ = FBSDKAccessToken.currentAccessToken() {
      print("FBAccessToken: \(FBSDKAccessToken.currentAccessToken)")
      FBSDKGraphRequest(graphPath: "me/photos", parameters:["fields": "images", "type": "uploaded", "limit": "4"]).startWithCompletionHandler({ (connection, result, error) -> Void in
        if let e = error {
          completed(photos: [], error: e)
          print(e.localizedDescription)
        } else {
          print("facebook get photos: \(result)")
          if let photos = Mapper<FBPhotos>().map(result) {
            completed(photos: photos.photos, error: nil)
          } else {
            let error = NSError(domain: "Pack", code: 1, userInfo: [NSLocalizedDescriptionKey: "Unkown Error"])
            completed(photos: [], error: error)
          }
        }
      })
    } else {
      let error = NSError(domain: "Pack", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Facebook accesstoken"])
      completed(photos: [], error: error)
    }
  }

  typealias GetMutualFriendsCompleted = (mutualFriend: Int, error: NSError?) -> ()
  class func getMutualFriends(fbId: String, completed: GetMutualFriendsCompleted) {
    if let _ = FBSDKAccessToken.currentAccessToken() {
      FBSDKGraphRequest(graphPath: "\(fbId)", parameters:["fields": "context.fields(all_mutual_friends.limit(1))"]).startWithCompletionHandler({ (connection, result, error) -> Void in
        if let e = error {
          completed(mutualFriend: 0, error: e)
          print(e.localizedDescription)
        } else {
          print("facebook get mutualFriends: \(result)")
          if let mutual = Mapper<FBMutualFriends>().map(result) {
            completed(mutualFriend: mutual.totalCount, error: nil)
          } else {
            let error = NSError(domain: "Pack", code: 1, userInfo: [NSLocalizedDescriptionKey: "Cannot parse json to object"])
            completed(mutualFriend: 0, error: error)
          }
        }
      })
    } else {
      let error = NSError(domain: "Pack", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Facebook accesstoken"])
      completed(mutualFriend: 0, error: error)
    }
  }
}
