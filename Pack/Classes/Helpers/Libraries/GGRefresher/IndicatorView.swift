import UIKit

public enum PullToRefreshViewState {
  case Loading
  case PullToRefresh
  case ReleaseToRefresh
}

class IndicatorView: UIView {

  @IBOutlet weak var imageView: UIImageView!
  var isRotating: Bool = false

  func pullToRefreshAnimationDidStart() {
    isRotating = true
    self.alpha = 1
    rotate360Degrees()
  }

  func pullToRefreshAnimationDidEnd() {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(0.4 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
      self.isRotating = false
    }
    rotateStop()
  }

  func rotate360Degrees() {
    if let _ = self.imageView.layer.animationForKey("annimation") {
      // None
    } else {
      let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
      rotateAnimation.fromValue = 0.0
      rotateAnimation.toValue = CGFloat(M_PI * 2.0)
      rotateAnimation.duration = 1
      rotateAnimation.repeatCount = MAXFLOAT

      self.imageView.layer.addAnimation(rotateAnimation, forKey: "annimation")
    }
  }

  func rotateStop() {
    self.imageView.layer.removeAllAnimations()
  }

  func rotateWithProgress(progress: CGFloat) {
    self.imageView.transform = CGAffineTransformMakeRotation(progress * CGFloat(M_PI))
  }

  func pullToRefresh(progress: CGFloat) {
    rotateWithProgress(progress)
  }

  func pullToRefresh(stateDidChange state: PullToRefreshViewState) {
    switch state {
    case .Loading:
      imageView.image = UIImage(named: "ic_loading.png")
    case .PullToRefresh:
      if (!isRotating) {
        imageView.image = UIImage(named: "ic_preload.png")
      }
    case .ReleaseToRefresh:
      imageView.image = UIImage(named: "ic_loading.png")
    }
  }

}
