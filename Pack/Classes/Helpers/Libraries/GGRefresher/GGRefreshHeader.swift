import UIKit
import Foundation

typealias BeginRefreshingBlock = () -> Void

class GGRefreshHeader: NSObject {

  var lastPosition: CGFloat!
  var contentHeight: CGFloat!
  var headerHeight: CGFloat!
  var isRefresh: Bool!
  var state: PullToRefreshViewState = PullToRefreshViewState.PullToRefresh
  var headerView: IndicatorView!
  var enableRefresh: Bool = true
  var contentInset: UIEdgeInsets!
  var scrollView: UIScrollView! {didSet {self.contentInset = self.scrollView?.contentInset}}
  var beginRefreshingBlock: BeginRefreshingBlock = {}

  init(scrollView: UIScrollView) {
    super.init()
    self.header(scrollView)
  }

  deinit {
    print("remove contentOffset")
    scrollView!.removeObserver(self, forKeyPath: "contentOffset", context: nil)
  }

  func updateIndicator(scrollView: UIScrollView) {
    self.contentInset = scrollView.contentInset
  }

  private func header(scrollView: UIScrollView) {
    self.scrollView = scrollView
    self.contentInset = scrollView.contentInset

    isRefresh = false
    lastPosition = 0
    headerHeight = 55

    if let cus = NSBundle.mainBundle().loadNibNamed("IndicatorView", owner: self, options: nil).first as? IndicatorView {
      headerView = cus

    }
    setIndicatorViewLayout()

    headerView.hidden = false
    headerView.alpha = 0

    scrollView.addObserver(self,
                           forKeyPath: "contentOffset",
                           options: [NSKeyValueObservingOptions.New, NSKeyValueObservingOptions.Old],
                           context: nil)
  }

  private func setIndicatorViewLayout() {

    headerView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.addSubview(headerView)

    let leftConstraint = NSLayoutConstraint(item: headerView,
                                            attribute: NSLayoutAttribute.Left,
                                            relatedBy: NSLayoutRelation.Equal,
                                            toItem: scrollView,
                                            attribute: NSLayoutAttribute.Left,
                                            multiplier: 1,
                                            constant: 0)

    let rightConstraint = NSLayoutConstraint(item: headerView,
                                             attribute: NSLayoutAttribute.Width,
                                             relatedBy: NSLayoutRelation.Equal,
                                             toItem: scrollView,
                                             attribute: NSLayoutAttribute.Width,
                                             multiplier: 1,
                                             constant: 0)

    let heightConstraint = NSLayoutConstraint(item: headerView,
                                              attribute: NSLayoutAttribute.Height,
                                              relatedBy: NSLayoutRelation.Equal,
                                              toItem: nil,
                                              attribute: NSLayoutAttribute.NotAnAttribute,
                                              multiplier: 1, constant: 55)

    let topConstraint = NSLayoutConstraint(item: headerView,
                                           attribute: NSLayoutAttribute.Top,
                                           relatedBy: NSLayoutRelation.Equal,
                                           toItem: scrollView,
                                           attribute: NSLayoutAttribute.Top,
                                           multiplier: 1,
                                           constant: -55)

    scrollView.addConstraints([leftConstraint, heightConstraint, rightConstraint, topConstraint])
  }

  override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {

    if keyPath == "contentOffset" {
      if enableRefresh == true {

        contentHeight = scrollView!.contentSize.height

        let currentPosition = scrollView!.contentOffset.y + scrollView!.contentInset.top
        self.headerView?.pullToRefresh(currentPosition/20)

        if scrollView!.dragging {
          headerView!.alpha = 1

          if !isRefresh {
            self.headerView?.pullToRefresh(stateDidChange: self.state)

            UIView.animateWithDuration(0.3, animations: { () -> Void in
              if currentPosition < (-self.headerHeight * 1.0) {

                self.state = PullToRefreshViewState.ReleaseToRefresh

              } else {
                let currentPosition = self.scrollView!.contentOffset.y
                if currentPosition - self.lastPosition > 5 {
                  self.lastPosition = currentPosition

                  self.state = PullToRefreshViewState.PullToRefresh

                } else if self.lastPosition - currentPosition > 5 {
                  self.lastPosition = currentPosition
                }
              }
            })
          }

        } else {
          if state == PullToRefreshViewState.ReleaseToRefresh {
            beginRefreshing()
          }
        }

        if !isRefresh && currentPosition >= 0 {
          headerView!.alpha = 0
        }
      }
    } else {
      super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
    }
  }

  func beginRefreshing() {
    print("frame scrollview: \(scrollView.frame)")
    if !isRefresh {
      isRefresh = true
      var rate: CGFloat = 1.5
      if self.contentInset.top < 20 {
        rate = 1.5
      } else {
        rate = 1.0
      }
      state = PullToRefreshViewState.Loading

      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        self.headerView?.pullToRefreshAnimationDidStart()
        UIView.animateWithDuration(0.3, animations: { () -> Void in
          self.scrollView!.contentInset = UIEdgeInsetsMake(self.contentInset.top + self.headerHeight * rate, self.contentInset.left, self.contentInset.bottom, self.contentInset.right)
        })
      })

      beginRefreshingBlock()
    }
  }

  func endRefreshing() {
    isRefresh = false

    dispatch_async(dispatch_get_main_queue(), { () -> Void in
      //self.headerView!.alpha = 0
      self.headerView?.pullToRefreshAnimationDidEnd()
      UIView.animateWithDuration(0.3, animations: { () -> Void in
        self.scrollView!.contentInset = self.contentInset!

      })
    })
  }

}
