import UIKit

class GGRefreshFooter: NSObject {

  var isAdd: Bool!
  var isRefresh: Bool!
  var enableLoadMore: Bool!
  var contentInset: UIEdgeInsets!
  var footerView: IndicatorView!
  var scrollView: UIScrollView!
  var contentHeight: CGFloat!
  var scrollViewFrameHeight: CGFloat!
  var scrollViewWidth: CGFloat!
  var footerHeight: CGFloat!
  var beginFoooterRefreshingBlock: BeginRefreshingBlock = {}
  var consBottomFooterView: NSLayoutConstraint!

  init(scrollView: UIScrollView) {
    super.init()
    self.footer(scrollView)
  }

  deinit {
    print("remove contentOffset")
    scrollView!.removeObserver(self, forKeyPath: "contentOffset", context: nil)
  }

  private func footer(scrollView: UIScrollView) {
    self.scrollView = scrollView
    self.contentInset = scrollView.contentInset

    scrollViewWidth = scrollView.frame.size.width
    footerHeight = 55

    isAdd = false
    isRefresh = false
    enableLoadMore = true

    if let cus = NSBundle.mainBundle().loadNibNamed("IndicatorView", owner: self, options: nil).first as? IndicatorView {
      footerView = cus
    }

    scrollView.addObserver(self,
                           forKeyPath: "contentOffset",
                           options: [NSKeyValueObservingOptions.New, NSKeyValueObservingOptions.Old],
                           context: nil)
  }

  private func setIndicatorViewLayout(isAdd: Bool = true) {
    footerView.removeFromSuperview()
    footerView.translatesAutoresizingMaskIntoConstraints = false
    if isAdd {
      scrollView.addSubview(footerView)
    } else {
      scrollView.insertSubview(footerView!, atIndex: 0)
    }

    let leftConstraint = NSLayoutConstraint(item: footerView,
                                            attribute: NSLayoutAttribute.Left,
                                            relatedBy: NSLayoutRelation.Equal,
                                            toItem: scrollView,
                                            attribute: NSLayoutAttribute.Left,
                                            multiplier: 1,
                                            constant: 0)

    let rightConstraint = NSLayoutConstraint(item: footerView,
                                             attribute: NSLayoutAttribute.Width,
                                             relatedBy: NSLayoutRelation.Equal,
                                             toItem: scrollView,
                                             attribute: NSLayoutAttribute.Width,
                                             multiplier: 1,
                                             constant: 0)

    let heightConstraint = NSLayoutConstraint(item: footerView,
                                              attribute: NSLayoutAttribute.Height,
                                              relatedBy: NSLayoutRelation.Equal,
                                              toItem: nil,
                                              attribute: NSLayoutAttribute.NotAnAttribute,
                                              multiplier: 1, constant: 55)

    let topConstraint = NSLayoutConstraint(item: footerView,
                                           attribute: NSLayoutAttribute.Top,
                                           relatedBy: NSLayoutRelation.Equal,
                                           toItem: scrollView,
                                           attribute: NSLayoutAttribute.Top,
                                           multiplier: 1,
                                           constant: scrollView.contentSize.height)

    consBottomFooterView = topConstraint
    scrollView.addConstraints([leftConstraint, heightConstraint, rightConstraint, topConstraint])

  }

  override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
    if keyPath == "contentOffset" {
      scrollViewFrameHeight = scrollView.frame.size.height
      contentHeight = scrollView!.contentSize.height

      if (contentHeight == 0) {
        contentHeight = CGRectGetHeight(scrollView!.frame) / 2 - footerHeight
      }

      if !isAdd {
        isAdd = true
        setIndicatorViewLayout()
      } else {
        if !isRefresh {
          // None
        }
      }

      let currentPosition = scrollView!.contentOffset.y + contentInset.top

      if enableLoadMore == true {
        footerView.hidden = false
      } else {
        footerView.hidden = true
      }

      if ((currentPosition >= contentHeight - scrollViewFrameHeight )  && (enableLoadMore == true) ) {
        if !isRefresh {
          self.beginRefreshing()
        }
      }
    } else {
      super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
    }
  }

  func beginRefreshing() {
    if !isRefresh {
      isRefresh = true

      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        if (self.scrollView.contentSize.height == 0) {
          self.consBottomFooterView.constant = self.scrollView!.contentInset.top + 20
        } else {
          self.consBottomFooterView.constant = self.scrollView.contentSize.height
        }
        self.footerView?.pullToRefreshAnimationDidStart()

        UIView.animateWithDuration(0.3, animations: { () -> Void in
          self.footerView?.alpha = 1
          self.scrollView!.contentInset = UIEdgeInsetsMake(self.contentInset.top, self.contentInset.left, self.footerHeight + self.contentInset.bottom, self.contentInset.right)
        })
      })

      beginFoooterRefreshingBlock()
    }
  }

  func endRefreshing() {
    if isRefresh == true {
      isRefresh = false

      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        self.footerView?.pullToRefreshAnimationDidEnd()
        self.footerView?.alpha = 0
        self.scrollView!.contentInset = self.contentInset
        self.footerView!.frame = CGRectMake(0, self.contentHeight, UIScreen.mainScreen().bounds.size.width, self.footerHeight)
      })
    }
  }

}
