#import "GGUINotification.h"

#define defaultBackgroundColor      [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1.0f]
#define timeAnimation 3.5f
#define guiWidthScreen [UIScreen mainScreen].bounds.size.width

@interface GGUINotification()

@property (nonatomic,strong) UIView *viewMain;
@property (nonatomic,assign) float heightView;
@property (nonatomic,strong) NSTimer *timerDelay;
@property (nonatomic,strong) UILabel *labelContent;
@property (nonatomic,strong) UIImageView *viewImage;
@property (nonatomic,assign) BOOL isShow;

@end

@implementation GGUINotification

- (id)init {
  self = [super init];
  if (self) {
    
    if ([UIApplication sharedApplication].isStatusBarHidden) {
      _heightView = 44.0f;
    }else{
      _heightView = 64.0f;
    }
    
    _viewMain = [[UIView alloc] initWithFrame:CGRectMake(0, -_heightView, guiWidthScreen, _heightView)];
    _viewMain.backgroundColor = [UIColor clearColor];
    _viewMain.alpha = 1.0f;
    _viewMain.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin;
    
    UIView *viewAlpha = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _viewMain.frame.size.width, _viewMain.frame.size.height)];
    viewAlpha.backgroundColor = defaultBackgroundColor;
    viewAlpha.alpha = 1.0f;
    viewAlpha.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [_viewMain addSubview:viewAlpha];
    
    UIView *viewContent = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _viewMain.frame.size.width, _viewMain.frame.size.height)];
    viewContent.backgroundColor = [UIColor clearColor];
    viewContent.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [_viewMain addSubview:viewContent];
    
    _viewImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 27, 30, 30)];
    _viewImage.image = [UIImage imageNamed:@"Icon.png"];
    _viewImage.layer.cornerRadius = 3.0f;
    _viewImage.layer.masksToBounds = YES;
    [viewContent addSubview:_viewImage];
    
    _labelContent = [[UILabel alloc] initWithFrame:CGRectMake(_viewImage.frame.origin.x+_viewImage.frame.size.width+10, _viewImage.frame.origin.y-5, guiWidthScreen - (_viewImage.frame.origin.x+_viewImage.frame.size.width+10+50), 40)];
    _labelContent.backgroundColor = [UIColor clearColor];
    _labelContent.textAlignment = NSTextAlignmentLeft;
    _labelContent.textColor = [UIColor whiteColor];
    _labelContent.numberOfLines = 2;
    _labelContent.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0];
    _labelContent.text = @"Pack";
    _labelContent.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [viewContent addSubview:_labelContent];
    
    UIButton *btnShow =  [UIButton buttonWithType:UIButtonTypeCustom];
    [btnShow setBackgroundColor:[UIColor clearColor]];
    [btnShow setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [btnShow addTarget:self action:@selector(tapToShow) forControlEvents:UIControlEventTouchUpInside];
    [btnShow setFrame:CGRectMake(0, 0, _labelContent.frame.origin.x+_labelContent.frame.size.width, _viewMain.frame.size.height)];
    btnShow.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [viewContent addSubview:btnShow];
    
    UIButton *btnHide =  [UIButton buttonWithType:UIButtonTypeCustom];
    [btnHide setImage:[UIImage imageNamed:@"ic_button_item_close.png"] forState:UIControlStateNormal];
    [btnHide setBackgroundColor:[UIColor clearColor]];
    [btnHide setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [btnHide addTarget:self action:@selector(tapToHide) forControlEvents:UIControlEventTouchUpInside];
    [btnHide setFrame:CGRectMake(btnShow.frame.origin.x+btnShow.frame.size.width, 20,_viewMain.frame.size.width-btnShow.frame.size.width, _viewMain.frame.size.height-20)];
    btnHide.autoresizingMask =UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    [viewContent addSubview:btnHide];
    
    _viewMain.alpha = 0.0f;
  }
  return self;
}

- (void)dealloc {
  _timerDelay = nil;
  self.viewMain = nil;
}

- (void)tapToShow {
  [self autoHideView];
  [_delegate didHideViewWithHaveTapSelect:YES];
}

- (void)tapToHide {
  [self autoHideView];
}

- (void)showPushNotificationViewWithInfo:(NSDictionary *)dicNoti {
  
  if ([UIApplication sharedApplication].isStatusBarHidden) {
    _heightView = 44.0f;
  }else{
    _heightView = 64.0f;
  }
  
  if (_isShow) {
    [_timerDelay invalidate];
    
    [UIView animateWithDuration:0.5f animations:^{
      _viewMain.frame = CGRectMake(_viewMain.frame.origin.x, -_heightView,guiWidthScreen, _heightView);
    }completion:^(BOOL finished){
      _isShow = NO;
      _viewMain.alpha = 0.0f;
      [self showPushNotificationViewWithInfo:dicNoti];
    }];
  } else {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    NSDictionary * dicInfo = [dicNoti valueForKey: @"aps"];
    _labelContent.text = [dicInfo valueForKey:@"alert"];
    _viewImage.image = [UIImage imageNamed:@"Icon.png"];
    [window addSubview:_viewMain];
    _viewMain.alpha = 1.0f;
    
    [UIView animateWithDuration:0.5f animations:^{
      _viewMain.frame = CGRectMake(_viewMain.frame.origin.x, 0, guiWidthScreen, _heightView);
    }completion:^(BOOL finished){
      _timerDelay = [NSTimer scheduledTimerWithTimeInterval:timeAnimation
                                                     target:self
                                                   selector:@selector(autoHideView)
                                                   userInfo:nil
                                                    repeats:NO];
      _isShow = YES;
    }];
  }
}

- (void)autoHideView {
  [_timerDelay invalidate];
  [UIView animateWithDuration:0.5f animations:^{
    _viewMain.frame = CGRectMake(_viewMain.frame.origin.x, -_heightView, guiWidthScreen, _heightView);
  }completion:^(BOOL finished){
    _isShow = NO;
    _viewMain.alpha = 0.0f;
  }];
}

@end
