#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol GGUINotificationDelegate;

@interface GGUINotification : NSObject

//protocol
@property (nonatomic, weak) id<GGUINotificationDelegate>delegate;

-(void)showPushNotificationViewWithInfo:(NSDictionary *)dicNoti;

@end

@protocol GGUINotificationDelegate <NSObject>

@required

-(void)didHideViewWithHaveTapSelect:(BOOL)isSelected;

@optional


@end