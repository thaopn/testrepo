#import <Foundation/Foundation.h>

#import "LabelTextkit.h"

@interface FormatsNSStringAttribute : NSObject

+ (NSStringAttributesObject *)getStringFormatWithString:(NSString *)string color:(UIColor *)color font:(UIFont *)font shadow:(NSShadow *)shadow andObjectId:(NSString *)objectId;
+ (NSStringAttributesObject *)getStringFormatForUsername:(NSString *)string andObjectId:(NSString *)objectId;
+ (NSStringAttributesObject *)getStringFormatForMessage:(NSString *)string;
+ (NSStringAttributesObject *)getStringFormatForForObjectName:(NSString *)string andObjectId:(NSString *)objectId;

@end
