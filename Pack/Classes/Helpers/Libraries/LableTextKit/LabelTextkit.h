#import <UIKit/UIKit.h>

@interface NSStringAttributesObject : NSObject

@property (nonatomic, strong) UIFont    *font;
@property (nonatomic, strong) UIColor   *color;
@property (nonatomic, strong) NSShadow  *shadow;
@property (nonatomic, strong) NSString  *string;
@property (nonatomic, strong) NSString  *objectId;
@property (nonatomic, strong) NSString  *objectName;

@property (nonatomic, assign) BOOL      isSelect;
@property (nonatomic, assign) BOOL      underline;

@end

typedef enum {
  LabelHighlightTypeNone,
  LabelHighlightTypeBackground,
  LabelHighlightTypeText,
  
}LabelHighlightType;

typedef void (^TouchReturnBlock)(NSInteger sender, NSStringAttributesObject *object);

@interface LabelTextkit : UILabel

- (void)setAttributesTextWithArrayText:(NSArray *)array;
- (void)setAttributesTextWithArrayText:(NSArray *)array labelHighlightType:(LabelHighlightType)labelHighlightType withColor:(UIColor *)color touchToReturnSender:(TouchReturnBlock)sender;

- (void)setAttributesForTexts:(NSArray *)array labelHighlightType:(LabelHighlightType)labelHighlightType withColor:(UIColor *)color touchToReturnSender:(TouchReturnBlock)sender;
- (void)setAgainAttributesText:(NSArray *)array;

- (void)setTextWithArrayTextHighlight:(NSArray *)array objectFullText:(NSStringAttributesObject *)objFullText labelHighlightType:(LabelHighlightType)labelHighlightType withColor:(UIColor *)color touchToReturnSender:(TouchReturnBlock)sender;

@end
