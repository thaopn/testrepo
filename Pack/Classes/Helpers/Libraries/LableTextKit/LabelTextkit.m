#import "LabelTextkit.h"
#import <CoreText/CoreText.h>

@implementation NSStringAttributesObject

- (NSString *)string
{
  if (!_string || (_string.length == 0)) {
    
    return @".....";
  }
  return _string;
}

@end

@interface LabelTextkit ()

@property(nonatomic, strong) NSSet* lastTouches;
@property(nonatomic, assign) LabelHighlightType labelHighlightType;
@property(nonatomic, strong) TouchReturnBlock   touchReturnBlock;
@property(nonatomic, strong) NSArray            *arrayString;
@property(nonatomic, strong) NSMutableArray     *matches;
@property(nonatomic, strong) UIColor            *colorHighlight;

@property(nonatomic) NSRange highlightedRange;

- (BOOL)didBeginTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex;
- (BOOL)didMoveTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex;
- (BOOL)didEndTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex;
- (BOOL)didCancelTouch:(UITouch *)touch;

@end

@implementation LabelTextkit

- (id)initWithFrame:(CGRect)frame {
  
  self = [super initWithFrame:frame];
  if (self) {
    
    self.userInteractionEnabled = YES;
    self.lineBreakMode = NSLineBreakByWordWrapping;
    
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    
    self.userInteractionEnabled = YES;
    self.lineBreakMode = NSLineBreakByWordWrapping;
  }
  return self;
}

- (CFIndex)characterIndexAtPoint:(CGPoint)point {
  UILabel *textLabel = self;
  CGPoint tapLocation = point;
  NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:textLabel.attributedText];
  
  // Init text storage
  NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:attributedText];
  NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
  [textStorage addLayoutManager:layoutManager];
  
  // Init text container
  NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(textLabel.frame.size.width, textLabel.frame.size.height+100) ];
  textContainer.lineFragmentPadding  = 0;
  textContainer.maximumNumberOfLines = textLabel.numberOfLines;
  textContainer.lineBreakMode        = textLabel.lineBreakMode;
  textContainer.layoutManager        = layoutManager;
  
  [layoutManager addTextContainer:textContainer];
  [layoutManager setTextStorage:textStorage];
  
  NSUInteger characterIndex = [layoutManager characterIndexForPoint:tapLocation
                                                    inTextContainer:textContainer
                           fractionOfDistanceBetweenInsertionPoints:NULL];
  return characterIndex;
}

#pragma mark -- TextRect
- (CGRect)textRect {
  CGRect textRect = [self textRectForBounds:self.bounds limitedToNumberOfLines:self.numberOfLines];
  textRect.origin.y = (self.bounds.size.height - textRect.size.height)/2;
  
  if (self.textAlignment == NSTextAlignmentCenter) {
    textRect.origin.x = (self.bounds.size.width - textRect.size.width)/2;
  }
  if (self.textAlignment == NSTextAlignmentRight) {
    textRect.origin.x = self.bounds.size.width - textRect.size.width;
  }
  
  return textRect;
}


#pragma mark -- View Event
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  self.lastTouches = touches;
  
  UITouch *touch = [touches anyObject];
  CFIndex index = [self characterIndexAtPoint:[touch locationInView:self]];
  
  if (![self didBeginTouch:touch onCharacterAtIndex:index]) {
    [super touchesBegan:touches withEvent:event];
  }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
  
  self.lastTouches = touches;
  
  UITouch *touch = [touches anyObject];
  CFIndex index = [self characterIndexAtPoint:[touch locationInView:self]];
  
  if (![self didMoveTouch:touch onCharacterAtIndex:index]) {
    [super touchesMoved:touches withEvent:event];
  }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
  if (!self.lastTouches) {
    return;
  }
  
  self.lastTouches = nil;
  
  UITouch *touch = [touches anyObject];
  CFIndex index = [self characterIndexAtPoint:[touch locationInView:self]];
  
  
  if (![self didEndTouch:touch onCharacterAtIndex:index]) {
    [super touchesEnded:touches withEvent:event];
  }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
  if (!self.lastTouches) {
    return;
  }
  
  self.lastTouches = nil;
  
  UITouch *touch = [touches anyObject];
  
  if (![self didCancelTouch:touch]) {
    [super touchesCancelled:touches withEvent:event];
  }
}

- (void)cancelCurrentTouch {
  if (self.lastTouches) {
    [self didCancelTouch:[self.lastTouches anyObject]];
    self.lastTouches = nil;
  }
}

#pragma mark - Methods handle
- (BOOL)didBeginTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
  if (_labelHighlightType == LabelHighlightTypeBackground) {
    
    [self highlightBackgroundWithIndex:charIndex];
    
  } else if (_labelHighlightType == LabelHighlightTypeText) {
    
    [self highlightLinksWithIndex:charIndex];
    
  } else if (_labelHighlightType == LabelHighlightTypeNone) {
    // None
  }
  
  return NO;
}

- (BOOL)didMoveTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
  if (_labelHighlightType == LabelHighlightTypeBackground) {
    
    [self highlightBackgroundWithIndex:charIndex];
    
  } else if (_labelHighlightType == LabelHighlightTypeText) {
    
    [self highlightLinksWithIndex:charIndex];
    
  } else if (_labelHighlightType == LabelHighlightTypeNone) {
    // None
  }
  
  return NO;
}

- (BOOL)didEndTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
  if (_labelHighlightType == LabelHighlightTypeBackground) {
    [self highlightBackgroundWithIndex:NSNotFound];
    
    for (int i = 0 ; i < self.matches.count; i++) {
      
      NSStringAttributesObject *obj = _arrayString[i];
      NSValue *value = self.matches[i];
      
      if (obj.isSelect) {
        
        NSRange matchRange = [value rangeValue];
        
        if ([self isIndex:charIndex inRange:matchRange]) {
          self.touchReturnBlock(i,obj);
          break;
        }
      }
    }
    
  } else if (_labelHighlightType == LabelHighlightTypeText) {
    
    [self highlightLinksWithIndex:NSNotFound];
    
    for (int i = 0 ; i < self.matches.count; i++) {
      
      NSStringAttributesObject *obj = _arrayString[i];
      
      if (obj.isSelect) {
        
        NSValue *value = self.matches[i];
        NSRange matchRange = [value rangeValue];
        if ([self isIndex:charIndex inRange:matchRange]) {
          self.touchReturnBlock(i,obj);
          break;
        }
      }
    }
  }else if (_labelHighlightType == LabelHighlightTypeNone) {
    // None
  }
  return NO;
}


- (BOOL)didCancelTouch:(UITouch *)touch
{
  if (_labelHighlightType == LabelHighlightTypeBackground) {
    
    [self highlightBackgroundWithIndex:NSNotFound];
    
  }else if (_labelHighlightType == LabelHighlightTypeText) {
    
    [self highlightLinksWithIndex:NSNotFound];
    
  }else if (_labelHighlightType == LabelHighlightTypeNone) {
    // None
  }
  return NO;
}

- (BOOL)isIndex:(CFIndex)index inRange:(NSRange)range {
  return index > range.location && index < range.location+range.length;
}

- (void)highlightLinksWithIndex:(CFIndex)index {
  
  NSMutableAttributedString* attributedString = [self.attributedText mutableCopy];
  
  for (int i = 0 ; i < self.matches.count; i++) {
    
    NSStringAttributesObject *obj = _arrayString[i];
    
    NSValue *value = self.matches[i];
    NSRange matchRange = [value rangeValue];
    
    if (obj.isSelect) {
      if ([self isIndex:index inRange:matchRange]) {
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:_colorHighlight range:matchRange];
      }
      else {
        [attributedString addAttribute:NSForegroundColorAttributeName value:obj.color range:matchRange];
      }
      
    }
    
  }
  
  self.attributedText = attributedString;
}

- (void)highlightBackgroundWithIndex:(CFIndex)index {
  
  NSMutableAttributedString* attributedString = [self.attributedText mutableCopy];
  
  for (int i = 0 ; i < self.matches.count; i++) {
    
    NSStringAttributesObject *obj = _arrayString[i];
    
    NSValue *value = self.matches[i];
    NSRange matchRange = [value rangeValue];
    
    if (obj.isSelect) {
      if ([self isIndex:index inRange:matchRange]) {
        
        [attributedString addAttribute:NSBackgroundColorAttributeName value:_colorHighlight range:matchRange];
      }
      else {
        [attributedString removeAttribute:NSBackgroundColorAttributeName range:matchRange];
      }
      
    }
    
  }
  
  self.attributedText = attributedString;
}

- (void)makeAttributes {
  if (self.matches) {
    self.matches = nil;
  }
  self.matches = [NSMutableArray array];
  
  if (self.labelHighlightType == LabelHighlightTypeText) {
    // None
  }
  NSMutableAttributedString* mutableString = [self.attributedText mutableCopy];
  
  __block NSMutableDictionary *dic = [NSMutableDictionary dictionary];
  for (int i = 0; i < _arrayString.count; i++) {
    
    __block NSRange rangeResult;
    __block NSStringAttributesObject *object = _arrayString[i];
    
    NSString *pattern = object.string;
    NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
    
    // Enumerate matches
    NSRange range = NSMakeRange(0,[self.text length]);
    __block int indexStringHighlight = 0;
    [expression enumerateMatchesInString:self.text options:0 range:range usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
      
      if (dic[object.string] != nil) {
        if (indexStringHighlight == [dic[object.string] integerValue]) {
          
          rangeResult = result.range;
          [dic setObject:[NSString stringWithFormat:@"%ld",[dic[object.string] integerValue]] forKey:object.string];
        }else {
          indexStringHighlight ++;
        }
      }else {
        
        rangeResult = result.range;
        [dic setObject:@"1" forKey:object.string];
      }
    }];
    
    [self.matches addObject:[NSValue valueWithRange:rangeResult]];
    [mutableString addAttribute:NSForegroundColorAttributeName value:object.color range:rangeResult];
    [mutableString addAttribute:NSFontAttributeName value:object.font range:rangeResult];
    if (object.underline == YES) {
      [mutableString addAttribute:NSUnderlineStyleAttributeName
                            value:@(NSUnderlineStyleSingle)
                            range:rangeResult];
    }
  }
  
  self.attributedText = mutableString;
}

#pragma mark - Methods Public
- (void)setAttributesTextWithArrayText:(NSArray *)array {
  self.arrayString = array;
  self.labelHighlightType = LabelHighlightTypeNone;
  NSStringAttributesObject *obj = array[0];
  
  NSMutableString *string = [NSMutableString stringWithFormat:@"%@",obj.string];
  for (int i = 1; i < array.count; i++) {
    NSStringAttributesObject *obj = array[i];
    [string appendString:[NSString stringWithFormat:@" %@",obj.string]];
  }
  self.text = string;
  [self makeAttributes];
}

- (void)setAttributesTextWithArrayText:(NSArray *)array labelHighlightType:(LabelHighlightType)labelHighlightType withColor:(UIColor *)color touchToReturnSender:(TouchReturnBlock)sender {
  self.touchReturnBlock = sender;
  self.labelHighlightType = labelHighlightType;
  self.arrayString = array;
  self.colorHighlight = color;
  
  if (array.count>0) {
    NSStringAttributesObject *obj = array[0];
    NSMutableString *string = [NSMutableString stringWithFormat:@"%@",obj.string];
    
    for (int i = 1; i < array.count; i++) {
      NSStringAttributesObject *obj = array[i];
      [string appendString:[NSString stringWithFormat:@" %@",obj.string]];
    }
    self.text = string;
  } else {
    self.text = @"";
  }
  [self makeAttributes];
}

- (void)setTextWithArrayTextHighlight:(NSArray *)array objectFullText:(NSStringAttributesObject *)objFullText labelHighlightType:(LabelHighlightType)labelHighlightType withColor:(UIColor *)color touchToReturnSender:(TouchReturnBlock)sender {
  if (objFullText && objFullText.string.length > 0) {
    
    self.touchReturnBlock = sender;
    self.labelHighlightType = labelHighlightType;
    self.arrayString = array;
    self.colorHighlight = color;
    
    self.text = objFullText.string;
    NSMutableAttributedString* mutableString = [self.attributedText mutableCopy];
    NSRange range = NSMakeRange(0,[self.text length]);
    [mutableString addAttribute:NSForegroundColorAttributeName value:objFullText.color range:range];
    [mutableString addAttribute:NSFontAttributeName value:objFullText.font range:range];
    self.attributedText = mutableString;
    
    [self makeAttributes];
  }
}

- (void)setAttributesForTexts:(NSArray *)array labelHighlightType:(LabelHighlightType)labelHighlightType withColor:(UIColor *)color touchToReturnSender:(TouchReturnBlock)sender {
  self.touchReturnBlock = sender;
  self.labelHighlightType = labelHighlightType;
  self.arrayString = array;
  self.colorHighlight = color;
  
  [self makeAttributes];
}

- (void)setAgainAttributesText:(NSArray *)array {
  self.arrayString = array;
  self.labelHighlightType = self.labelHighlightType;
  
  [self makeAttributes];
}

@end

