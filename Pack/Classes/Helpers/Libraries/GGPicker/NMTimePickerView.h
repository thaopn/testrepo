#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NMTimePickerViewDelegate;

@interface NMTimePickerView : NSObject

@property (nonatomic, strong) id <NMTimePickerViewDelegate>   delegate;

@property (nonatomic, strong) UINavigationBar           *navigationBar;
@property (nonatomic, strong) UIDatePicker              *datePicker;

@property (nonatomic, strong) NSString  *strDone;
@property (nonatomic, strong) NSString  *strCancel;

- (void)showWithSupperViewFrame:(UITabBar *)tabbar;
- (void)showActionSheetInView:(UIView *)view;
- (id)init:(BOOL)isShowDateFuture;

@end

@protocol NMTimePickerViewDelegate <NSObject>

- (void) datePicker:(NMTimePickerView *)picker dismissWithButtonDone:(BOOL)done;

@end
