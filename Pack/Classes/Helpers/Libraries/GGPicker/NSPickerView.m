#import "NSPickerView.h"

#define defaultBackgroundColor    [UIColor colorWithRed:241/255.0 green:241/255.0 blue:239/255.0 alpha:1.0]
#define defaultColorBarButton     [UIColor colorWithRed:0/255.0 green:122/255.0 blue:255/255.0 alpha:1.0]
#define defaultFontBarButton      [UIFont boldSystemFontOfSize:15.0]
#define defaultTintNavibarColor   [UIColor colorWithRed:101/255.0 green:135/255.0 blue:199/255.0 alpha:1.0]
#define screenWidth               [UIScreen mainScreen].bounds.size.width

@interface NSPickerView()<UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, retain) UIView     *viewBackGround;
@property (nonatomic, retain) UIView     *viewSubBackGround;
@property (nonatomic, retain) UIView     *viewActionSheet;
@property (nonatomic, strong) NSDictionary *dicSelect;
@property (assign) BOOL isSelected;

@end

@implementation NSPickerView

- (id)init {
  self = [super init];
  if (self) {
    self.delegate = nil;
    
    _arrayDataSource = [NSMutableArray array];
    
    _isSelected = NO;
    
    self.picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44, screenWidth, 216)];
    [self.picker setBackgroundColor:[UIColor colorWithRed:216/255.0 green:220/255.0 blue:228/255.0 alpha:1.0]];
    self.picker .dataSource = self;
    self.picker .delegate = self;
    
    self.navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    
    UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@""];
    [self.navigationBar pushNavigationItem:navigationItem animated:NO];
    [self.navigationBar setTintColor:defaultBackgroundColor];
    
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:_strCancel.length>0?_strCancel: @"      Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(tapToCancel)];
    NSDictionary *attrTextCancel = [NSDictionary dictionaryWithObjectsAndKeys:
                                    defaultColorBarButton,NSForegroundColorAttributeName,
                                    defaultFontBarButton,NSFontAttributeName,
                                    nil];
    [btnCancel setTitleTextAttributes:attrTextCancel
                             forState:UIControlStateNormal];
    [btnCancel setTintColor:defaultTintNavibarColor];
    
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:_strDone.length>0?_strDone: @"Done      " style:UIBarButtonItemStylePlain target:self action:@selector(tapToDone)];
    NSDictionary *attrTextDone = [NSDictionary dictionaryWithObjectsAndKeys:
                                  defaultColorBarButton,NSForegroundColorAttributeName,
                                  defaultFontBarButton,NSFontAttributeName,
                                  nil];
    [btnDone setTitleTextAttributes:attrTextDone
                           forState:UIControlStateNormal];
    [btnDone setTintColor:defaultTintNavibarColor];
    
    
    self.navigationBar.topItem.rightBarButtonItems = @[btnDone];
    self.navigationBar.topItem.leftBarButtonItems = @[btnCancel];
    
    _viewBackGround = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    _viewBackGround.backgroundColor = [UIColor clearColor];
    _viewBackGround.alpha = 0.0f;
    
    UIButton *btDone =  [UIButton buttonWithType:UIButtonTypeCustom];
    [btDone setBackgroundColor:[UIColor clearColor]];
    [btDone setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [btDone addTarget:self action:@selector(tapToCancel) forControlEvents:UIControlEventTouchUpInside];
    [btDone setFrame:CGRectMake(0, 0, screenWidth, [UIScreen mainScreen].bounds.size.height - 216)];
    [_viewBackGround addSubview:btDone];
    
    _viewSubBackGround = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-260, screenWidth, 216)];
    _viewSubBackGround.backgroundColor = [UIColor clearColor];
    _viewBackGround.alpha = 0.0f;
    
    _viewActionSheet = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-260, screenWidth, 260)];
    [_viewActionSheet addSubview:self.navigationBar];
    [_viewActionSheet addSubview:self.picker];
    _viewBackGround.alpha = 0.0f;
    
  }
  return self;
}

- (void)reActive {
  UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:_strCancel.length>0?_strCancel: @" Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(tapToCancel)];
  NSDictionary *attrTextCancel = [NSDictionary dictionaryWithObjectsAndKeys:
                                  defaultColorBarButton,NSForegroundColorAttributeName,
                                  defaultFontBarButton,NSFontAttributeName,
                                  nil];
  [btnCancel setTitleTextAttributes:attrTextCancel
                           forState:UIControlStateNormal];
  [btnCancel setTintColor:defaultTintNavibarColor];
  
  
  UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:_strDone.length>0?_strDone:@"Done  " style:UIBarButtonItemStylePlain target:self action:@selector(tapToDone)];
  NSDictionary *attrTextDone = [NSDictionary dictionaryWithObjectsAndKeys:
                                defaultColorBarButton,NSForegroundColorAttributeName,
                                defaultFontBarButton,NSFontAttributeName,
                                nil];
  [btnDone setTitleTextAttributes:attrTextDone
                         forState:UIControlStateNormal];
  [btnDone setTintColor:defaultTintNavibarColor];
  
  
  self.navigationBar.topItem.rightBarButtonItems = @[btnDone];
  //    self.navigationBar.topItem.leftBarButtonItems = @[btnCancel];
}

- (void)dealloc {
  self.delegate = nil;
  
  self.viewBackGround = nil;
  self.viewSubBackGround = nil;
  self.viewActionSheet = nil;
  
}

#pragma mark - UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
  return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
  return [_arrayDataSource count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
  NSDictionary *dict = [_arrayDataSource objectAtIndex:row];
  return [dict objectForKey:@"name"];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
  return 300;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
  
  if (row == 0) {
    [_picker reloadAllComponents];
    [_picker selectRow:1 inComponent:0 animated:YES];
    _dicSelect = [_arrayDataSource objectAtIndex:1];
  } else {
    _dicSelect = [_arrayDataSource objectAtIndex:row];
  }
}

- (void)showActionSheetInView:(UIView *)view {
  [_picker reloadAllComponents];
  
  [self showViewFakeActionSheetWithView:view];
}

- (void)setValueWhenReShow:(NSInteger )index {
  [_picker selectRow:index inComponent:0 animated:NO];
  _dicSelect = [_arrayDataSource objectAtIndex:index];
}

- (void)tapToDone {
  if (self.delegate && [self.delegate respondsToSelector:@selector(picker:dismissWithButtonDone:withDic:)]) {
    if (_dicSelect) {
      //
    }else{
      _dicSelect = [_arrayDataSource objectAtIndex:0];
    }
    
    [self.delegate picker:self dismissWithButtonDone:YES withDic:_dicSelect];
  }
  
  [self hideViewFakeActionSheet];
}


- (void)tapToCancel {
  if (_dicSelect) {
    //
  }else{
    _dicSelect = [_arrayDataSource objectAtIndex:0];
  }
  
  if (self.delegate && [self.delegate respondsToSelector:@selector(picker:dismissWithButtonDone:withDic:)]) {
    [self.delegate picker:self dismissWithButtonDone:NO withDic:_dicSelect];
  }
  
  [self hideViewFakeActionSheet];
}

- (void)showViewFakeActionSheetWithView:(UIView *)view {
  UIWindow *window = [[UIApplication sharedApplication] keyWindow];
  [window addSubview:_viewBackGround];
  [window addSubview:_viewSubBackGround];
  [window addSubview:_viewActionSheet];
  
  _viewBackGround.alpha = 0.25f;
  _viewSubBackGround.alpha = 1.0;
  _viewActionSheet.alpha = 1.0;
  
  _viewSubBackGround.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, screenWidth, 216);
  _viewActionSheet.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, screenWidth, 260);
  [UIView animateWithDuration:0.3 animations:^{
    _viewSubBackGround.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-216, screenWidth, 216);
    _viewActionSheet.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-260, screenWidth, 260);
  }];
}

- (void)hideViewFakeActionSheet {
  _viewBackGround.alpha = 0;
  
  [UIView animateWithDuration:0.3 animations:^{
    _viewSubBackGround.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, screenWidth, 216);
    _viewActionSheet.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, screenWidth, 260);
    
  }];
}

@end
