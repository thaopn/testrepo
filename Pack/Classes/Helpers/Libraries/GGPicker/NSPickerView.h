#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NSPickerViewDelegate;

@interface NSPickerView : NSObject

@property (nonatomic, weak) id <NSPickerViewDelegate>   delegate;

@property (nonatomic, strong) NSMutableArray *arrayDataSource;
@property (nonatomic, strong) UINavigationBar           *navigationBar;
@property (nonatomic, strong) UIPickerView *picker;

@property (nonatomic, strong) NSString  *strDone;
@property (nonatomic, strong) NSString  *strCancel;
@property (nonatomic, strong) NSIndexPath  *indexPath;

-(void)showActionSheetInView:(UIView *)view;
-(void)setValueWhenReShow:(NSInteger )index;
-(void)reActive;

@end

@protocol NSPickerViewDelegate <NSObject>

- (void)picker:(NSPickerView *)picker dismissWithButtonDone:(BOOL)done withDic:(NSDictionary *)dic;

@end