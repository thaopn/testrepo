//
//  Helpers.swift
//  Pack
//
//  Created by Đăng Hoà on 8/27/15.
//  Copyright (c) 2015 ___GREENGLOBAL___. All rights reserved.
//

import Foundation

public extension String {
  func encoding() -> String {
    //Encode Url String
    let customAllowedSet =  NSCharacterSet(charactersInString:"=\"#%/<>?@\\^`{|} ").invertedSet
    let urlStringEncoded = self.stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
    return urlStringEncoded!
  }
}
