//
//  Define.swift
//  Pack
//
//  Created by Đăng Hoà on 8/13/15.
//  Copyright (c) 2015 ___GREENGLOBAL___. All rights reserved.
//

import Foundation
import UIKit

// MARK: - App Name
let appName                           = "Pack"

// MARK: - Banner Height
let bannerHeight: CGFloat             = 64.0

// MARK: - Type message part
let typeTextPlain                     = "text/plain"
let typeTextHTML                      = "text/HTML"
let typeImagePNG                      = "image/png"
let typeImageGIF                      = "image/gif"
let typeVideoQuickTime                = "video/quicktime"
let typeImageSize                     = "application/json+imageSize"
let typeImageJPEG                     = "image/jpeg"
let typeImageJPEGPreview              = "image/jpeg+preview"
let typeImageGIFPreview               = "image/gif+preview"
let typeLocation                      = "location/coordinate"
let typeDate                          = "text/date"
let typeVideoMP4                      = "video/mp4"

// MARK: - Duration
let duration                          = 60 * 60

// MARK: - Name Screen
let nameLoginScreen                   = "Login"

//Font
let nameFontMedium                    =  "Helvetica-Medium"
let nameFontRegular                   =  "Helvetica"
let nameFontLight                     =  "Helvetica-Light"
let nameFontOblique                   =  "Helvetica-Oblique"
let nameFontLightCond                 =  "Helvetica-LightOblique"
let nameFontBold                      =  "Helvetica-Bold"

let robotoLight                       = "Roboto-Light"
let robotoThin                        = "Roboto-Thin"
let robotoRegular                     = "Roboto-Regular"
let robotoBold                        = "Roboto-Bold"
let robotoMedium                      = "Roboto-Medium"

let arialRegular                      = "Arial"


// MARK: Notification
let notificationGetInfo               = "NotificationGetInfo"

// MARK: Size
let screenWidth   = UIScreen.mainScreen().bounds.size.width
let screenHeight  = UIScreen.mainScreen().bounds.size.height

let navibarChangePoint: CGFloat = 50

func changeFontSizeWithDevice(size: CGFloat) -> CGFloat {
  if UIScreen.mainScreen().bounds.size.height == 480 {
    // iPhone 4
    return size
  } else if UIScreen.mainScreen().bounds.size.height == 568 {
    // IPhone 5
    return size
  } else if UIScreen.mainScreen().bounds.size.width == 375 {
    // iPhone 6
    return size + 1
  } else if UIScreen.mainScreen().bounds.size.width == 414 {
    // iPhone 6+
    return size + 2
  } else if UIScreen.mainScreen().bounds.size.width == 768 {
    // iPad
    return size + 2
  }
  return size
}

// MARK: - Font with size
func getRobotoMediumWith(size: CGFloat) -> UIFont {
  return UIFont(name: robotoMedium, size: size)!
}

func getRobotoThinWith(size: CGFloat) -> UIFont? {
  return getFontWith(robotoThin, size: size)
}

func getRobotoBoldWith(size: CGFloat) -> UIFont? {
  return getFontWith(robotoBold, size: size)
}

func getRobotoLightWith(size: CGFloat) -> UIFont? {
  return getFontWith(robotoLight, size: size)
}

func getRobotoRegularWith(size: CGFloat) -> UIFont? {
  return getFontWith(robotoRegular, size: size)
}

func getFontWith(name: String, size: CGFloat) -> UIFont? {
  return UIFont(name: name, size: size)
}

// MARK: - Notification
let NotificationUpdateMessage: String                 = "NotificationUpdateMessage"
let NotificationConnected: String                     = "NotificationConnected"
let NotificationUpdateInfoSuccess: String             = "NotificationUpdateInfoSuccess"
let NotificationChangeImage: String                   = "NotificationChangeImage"
let NotificationChangePersonal: String                = "NotificationChangePersonal"
let NotificationUpdateInfoFailed: String              = "NotificationUpdateInfoFailed"
let NotificationGetInfoAfterUpdateSuccess: String     = "NotificationGetInfoAfterUpdateSuccess"
let NotificationUpdateListMatched: String             = "NotificationUpdateListMatched"
let NotificationUploadPhotoDone: String               = "NotificationUploadPhotoDone"
let NotificationUploadingPhoto: String                = "NotificationUploadingPhoto"
let NotificationDeletePhoto: String                   = "NotificationDeletePhoto"
let NotificationNotDeletePhoto: String                = "NotificationNotDeletePhoto"
let NotificationUpdateWorkHistorySuccess: String = "NotificationUpdateWorkHistorySuccess"

// MARK: - Time Animation
let timeAnimation = 3.5

// MARK: - Default Banner Color
let defaultBackgroundColor = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)

let defaultAvatar = UIImage(named: "img_avatar_default.jpg")
