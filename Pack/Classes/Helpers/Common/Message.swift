//
//  Message.swift
//  Pack
//
//  Created by Đăng Hoà on 8/13/15.
//  Copyright (c) 2015 ___GREENGLOBAL___. All rights reserved.
//

import Foundation

let textLoginVCTermNote                    = "By signing up you agree to our "
let textTermOfService                      = "Terms of Service"
let textLoginVCSlideOne                    = "Find Roommates on Pack"
let textLoginVCSlideTwo                    = "Match with Compatible Roommates"
let textLoginVCSlideThree                  = "Message and Find a Room Together"
let textPleaseChooseLocation               = "Please choose a location"
let textUploadPhotoSuccess                 = "Upload photo success"
let textDeletePhotoSuccess                 = "Delete photo success"
let textMatchingLoading                    = "Matching roommates for you now..."
let textInterested                         = "Interested"
let textNoInterested                       = "Not Interested"
let messageNotConnected                    = "Can not connect server chat"
let textUploadInfoUserSuccess              = "Upload user info success"
let textUploadInfoUserFailed               = "Upload user info failed"
let textNone                               = "None"
let textUpdateSettingSuccess               = "Update setting success"
let textUpdateSettingFailed                = "Update setting failed"
let textLoginWithInstagramFailed           = "Login failed"
let textAgeMuchOlderThan18                 = "Please choose age older than 18"
let textConnectLinkedin                    = "Connect Linkedin"
let textDisconnectLinkedin                 = "Disconnect Linkedin"
let textConfirmDisconnectLinkedin          = "Are you sure to disconnect Linkedin"
let textDontEmptyGender                    = "Please choose gender"
let textImageLeastOnePhoto                 = "Please don't empty photo\n(least 1 photo)"
let textConnectInstagramSuccess            = "Connect instagram success"
let textConnectInstagramFailed             = "Connect instagram failed"
let textDisconnectInstagramSuccess         = "Disconnect instagram success"
let textDisconnectInstagramFailed          = "Disconnect instagram failed"
let textSizeFileTooBig                     = "File's size is too large"
let textRequestPermissionLocation          = "Please allow Pack get your location."
let textUploadingPhoto                     = "Photo is uploading you must wait upload done"
let textLoginVCNote                        = "We don't post anything on your Facebook or Instagram feed"
let textMatchingNoBody                     = "Sorry, there is no more people showing here. Update your    roommate perferences to view more people."
let textInterestedContent                  = "Dragging a picture to the right indicates you are interested in"
let textChooseCollegeNoticeNone            = "If your current school isn't show, please update it on Facebook and it will appear here."
let textNoInterestedContent                = "Dragging a picture to the left indicates you are not interested in"
let textAlertWhenBlocked                   = "Your account has been locked. Please contact us for more information."
let textConfirmReport                      = "Are you sure you want to report this user?"
let textReportSuccess                      = "Thank you for your report, we will process your report shortly."
let textInstaAuthen                        = "Please login with Instagram again"
let textConnectInstagramExisted            = "This instagram account is existed"
let messageDataWrong                       = "There is something wrong when get user information"
