//
//  AppDelegate.swift
//  Pack
//
//  Created by Pham Ngoc Thao on 5/3/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import FBSDKLoginKit
import LayerKit
import Fabric
import Crashlytics
import Appsee
import ZendeskSDK
import Localytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var layerClient: LYRClient!
  var isLoadingListUser = false
  var isLoadingApp = false
  var isAppTerminal = false
  var notiInfo: [NSObject : AnyObject]?
  var isNotFoundUserMatching = false
  var isFromNotification = false

  typealias AuthenticationCompletionBlock = (Bool, error: NSError?) -> Void
  typealias IdentityTokenCompletionBlock = (Bool, error: NSError?) -> Void

  class func shareInstance() -> AppDelegate {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    return appDelegate
  }

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    window = UIWindow(frame: UIScreen.mainScreen().bounds)
    window?.backgroundColor = UIColor.whiteColor()
    window?.makeKeyAndVisible()

    let sb = UIStoryboard(name: "LaunchScreen", bundle: nil)
    let vc = sb.instantiateInitialViewController()
    window?.rootViewController = vc

    GMSServices.provideAPIKey(API_LOCATION_KEY)

    FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

    AccountFlowManager.shareInstance.start()

    flowRegister()

    // Initializes a LYRClient
    let appID = NSURL(string: LAYER_APP_ID)
    layerClient = LYRClient(appID: appID!)
    layerClient.autodownloadMIMETypes = NSSet(objects: typeImagePNG, typeImageJPEG, typeImageJPEGPreview, typeImageGIF, typeImageGIFPreview, typeLocation) as? Set<String>
    layerClient.autodownloadMaximumContentSize = 1024 * 1024 * 20

    // Notification:
    if NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != nil && NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != "" {
      if let localNoti = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] {
        notiInfo = localNoti as? [NSObject : AnyObject]
        isAppTerminal = true
      }
    }

    // Integrate SDK
    integrateSDK(application, didFinishLaunchingWithOptions: launchOptions)

    return true
  }

  // MARK: - Integrate SDK
  private func integrateSDK(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) {

    // Crashlitics
    Fabric.with([Crashlytics.self])

    // Localytics
    Localytics.integrate("a1e312f2f1647edc4ae441a-ac6f0a88-1622-11e6-4431-00adad38bc8d")
    if application.applicationState != UIApplicationState.Background {
      Localytics.openSession()
    }

    // Taplytics
    Taplytics.startTaplyticsAPIKey("f665d1fbb42ee0d3ec026bee940bcedca23b5e89", options: ["liveUpdate": NSNumber(bool: false)])

    // Appsee
    Appsee.start("3bf74c675860408c9c787c519020ee2c")

    // Zendesk
    ZDKConfig.instance().initializeWithAppId("9765d4ee283ba06a6e9fbf18ce2a7052cb3ab8c467e4e327",
                                             zendeskUrl: "https://greenglobalhelp.zendesk.com",
                                             clientId: "mobile_sdk_client_3202b3fd2f30cc360910")
    let identity = ZDKAnonymousIdentity()
    ZDKConfig.instance().userIdentity = identity

    Rollout.setupWithKey("5760db4cb9823b964d5a9e85")
  }

  func flowRegister(isLauching: Bool = true) -> Void {
    if isLauching == true {
      if UIApplication.sharedApplication().isRegisteredForRemoteNotifications() {
        registerAccessToken()
      } else {
        // None
      }
    } else {
      if UIApplication.sharedApplication().isRegisteredForRemoteNotifications() == false {
        registerAccessToken()
      } else {
        // None
      }
    }
  }

  func registerAccessToken() -> Void {
    // Set up notification
    switch(getMajorSystemVersion()) {
    case 8: // iOS 8 registration code
      UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil))
      UIApplication.sharedApplication().registerForRemoteNotifications()
    case 9: // iOS 9 registration code when the time comes
      UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil))
      UIApplication.sharedApplication().registerForRemoteNotifications()
    default: // do nothing as we cannot tell!
      UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil))
      UIApplication.sharedApplication().registerForRemoteNotifications()
    }
  }

  func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
    if url.absoluteString.containsString("tl-") {
      return false
    }
    return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url,
                                                                 sourceApplication: sourceApplication,
                                                                 annotation: annotation)
  }

  func getMajorSystemVersion() -> Int {
    return Int(String(Array(UIDevice.currentDevice().systemVersion.characters)[0]))!
  }

  func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {

    // Get deviceToken
    let token: String? = deviceToken.description

    if let letToken = token {
      let trimEnds: String = letToken.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
      let cleanToken = trimEnds.stringByReplacingOccurrencesOfString(" ", withString: "", options: .RegularExpressionSearch)
      print("Got token data! \(cleanToken)")
      NSUserDefaults.standardUserDefaults().setObject(cleanToken, forKey: userKeyDeviceToken)
      NSUserDefaults.standardUserDefaults().synchronize()
    } else {
      NSUserDefaults.standardUserDefaults().setObject("", forKey: userKeyDeviceToken)
      NSUserDefaults.standardUserDefaults().synchronize()
    }
    // Send device token to Layer
    var error: NSError?
    let isSuccess: Bool

    NSUserDefaults.standardUserDefaults().setObject(deviceToken, forKey: userKeyDataDeviceToken)
    NSUserDefaults.standardUserDefaults().synchronize()
    do {
      try layerClient!.updateRemoteNotificationDeviceToken(deviceToken)
      isSuccess = true
    } catch let err as NSError {
      error = err
      isSuccess = false
    }
    if (isSuccess) {
      print("Application did register for remote notifications: \(deviceToken)")
    } else {
      print("Failed updating device token with error: \(error)")
    }
    // Update Device Token
    if NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != nil && NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != "" && NSUserDefaults.standardUserDefaults().objectForKey(userKeyDeviceToken)  != nil && NSUserDefaults.standardUserDefaults().objectForKey(userKeyDeviceToken) as! String  != "" && NSUserDefaults.standardUserDefaults().objectForKey(userDeviceId) != nil {
      let param = [
        "enabled": "true",
        "token":NSUserDefaults.standardUserDefaults().objectForKey(userKeyDeviceToken)!,
        ]
      GGWebservice.callWebServiceWithRequest(.PATCH, urlString: apiGetUpdateDeviceId(NSUserDefaults.standardUserDefaults().objectForKey(userDeviceId) as! String), param: param, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
        if statusCode == 200 || statusCode == 201 {
          print(value)
        } else {
          print("Some thing went wrong")
        }
      }
    }
  }

  func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
    print("Couldn't register: \(error)")
    NSUserDefaults.standardUserDefaults().setObject("", forKey: userKeyDeviceToken)
    NSUserDefaults.standardUserDefaults().synchronize()
  }

  func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
    print("Notification Info: \(userInfo)")
    isFromNotification = true
    ReceiverNotiControl.shareInstance().receiveNoti(userInfo)
  }

  func applicationDidEnterBackground(application: UIApplication) {
    Localytics.closeSession()
    Localytics.upload()
  }

  func applicationDidBecomeActive(application: UIApplication) {
    FBSDKAppEvents.activateApp()
    ReceiverNotiControl.shareInstance().receiveNoti(nil)
    Localytics.openSession()
    Localytics.upload()
    isFromNotification = false
    DHIndicator.hide()
  }

  func applicationWillTerminate(application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    self.saveContext()
  }

  // MARK: - Core Data stack
  lazy var applicationDocumentsDirectory: NSURL = {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "GreenGlobal.Pack" in the application's documents Application Support directory.
    let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
    return urls[urls.count-1]
  }()

  lazy var managedObjectModel: NSManagedObjectModel = {
    // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
    let modelURL = NSBundle.mainBundle().URLForResource("Pack", withExtension: "momd")!
    return NSManagedObjectModel(contentsOfURL: modelURL)!
  }()

  lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
    // Create the coordinator and store
    let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
    let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
    var failureReason = "There was an error creating or loading the application's saved data."
    do {
      try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
    } catch {
      // Report any error we got.
      var dict = [String: AnyObject]()
      dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
      dict[NSLocalizedFailureReasonErrorKey] = failureReason

      dict[NSUnderlyingErrorKey] = error as NSError
      let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
      // Replace this with code to handle the error appropriately.
      // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
      NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
      abort()
    }
    return coordinator
  }()

  lazy var managedObjectContext: NSManagedObjectContext = {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
    let coordinator = self.persistentStoreCoordinator
    var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
    managedObjectContext.persistentStoreCoordinator = coordinator
    return managedObjectContext
  }()

  // MARK: - Core Data Saving support
  func saveContext () {
    if managedObjectContext.hasChanges {
      do {
        try managedObjectContext.save()
      } catch {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        let nserror = error as NSError
        NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
        abort()
      }
    }
  }

  // MARK: - Function Authenticate Layer
  func authenticateLayerWithUserID(userID: String, completion: AuthenticationCompletionBlock) {

    // Check to see if the layerClient is already authenticated.
    if let authenticatedUser = layerClient.authenticatedUser {
      // If the layerClient is authenticated with the requested userID, complete the authentication process.
      if authenticatedUser.userID == AccountFlowManager.currentUser.id {
        print("Layer authenticated as user id: \(authenticatedUser.userID)")
        completion(true, error: nil)
      } else {

        // If the authenticated userID is different, then deauthenticate the current client and re-authenticate with the new userID.
        layerClient.deauthenticateWithCompletion({ [weak self](isSuccess, error) in
          if isSuccess {
            self?.layerClient.requestAuthenticationNonceWithCompletion({ (nonce, error) in
              guard let nonce = nonce else {
                completion(false, error: error)
                return
              }
              self?.requestIdentityTokenForUserID(userID, nonce: nonce, completion: { (isSuccess, error) in
                completion(isSuccess, error: error)
              })
            })

          } else {
            completion(false, error: error)
          }
        })
      }

    } else {
      // If the layerClient isn't already authenticated, then authenticate.
      layerClient.requestAuthenticationNonceWithCompletion({ [weak self](nonce, error) in
        guard let nonce = nonce else {
          completion(false, error: error)
          return
        }

        self?.requestIdentityTokenForUserID(userID, nonce: nonce, completion: { (isSuccess, error) in
          completion(isSuccess, error: error)
        })
      })
    }
  }

  func requestIdentityTokenForUserID(userID: String, nonce: String, completion: IdentityTokenCompletionBlock) {

    let param: [String:String] = [
      "user_id" : userID,
      "nonce"   : nonce
    ]
    let header = [
      "Content-Type" : "application/x-www-form-urlencoded",
      "Accept" : "application/json"
    ]
    GGWebservice.callWebServiceWithHeader(.POST, urlString: apiIdentityToken, header: header, param: param) { (value, statusCode, error) in
      if let value = value {
        let identityToken = value["identity_token"] as! String
        self.layerClient.authenticateWithIdentityToken(identityToken, completion: { (authenticatedUser, error) in
          if let authenticatedUser = authenticatedUser {
            completion(true, error: error)
            print("Layer authenticated as user: \(authenticatedUser.userID)")
          } else {
            completion(false, error: error)
          }
        })
      } else {
        completion(false, error: nil)
      }
    }
  }
}
