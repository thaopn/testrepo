//
//  FBUserProfile.swift
//  Pack
//
//  Created by admin on 6/2/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class FBUserProfile: NSObject, Mappable {

  var fbID: String?
  var fbName: String?
  var fbFirstname: String?
  var fbLastname: String?
  var fbAvatarLarge: String?
  var fbEmail: String?
  var fbBirthday: String?
  var fbBio: String?
  var fbGender: String?
  var fbEducation: [SchoolObj]?
  var fbWorkHistory: [FBWork]?
  var fbToken: String?


  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    fbID          <- map["id"]
    fbName        <- map["name"]
    fbFirstname   <- map["first_name"]
    fbLastname    <- map["last_name"]
    fbAvatarLarge <- map["picture.data.url"]
    fbEmail       <- map["email"]
    fbBirthday    <- map["birthday"]
    fbBio         <- map["bio"]
    fbGender      <- map["gender"]
    fbEducation   <- map["education"]
    fbWorkHistory <- map["work"]
  }
}
