//
//  FBMutualFriends.swift
//  Pack
//
//  Created by admin on 6/7/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class FBMutualFriends: NSObject, Mappable {
  var totalCount: Int = 0


  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    totalCount <- map["context.all_mutual_friends.summary.total_count"]
  }
}
