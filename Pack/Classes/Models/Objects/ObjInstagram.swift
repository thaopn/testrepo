//
//  ObjInstagram.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/25/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class ObjInstagram: NSObject, Mappable {

  var objId: String = ""
  var objType: String = ""
  var objLocation: LocationObjInstagramm?
  var imageInstagram: ImageInstagram?

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    objId           <- map["id"]
    objType         <- map["type"]
    objLocation     <- map["location"]
    imageInstagram  <- map["images"]
  }
}

class ImageInstagram: NSObject, Mappable {

  var lowResolution: ResolutionInstagrm?
  var standardResolution: ResolutionInstagrm?
  var thumbnail: ResolutionInstagrm?

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    lowResolution       <- map["low_resolution"]
    standardResolution  <- map["standard_resolution"]
    thumbnail           <- map["thumbnail"]
  }
}

class userInstagram: NSObject, Mappable {

  var userNameInstagram: String = ""
  var idUserInstagram: String = ""
  var profilePictureInstagram: String = ""
  var fullNameInstagram: String = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    userNameInstagram       <- map["username"]
    idUserInstagram         <- map["id"]
    profilePictureInstagram <- map["profile_picture"]
    fullNameInstagram       <- map["full_name"]
  }
}

class ResolutionInstagrm: NSObject, Mappable {

  var url: String = ""
  var width: Float = 0.0
  var height: Float = 0.0

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    url     <- map["url"]
    width   <- map["width"]
    height  <- map["height"]
  }
}

class LocationObjInstagramm: NSObject, Mappable {

  var locId: String = ""
  var locLatitude: Double = 0.0
  var locLogitude: Double = 0.0
  var locName: String = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    locId         <- map["id"]
    locLatitude   <- map["latitude"]
    locLogitude   <- map["longitude"]
    locName       <- map["name"]
  }
}

class ListObjInstagram: NSObject, Mappable {

  var data: [ObjInstagram] = []

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    data    <- map["data"]
  }
}
