//
//  UserMatched.swift
//  Pack
//
//  Created by Ngoc Anh Truong on 5/29/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class UserMatched: NSObject, Mappable {

  var matchedId: String = ""
  var userId: String = ""
  var firstName: String = ""
  var lastName: String = ""
  var fullName: String = ""
  var primaryImage: String = ""
  var dateMatch: String = ""
  var images: [Image] = []

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    matchedId       <- map["id"]
    userId          <- map["relationships.matched_user.id"]
    firstName       <- map["relationships.matched_user.attributes.first_name"]
    lastName        <- map["relationships.matched_user.attributes.last_name"]
    fullName        <- map["relationships.matched_user.attributes.full_name"]
    primaryImage    <- map["relationships.matched_user.attributes.primary_image"]
    dateMatch       <- map["attributes._created"]
    images          <- map["relationships.matched_user.attributes.images"]
  }

  func getPrimaryImageUrl() -> String {
    if images.count == 0 {
      return ""
    }

    if let strPrimary = images.first?.urlImg {
      if strPrimary.containsString("http") || strPrimary.containsString("https") {
        if strPrimary.containsString("facebook") {
          return strPrimary + "?type=large"
        }
        return strPrimary
      } else {
        return apiHost + strPrimary
      }
    } else {
      return ""
    }
  }

  func getUserName() -> String {
    if firstName.characters.count > 0 {
      return firstName
    }
    return fullName
  }

  func getDateMatched() -> String {
    if dateMatch == "" {
      return ""
    } else {
      let dateformatter = Common.getDateFormat()

      guard let date = dateformatter.dateFromString(dateMatch) else {
        return ""
      }

      let arrDMY = dateformatter.stringFromDate(date).componentsSeparatedByString("-")

      if arrDMY.count >= 3 {
        let arrDay = arrDMY[2].componentsSeparatedByString("T")
        let strDate = arrDMY[1] + "/" + arrDay[0] + "/" + arrDMY[0]
        return strDate
      }

      return ""

    }
  }
}

class ListMatched: NSObject, Mappable {

  var matches: [UserMatched] = []

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    matches   <- map["data"]
  }
}
