//
//  FBPhotos.swift
//  Pack
//
//  Created by admin on 6/2/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class FBPhotos: NSObject, Mappable {
  var photos = [FBImages]()

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    photos <- map["data"]
  }
}

class FBImage: NSObject, Mappable {
  var height: CGFloat = 0.0
  var width: CGFloat = 0.0
  var url = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    height  <- map["height"]
    width   <- map["width"]
    url     <- map["source"]
  }
}

class FBImages: NSObject, Mappable {
  var images = [FBImage]()
  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    images <- map["images"]
  }
}
