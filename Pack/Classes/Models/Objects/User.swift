//
//  User.swift
//  GGStructureSwift
//
//  Created by Đăng Hoà on 9/3/15.
//  Copyright (c) 2015 ___GREENGLOBAL___. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyJSON


let userAccesstokenKeyConstant      = "__authToken"
let userTypeAccesstokenKeyConstant  = "__typeToken"
let userEmailKeyConstant            = "__authEmail"
let userIDKeyConstant               = "__authID"
let userKeyDeviceToken              = "__deviceToken"
let userFirstSwipeRignt             = "__firstRight"
let userFirstSwipeLeft              = "__firstLeft"
let userIsLoginWithFacebook         = "__loginWithFacebook"
let userIsLoginWithInstagram        = "__loginWithInstagram"
let userDeviceId                    = "__deviceId"
let userKeyDataDeviceToken          = "__dataDeviceToken"
let userInstaToken                  = "__userInstaToken"
let userLinkedInToken               = "__userLinkedInToken"

class User: NSObject, Mappable {

  var id: String = ""
  var email: String = ""
  var username: String = ""
  var gender: String = ""
  var about: String = ""
  var budgetMin: Float = 0.0
  var budgetMax: Float = 0.0
  var lastName: String = ""
  var firstName: String = ""
  var primaryImage: String = ""
  var interests: String = ""
  var dob: String = ""
  var workHistory: String = ""
  var eTag: String = ""
  var images: [Image] = []
  var moveInDate: String = ""
  var listIdImage: [String] = []

  var prefGender: String = ""
  var prefAgeMin: Float = 0.0
  var prefAgeMax: Float = 0.0
  var preProfession: String = ""
  var prefBudgetMin: Float = 0.0
  var prefBudgetMax: Float = 0.0
  var prefShowInDiscovery: String = ""
  var prefNewMatch: String = ""
  var prefMessage: String = ""

  var authToken: String = ""
  var typeToken: String = ""
  var isLogin: Bool = false
  var isLoginWithFacebook: Bool = false
  var isLoginWithInstagram: Bool = false
  var deviceId: String = ""

  var fbUpdatedTime: String = ""
  var fbVerified: Bool = false
  var fullName: String = ""
  var timeZone: String = ""
  var fbLink: String = ""

  var uLocation: String = ""
  var uLatitude: Double?
  var uLongtitude: Double?
  var uEducation: String = ""
  var uInstaUsername: String = ""
  var uInstaId: String = ""
  var uFbId: String = ""
  var deviceToken: String = ""
  var dateMatch: String = ""
  var uInstaPhotos: String = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  // Mappable
  func mapping(map: Map) {
    id                  <- map["id"]
    email               <- map["attributes.email"]
    gender              <- map["attributes.gender"]
    about               <- map["attributes.about_me"]
    username            <- map["attributes.username"]
    budgetMin           <- map["attributes.budget_min"]
    budgetMax           <- map["attributes.budget_max"]
    firstName           <- map["attributes.first_name"]
    lastName            <- map["attributes.last_name"]
    primaryImage        <- map["attributes.primary_image"]
    dob                 <- map["attributes.dob"]
    workHistory         <- map["attributes.work_history"]
    images              <- map["attributes.images"]
    interests           <- map["attributes.interests"]
    moveInDate          <- map["attributes.movein_date"]
    deviceId            <- map["attributes.deviceid"]
    listIdImage         <- map["attributes.imageids"]

    prefAgeMin          <- map["attributes.pref_age_min"]
    prefAgeMax          <- map["attributes.pref_age_max"]
    preProfession       <- map["attributes.pref_profession"]
    prefBudgetMin       <- map["attributes.pref_budget_min"]
    prefBudgetMax       <- map["attributes.pref_budget_max"]
    prefMessage         <- map["attributes.pref_notify_message"]
    prefNewMatch        <- map["attributes.pref_notify_new_match"]
    prefShowInDiscovery <- map["attributes.pref_show_in_discovery"]
    prefGender          <- map["attributes.pref_gender"]
    uInstaId            <- map["attributes.insta_id"]
    uInstaUsername      <- map["attributes.insta_username"]

    authToken           <- map["attributes.token"]
    fbVerified          <- map["attributes.fb_verified"]
    fullName            <- map["attributes.full_name"]
    timeZone            <- map["attributes.timezone"]
    fbLink              <- map["attributes.fb_link"]
    eTag                <- map["attributes._etag"]

    uLocation           <- map["attributes.location"]
    uLatitude           <- map["attributes.latitude"]
    uLongtitude         <- map["attributes.longtitude"]
    uEducation          <- map["attributes.education"]
    uFbId               <- map["attributes.fb_id"]
    uInstaPhotos        <- map["attributes.insta_photos"]
  }

  func getPrimaryImageUrl() -> String {
    if images.count == 0 {
      return ""
    }

    if let strPrimary = images.first?.urlImg {
      if strPrimary.containsString("http") || strPrimary.containsString("https") {
        if strPrimary.containsString("facebook") {
          return strPrimary + "?type=large"
        }
        return strPrimary
      } else {
        return apiHost + strPrimary
      }
    } else {
      return ""
    }
  }

  func getListEducation() -> [String] {
    var list = [String]()
    if let da = uEducation.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
      let j = JSON(data: da)
      for sch in j.array! {

        if let mapSchool = Mapper<SchoolObj>().map(sch.dictionaryObject) {
          print(mapSchool.sName)
          list.append(mapSchool.sName)
        }
      }
    }

    return list
  }

  func getWorkHistoryDidTrim() -> String {
    let work = workHistory.lowercaseString

    if work.containsString("at") {
      let atRange = (work as NSString).rangeOfString("at")

      let wo = (workHistory as NSString).substringWithRange(NSRange(location: 0, length: atRange.location))

      return wo
    }
    return workHistory
  }

  func getWorkHistory() -> String {
    var workHis = ""
    let arrWork = workHistory.componentsSeparatedByString("|")
    if arrWork.count >= 2 {
      for i in 0..<arrWork.count {
        let work = arrWork[i]
        if i == arrWork.count - 1 {
          workHis.appendContentsOf(work)
        } else {
          workHis.appendContentsOf(work + " | ")
        }
      }
    } else {
      workHis = workHistory
    }
    return workHis
  }

  class func getListSchoolObject(jsonString: String) -> [SchoolObj] {
    if jsonString.containsString("{") == false {
      return []
    }

    var list = [SchoolObj]()
    if let da = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
      let j = JSON(data: da)
      for sch in j.array! {

        if let mapSchool = Mapper<SchoolObj>().map(sch.dictionaryObject) {
          print(mapSchool.sName)
          list.append(mapSchool)
        }
      }
    }
    return list
  }

  class func convertEducationToJsonString(schoolObjects: [SchoolObj]) -> String {

    var listDic = [NSDictionary]()
    for obj in schoolObjects {
      let dSchool = ["id": obj.sSchoolId, "name": obj.sName]
      var dSchoolType = [String: AnyObject]()

      dSchoolType = ["school": dSchool,
                     "choose": obj.sChoose,
                     "type": obj.sType,
                     "id": obj.sTypeId]

      listDic.append(dSchoolType)
    }

    if let json = JSON(listDic).rawString(NSUTF8StringEncoding, options: NSJSONWritingOptions.PrettyPrinted) {
      return json
    }

    return ""
  }

  class func getEducationFromJsonString(jsonString: String) -> String {
    if jsonString.characters.count == 0 {
      return ""
    }

    if jsonString.containsString("{") {
      let list = User.getListSchoolObject(jsonString)
      for obj in list {
        if obj.sChoose == "co" {
          return obj.sName
        }
      }
      return ""
    } else {
      return jsonString
    }
  }

  func getEducation() -> String {
    if uEducation == "\"\"" {
      return ""
    }
    return User.getEducationFromJsonString(uEducation)
  }

  func getUserName() -> String {
    if firstName.characters.count > 0 {
      return firstName
    }

    return fullName
  }

  func getFirstName() -> String {
    if firstName.characters.count > 0 {
      return firstName
    }
    return fullName
  }
}

class ListUser: NSObject, Mappable {

  var listUser: [User] = []

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    listUser    <- map["data"]
  }
}

class SchoolObj: NSObject, Mappable {
  var sName = ""
  var sType = ""
  var sSchoolId = ""
  var sTypeId = ""
  var sChoose: String = "khong"

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    sName     <- map["school.name"]
    sType     <- map["type"]
    sSchoolId <- map["school.id"]
    sTypeId   <- map["id"]
    sChoose   <- map["choose"]
  }
}

class SchoolsObj: NSObject, Mappable {
  var schools = [SchoolObj]()

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    schools    <- map["school.name"]
  }
}
