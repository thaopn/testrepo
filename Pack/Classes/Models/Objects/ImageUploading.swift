//
//  ImageUploading.swift
//  Pack
//
//  Created by Le Kim Tuan on 6/5/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class ImageUploading: NSObject {
  var image: UIImage = UIImage()
  var isUploading: Bool = false
  var isUploadDone: Bool = false
}
