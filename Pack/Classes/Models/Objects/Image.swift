//
//  Image.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/18/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class Image: NSObject, Mappable {

  var urlImg: String = ""
  var userId: String = ""
  var imgId: String = ""
  var eTagImg: String = ""
  var disabled: Bool = false

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    urlImg       <- map["attributes.url"]
    userId       <- map["attributes.userid"]
    imgId        <- map["id"]
    eTagImg      <- map["attributes._etag"]
    disabled     <- map["attributes.disabled"]
  }

}

class ListImage: NSObject, Mappable {
  var data: [Image] = []

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    data       <- map["data"]
  }
}
