//
//  Personalities.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/17/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class Personalities: NSObject, Mappable {
  var namePersonalities: String = ""
  var create: String = ""
  var eTag: String = ""
  var link: Link?
  var type: String = ""
  var id: String = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    namePersonalities     <- map["attributes.text"]
    link                  <- map["attributes.links"]
    eTag                  <- map["attributes._etag"]
    type                  <- map["type"]
    id                    <- map["id"]
  }
}

class ListPersonalities: NSObject, Mappable {
  var personalities: [Personalities] = []

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    personalities   <- map["data"]
  }
}
