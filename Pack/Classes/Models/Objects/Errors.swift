//
//  Errors.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/16/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class Error: NSObject, Mappable {

  var status: String = ""
  var errCode: Int = 0
  var errMessage: String = ""
  var eTag: String = ""
  var id: String = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    errCode         <- map["attributes._error.code"]
    errMessage      <- map["attributes._error.message"]
    status          <- map["attributes._status"]
    eTag            <- map["_etag"]
    id              <- map["_id"]
  }
}
