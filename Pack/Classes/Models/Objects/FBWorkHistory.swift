//
//  FBWorkHistory.swift
//  Pack
//
//  Created by admin on 6/3/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class FBWorkHistory: NSObject, Mappable {
  var works = [FBWork]()

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    works <- map["work"]
  }
}

class FBWork: NSObject, Mappable {
  var employerName: String?
  var positionName: String?

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    employerName <- map["employer.name"]
    positionName <- map["position.name"]
  }
}
