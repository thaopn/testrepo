//
//  Link.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/17/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class Link: NSObject, Mappable {
  var urlString: String = ""
  var title: String = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    urlString       <- map["self.href"]
    title           <- map["self.title"]
  }
}
