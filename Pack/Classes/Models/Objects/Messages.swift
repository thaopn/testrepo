//
//  Messages.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/9/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class Messages: NSObject {
  var msg_id: Int = 0
  var room_id: String = ""
  var sender_id: String = ""
  var receiver_id: String = ""
  var content: String = ""
  var created: String = ""
  var updated: String = ""
  var as_read: Bool = false
  var is_deleted: Bool = false
  var partner_id: String = ""
  var listImageStr: [String] = []
}
