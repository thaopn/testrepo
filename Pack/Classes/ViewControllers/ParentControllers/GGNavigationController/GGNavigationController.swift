import UIKit

class GGNavigationController: UINavigationController {
  
  // MARK: - Variable
  var naviBar: UINavigationBar?

  // MARK: - Override method
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationBarHidden = true
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.

    naviBar = UINavigationBar(frame:CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, heightNavibar))
    naviBar?.autoresizingMask = [UIViewAutoresizing.FlexibleTopMargin, UIViewAutoresizing.FlexibleRightMargin, UIViewAutoresizing.FlexibleLeftMargin, UIViewAutoresizing.FlexibleWidth]
    naviBar?.barTintColor = colorNavibar
    naviBar?.translucent = false
    view.addSubview(naviBar!)
  }

  override func shouldAutorotate() -> Bool {
    return true
  }

  override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.Portrait
  }
}
