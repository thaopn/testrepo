import UIKit
import Alamofire
import Localytics

let heightNavibar: CGFloat = 64
let originNavibar: CGFloat = 0
let colorNavibar: UIColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)

class GGParentVC: UIViewController {

  // MARK: - Enum
  enum LeftItems {
    case Cancel
    case Back
    case Profile
    case Home
    case Close
    case None
  }

  enum RightItems {
    case Done
    case Home
    case Message
    case Flag
    case Save
    case None
  }

  enum CenterItems {
    case Title
    case None
  }

  //MARK: - Variable
  var indicator: DHActiIndicator = DHActiIndicator()
  var request: Request?
  
  var lblTitle: UILabel! = UILabel()
  var strTitle: String! = ""
  var fontTitle: UIFont! = getRobotoMediumWith(18)
  
  private var navigationVCCurrent: GGNavigationController?
  private var previousScrollViewYOffset: CGFloat! = 0.0
  private var frameViewCustom: CGRect! = nil
  private var scrollBackUp: UIScrollView! = nil

  // MARK: - Override Method
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init(leftType: LeftItems, centerType: CenterItems, rightType: RightItems) {
    self.init(nibName:nil, bundle: nil)

    navigationItem.hidesBackButton = true

    makeLeftButtons(withType: leftType)
    makeRightButtons(withType: rightType)
    makeCenterItems(withType: centerType)
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)

    UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
    UIApplication.sharedApplication().statusBarHidden = false

    navigationVCCurrent?.naviBar?.hidden = false

    reSetNavigationBarItem()

    setImageNavibar()
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    indicator.center = view.center
    view.addSubview(indicator)
    setImageNavibar()

  }

  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    Localytics.tagScreen("\(classForCoder)")
  }

  override func prefersStatusBarHidden() -> Bool {
    return false
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    if (UIDevice.currentDevice().systemVersion as NSString).floatValue >= 7.0 {
      self.automaticallyAdjustsScrollViewInsets = false
    } else {

    }
    navigationVCCurrent = self.navigationController as? GGNavigationController
    setTitleForVC("")
  }

  deinit {
    // perform the deinitialization
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  // MARK: - Configure Method


  // MARK: - Public Method


  func setTitleForVC(title: String, font: UIFont = getRobotoMediumWith(18)) -> Void {
    strTitle = title
    fontTitle = font

    if (lblTitle != nil) {
      lblTitle.text = strTitle
      lblTitle.font = fontTitle
    }
  }

  func setRightButtonEnable(enable: Bool) {
    if enable {
      makeRightButtons(withType: GGParentVC.RightItems.Done)
    } else {
      makeRightButtons(withType: GGParentVC.RightItems.None)
    }
  }

  func changeLeftItem(withType leftType: LeftItems) -> Void {
    makeLeftButtons(withType: leftType)
  }

  func changeRightItem(withType rightType: RightItems) -> Void {
    makeRightButtons(withType: rightType)

  }

  func setImageNavibar(image: UIImage? = UIImage(named: "image_navibar")!.resizableImageWithCapInsets(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), resizingMode: .Stretch)) -> Void {
    self.navigationVCCurrent?.naviBar?.setBackgroundImage(image, forBarMetrics: UIBarMetrics.Default)
    self.navigationVCCurrent?.naviBar?.shadowImage = UIImage()
  }

  func setHidenNaviBar(isHiden: Bool) -> Void {

    if (isHiden == true) {
      self.navigationVCCurrent?.naviBar?.hidden = true

    } else {
      self.navigationVCCurrent?.naviBar?.hidden = false
    }
  }

  func setHidenStatusBar(isHiden: Bool) -> Void {

    if (isHiden == true) {
      UIApplication.sharedApplication().statusBarHidden = true

    } else {
      UIApplication.sharedApplication().statusBarHidden = false
    }
  }

  func setNaviItemLeftHiden() -> Void {
    self.navigationItem.leftBarButtonItem?.customView?.alpha = 0.0
  }

  func setNaviItemRightHiden() -> Void {
    self.navigationItem.rightBarButtonItem?.customView?.alpha = 0.0
  }

  func setNaviItemTitleHiden() -> Void {
    self.navigationItem.titleView?.alpha = 0.0
  }

  func resetNavibar() -> Void {

    if navigationVCCurrent?.naviBar?.frame.origin.y != 0 {
      if scrollBackUp != nil {
        scrollBackUp.contentOffset = CGPointMake( self.scrollBackUp.contentOffset.x, self.scrollBackUp.contentOffset.y - 64)
      } else {

      }
    } else {
      
    }

    navigationItem.leftBarButtonItem?.customView?.alpha = 1.0
    navigationItem.rightBarButtonItem?.customView?.alpha = 1.0
    navigationItem.titleView?.alpha = 1.0
  }

  func scrollAutoAnimationWhenEndDrag(scrollView: UIScrollView!, andPlusHeightView viewCustom: UIView!, andPlusHeight heightPlus: CGFloat!) -> Void {
    if viewCustom == nil {
      return
    }
    if self.navigationVCCurrent?.naviBar?.frame.origin.y == -44 ||  self.navigationVCCurrent?.naviBar?.frame.origin.y == 0 {

      if viewCustom.frame.origin.y < (64 - viewCustom.frame.size.height) || viewCustom.frame.origin.y == 64 {
        
      } else {
        if viewCustom.frame.origin.y > (64 - viewCustom.frame.size.height/2) {
          let plus = 64 - viewCustom.frame.origin.y
          scrollView.setContentOffset(CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y-plus), animated: true)
        } else {
          let plus = viewCustom.frame.origin.y - (64 - viewCustom.frame.size.height)
          scrollView.setContentOffset(CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y+plus), animated: true)
        }
      }
    } else {
      if self.navigationVCCurrent?.naviBar?.frame.origin.y > -32 {
        let origin: CGFloat! = self.navigationVCCurrent?.naviBar?.frame.origin.y
        var plus = (origin * -1)
        if viewCustom != nil {
          plus += viewCustom.frame.size.height
        } else {
          plus += viewCustom.frame.size.height
        }
        scrollView.setContentOffset(CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y-plus), animated: true)
      } else {
        let origin: CGFloat! = self.navigationVCCurrent?.naviBar?.frame.origin.y
        var plus = 44 - (origin * -1)
        if viewCustom != nil {
          plus += viewCustom.frame.size.height
        } else {
          plus += viewCustom.frame.size.height
        }
        scrollView.setContentOffset(CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y+plus), animated: true)
      }
    }

  }

  func scrollAnimationNavigationbarWithRect(scrollView: UIScrollView!, andPlusHeightView viewCustom: UIView!, andPlusHeight heightPlus: CGFloat!) -> Void {

    scrollBackUp = scrollView

    if viewCustom == nil {
      if frameViewCustom == nil {
        frameViewCustom = CGRectMake(0, heightNavibar, UIScreen.mainScreen().bounds.size.width, 0)
      }
    }


    if (viewCustom != nil) {
      if (scrollView.contentOffset.y + heightNavibar + heightPlus + viewCustom.frame.height > 0) {

        if frameViewCustom == nil {
          frameViewCustom = CGRectMake(0, heightNavibar, UIScreen.mainScreen().bounds.size.width, viewCustom.frame.size.height)
        }

        let newOffset: CGFloat! = (scrollView.contentOffset.y + heightPlus + heightNavibar + viewCustom.frame.height)
        let scrollDiff: CGFloat! = newOffset - previousScrollViewYOffset
        let maxVY: CGFloat! = heightNavibar, minVY: CGFloat! = -viewCustom.frame.height
        let realVY = (frameViewCustom.origin.y-scrollDiff)

        if (realVY < minVY) {
          viewCustom?.frame.origin.y = minVY
        } else if (realVY > maxVY) {
          viewCustom?.frame.origin.y = maxVY
        } else {
          viewCustom?.frame.origin.y = realVY
        }

        let maxNY: CGFloat! = originNavibar, minNY: CGFloat! = -44
        let realNY = viewCustom.frame.origin.y - (heightNavibar - viewCustom.frame.height)

        if (realNY < minNY) {
          navigationVCCurrent?.naviBar?.frame.origin.y = minNY
        } else if (realNY > maxNY) {
          navigationVCCurrent?.naviBar?.frame.origin.y = maxNY
        } else {
          navigationVCCurrent?.naviBar?.frame.origin.y = realNY
        }

        frameViewCustom = viewCustom.frame
        previousScrollViewYOffset = newOffset

      } else {
        navigationVCCurrent?.naviBar?.frame.origin.y = originNavibar
      }

    } else {
      navigationVCCurrent?.naviBar?.frame.origin.y = originNavibar
    }


    let alpha: CGFloat! = navigationVCCurrent?.naviBar?.frame.origin.y

    if alpha == nil {
      navigationItem.titleView?.alpha = 1.0
      navigationItem.leftBarButtonItem?.customView?.alpha = 1.0
      navigationItem.rightBarButtonItem?.customView?.alpha = 1.0
    } else {
      navigationItem.titleView?.alpha = 1 - (alpha * -1)/44
      navigationItem.leftBarButtonItem?.customView?.alpha = 1 - (alpha * -1)/44
      navigationItem.rightBarButtonItem?.customView?.alpha = 1 - (alpha * -1)/44
    }
  }

  // MARK: - Configue Navibar
  private func makeLeftButtons(withType leftType: LeftItems) -> Void {
    if leftType == LeftItems.Back {
      let image = UIImage(named: "ic_arrow_back")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
      let itemLeft: UIBarButtonItem = UIBarButtonItem(image: image, landscapeImagePhone:image, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToBack(_:)))
      navigationItem.setLeftBarButtonItems([itemLeft], animated: true)
    } else if leftType == LeftItems.Cancel {
      let itemLeft = UIBarButtonItem(title: "  Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToCancel(_:)))
      let font = getRobotoMediumWith(14)
      let dic = [NSFontAttributeName: font, NSForegroundColorAttributeName : UIColor.whiteColor()]
      itemLeft.setTitleTextAttributes(dic, forState: UIControlState.Normal)
      navigationItem.setLeftBarButtonItems([itemLeft], animated: true)
    } else if leftType == LeftItems.Close {
      let image = UIImage(named: "ic_button_item_close")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
      let itemLeft: UIBarButtonItem = UIBarButtonItem(image: image, landscapeImagePhone:image, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToClose(_:)))
      navigationItem.setLeftBarButtonItems([itemLeft], animated: true)
    } else if leftType == LeftItems.Profile {
      let image = UIImage(named: "ic_button_item_user")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
      let itemLeft: UIBarButtonItem = UIBarButtonItem(image: image, landscapeImagePhone:image, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToShowUser(_:)))
      navigationItem.setLeftBarButtonItems([itemLeft], animated: true)
    } else if leftType == LeftItems.Home {
      let image = UIImage(named: "ic_button_item_home")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
      let itemLeft: UIBarButtonItem = UIBarButtonItem(image: image, landscapeImagePhone:image, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToHome(_:)))
      navigationItem.setLeftBarButtonItems([itemLeft], animated: true)
    } else {
      let itemLeft = UIBarButtonItem(title: "       ", style: UIBarButtonItemStyle.Plain, target: self, action: nil)
      let font = getRobotoMediumWith(14)
      let dic = [NSFontAttributeName: font, NSForegroundColorAttributeName : UIColor.whiteColor()]
      itemLeft.setTitleTextAttributes(dic, forState: UIControlState.Normal)
      navigationItem.setLeftBarButtonItems([itemLeft], animated: true)
    }
  }

  private func makeRightButtons(withType rightType: RightItems) -> Void {
    if rightType == RightItems.Home {
      let image = UIImage(named: "ic_button_item_home")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
      let itemRight: UIBarButtonItem = UIBarButtonItem(image: image, landscapeImagePhone:image, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToHome(_:)))
      navigationItem.setRightBarButtonItems([itemRight], animated: true)
    } else if rightType == RightItems.Message {
      let image = UIImage(named: "ic_button_item_message")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
      let itemRight: UIBarButtonItem = UIBarButtonItem(image: image, landscapeImagePhone:image, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToMessage(_:)))
      navigationItem.setRightBarButtonItems([itemRight], animated: true)
    } else if rightType == RightItems.Done {
      let itemRight = UIBarButtonItem(title: "Done  ", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToDone(_:)))
      let font = getRobotoMediumWith(14)
      let dic = [NSFontAttributeName: font, NSForegroundColorAttributeName : UIColor.whiteColor()]
      itemRight.setTitleTextAttributes(dic, forState: UIControlState.Normal)

      self.navigationItem.setRightBarButtonItems([itemRight], animated: true)
    } else if rightType == RightItems.Save {
      let itemRight = UIBarButtonItem(title: "Save  ", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToSave(_:)))
      let font = getRobotoMediumWith(14)
      let dic = [NSFontAttributeName: font, NSForegroundColorAttributeName : UIColor.whiteColor()]
      itemRight.setTitleTextAttributes(dic, forState: UIControlState.Normal)
      navigationItem.setRightBarButtonItems([itemRight], animated: true)
    } else if rightType == RightItems.Flag {
      let image = UIImage(named: "ic_button_item_flag")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
      let itemRight: UIBarButtonItem = UIBarButtonItem(image: image, landscapeImagePhone:image, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(tapToFlag(_:)))
      navigationItem.setRightBarButtonItems([itemRight], animated: true)
    } else {
      let itemRight = UIBarButtonItem(title: "           ", style: UIBarButtonItemStyle.Plain, target: self, action: nil)
      let font = getRobotoMediumWith(14)
      let dic = [NSFontAttributeName: font, NSForegroundColorAttributeName : UIColor.whiteColor()]
      itemRight.setTitleTextAttributes(dic, forState: UIControlState.Normal)
      navigationItem.setRightBarButtonItems([itemRight], animated: true)
    }
  }

  private func makeCenterItems(withType centerItem: CenterItems) -> Void {
    if centerItem == CenterItems.Title {
      lblTitle = UILabel(frame: CGRectMake(0, (screenWidth / 2) - 115, 150, 25))
      lblTitle.backgroundColor = UIColor.clearColor()
      lblTitle.textAlignment = NSTextAlignment.Center
      lblTitle.textColor = UIColor.whiteColor()
      lblTitle.font = fontTitle
      lblTitle.text = strTitle
      lblTitle.userInteractionEnabled = true
      let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapToTitle(_:)))
      tapGesture.numberOfTapsRequired = 1
      lblTitle.addGestureRecognizer(tapGesture)

      navigationItem.titleView = lblTitle
    } else {
      navigationItem.titleView = nil
      navigationItem.title = ""
    }
  }


  private func reSetNavigationBarItem() -> Void {
    self.navigationVCCurrent?.naviBar?.setItems([self.navigationItem], animated: true)
  }

  // MARK: - Public Method
  func tapToCancel(sender: UIButton!) {
    self.request?.cancel()
    print("tapToCancel")
    self.view.endEditing(true)
    self.dismissViewControllerAnimated(true, completion: nil)
  }

  func tapToBack(sender: UIButton!) {
    self.request?.cancel()
    print("tapToBack")
    self.view.endEditing(true)
    self.navigationController?.popViewControllerAnimated(true)
  }

  func tapToSave(sender: UIButton!) {
    print("tapToSave")
  }

  func tapToFlag(sender: UIButton!) {
    print("tapToFlag")
  }

  func tapToClose(sender: UIButton!) {
    print("tapToClose")
    self.view.endEditing(true)
    self.dismissViewControllerAnimated(true, completion: nil)
  }

  func tapToTitle(sender: UIButton!) {
    print("tapToTitle")
  }

  func tapToDone(sender: UIButton!) {
    print("Done")
  }

  func tapToMessage(sender: UIButton!) {
    print("tapToMessage")
    if let vcs = navigationController?.viewControllers {
      for vc in vcs {
        if vc is MessagesVC {
          print("Found MessageVC")
          navigationController?.popToViewController(vc, animated: true)
          return
        }
      }
    }
    let vc = MessagesVC()
    self.navigationController?.pushViewController(vc, animated: true)
  }

  func tapToHome(sender: UIButton!) {
    print("tapToHome")

    if AppDelegate.shareInstance().isNotFoundUserMatching {
      if let vcs = navigationController?.viewControllers {
        for vc in vcs {
          if vc is LoadingVC {
            print("Found LoadingVC")
            navigationController?.popToViewController(vc, animated: true)
            return
          }
        }
      } else {
        let vc = LoadingVC()
        navigationController?.pushViewController(vc, animated: true)
      }
      AppDelegate.shareInstance().isNotFoundUserMatching = false
    } else {
      if let vcs = navigationController?.viewControllers {
        if AppDelegate.shareInstance().isLoadingListUser == true {
          for vc in vcs {
            if vc is LoadingVC {
              print("Found LoadingVC")
              navigationController?.popToViewController(vc, animated: true)
              return
            }
          }
        } else {
          for vc in vcs {
            if vc is MainScreenVC {
              print("Found HomeVC")
              navigationController?.popToViewController(vc, animated: true)
              return
            }
          }
        }
      }
      let vc = MainScreenVC()
      self.navigationController?.pushViewController(vc, animated: true)
    }

  }

  func tapToShowUser(sender: UIButton!) {
    print("tapToShowUser")
    if let vcs = navigationController?.viewControllers {
      for vc in vcs {
        if vc is OwnerProfileVC {
          print("Found OwnerProfileVC")
          navigationController?.popToViewController(vc, animated: true)
          return
        }
      }
    }
    let vc = OwnerProfileVC()
    self.navigationController?.pushViewController(vc, animated: true)
  }
}
