//
//  ProfilePhotoView.swift
//  Pack
//
//  Created by Pham Ngoc Thao on 5/7/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol ProfilePhotoViewDelegate: NSObjectProtocol {
  optional func didUploadPhotoDone()
  optional func didUploadingPhoto()
  optional func didChangePhoto()
  optional func didDeletePhoto()
  optional func didNotDeletePhoto()
}

class ProfilePhotoView: UIView {

  weak var delegate: ProfilePhotoViewDelegate?

  private let pading: CGFloat = 19
  private let space: CGFloat = 14
  private var sheetCurrentPhoto: UIAlertController!
  private var listView = [ChoosePhotoView]()
  private var listButton = [UIButton]()
  private var countImage: Int = 0
  private var arrStringImg: [String] = [String]()
  private var arrImage: [Image] = [Image]()
  private var arrImgDeleted: [Image] = [Image]()
  private var arrImgPreUpload: [ImageUploading] = [ImageUploading]()
  private var arrIdImgUploaded: [String] = []
  private var arrImgJustUpload: [UIImage] = []
  private var sheetDeletePhoto: UIAlertController?
  private var sheetReplacePhoto: UIAlertController?
  private var isReplaceMainPhoto: Bool = false
  private var primaryImage: ImageUploading = ImageUploading()
  private var isFirstLogin: Bool = false
  private var arrImageInstagram: [UIImage] = []
  private var isChangePrimaryImage: Bool = false

  //MARK: - Publics
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    configureSheetChoosePhoto()
    configureView(bounds.size)
    configNotification()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init(withView: UIView) {
    self.init(frame: withView.frame)
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    print("profilePhotoViewLayout")
    fixSize()
  }

  func getListIdImage() -> [String] {
    return self.arrIdImgUploaded
  }

  func getImages() -> [UIImage] {
    var listImage = [UIImage]()

    for i in countImage..<listView.count {
      let view = listView[i]
      if let image = view.getImage() {
        listImage.append(image)
      }
    }
    return listImage
  }

  func getArrImageDeleted() -> [Image] {
    return arrImgDeleted
  }

  func getArrImagePreUpload() -> [ImageUploading] {
    return arrImgPreUpload
  }

  func getImagePrimary() -> ImageUploading? {
    return primaryImage
  }

  func setImageInstagram(images: [UIImage]) {
    isFirstLogin = true
    arrImageInstagram = images
    for i in 0..<images.count {
      let image = images[i]
      listView[i + 1].addImageWithUIImage(image)
      listView[i + 1].indicator.stopAnimating()
      listView[i + 1].isShowImage = true
      listButton[i + 1].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)

      let api = CreateProfileManageAPI()
      if let dataImage = UIImageJPEGRepresentation(image, 1.0) {

        api.uploadPhoto(AccountFlowManager.currentUser.id, dataImage: dataImage, indexImage: i + 1, completed: { (indexImg, result, success, error) in
          if let error = error {
            Common.showAlert(error.error_msg)
          } else {
            DHIndicator.hide()
            print("Upload Photo Success")
            if let maskView = self.listView[indexImg].maskView {
              maskView.alpha = 0
            }
            if let idImg = result?.id {
              self.arrIdImgUploaded.append(idImg)
            }
          }
        })
      }
    }

    if var strAvatar = AccountFlowManager.currentUser.images.first?.urlImg {
      if strAvatar.containsString("http") || strAvatar.containsString("https") {
        strAvatar = strAvatar + "?type=large"
      } else {
        strAvatar = apiHost + strAvatar + "?type=large"
      }
      if let url = NSURL(string: strAvatar) {
        listView[0].addImage(url)
        listView[0].isShowImage = true
        listButton[0].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
      }

      for i in 0..<6 {
        self.listView[i].indicator.stopAnimating()
      }
    }
  }

  func setImage(images: [Image]) {
    arrImage = images
    arrIdImgUploaded.removeAll()

    for image in arrImage {
      arrIdImgUploaded.append(image.imgId)
    }
    for i in 0..<images.count {
      if i > 5 {

      } else {
        if i == 0 {
          if Common.isIPhone5or4() {
            listView[i].indicator = UIActivityIndicatorView(frame: CGRect(x: 80, y: 80, width: 40, height: 40))
          } else if Common.isIPhone6plus() {
            listView[i].indicator = UIActivityIndicatorView(frame: CGRect(x: 100, y: 100, width: 40, height: 40))
          } else {
            listView[i].indicator = UIActivityIndicatorView(frame: CGRect(x: 90, y: 90, width: 40, height: 40))
          }
        }
        let image = images[i]
        if image.urlImg.containsString("http") || image.urlImg.containsString("https") {
          if let url = NSURL(string: image.urlImg + "?type=large") {
            listView[i].addImage(url)
            listView[i].isShowImage = true
            listButton[i].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
          }
        } else {
          let stringImg = apiHost + image.urlImg + "?type=large"
          if let url = NSURL(string: stringImg) {
            listView[i].addImage(url)
            listView[i].isShowImage = true
            listButton[i].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
          }
        }
      }
    }
  }

  func resetListImages() {
    for i in 1..<6 {
      listView[i].imvImage.image = nil
      listView[i].isShowImage = false
      listView[i].removeMaskUploadImage()
      listView[i].imvImage.image = UIImage(named: "ic_updateprofile_plus.png")
      listButton[i].setImage(UIImage(named:"ic_updateprofile_add.png"), forState: .Normal)
    }

    if isChangePrimaryImage {
      if primaryImage.isUploading {
        listView[0].imvImage.image = primaryImage.image
        listView[0].isShowImage = true
        listButton[0].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
      } else {
        listView[0].addImageWithUIImage(primaryImage.image)
        listView[0].isShowImage = true
        listView[0].userInteractionEnabled = false
        listButton[0].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
        listButton[0].userInteractionEnabled = false
        uploadPhoto(primaryImage, index: 0)
        primaryImage.isUploading = true
      }

      for i in 0..<arrImage.count {
        let image = arrImage[i]
        if i >= 6 {
          break
        }

        var strImg = image.urlImg

        if strImg.containsString("http") || strImg.containsString("https") {
          strImg = strImg + "?type=large"
        } else {
          strImg = apiHost + strImg + "?type=large"
        }

        if let url = NSURL(string: strImg) {
          listView[i + 1].addImage(url)
          listView[i + 1].isShowImage = true
          listButton[i + 1].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
        }
      }
      if arrImgPreUpload.count != 0 {
        for i in (arrImage.count + 1)...(arrImage.count + arrImgPreUpload.count) {
          let image = arrImgPreUpload[i - arrImage.count - 1]
          if image.isUploading == false {
            listView[i].addImageWithUIImage(image.image)
            listView[i].isShowImage = true
            listView[i].userInteractionEnabled = false
            listButton[i].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
            listButton[i].userInteractionEnabled = false
            uploadPhoto(image, index: i)
            self.delegate?.didUploadingPhoto!()
          } else {
            if image.isUploadDone == true {
              listView[i].imvImage.image = image.image
              listView[i].isShowImage = true
              listButton[i].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
            } else {
              listView[i].addImageWithUIImage(image.image)
              listView[i].isShowImage = true
              listButton[i].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
            }
          }
        }
      } else {

      }
    } else {
      // Not Primary Photo
      for i in 0..<arrImage.count {
        let image = arrImage[i]
        if i >= 6 {
          break
        }

        var strImg = image.urlImg

        if strImg.containsString("http") || strImg.containsString("https") {
          strImg = strImg + "?type=large"
        } else {
          strImg = apiHost + strImg + "?type=large"
        }

        if let url = NSURL(string: strImg) {
          listView[i].addImage(url)
          listView[i].isShowImage = true
          listButton[i].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
        }
      }
      for i in arrImage.count..<(arrImage.count + arrImgPreUpload.count) {
        let image = arrImgPreUpload[i - arrImage.count]
        if image.isUploading == false {
          listView[i].addImageWithUIImage(image.image)
          listView[i].isShowImage = true
          listView[i].userInteractionEnabled = false
          listButton[i].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
          listButton[i].userInteractionEnabled = false
          uploadPhoto(image, index: i)
          self.delegate?.didUploadingPhoto!()
        } else {
          if image.isUploadDone == true {
            listView[i].imvImage.image = image.image
            listView[i].isShowImage = true
            listButton[i].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
          } else {
            listView[i].addImageWithUIImage(image.image)
            listView[i].isShowImage = true
            listButton[i].setImage(UIImage(named:"ic_updateprofile_close.png"), forState: .Normal)
          }
        }
      }
    }

    if arrImage.count <= 6 {
      for i in (arrImage.count + arrImgPreUpload.count)..<6 {
        listView[i].indicator.stopAnimating()
      }
    } else {

    }
  }

  func tapToDeletePhoto(sender: AnyObject) {
    if sender.tag - 1 == 0 {
      isReplaceMainPhoto = true
      if isChangePrimaryImage {
        if arrImage.count + arrImgPreUpload.count + 1 >= 6 {
          configActionSheetReplacePhoto(true, isFullImageSmall: true)
        } else {
          configActionSheetReplacePhoto(true, isFullImageSmall: false)
        }
      } else {
        if arrImage.count + arrImgPreUpload.count >= 6 {
          configActionSheetReplacePhoto(true, isFullImageSmall: true)
        } else {
          configActionSheetReplacePhoto(true, isFullImageSmall: false)
        }
      }
      UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(sheetReplacePhoto!, animated: true, completion: nil)
    } else if listView[sender.tag - 1].isShowImage == true {
      configActionSheetDeletePhoto(sender.tag - 1)
      UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(sheetDeletePhoto!, animated: true, completion: nil)

    } else {
      isReplaceMainPhoto = false
      if arrImage.count + arrImgPreUpload.count >= 5 {
        configActionSheetReplacePhoto(false, isFullImageSmall: true)
      } else {
        configActionSheetReplacePhoto(false, isFullImageSmall: false)
      }
      UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(sheetReplacePhoto!, animated: true, completion: nil)
    }
  }

  func updateListImage() {
    arrImage = AccountFlowManager.currentUser.images
  }

  // MARK: - Private
  private func configNotification () {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfilePhotoView.updateListImage), name: notificationGetInfo, object: nil)
  }

  private func configureView(size: CGSize) {
    let smallWidth = (size.width - ((pading + space) * 2)) / 3

    let smallSize = CGSize(width: smallWidth, height: smallWidth)
    let largeSize = CGSize(width: smallWidth*2 + space, height: smallWidth*2 + space)
    var lastFrame = CGRect.zero

    if listView.count < 6 {
      listView = [ChoosePhotoView]()
      listButton = [UIButton]()
      for i in 1...6 {
        listView.append(ChoosePhotoView(frame: CGRect.zero))
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        btn.addTarget(self, action: #selector(ProfilePhotoView.tapToDeletePhoto(_:)), forControlEvents: .TouchUpInside)
        btn.setImage(UIImage(named: "ic_updateprofile_add.png"), forState: .Normal)
        btn.tag = i
        listButton.append(btn)
      }
    }

    lastFrame = CGRect(x: pading, y: pading, width: largeSize.width, height: largeSize.height)
    listView[0].frame = lastFrame
    listView[0].index = 0
    listView[0].isShowImage = false
    listButton[0].center = CGPoint(x: lastFrame.maxX - 5, y: lastFrame.minY + 5)

    lastFrame = CGRect(x: lastFrame.maxX + space, y: pading, width: smallSize.width, height: smallSize.height)
    listView[1].frame = lastFrame
    listView[1].index = 1
    listView[1].isShowImage = false
    listButton[1].center = CGPoint(x: lastFrame.maxX - 5, y: lastFrame.minY + 5)

    lastFrame = CGRect(x: lastFrame.minX, y: lastFrame.maxY + space, width: smallSize.width, height: smallSize.height)
    listView[2].frame = lastFrame
    listView[2].index = 2
    listView[2].isShowImage = false
    listButton[2].center = CGPoint(x: lastFrame.maxX - 5, y: lastFrame.minY + 5)

    lastFrame = CGRect(x: lastFrame.minX, y: lastFrame.maxY + space, width: smallSize.width, height: smallSize.height)
    listView[3].frame = lastFrame
    listView[3].index = 3
    listView[3].isShowImage = false
    listButton[3].center = CGPoint(x: lastFrame.maxX - 5, y: lastFrame.minY + 5)

    lastFrame = CGRect(x: lastFrame.minX - space - smallSize.width, y: lastFrame.minY, width: smallSize.width, height: smallSize.height)
    listView[4].frame = lastFrame
    listView[4].index = 4
    listView[4].isShowImage = false
    listButton[4].center = CGPoint(x: lastFrame.maxX - 5, y: lastFrame.minY + 5)

    lastFrame = CGRect(x: lastFrame.minX - space - smallSize.width, y: lastFrame.minY, width: smallSize.width, height: smallSize.height)
    listView[5].frame = lastFrame
    listView[5].index = 5
    listView[5].isShowImage = false
    listButton[5].center = CGPoint(x: lastFrame.maxX - 5, y: lastFrame.minY + 5)

    for vi in listView {
      if let _ = vi.superview {
        vi.fixSize()
      } else {
        vi.delegate = self
        addSubview(vi)
      }
    }

    for btn in listButton {
      if let _ = btn.superview {

      } else {
        addSubview(btn)
      }
    }
    resetListImages()
  }

  private func fixSize() {
    if let superView = superview {
      frame = superView.bounds
      configureView(superView.bounds.size)
    }
  }

  private func configActionSheetReplacePhoto(isMainPhoto: Bool, isFullImageSmall: Bool) {
    sheetReplacePhoto = UIAlertController()

    let replaceAction: UIAlertAction = UIAlertAction(title: "Replace Main Photo", style: .Default) { (action) in
      self.isReplaceMainPhoto = true
      UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(self.sheetCurrentPhoto, animated: true, completion: nil)
    }

    let addNewAction: UIAlertAction = UIAlertAction(title: "Add A New Photo", style: .Default) { (action) in
      self.isReplaceMainPhoto = false
      UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(self.sheetCurrentPhoto, animated: true, completion: nil)
    }

    let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
      // Just dismiss the action sheet
    }

    if isMainPhoto {
      if isFullImageSmall {
        sheetReplacePhoto?.addAction(replaceAction)
        sheetReplacePhoto?.addAction(cancelAction)
      } else {
        sheetReplacePhoto?.addAction(replaceAction)
        sheetReplacePhoto?.addAction(addNewAction)
        sheetReplacePhoto?.addAction(cancelAction)
      }
    } else {
      sheetReplacePhoto?.addAction(addNewAction)
      sheetReplacePhoto?.addAction(cancelAction)
    }

  }

  private func resetPhotoView() {
    for i in 0..<6 {
      listView[i] = ChoosePhotoView()
    }
  }

  private func configActionSheetDeletePhoto(index: Int) {
    sheetDeletePhoto = UIAlertController()

    let deleteAction: UIAlertAction = UIAlertAction(title: "Delete Photo", style: .Default) { (action) in
      for image in self.arrImgPreUpload {
        image.isUploading = true
      }
      if self.isChangePrimaryImage {
        if index < self.arrImage.count + 1 {
          self.arrImage.removeAtIndex(index - 1)
          self.arrIdImgUploaded.removeAtIndex(index)
        } else {
          self.arrIdImgUploaded.removeAtIndex(index)
          self.arrImgPreUpload.removeAtIndex(index - self.arrImage.count - 1)
        }
      } else {
        if index < self.arrImage.count {
          let image = self.arrImage[index]
          self.arrImgDeleted.append(image)
          self.arrImage.removeAtIndex(index)
          self.arrIdImgUploaded.removeAtIndex(index)
        } else {
          self.arrIdImgUploaded.removeAtIndex(index)
          self.arrImgPreUpload.removeAtIndex(index - self.arrImage.count)
        }
      }
      self.resetListImages()
      self.delegate?.didChangePhoto!()
      self.delegate?.didDeletePhoto!()
    }

    let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
      //Just dismiss the action sheet
    }

    sheetDeletePhoto?.addAction(deleteAction)
    sheetDeletePhoto?.addAction(cancelAction)

  }

  private func configureSheetChoosePhoto() {
    sheetCurrentPhoto = UIAlertController(title: "SELECT PHOTO", message:nil, preferredStyle: UIAlertControllerStyle.ActionSheet)

    let libButton: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .Default) {[weak self] action -> Void in
      if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
        let imag = UIImagePickerController()
        imag.navigationBar.barTintColor = UIColor(red: 81/255.0, green: 156/255.0, blue: 255/255.0, alpha: 1.0)
        imag.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        imag.navigationBar.tintColor = UIColor.whiteColor()
        imag.navigationBar.translucent = false
        imag.delegate = self
        imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imag.allowsEditing = true

        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(imag, animated: true, completion: nil)
      } else {
        // None
      }
    }

    let takeButton: UIAlertAction = UIAlertAction(title: "Take A Photo", style: .Default) {[weak self] action -> Void in
      if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
        let imag = UIImagePickerController()
        imag.navigationBar.barTintColor = UIColor(red: 81/255.0, green: 156/255.0, blue: 255/255.0, alpha: 1.0)
        imag.navigationBar.tintColor = UIColor.whiteColor()
        imag.navigationBar.translucent = false
        imag.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        imag.delegate = self
        imag.sourceType = UIImagePickerControllerSourceType.Camera
        imag.allowsEditing = true
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(imag, animated: true, completion: nil)
      } else {
        // None
      }
    }

    let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
      // Just dismiss the action sheet
    }

    sheetCurrentPhoto?.addAction(libButton)
    sheetCurrentPhoto?.addAction(takeButton)
    sheetCurrentPhoto?.addAction(cancelAction)
  }

  private func uploadPhoto(image: ImageUploading, index: Int) {
    let api = CreateProfileManageAPI()
    if let dataImage = UIImageJPEGRepresentation(image.image, 1.0) {
      api.uploadPhoto(AccountFlowManager.currentUser.id, dataImage: dataImage, indexImage: index, completed: { (indexImg, result, success, error) in
        if let error = error {
          Common.showAlert(error.error_msg)
        } else {
          DHIndicator.hide()
          print("Upload Photo Success")
          image.isUploadDone = true
          self.listView[indexImg].removeMaskUploadImage()
          self.listView[indexImg].userInteractionEnabled = true
          self.listButton[indexImg].userInteractionEnabled = true
          self.delegate?.didUploadPhotoDone!()
          if let idImg = result?.id {
            if self.isFirstLogin {
              self.arrIdImgUploaded.append(idImg)
              if self.arrIdImgUploaded.count == self.arrImageInstagram.count {
                self.isFirstLogin = false
              }
            } else {
              if self.isReplaceMainPhoto {
                if self.arrIdImgUploaded.count != 0 {
                  self.arrIdImgUploaded.removeFirst()
                } else {

                }
                self.arrIdImgUploaded.insert(idImg, atIndex: 0)
              } else {
                self.arrIdImgUploaded.append(idImg)
              }
            }
          }
        }
      })
    }
  }
}

// MARK: - ChoosePhotoViewDelegate
extension ProfilePhotoView: ChoosePhotoViewDelegate {
  func choosePhotoTapChoose(index: Int, photoView: ChoosePhotoView) {
    if index == 0 {
      if isChangePrimaryImage {
        if arrImage.count + arrImgPreUpload.count + 1 >= 6 {
          configActionSheetReplacePhoto(true, isFullImageSmall: true)
        } else {
          configActionSheetReplacePhoto(true, isFullImageSmall: false)
        }
      } else {
        if arrImage.count + arrImgPreUpload.count >= 6 {
          configActionSheetReplacePhoto(true, isFullImageSmall: true)
        } else {
          configActionSheetReplacePhoto(true, isFullImageSmall: false)
        }
      }
    } else {
      isReplaceMainPhoto = false
      if arrIdImgUploaded.count >= 5 {
        configActionSheetReplacePhoto(false, isFullImageSmall: true)
      } else {
        configActionSheetReplacePhoto(false, isFullImageSmall: false)
      }
    }

    if photoView.isShowImage == false {
      UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(sheetReplacePhoto!, animated: true, completion: nil)
    } else {
      if index == 0 {
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(sheetReplacePhoto!, animated: true, completion: nil)
      } else {
        configActionSheetDeletePhoto(index)
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(sheetDeletePhoto!, animated: true, completion: nil)
      }
    }
  }
}

extension ProfilePhotoView: UINavigationControllerDelegate {
  
}

// MARK: - UIImagePickerControllerDelegate
extension ProfilePhotoView: UIImagePickerControllerDelegate {
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {

    let image = info[UIImagePickerControllerEditedImage] as! UIImage
    delegate?.didChangePhoto!()
    delegate?.didNotDeletePhoto!()

    for image in arrImgPreUpload {
      image.isUploading = true
    }
    let img = ImageUploading()
    img.image = image
    img.isUploading = false
    img.isUploadDone = false

    if isReplaceMainPhoto {
      if !isChangePrimaryImage {
        if arrImage.count != 0 {
          arrImage.removeFirst()
        } else {

        }
      }
      primaryImage = img
      isChangePrimaryImage = true
    } else {
      arrImgPreUpload.append(img)
    }
    resetListImages()
    picker.dismissViewControllerAnimated(true, completion: nil)
  }
}
