//
//  ChoosePhotoView.swift
//  Pack
//
//  Created by Pham Ngoc Thao on 5/7/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import Async

protocol ChoosePhotoViewDelegate: NSObjectProtocol {
  func choosePhotoTapChoose(index: Int, photoView: ChoosePhotoView)
}

class ChoosePhotoView: UIView {
  var imvImage: UIImageView!
  var indicator: UIActivityIndicatorView!
  var btnClose: UIButton!
  var index: Int = 0
  var isShowImage: Bool = false
  var viewMask: UIView!

  weak var delegate: ChoosePhotoViewDelegate?

  override init(frame: CGRect) {
    super.init(frame: frame)
    imvImage = UIImageView(frame: bounds)
    imvImage.contentMode = UIViewContentMode.ScaleAspectFill
    imvImage.userInteractionEnabled = true
    imvImage.layer.cornerRadius = 3
    imvImage.layer.masksToBounds = true
    imvImage.image = UIImage(named: "ic_updateprofile_plus.png")
    addSubview(imvImage)

    viewMask = UIView(frame: bounds)
    viewMask?.backgroundColor = UIColor(red: 81/255, green: 156/255, blue: 255/255, alpha: 0.3)
    imvImage.addSubview(viewMask)

    if Common.isIPhone5or4() {
      indicator = UIActivityIndicatorView(frame: CGRect(x: 20, y: 20, width: 40, height: 40))
    } else if Common.isIPhone6plus() {
      indicator = UIActivityIndicatorView(frame: CGRect(x: 40, y: 40, width: 40, height: 40))
    } else {
      indicator = UIActivityIndicatorView(frame: CGRect(x: 30, y: 30, width: 40, height: 40))
    }
    addSubview(indicator)
    indicator.startAnimating()

    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapToChoosePhoto))
    imvImage.addGestureRecognizer(tapGesture)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  // MARK: - Public
  func addImage(urlImage: NSURL) {
    if imvImage.frame == CGRect.zero {
      imvImage = UIImageView(frame: bounds)
      imvImage.contentMode = UIViewContentMode.ScaleAspectFill
      imvImage.userInteractionEnabled = true
      imvImage.layer.cornerRadius = 3
      imvImage.layer.masksToBounds = true
      imvImage.image = UIImage(named: "ic_updateprofile_plus.png")
      addSubview(imvImage)

      let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapToChoosePhoto))
      imvImage.addGestureRecognizer(tapGesture)
    }
    addSubview(indicator)
    indicator.startAnimating()
    imvImage.kf_setImageWithURL(urlImage, placeholderImage: nil, optionsInfo: nil, progressBlock: nil) { (image, error, cacheType, imageURL) in
      self.indicator.stopAnimating()
      if let e = error {
        print("imvImage.kf_setImageWithURL: \(e.localizedDescription)")
      } else {
        self.imvImage.image = image
      }
    }
  }

  func addImageWithUIImage(image: UIImage) {
    if imvImage.frame == CGRect.zero {
      imvImage = UIImageView(frame: bounds)
      imvImage.contentMode = UIViewContentMode.ScaleAspectFill
      imvImage.userInteractionEnabled = true
      imvImage.layer.cornerRadius = 3
      imvImage.layer.masksToBounds = true
      imvImage.image = UIImage(named: "ic_updateprofile_plus.png")
      addSubview(imvImage)

      let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapToChoosePhoto))
      imvImage.addGestureRecognizer(tapGesture)
    }
    viewMask = UIView(frame: bounds)
    viewMask?.backgroundColor = UIColor(red: 81/255, green: 156/255, blue: 255/255, alpha: 0.5)
    addSubview(viewMask)
    imvImage.image = image
  }

  func removeMaskUploadImage() {
    viewMask.alpha = 0
  }

  func fixSize() {
    imvImage.frame = bounds
    btnClose?.center = CGPoint(x: bounds.maxX - 5, y: bounds.minY + 5)
  }

  func tapToChoosePhoto() {
    delegate?.choosePhotoTapChoose(index, photoView: self)
  }

  func setImage(image: UIImage?) {
    if let _ = image {
      imvImage.image = image
    } else {
      imvImage.image = UIImage(named: "ic_updateprofile_plus.png")
    }
  }

  func getImage() -> UIImage? {
    if imvImage.image == UIImage(named: "ic_updateprofile_plus.png") {
      return nil
    }
    return imvImage.image
  }

  // MARK: - Private
  private func deleteImage() {
    imvImage.image = UIImage(named: "ic_updateprofile_plus.png")
  }
}
