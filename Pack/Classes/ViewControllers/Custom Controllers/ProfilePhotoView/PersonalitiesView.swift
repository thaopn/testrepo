//
//  PersonalitiesView.swift
//  Pack
//
//  Created by Pham Ngoc Thao on 5/8/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

@objc protocol PersonalitiesViewDelegate: NSObjectProtocol {
  optional func didChangePersonal()
}

class PersonalitiesView: UIView {

  weak var delegate: PersonalitiesViewDelegate?
  weak var heightSuperView: NSLayoutConstraint!
  var listPersonalities: [Personalities] = []

  private let pading: CGFloat = 19
  private let space: CGFloat = 10
  private var isCreateButton: Bool = true
  private var changePersonal: Int = 0
  private var arrPersonal: [Personalities] = []
  private var personalityButtons = [UIButton]()

  // MARK: - Publics
  override init(frame: CGRect) {
    super.init(frame: frame)
    configureView(bounds.size)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init(withView: UIView) {
    self.init(frame: withView.frame)
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    print("PersonalitiesViewLayout")
    fixSize()
  }

  func personalitiesTapped(sender: AnyObject) {
    if changePersonal >= arrPersonal.count {
      self.delegate?.didChangePersonal!()
    } else {
      changePersonal = changePersonal + 1
    }
    if let btn = sender as? UIButton {
      btn.selected = !btn.selected
    }
  }

  func setStatusPersonality(personality: Personalities) {
    for btn in personalityButtons {
      if btn.currentTitle == personality.namePersonalities {
        personalitiesTapped(btn)
      }
    }
  }

  func setStatusPersonalities(personalities: [Personalities]) {
    arrPersonal = personalities
    for personality in personalities {
      setStatusPersonality(personality)
    }
  }

  func setDataForPersonal() {
    configureView(bounds.size)
  }

  func getPersonalities(listPersonal: [Personalities]) -> [Personalities] {
    var personalities = [Personalities]()

    if listPersonal.count > 0 {
      for i in 0...listPersonal.count - 1 {
        let btn = personalityButtons[i]
        if btn.selected {
          personalities.append(listPersonal[i])
        }
      }
    }

    return personalities
  }

  // MARK: - Privates
  private func makeupButton(button: UIButton) {
    button.titleLabel?.font = getRobotoMediumWith(12)
    button.setTitleColor(UIColor(red: 81/255, green: 156/255, blue: 1, alpha: 1), forState: .Normal)
    button.setTitleColor(UIColor.whiteColor(), forState: .Selected)
    let widthButton = getWidthText(button.titleLabel!.text!, font: button.titleLabel!.font)
    button.frame = CGRect(x: 0, y: 0, width: widthButton + 30, height: 32)
    button.layer.borderWidth = 1
    button.layer.borderColor = UIColor(red: 81/255, green: 156/255, blue: 1, alpha: 1).CGColor
    button.setBackgroundImage(imageWithColor(UIColor(red: 81/255, green: 156/255, blue: 1, alpha: 1)), forState: .Selected)
    button.setBackgroundImage(imageWithColor(UIColor.whiteColor()), forState: .Normal)
    button.layer.cornerRadius = 16
    button.layer.masksToBounds = true
    button.addTarget(self, action: #selector(personalitiesTapped(_:)), forControlEvents: .TouchUpInside)
  }

  private func getWidthText(str: String, font: UIFont) -> CGFloat {
    let lbl = UILabel()
    lbl.font = font
    lbl.text = str
    return lbl.sizeThatFits(CGSize(width: 10000, height: 40)).width
  }

  private func imageWithColor(color: UIColor) -> UIImage {
    let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()
    CGContextSetFillColorWithColor(context, color.CGColor)
    CGContextFillRect(context, rect)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return image
  }

  private func createButtons() {
    personalityButtons.removeAll()
    for personal in listPersonalities {
      let btn = UIButton()
      let str = personal.namePersonalities
      btn.setTitle(str, forState: .Normal)
      makeupButton(btn)
      personalityButtons.append(btn)
    }
  }

  private func fixSize() {
    if let superView = superview {
      frame = superView.bounds
    }
    var currentY: CGFloat = pading
    var lastPoint = CGPoint.zero
    var lastFrame = CGRect.zero

    for btn in personalityButtons {
      if let _ = btn.superview {

      } else {
        addSubview(btn)
      }
      if lastFrame == CGRect.zero {
        lastPoint = CGPoint(x: pading, y: currentY)
        btn.frame.origin = lastPoint
        lastFrame = btn.frame
      } else {
        if lastFrame.maxX + space + CGRectGetWidth(btn.frame) > bounds.width - pading {
          currentY = space + lastFrame.maxY
          lastPoint = CGPoint(x: pading, y: currentY)
        } else {
          lastPoint = CGPoint(x: lastFrame.maxX + space, y: currentY)
        }
        btn.frame.origin = lastPoint
        lastFrame = btn.frame
      }
    }

    frame.size = CGSize(width: frame.width, height: lastFrame.maxY + pading)
    heightSuperView?.constant = frame.height
  }

  private func configureView(size: CGSize) {
    createButtons()
    fixSize()
  }
}
