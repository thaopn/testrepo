//
//  AccountFlowManager.swift
//  Pack
//
//  Created by Pham Ngoc Thao on 5/3/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper
import Localytics
import Appsee
import Async

class AccountFlowManager: NSObject {
  static let shareInstance = AccountFlowManager()
  static var currentUser: User! = User()
  override private init() {}

  var page = 0
  var listPersonal = [Personalities]()

  internal func start() {
    let defaults = NSUserDefaults.standardUserDefaults()

    if (defaults.stringForKey(userAccesstokenKeyConstant) != nil && defaults.stringForKey(userAccesstokenKeyConstant) != "") {
      DHIndicator.show()
      getMeInfo({ (error) in
        DHIndicator.hide()
        if let e = error {
          print(e.errMessage)
          self.clearData()
          self.goLogin()
        } else {
          self.goLoadingPage()
          self.page = 1
          self.listPersonal = [Personalities]()
          self.getPersionalities()
        }
      })
    } else {
      clearData()
      goLogin()
    }
  }

  internal func loginChatServer() {
    AppDelegate.shareInstance().authenticateLayerWithUserID(AccountFlowManager.currentUser.id) { (isSuccess, error) in
      if isSuccess {

      } else {
        if let error = error {
          print(error.localizedDescription)
        }
      }
    }
  }

  internal func goLogin() {
    if let window = AppDelegate.shareInstance().window {

      UIView.transitionWithView(window, duration:0.3, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
        let oldState: Bool = UIView.areAnimationsEnabled()
        UIView.setAnimationsEnabled(false)
        let viewController = LoginVC()

        AppDelegate.shareInstance().window?.rootViewController = viewController

        UIView.setAnimationsEnabled(oldState)
        }, completion: nil)
    }
  }

  typealias GetMeInfoCompleted = (error: Error?) -> ()
  internal func getMeInfo(completed: GetMeInfoCompleted) {

    guard let userId = NSUserDefaults.standardUserDefaults().stringForKey(userIDKeyConstant) else {
      goLogin()
      return
    }

    GGWebservice.callWebServiceWithRequest(.GET, urlString: ApiGetUserInfo(userId), param: nil, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in

      print(value)
      if let value = value {
        if statusCode == 200 || statusCode == 201 {
          let user: User = Mapper<User>().map(value)!
          AccountFlowManager.currentUser = user
          let defaults = NSUserDefaults.standardUserDefaults()
          AccountFlowManager.currentUser.authToken = (defaults.stringForKey(userAccesstokenKeyConstant))!
          AccountFlowManager.currentUser.isLoginWithFacebook = defaults.boolForKey(userIsLoginWithFacebook)
          AccountFlowManager.currentUser.isLoginWithInstagram = defaults.boolForKey(userIsLoginWithInstagram)
          print(user.uLatitude)
          print(user.uLongtitude)
          print(user.dob)
          Appsee.setUserID(user.id)
          if user.uLatitude == nil || user.uLongtitude == nil || user.dob == "" {
            self.goWelcomeVC()
          } else {
            completed(error: nil)
          }
        } else {
          let result = Mapper<Error>().map(value)
          completed(error: result)
        }
      } else {
        Common.showAlert(error?.error_msg)
        self.clearData()
        self.goLogin()
      }
    }
  }

  internal func goWelcomeVC() {
    UIView.transitionWithView(AppDelegate.shareInstance().window!, duration:0.3, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
      let oldState: Bool = UIView.areAnimationsEnabled()
      UIView.setAnimationsEnabled(false)

      let viewController = WellcomeVC()

      AppDelegate.shareInstance().window?.rootViewController = viewController
      UIView.setAnimationsEnabled(oldState)
      }, completion: { finished in

    })
  }

  internal func goLoadingPage() {
    UIView.transitionWithView(AppDelegate.shareInstance().window!, duration:0.3, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
      let oldState: Bool = UIView.areAnimationsEnabled()
      UIView.setAnimationsEnabled(false)

      let viewController = MainScreenVC()

      AppDelegate.shareInstance().window?.rootViewController = GGNavigationController(rootViewController: viewController)
      UIView.setAnimationsEnabled(oldState)
      }, completion: { finished in

    })
  }

  typealias SignUpCompleted = (user: User?, error: GGWSError?) -> Void

  internal func loginWithFB(fbUserInfo: FBUserProfile?) {
    if let profile = fbUserInfo {
      guard let token = profile.fbToken else {return}
      DHIndicator.show()

      let header = [Authorization: "Token " + token]
      let param: [String: AnyObject]!
      if let deviceToken = NSUserDefaults.standardUserDefaults().stringForKey(userKeyDeviceToken) {
        param = [DeviceToken: deviceToken]
      } else {
        param = nil
      }
      GGWebservice.callWebServiceWithHeader(.POST, urlString: apiLoginWithFacebook, header: header, param: param) { (value, statusCode, error) in
        DHIndicator.hide()
        if let error = error {
          print(error.error_msg)
          if statusCode == 403 {
            Common.showAlert(error.error_msg)
          } else {
            Common.showToastWithMessage(error.error_msg)
          }
        } else {
          let user: User = Mapper<User>().map(value)!
          AccountFlowManager.currentUser = user
          AccountFlowManager.currentUser.isLogin = true
          AccountFlowManager.currentUser.isLoginWithFacebook = true
          AccountFlowManager.currentUser.isLoginWithInstagram = false
          self.saveData()

          Appsee.setUserID(user.id)
          self.page = 1
          self.listPersonal = [Personalities]()
          self.getPersionalities()

          if statusCode == 201 {
            DHIndicator.show()

            self.localyticsRegister(user, method: "Facebook")

            self.uploadInfoFromeFacebook(profile, meID: AccountFlowManager.currentUser.id, completed: { (error) in
              DHIndicator.hide()
              if let e = error {
                print("error: \(e.errMessage)")
              } else {
                self.goWelcomeVC()
                return
              }
            })
          } else if statusCode == 200 {

            self.localyticsLogin(user, method: "Facebook")

            if user.uLatitude == nil || user.uLongtitude == nil || user.dob == "" {
              self.goWelcomeVC()
            } else {
              self.goLoadingPage()
              return
            }
          } else {
            if let error: Error = Mapper<Error>().map(value) {
              print(error.errMessage)
              Common.showToastWithMessage(error.errMessage)
            }
          }
        }
      }
    } else {
      print("Profile facebook is nil")
      Common.showToastWithMessage("Profile facebook is nil")
    }
  }

  internal func logoutWithFB() {
    DHSocial.logoutWithFacebook { (result) in
      if let result = result {
        if result {
          AccountFlowManager.currentUser.isLogin = false
          if let deviceId = NSUserDefaults.standardUserDefaults().stringForKey(userDeviceId) {
            self.logOut(deviceId, success: { (success, error) in
              self.clearData()
              self.deauthenticateUserId()
              self.goLogin()
            })
          } else {
            self.clearData()
            self.deauthenticateUserId()
            self.goLogin()
          }
        } else {
          print("Logout Failed")
        }
      }
    }
  }

  internal func logInWithInstagram(instaToken: String) {
    DHIndicator.show()

    let header = [Authorization: "Token " + instaToken]
    let param: [String: AnyObject]!
    if let deviceToken = NSUserDefaults.standardUserDefaults().stringForKey(userKeyDeviceToken) {
      param = [DeviceToken: deviceToken]
    } else {
      param = nil
    }
    GGWebservice.callWebServiceWithHeader(.POST, urlString: apiLoginWithInstagram, header: header, param: param) { (value, statusCode, error) in
      DHIndicator.hide()
      print(value)
      if let _ = error {
        print(error?.error_msg)
        Common.showToastWithMessage(textLoginWithInstagramFailed)
      } else {
        let user: User = Mapper<User>().map(value)!
        AccountFlowManager.currentUser = user
        AccountFlowManager.currentUser.isLogin = true
        AccountFlowManager.currentUser.isLoginWithFacebook = false
        AccountFlowManager.currentUser.isLoginWithInstagram = true
        self.saveData()

        self.page = 1
        self.listPersonal = [Personalities]()
        self.getPersionalities()

        LoginWithInstagramVC.getPhotos(AccountFlowManager.currentUser.uInstaId, accessToken: instaToken) { (result) in
          if let result = result {
            print(result.count)
            CreateProfileManageAPI.updateInstagramPhoto(AccountFlowManager.currentUser.id, instagramPhotos: result)
          } else {
            print("result nil")
          }
        }

        Appsee.setUserID(user.id)

        if statusCode == 201 {

          self.localyticsRegister(user, method: "Instagram")

          self.goWelcomeVC()
          return
        } else if statusCode == 200 {
          self.localyticsLogin(user, method: "Instagram")

          if user.uLatitude == nil || user.uLongtitude == nil || user.dob == "" {
            self.goWelcomeVC()
          } else {
            self.goLoadingPage()
            return
          }
        } else {
          if let error: Error = Mapper<Error>().map(value) {
            Common.showToastWithMessage(error.errMessage)
          }
        }
      }
    }
  }

  internal func logoutWithInstagram () {
    AccountFlowManager.currentUser.isLogin = false
    if let deviceId = NSUserDefaults.standardUserDefaults().stringForKey(userDeviceId) {
      logOut(deviceId, success: { (success, error) in
        self.clearData()
        self.deauthenticateUserId()
        self.goLogin()
      })
    } else {
      clearData()
      deauthenticateUserId()
      goLogin()
    }
  }

  internal func checkIdentityUser() -> Bool {
    let layerClient = AppDelegate.shareInstance().layerClient
    var success: Bool = true

    if layerClient.authenticatedUser != nil && layerClient.authenticatedUser?.userID == AccountFlowManager.currentUser.id {
      success = true
    } else {
      AppDelegate.shareInstance().authenticateLayerWithUserID(AccountFlowManager.currentUser.id, completion: { (isSuccess, error) in
        success = isSuccess
      })
    }

    return success

  }

  internal func deauthenticateUserId() {
    AppDelegate.shareInstance().layerClient.deauthenticateWithCompletion { (success, error) -> Void in
      if error == nil {
        print("Disconnect success")
      } else {
        print("Failed to deauthenticate: \(error)")
      }
    }

  }

  internal func updateRemoteNotificationDeviceToken(deviceToken: NSData?) {
    var error: NSError?
    let isSuccess: Bool

    do {
      try AppDelegate.shareInstance().layerClient.updateRemoteNotificationDeviceToken(deviceToken)
      isSuccess = true
    } catch let err as NSError {
      error = err
      isSuccess = false
    }

    if (isSuccess) {
      print("Application did register for remote notifications: \(deviceToken)")
    } else {
      print("Failed updating device token with error: \(error)")
    }

  }

  typealias LogoutSuccess = (success: Bool, error: Error?) -> Void
  internal func logOut(deviceId: String, success: LogoutSuccess) {

    localyticsLogout()

    let param = ["enabled": "false"]
    GGWebservice.callWebServiceWithRequest(.PATCH, urlString: apiGetUpdateDeviceId(deviceId), param: param, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in

      if statusCode == 200 || statusCode == 201 {
        success(success: true, error: nil)
      } else {
        if let error: Error = Mapper<Error>().map(value) {
          success(success: false, error: error)
        } else {
          success(success: false, error: nil)
        }
      }
    }
  }

  typealias ReportSuccess = (error: Error?) -> ()
  internal func reportUser(targetUserId: String, success: ReportSuccess) {

    let param = ["userid": AccountFlowManager.currentUser.id,
                 "target_userid": targetUserId]

    GGWebservice.callWebServiceWithRequest(.POST, urlString: apiReportUser, param: param, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
      if statusCode == 200 || statusCode == 201 {
        success(error: nil)
      } else {
        if let error = Mapper<Error>().map(value) {
          success(error: error)
        } else {
          let error = Error()
          error.errMessage = "Error with code \(statusCode)"
          success(error: error)
        }
      }
    }
  }

  internal func handle403Error() {
    deauthenticateUserId()
    clearData()
    goLogin()
  }


  // MARK: - Privates
  private func clearData() {
    NSUserDefaults.standardUserDefaults().removeObjectForKey(userAccesstokenKeyConstant)
    NSUserDefaults.standardUserDefaults().removeObjectForKey(userTypeAccesstokenKeyConstant)
    NSUserDefaults.standardUserDefaults().removeObjectForKey(userEmailKeyConstant)
    NSUserDefaults.standardUserDefaults().removeObjectForKey(userIDKeyConstant)
    NSUserDefaults.standardUserDefaults().removeObjectForKey(userIsLoginWithInstagram)
    NSUserDefaults.standardUserDefaults().removeObjectForKey(userIsLoginWithFacebook)
    NSUserDefaults.standardUserDefaults().removeObjectForKey(userDeviceId)
    NSUserDefaults.standardUserDefaults().removeObjectForKey(userInstaToken)
    NSUserDefaults.standardUserDefaults().removeObjectForKey(userLinkedInToken)
    NSUserDefaults.standardUserDefaults().synchronize()

    AccountFlowManager.currentUser = User()
  }

  private func saveData() {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setObject(AccountFlowManager.currentUser.authToken, forKey: userAccesstokenKeyConstant)
    defaults.setObject(AccountFlowManager.currentUser.typeToken, forKey: userTypeAccesstokenKeyConstant)
    defaults.setObject(AccountFlowManager.currentUser.isLoginWithInstagram, forKey: userIsLoginWithInstagram)
    defaults.setObject(AccountFlowManager.currentUser.isLoginWithFacebook, forKey: userIsLoginWithFacebook)
    defaults.setObject(AccountFlowManager.currentUser.email, forKey: userEmailKeyConstant)
    defaults.setObject(AccountFlowManager.currentUser.id, forKey: userIDKeyConstant)
    defaults.setObject(AccountFlowManager.currentUser.deviceId, forKey: userDeviceId)
    defaults.synchronize()
  }

  typealias UpdateInfoFromFacebookCompleted = (error: Error?) -> ()
  private func uploadInfoFromeFacebook(fbUserInfo: FBUserProfile, meID: String, completed: UpdateInfoFromFacebookCompleted) {
    let url = getApiUpdateInfoUser(meID)

    var param = [String: AnyObject]()

    if let email = fbUserInfo.fbEmail {
      param["email"] = email
    }

    if let fullname = fbUserInfo.fbName {
      param["full_name"] = fullname
    }

    if let firstname = fbUserInfo.fbFirstname {
      param["first_name"] = firstname
    }

    if let lastname = fbUserInfo.fbLastname {
      param["last_name"] = lastname
    }

    if let about = fbUserInfo.fbBio {
      param["about_me"] = about
    }

    if let gender = fbUserInfo.fbGender {
      param["gender"] = gender
    }

    if let education = fbUserInfo.fbEducation where education.count > 0 {
      param["education"] = User.convertEducationToJsonString(education)
    }

    if let work = fbUserInfo.fbWorkHistory {
      if work.count > 0, let workItem = work.first, company = workItem.employerName {
        var strWork = ""
        if let role = workItem.positionName {
          if company.characters.count > 0 {
            strWork = role + " at " + company
          } else {
            strWork = role
          }
        } else {
          if company.characters.count > 0 {
            strWork = "at " + company
          } else {
            strWork = ""
          }
        }
        param["work_history"] = strWork
      }
    }

    if let birthday = fbUserInfo.fbBirthday {
      var arrDate = birthday.componentsSeparatedByString("/")
      if arrDate.count >= 3 {
        var strBir = arrDate.last!
        arrDate.removeLast()
        arrDate.insert(strBir, atIndex: 0)
        strBir = arrDate.joinWithSeparator("-") + "T00:00:00.000000Z"
        param["dob"] = strBir
      }
    }

    print(param)

    let accessToken = NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != nil ?NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant)!:""
    let header = [
      Authorization : "Bearer " + accessToken,
      ]

    GGWebservice.callWebServiceWithHeader(.PATCH, urlString: url, header: header, param: param) { (value, statusCode, error) in
      print(value)
      if statusCode == 200 || statusCode == 201 {
        print("update UserInfo success!")

        if let gender = param["gender"] as? String {
          AccountFlowManager.currentUser.gender = gender
        }

        if let education = param["education"] as? String {
          AccountFlowManager.currentUser.uEducation = education
        }

        if let dob = param["dob"] as? String {
          AccountFlowManager.currentUser.dob = dob
        }

        if let workHistory = param["work_history"] as? String {
          AccountFlowManager.currentUser.workHistory = workHistory
        }

        if let aboutMe = param["about_me"] as? String {
          AccountFlowManager.currentUser.about = aboutMe
        }

        completed(error: nil)
        return
      }

      if let error = Mapper<Error>().map(value) {
        if error.status == "ERR" {
          print("Error: \(error.errMessage)")
        } else if error.status == "OK" {
          print("Update UserInfo Success")
        }
        completed(error: error)
      } else {
        let error = Error()
        error.status = "ERR"
        error.errMessage = "Unknow Error"
        completed(error: error)
      }
    }

  }

  private func getPersionalities() {
    let api = CreateProfileManageAPI()
    api.delegate = self
    api.getPersonalities(page)
  }

  private func localyticsRegister(user: User, method: String) {
    Localytics.tagCustomerRegistered(LLCustomer(block: { (builder) in
      builder.customerId = user.id
      builder.firstName = user.firstName
      builder.lastName = user.lastName
      builder.fullName = user.fullName
      builder.emailAddress = user.email
    }), methodName: method, attributes: nil)
  }

  private func localyticsLogin(user: User, method: String) {
    Localytics.tagCustomerLoggedIn(LLCustomer(block: { (builder) in
      builder.customerId = user.id
      builder.firstName = user.firstName
      builder.lastName = user.lastName
      builder.fullName = user.fullName
      builder.emailAddress = user.email
    }), methodName: method, attributes: nil)
  }

  private func localyticsLogout() {
    Localytics.tagCustomerLoggedOut(nil)
  }
}

// MARK: - CreateProfileDelegate
extension AccountFlowManager: CreateProfileDelegate {
  func didRequestPersonality(listPersonal: [Personalities]) {
    if listPersonal.count > 0 {
      for personal in listPersonal {
        self.listPersonal.append(personal)
      }
      page = page + 1
      getPersionalities()
    } else {

    }
  }
}
