//
//  DetectProfesional.swift
//  Pack
//
//  Created by admin on 5/30/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

enum Professions: String {
  case Student = "student"
  case Professional = "professional"
}

class DetectProfesional: NSObject {
  static let ageMax = 22
  static let ageMin = 18

  // MARK: - Publics
  class func detectWithUser(user: User) -> Professions {
    if user.uFbId.characters.count > 0 {
      if checkHasWork(user) {
        return .Professional
      } else {
        let age = Common.caculateAge(user.dob)

        if age >= ageMin && age <= ageMax {
          return .Student
        }
        return .Professional
      }
    } else if user.uInstaId.characters.count > 0 {
      if checkHasWork(user) {
        return .Professional
      } else {
        let age = Common.caculateAge(user.dob)

        if age >= ageMin && age <= ageMax {
          return .Student
        }

        return .Professional
      }
    }
    return .Professional
  }

  class func detectWithLinkedinProfile(user: User, lProfile: LinkedinProfile) -> Professions {
    if lProfile.lPositions?.value > 0 {
      return .Professional
    }

    let age = Common.caculateAge(user.dob)

    if age >= ageMin && age <= ageMax {
      return .Student
    }

    return .Professional
  }

  // MARK: - Private
  class private func checkHasWork(user: User) -> Bool {
    if user.workHistory.characters.count > 0 {
      return true
    }
    return false
  }

  class private func checkHasSchool(user: User) -> Bool {
    if user.getEducation().characters.count > 0 {
      return true
    }
    return false
  }
}
