//
//  ReceiverNotiControl.swift
//  Pack
//
//  Created by iOS on 5/25/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import LayerKit

class ReceiverNotiControl: NSObject {

  private static var banner: GGUINotification = GGUINotification()
  private static var manager: ReceiverNotiControl! = nil
  private static var info: [NSObject : AnyObject]?
  private static var user: User = User()
  private static var matchId: String = ""

  class func shareInstance() -> ReceiverNotiControl {
    if manager != nil {
      return manager
    } else {
      manager = ReceiverNotiControl()
      return manager
    }
  }

  func receiveNoti(notiInfo: [NSObject : AnyObject]?) -> Void {

    if notiInfo == nil {
      print("ReceiverNotiControl: NotiInfo nil")
      UIApplication.sharedApplication().applicationIconBadgeNumber = 0
      updateBadgeNotification()

    } else {
      if UIApplication.sharedApplication().applicationState == UIApplicationState.Active {
        if notiInfo != nil {
          if AppDelegate.shareInstance().isLoadingApp {
            if checkUserLogined() {
              if !isPushMessage(notiInfo) {
                getUserInfoFromNotiInfo(notiInfo)
                goToMatchVC(ReceiverNotiControl.user)
                AppDelegate.shareInstance().isLoadingApp = false
              } else {
                guard let layer = notiInfo!["layer"] as? NSDictionary else {return}
                guard let identifier = layer.valueForKey("conversation_identifier") as? String else {return}
                goToMessageDetail(identifier)
                AppDelegate.shareInstance().isLoadingApp = false
              }
            }
          } else {
            NSLog("ReceiverNotiControl: Active")
            ReceiverNotiControl.info = notiInfo

            if isPushMessage(notiInfo) {
              guard let dicAps = notiInfo!["aps"] as? NSDictionary else {return}
              guard let message = dicAps.valueForKey("alert") as? String else {return}
              guard let layer = notiInfo!["layer"] as? NSDictionary else {return}
              guard let identifier = layer.valueForKey("conversation_identifier") as? String else {return}
              print(message)

              if checkUserLogined() {
                if let navi = AppDelegate.shareInstance().window?.rootViewController as? GGNavigationController {
                  if let controller = navi.viewControllers.last {
                    if controller.isKindOfClass(MessagesDetailVC.classForCoder()) {
                      if let vc = controller as? MessagesDetailVC {
                        guard let conversationIdentifier = vc.conversation?.identifier else {return}
                        print(conversationIdentifier)
                        print(identifier)
                        if conversationIdentifier.absoluteString == identifier {
                          // no show push
                          print("No show push")
                        } else {
                          ReceiverNotiControl.banner.showPushNotificationViewWithInfo(notiInfo)
                          ReceiverNotiControl.banner.delegate = ReceiverNotiControl.manager
                        }
                      }
                    } else {
                      ReceiverNotiControl.banner.showPushNotificationViewWithInfo(notiInfo)
                      ReceiverNotiControl.banner.delegate = ReceiverNotiControl.manager
                    }
                  }
                }
              }
            } else {
              ReceiverNotiControl.banner.showPushNotificationViewWithInfo(notiInfo)
              ReceiverNotiControl.banner.delegate = ReceiverNotiControl.manager
            }
          }
        }
      } else if UIApplication.sharedApplication().applicationState == UIApplicationState.Inactive {
        NSLog("ReceiverNotiControl: InActive")

        if checkUserLogined() {
          if isPushMessage(notiInfo) {
            guard let layer = notiInfo!["layer"] as? NSDictionary else {return}
            guard let identifier = layer.valueForKey("conversation_identifier") as? String else {return}

            if let navi = AppDelegate.shareInstance().window?.rootViewController as? GGNavigationController {
              if let controller = navi.viewControllers.last {
                if controller.isKindOfClass(MessagesDetailVC.classForCoder()) {
                  if let vc = controller as? MessagesDetailVC {
                    guard let conversationIdentifier = vc.conversation?.identifier else {return}
                    if conversationIdentifier.absoluteString == identifier {
                      return
                    }
                  }
                }
              }
            }
            if let topVC = AppDelegate.shareInstance().window?.rootViewController {
              if let presentedVC = topVC.presentedViewController {
                presentedVC.dismissViewControllerAnimated(true, completion: nil)
              }
            }

            goToMessageDetail(identifier)

          } else {
            getUserInfoFromNotiInfo(notiInfo)
            goToMatchVC(ReceiverNotiControl.user)
          }
        }
      } else {
        NSLog("ReceiverNotiControl: Background")
      }
    }
  }

  func checkUserLogined() -> Bool {
    if NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != nil && NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != "" {
      return true
    } else {
      return false
    }
  }

  func goToMessageDetail(identifier: String) {
    DHIndicator.show()
    if let navi = AppDelegate.shareInstance().window?.rootViewController as? GGNavigationController {
      let vc = MessagesDetailVC(user: User(), isNewConversation: false, identifier: identifier)
      navi.pushViewController(vc, animated: true)
    }
  }

  func isPushMessage(notiInfo: [NSObject : AnyObject]?) -> Bool {
    guard let layer = notiInfo!["layer"] as? NSDictionary else {
      return false
    }
    print(layer)
    return true
  }

  func goToMatchVC(user: User) {
    let vc = MatchVC()
    vc.targetUser = user
    vc.delegate = self
    AppDelegate.shareInstance().window?.rootViewController?.presentViewController(vc, animated: true, completion: nil)
  }

  func getUserInfoFromNotiInfo(notiInfo: [NSObject : AnyObject]?) {

    if let info = notiInfo {

      guard let id = info["id"] as? String else {return}
      guard let firstName = info["first_name"] as? String else {return}
      guard let lastName = info["last_name"] as? String else {return}
      guard let fullName = info["full_name"] as? String else {return}
      guard let primaryImage = info["primary_image"] as? String else {return}
      guard let matchId = info["matchid"] as? String else {return}

      ReceiverNotiControl.user.id = id
      ReceiverNotiControl.user.firstName = firstName
      ReceiverNotiControl.user.lastName = lastName
      ReceiverNotiControl.user.fullName = fullName
      ReceiverNotiControl.user.primaryImage = primaryImage
      ReceiverNotiControl.matchId = matchId

    }

  }

  func updateBadgeNotification() {
    let param = ["lastbadge": 0]
    if let deviceId = NSUserDefaults.standardUserDefaults().stringForKey(userDeviceId) {
      GGWebservice.callWebServiceWithRequest(.PATCH, urlString: apiGetUpdateDeviceId(deviceId), param: param, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in

        if statusCode == 200 || statusCode == 201 {
          print("Update badge success")
        } else {
          print("Update badge error")
        }
      }
    }
  }

}

extension ReceiverNotiControl: GGUINotificationDelegate {
  func didHideViewWithHaveTapSelect(isSelected: Bool) {
    if checkUserLogined() {
      if isPushMessage(ReceiverNotiControl.info) {
        guard let layer = ReceiverNotiControl.info!["layer"] as? NSDictionary else {return}
        guard let identifier = layer.valueForKey("conversation_identifier") as? String else {return}

        if let topVC = AppDelegate.shareInstance().window?.rootViewController {
          if let presentedVC = topVC.presentedViewController {
            // presented is MatchVC
            presentedVC.dismissViewControllerAnimated(true, completion: nil)
          }
          // Now, the MatchVC was dismiss
        }
        goToMessageDetail(identifier)
      } else {
        getUserInfoFromNotiInfo(ReceiverNotiControl.info)
        goToMatchVC(ReceiverNotiControl.user)
        updateBadgeNotification()
      }
    }
  }
}

extension ReceiverNotiControl: MatchDelegate {
  func gotoMessage(user: User) {
    if let navi = AppDelegate.shareInstance().window?.rootViewController as? GGNavigationController {
      let vc = MessagesDetailVC(user: user, isNewConversation: true, conversation: nil)
      vc.matchId = ReceiverNotiControl.matchId
      vc.isNeedUpdateStatus = true
      navi.pushViewController(vc, animated: true)
    }
  }
}
