//
//  LoadingView.swift
//  Pack
//
//  Created by admin on 6/13/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import Pulsator
import pop

protocol LoadingViewDelegate: NSObjectProtocol {
  func loadingViewDidCompleted(users: [User])
}

class LoadingView: UIView {

  // MARK: - Outlets
  @IBOutlet weak var imgBackgroundBig: UIImageView!
  @IBOutlet weak var imgBackgroundNormal: UIImageView!
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var imgIconBall1: UIImageView!
  @IBOutlet weak var imgIconBall2: UIImageView!
  @IBOutlet weak var imgIconBall3: UIImageView!
  @IBOutlet weak var lblMatching: UILabel!

  //MARK: - Variables
  weak var delegate: LoadingViewDelegate?

  private let api = MainScreenManageAPI()
  let pulsator = Pulsator()

  internal func setUpView() {
    imgIconBall1.alpha = 1
    imgIconBall2.alpha = 1
    imgIconBall3.alpha = 1
    addAnimationForImageAvatar(true)
    addAnimationForIconBall(true)
    configViewDidLoad()
  }

  internal func requestApi() {
    api.delegate = self
    api.getListMatchingUser()
    if AppDelegate.shareInstance().isAppTerminal {
      AppDelegate.shareInstance().isLoadingApp = true
      AppDelegate.shareInstance().isAppTerminal = false
      ReceiverNotiControl.shareInstance().receiveNoti(AppDelegate.shareInstance().notiInfo)
    } else {
      AppDelegate.shareInstance().isLoadingApp = false
    }
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    print("center Avatar: \(imgAvatar.center)")
    pulsator.position = imgAvatar.center
  }

  func tapOnAvatar() {
    let pop = POPSpringAnimation(propertyNamed: kPOPViewScaleXY)
    pop.toValue = NSValue(CGPoint: CGPoint(x: 1, y: 1))
    pop.velocity = NSValue(CGPoint: CGPoint(x: 3, y: 3))
    pop.springBounciness = 20
    pop.springSpeed = 10
    imgAvatar.pop_addAnimation(pop, forKey: "springAnimation")
  }
  // MARK: - Private method
  private func configViewDidLoad() {
    imgAvatar.layer.cornerRadius = imgAvatar.frame.size.width / 2
    imgAvatar.image = nil
    imgAvatar.layer.borderWidth = 1
    imgAvatar.layer.borderColor = UIColor(red: 223/255, green: 223/255, blue: 223/255, alpha: 1).CGColor
    imgAvatar.clipsToBounds = true

    pulsator.numPulse = 3
    pulsator.radius = 240.0
    layer.insertSublayer(pulsator, below: imgAvatar.layer)
    pulsator.start()

    imgIconBall1.layer.cornerRadius = imgIconBall1.frame.size.width / 2
    imgIconBall2.layer.cornerRadius = imgIconBall2.frame.size.width / 2
    imgIconBall3.layer.cornerRadius = imgIconBall3.frame.size.width / 2

    if var strAvatar = AccountFlowManager.currentUser.images.first?.urlImg {
      if strAvatar.containsString("http") || strAvatar.containsString("https") {
        if let url = NSURL(string: strAvatar + "?type=large") {
          imgAvatar.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            if let e = error {
              print("imgAvatar.kf_setImageWithURL: \(e.localizedDescription)")
              self.imgAvatar.image = defaultAvatar
            } else {
              self.imgAvatar.image = image
            }
          })
        }
      } else {
        strAvatar = apiHost + strAvatar + "?type=large"
        if let url = NSURL(string: strAvatar) {
          imgAvatar.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            if let e = error {
              print("imgAvatar.kf_setImageWithURL: \(e.localizedDescription)")
              self.imgAvatar.image = defaultAvatar
            } else {
              self.imgAvatar.image = image
            }
          })
        }
      }
    } else {
      imgAvatar.image = UIImage(named: "img_avatar_default.jpg")
    }
    lblMatching.text = textMatchingLoading

    let tapAvatar = UITapGestureRecognizer(target: self, action: #selector(tapOnAvatar))
    imgAvatar.userInteractionEnabled = true
    imgAvatar.addGestureRecognizer(tapAvatar)
  }

  private func addAnimationForImageAvatar(isAnimation: Bool) {
    let pulseAnimationBig = CABasicAnimation(keyPath: "opacity")
    pulseAnimationBig.duration = 0.8
    pulseAnimationBig.fromValue = 0
    pulseAnimationBig.toValue = 1
    pulseAnimationBig.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationBig.autoreverses = true
    pulseAnimationBig.repeatCount = FLT_MAX

    let pulseAnimationNormal = CABasicAnimation(keyPath: "opacity")
    pulseAnimationNormal.duration = 1
    pulseAnimationNormal.fromValue = 0
    pulseAnimationNormal.toValue = 1
    pulseAnimationNormal.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationNormal.autoreverses = true
    pulseAnimationNormal.repeatCount = FLT_MAX

    if isAnimation {
      imgBackgroundBig.layer.addAnimation(pulseAnimationBig, forKey: nil)
      imgBackgroundNormal.layer.addAnimation(pulseAnimationNormal, forKey: nil)
    } else {
      imgBackgroundBig.layer.removeAllAnimations()
      imgBackgroundNormal.layer.removeAllAnimations()
    }
  }

  private func addAnimationForIconBall(isAnimation: Bool) {
    let pulseAnimationBall1 = CABasicAnimation(keyPath: "opacity")
    pulseAnimationBall1.duration = 0.3
    pulseAnimationBall1.fromValue = 0
    pulseAnimationBall1.toValue = 1
    pulseAnimationBall1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationBall1.autoreverses = true
    pulseAnimationBall1.repeatCount = FLT_MAX

    let pulseAnimationBall2 = CABasicAnimation(keyPath: "opacity")
    pulseAnimationBall2.duration = 0.5
    pulseAnimationBall2.fromValue = 0
    pulseAnimationBall2.toValue = 1
    pulseAnimationBall2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationBall2.autoreverses = true
    pulseAnimationBall2.repeatCount = FLT_MAX

    let pulseAnimationBall3 = CABasicAnimation(keyPath: "opacity")
    pulseAnimationBall3.duration = 0.8
    pulseAnimationBall3.fromValue = 0
    pulseAnimationBall3.toValue = 1
    pulseAnimationBall3.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationBall3.autoreverses = true
    pulseAnimationBall3.repeatCount = FLT_MAX

    if isAnimation {
      imgIconBall1.layer.addAnimation(pulseAnimationBall1, forKey: nil)
      imgIconBall2.layer.addAnimation(pulseAnimationBall2, forKey: nil)
      imgIconBall3.layer.addAnimation(pulseAnimationBall3, forKey: nil)
    } else {
      imgIconBall1.layer.removeAllAnimations()
      imgIconBall2.layer.removeAllAnimations()
      imgIconBall3.layer.removeAllAnimations()
    }
  }

  private func showInfoNoUser() {
    addAnimationForImageAvatar(false)
    addAnimationForIconBall(false)
    imgIconBall1.alpha = 0
    imgIconBall2.alpha = 0
    imgIconBall3.alpha = 0
    lblMatching.text = textMatchingNoBody
    pulsator.stop()
  }

}

extension LoadingView: MainScreenAPIDelegate {
  func didRequestListUser(listUser: ListUser!, error: Error?) {
    if let e = error {
      print("didRequestListUser :\(e.errMessage)")
      showInfoNoUser()
      AppDelegate.shareInstance().isNotFoundUserMatching = true
      delegate?.loadingViewDidCompleted([])
    } else {
      if let list = listUser {
        if list.listUser.count > 0 {
          delegate?.loadingViewDidCompleted(list.listUser)
        } else {
          showInfoNoUser()
          AppDelegate.shareInstance().isNotFoundUserMatching = true
          delegate?.loadingViewDidCompleted([])
        }
      }
    }
  }

  func likeUser(target: String, matchId: String, error: Error?) { }
  func dislikeUser(error: Error?) { }
  func getStatusLikedCompleted(status: LikeStatusObject?, error: Error?) { }
}
