//
//  MainScreenManageAPI.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/11/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper
import Async

protocol MainScreenAPIDelegate: NSObjectProtocol {
  func didRequestListUser(listUser: ListUser!, error: Error?)
  func likeUser(targetId: String, matchId: String, error: Error?)
  func dislikeUser(error: Error?)
}

class MainScreenManageAPI: NSObject {

  weak var delegate: MainScreenAPIDelegate?

  func getListMatchingUser() {
    AppDelegate.shareInstance().isLoadingListUser = true
    let url = apiGetUserMatching(AccountFlowManager.currentUser.id)

    let request = NSMutableURLRequest(URL: NSURL(string: url)!,
                                      cachePolicy: .ReloadIgnoringCacheData,
                                      timeoutInterval: 60.0)

    NSURLCache.sharedURLCache().removeAllCachedResponses()

    request.HTTPMethod = "GET"
    request.allHTTPHeaderFields = GetHeader()

    let session = NSURLSession.sharedSession()
    let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
      AppDelegate.shareInstance().isLoadingListUser = false
      if (error != nil) {
        Async.main(block: {
          print(error)
          DHIndicator.hide()
          Common.showAlert(error?.localizedDescription)
          let e = Error()
          e.errMessage = error!.localizedDescription
          self.delegate?.didRequestListUser(nil, error: e)
        })

      } else {
        Async.main(block: {
          let httpResponse = response as? NSHTTPURLResponse
          print(httpResponse?.statusCode)
          if httpResponse?.statusCode == 502 {
            self.getListMatchingUser()
          } else {
            if let data = data, json = Common.nsdataToJSON(data) {
              if httpResponse?.statusCode == 200 {
                Async.main(block: {
                  let listUser: ListUser = Mapper<ListUser>().map(json)!
                  Async.main(block: {
                    self.delegate?.didRequestListUser(listUser, error: nil)
                  })
                })
              } else {
                let error = Mapper<Error>().map(json)
                self.delegate?.didRequestListUser(nil, error: error)
              }
            } else {
              print(data)
              if let data = data {
                print("String of data: \(NSString(data: data, encoding: NSUTF8StringEncoding))")
              }
              let e = Error()
              e.errMessage = "Can't parse data to json"
              self.delegate?.didRequestListUser(nil, error: e)
            }
          }
        })

      }
    })

    dataTask.resume()
  }

  func likeUser(targeUserId: String) {
    let url = apiLikeUser()
    print("api like: \(url)")

    let param = ["userid": AccountFlowManager.currentUser.id,
                 "target_userid": targeUserId]

    GGWebservice.callWebServiceWithRequest(.POST, urlString: url, param: param, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
      print("Like \(value)")
      if statusCode == 201 {
        var matchedId: String = ""
        if let value = value {
          if let attributes = value["attributes"] as? NSDictionary {
            if let matchId = attributes.valueForKey("matchid") as? String {
              matchedId = matchId
            }
          }
        }
        print("Match id: \(matchedId)")
        self.delegate?.likeUser(targeUserId, matchId: matchedId, error: nil)
      } else {
        if let error: Error = Mapper<Error>().map(value) {
          self.delegate?.likeUser("", matchId: "", error: error)
        }
      }
    }
  }

  func dislikeUser(targeUserId: String) {
    let url = apiDislikeUser()
    print("api like: \(url)")

    let param = ["userid": AccountFlowManager.currentUser.id,
                 "target_userid": targeUserId]

    GGWebservice.callWebServiceWithRequest(.POST, urlString: url, param: param, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
      if statusCode == 201 {
        self.delegate?.dislikeUser(nil)
      } else {
        if let error: Error = Mapper<Error>().map(value) {
          self.delegate?.dislikeUser(error)
        }
      }
    }
  }
}

class LikeStatusObject: NSObject, Mappable {

  var total: Int = 0

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    total     <- map["meta.total"]
  }
}
