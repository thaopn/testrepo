//
//  CustomSwipeCardView.swift
//  Pack
//
//  Created by admin on 5/18/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import Kingfisher

class CustomSwipeCardView: UIView {

  //MARK: - Outlets
  @IBOutlet weak var viewContent: UIView!
  @IBOutlet weak var imvAvatar: UIImageView!
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblMutualFriends: UILabel!
  @IBOutlet weak var lblWork: UILabel!
  @IBOutlet weak var lblBudget: UILabel!
  @IBOutlet weak var viewPersonalities: SwipeCardPersonalView!

  //MARK: - Publics
  override func awakeFromNib() {
    layer.cornerRadius = 3

    backgroundColor = UIColor.clearColor()
    layer.shadowColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 28).CGColor
    layer.shadowOffset = CGSize(width: 0, height: 0)
    layer.shadowOpacity = 0.7
    layer.shadowRadius = 2.0

    viewContent.layer.cornerRadius = 3
    viewContent.layer.masksToBounds = true
    viewContent.layer.borderWidth = 0.3
    viewContent.layer.borderColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1).CGColor

    configureFont()

  }

  class func getNameAndAge(user: User) -> NSAttributedString {
    let strName = user.getUserName()
    let strAge = ", \(Common.caculateAge(user.dob))"

    let strTitle = strName + strAge

    let attrStr = NSMutableAttributedString(string: strTitle, attributes: [NSFontAttributeName: getRobotoBoldWith(16)!])
    attrStr.addAttributes([NSFontAttributeName: getRobotoRegularWith(16)!], range: (strTitle as NSString).rangeOfString(strAge))

    return attrStr
  }

  internal func setupCard(user: User) {
    print("birthday: \(user.dob) age: \(Common.caculateAge(user.dob))")
    print("name: \(user.fullName) url: \(user.getPrimaryImageUrl())")
    imvAvatar.image = defaultAvatar

    if let url = NSURL(string: user.getPrimaryImageUrl()) {
      imvAvatar.kf_setImageWithURL(url, placeholderImage: defaultAvatar, optionsInfo: nil, progressBlock: nil, completionHandler: {[weak self] (image, error, cacheType, imageURL) in
        if let e = error {
          print("imvAvatar.kf_setImageWithURL: \(e.localizedDescription)")
          self?.imvAvatar.image = defaultAvatar
        } else {
          self?.imvAvatar.image = image
        }
      })
    } else {
      imvAvatar.image = defaultAvatar
    }

    lblWork.text = user.getWorkHistory()

    lblName.attributedText = CustomSwipeCardView.getNameAndAge(user)

    let strBudgetMin = Common.formatNumber(Int(user.budgetMin))
    let strBudgetMax = Common.formatNumber(Int(user.budgetMax))
    lblBudget.text = "Budget: " + "$\(strBudgetMin) - " + "$\(strBudgetMax)"

    viewPersonalities.personalString = user.interests

    if user.uFbId.characters.count > 0 {
      DHSocial.getMutualFriends(user.uFbId, completed: {[weak self] (mutualFriend, error) in
        if let e = error {
          print("DHSocial.getMutualFriends Error: \(e.localizedDescription)")
        } else {
          self?.lblMutualFriends?.text = "(\(mutualFriend))"
        }
      })
    }
  }

  //MARK: - Privates
  private func configureFont() {
    lblName.font = getRobotoBoldWith(16)
    lblMutualFriends.font = getRobotoMediumWith(14)
    lblWork.font = getRobotoRegularWith(14)
    lblBudget.font = getRobotoRegularWith(14)
  }

  private func makeupButton(label: UILabel) {
    label.font = getRobotoMediumWith(12)
    label.textColor = UIColor.whiteColor()
    let widthButton = getWidthText(label.text!, font: label.font)
    label.frame = CGRect(x: 0, y: 0, width: widthButton + 30, height: 32)
    label.textAlignment = .Center
    label.backgroundColor = UIColor(red: 81/255, green: 156/255, blue: 1, alpha: 1)
    label.layer.cornerRadius = 16
    label.layer.masksToBounds = true
  }

  private func getWidthText(str: String, font: UIFont) -> CGFloat {
    let lbl = UILabel()
    lbl.font = font
    lbl.text = str
    return lbl.sizeThatFits(CGSize(width: 10000, height: 40)).width
  }


  private func setupPersonalitiesView(interests: String) {
    if interests.characters.count == 0 {
      return
    }

    let personalities = interests.componentsSeparatedByString("|")
    if personalities.count == 0 {
      return
    }

    var labelPersonalities = [UILabel]()
    for personal in personalities {
      let lbl = UILabel()
      lbl.text = personal
      makeupButton(lbl)
      labelPersonalities.append(lbl)
    }
    let space: CGFloat = 10.0
    let pading: CGFloat = 0.0
    let currentY: CGFloat = pading
    var lastPoint = CGPoint.zero
    var lastFrame = CGRect.zero

    for l in labelPersonalities {

      if lastFrame == CGRect.zero {
        lastPoint = CGPoint(x: pading, y: currentY)
        l.frame.origin = lastPoint
        lastFrame = l.frame
      } else {
        if lastFrame.maxX + space + CGRectGetWidth(l.frame) > CGRectGetWidth(viewPersonalities.bounds) - pading {
          return
        } else {
          lastPoint = CGPoint(x: lastFrame.maxX + space, y: currentY)
        }
        l.frame.origin = lastPoint
        lastFrame = l.frame
      }
      viewPersonalities.addSubview(l)
    }
  }
}
