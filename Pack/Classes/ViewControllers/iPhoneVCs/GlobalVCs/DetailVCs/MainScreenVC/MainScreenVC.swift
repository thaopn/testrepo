//
//  MainScreenVC.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/6/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import Async
import Localytics

class MainScreenVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var viewInfoUser: KolodaView!
  @IBOutlet weak var viewContent: UIView!
  @IBOutlet weak var consHeightViewInfoUser: NSLayoutConstraint!
  @IBOutlet var loadingView: LoadingView!

  // MARK: - Variables
  var listUser: [User] = [] {
    didSet {
      print("listUser: \(listUser.count)")
      viewInfoUser?.resetCurrentCardNumber()
    }
  }
  private let api = MainScreenManageAPI()
  private var targetUser: User?
  private var matchId: String = ""

  // MARK: - Lifecycle
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "MainScreenVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.Profile, centerType:CenterItems.Title, rightType:RightItems.Message)
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    setTitleForVC("Pack")
    viewInfoUser.refreshFirstCard()
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    configViewDidLoad()
    api.delegate = self
    viewInfoUser.dataSource = self
    viewInfoUser.delegate = self
  }

  // MARK: - Private method
  private func presentStackViewController(parentVC: UIViewController, presentVC: UIViewController, animation: Bool) {
    if let presented = parentVC.presentedViewController {
      presentStackViewController(presented, presentVC: presentVC, animation: animation)
    } else {
      parentVC.presentViewController(presentVC, animated: true, completion: nil)
    }
  }

  private func configViewDidLoad() {
    if Common.isIPhone5() {
      consHeightViewInfoUser.constant = 388
    } else if Common.isIPhone6() {
      consHeightViewInfoUser.constant = 440
    } else if Common.isIPhone4() {
      consHeightViewInfoUser.constant = 388
    } else if Common.isIPhone6plus() {
      consHeightViewInfoUser.constant = 470
    }
    loadingView.delegate = self
    loadUsers()
  }

  private func checkForTheFirst(direction: SwipeResultDirection) -> Bool {
    let userDefault = NSUserDefaults.standardUserDefaults()

    var value: String!
    if direction == .Right {
      value = userDefault.stringForKey(userFirstSwipeRignt)
    }

    if direction == .Left {
      value = userDefault.stringForKey(userFirstSwipeLeft)
    }

    if value == nil {
      return true
    }

    return false
  }

  private func showTheFirstAlert(direction: SwipeResultDirection, index: UInt) {
    var strTitle = ""
    var strAction = ""
    var strMessage = ""
    let user = listUser[Int(index)]


    if direction == .Right {
      strTitle = textInterested
      strAction = textInterested
      strMessage = textInterestedContent + " \(user.getUserName())"
      NSUserDefaults.standardUserDefaults().setObject("1", forKey: userFirstSwipeRignt)
      NSUserDefaults.standardUserDefaults().synchronize()
    } else if direction == .Left {
      strTitle = textNoInterested
      strAction = textNoInterested
      strMessage = textNoInterestedContent + " \(user.getUserName())"
      NSUserDefaults.standardUserDefaults().setObject("1", forKey: userFirstSwipeLeft)
      NSUserDefaults.standardUserDefaults().synchronize()
    } else {
      return
    }
    strTitle += "?"

    let alertView: UIAlertController = UIAlertController(title: strTitle, message: strMessage, preferredStyle:UIAlertControllerStyle.Alert)
    let cancel = UIAlertAction(title: "Cancel", style: .Default) { _ in
      self.viewInfoUser.revertAction()
    }
    alertView.addAction(cancel)
    let action = UIAlertAction(title: strAction, style: .Default) { (_) in
      if direction == .Right {
        self.targetUser = user
        self.api.likeUser(user.id)
      } else {
        self.api.dislikeUser(user.id)
      }

    }
    alertView.addAction(action)

    presentViewController(alertView, animated: true, completion: nil)
  }

  private func loadUsers() {
    showLoadingView()
    loadingView.requestApi()
  }

  private func showLoadingView() {
    loadingView.alpha = 0
    loadingView.frame = view.bounds
    loadingView.setUpView()
    view.addSubview(loadingView)
    UIView.animateWithDuration(0.3) {
      self.loadingView.alpha = 1
    }
  }

  private func localyticsSwipe(direction: SwipeResultDirection, targetUser: User) {
    var action = ""
    if direction == .Right {
      action = "Like"
    } else {
      action = "Dislike"
    }

    let extraAttributes = ["Action": action,
                           "OwnerUserID": AccountFlowManager.currentUser.id,
                           "OwnerUserFirstName": AccountFlowManager.currentUser.firstName,
                           "OwnerUserLastName": AccountFlowManager.currentUser.lastName,
                           "OwnerUserFullName": AccountFlowManager.currentUser.fullName,
                           "TargetUserID": targetUser.id,
                           "TargetUserFirstName": targetUser.firstName,
                           "TargetUserLastName": targetUser.lastName,
                           "TargetUserFullName": targetUser.fullName]

    Localytics.tagEvent("Swipe", attributes: extraAttributes)
  }

  // MARK: - Actions
  @IBAction func btnDislikeTapped(sender: AnyObject) {
    viewInfoUser?.swipe(SwipeResultDirection.Left)
  }

  @IBAction func btnLikeTapped(sender: AnyObject) {
    viewInfoUser?.swipe(SwipeResultDirection.Right)
  }
}

// MARK: - MainScreenAPI Delegate
extension MainScreenVC: MainScreenAPIDelegate {
  func didRequestListUser(listUser: ListUser!, error: Error?) {

  }

  func likeUser(target: String, matchId: String, error: Error?) {
    if let e = error {
      print(e.errMessage)
      viewInfoUser.revertAction()
    } else {
      self.matchId = matchId

      if matchId.characters.count > 0 {
        let vc = MatchVC()
        vc.delegate = self
        vc.targetUser = targetUser
        presentViewController(vc, animated: true, completion: nil)
      }
    }
  }

  func dislikeUser(error: Error?) {
    if let e = error {
      print(e.errMessage)
      viewInfoUser.revertAction()
    } else {
      print("Dislike OK")
    }
  }
}


// MARK: - KolodaViewDelegate
extension MainScreenVC: KolodaViewDelegate {
  func koloda(koloda: KolodaView, didSwipedCardAtIndex index: UInt, inDirection direction: SwipeResultDirection) {
    AppDelegate.shareInstance().flowRegister(false)

    print("index card: \(index) direction: \(direction)")
    if checkForTheFirst(direction) {
      showTheFirstAlert(direction, index: index)
    } else {
      let user = listUser[Int(index)]
      switch direction {
      case .Left:
        api.dislikeUser(user.id)
      case .Right:
        targetUser = user
        api.likeUser(user.id)
      default:
        break
      }
      localyticsSwipe(direction, targetUser: user)
    }
  }

  func koloda(kolodaDidRunOutOfCards koloda: KolodaView) {
    print("Out of card")
    loadUsers()
  }

  func koloda(koloda: KolodaView, didSelectCardAtIndex index: UInt) {
    print("tap to card")
    let vc = ProfileViewVC()
    vc.setupViewWith(listUser[Int(index)])
    vc.delegate = self
    presentViewController(vc, animated: true, completion: nil)
  }
}

// MARK: - KolodaViewDataSource
extension MainScreenVC: KolodaViewDataSource {

  func koloda(kolodaNumberOfCards koloda: KolodaView) -> UInt {
    return UInt(listUser.count)
  }

  func koloda(koloda: KolodaView, viewForCardAtIndex index: UInt) -> UIView {
    let card = NSBundle.mainBundle().loadNibNamed("CustomSwipeCardView", owner: self, options: nil)[0] as! CustomSwipeCardView
    let user = listUser[Int(index)]
    card.setupCard(user)

    return card
  }

  func koloda(koloda: KolodaView, viewForCardOverlayAtIndex index: UInt) -> OverlayView? {
    return NSBundle.mainBundle().loadNibNamed("OverlayView", owner: self, options: nil)[0] as? OverlayView
  }
}

// MARK: - Profile View Delegate
extension MainScreenVC: ProfileViewDelegate {
  func profileViewLike() {
    btnLikeTapped(self)
  }

  func profileViewDislike() {
    btnDislikeTapped(self)
  }
}

// MARK: - Match Delegate
extension MainScreenVC: MatchDelegate {
  func gotoMessage(user: User) {
    let vc = MessagesDetailVC(user: user, isNewConversation: true, conversation: nil)
    vc.matchId = matchId
    vc.isNeedUpdateStatus = true
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - LoadingViewDelegate
extension MainScreenVC: LoadingViewDelegate {
  func loadingViewDidCompleted(users: [User]) {
    if users.count > 0 {
      loadingView.removeFromSuperview()
      listUser = users
    }
  }
}
