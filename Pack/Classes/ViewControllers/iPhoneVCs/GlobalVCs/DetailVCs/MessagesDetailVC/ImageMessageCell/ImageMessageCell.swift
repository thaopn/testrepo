//
//  ImageMessageCell.swift
//  Pack
//
//  Created by iOS on 5/18/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class ImageMessageCell: UITableViewCell {

  // MARK: - Outlet
  @IBOutlet weak var viewTimeMessage: UIView!
  @IBOutlet weak var lblTimeRead: UILabel!
  @IBOutlet weak var consBottomLblTimeReadToContentView: NSLayoutConstraint!
  @IBOutlet weak var consTopLblTimeReadToViewMessage: NSLayoutConstraint!
  @IBOutlet weak var consTopViewMessageToContentView: NSLayoutConstraint!
  @IBOutlet weak var lblTimeMessage: UILabel!
  @IBOutlet weak var viewMyMessage: UIView!
  @IBOutlet weak var imgBackgroundMessage: UIImageView!
  @IBOutlet weak var imgMessage: UIImageView!
  @IBOutlet weak var consHeightImgMessage: NSLayoutConstraint!

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    viewMyMessage.layer.cornerRadius = 8
    imgMessage.layer.cornerRadius = 8
    selectionStyle = UITableViewCellSelectionStyle.None
  }
  
}
