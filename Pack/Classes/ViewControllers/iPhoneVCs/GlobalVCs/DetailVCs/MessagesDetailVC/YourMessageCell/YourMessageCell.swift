//
//  YourMessageCell.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/8/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class YourMessageCell: UITableViewCell {

  // MARK: - Outlets
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var viewYourMessage: UIView!
  @IBOutlet weak var lblMessage: TTTAttributedLabel!
  @IBOutlet weak var consTopImgToViewCell: NSLayoutConstraint!
  @IBOutlet weak var viewTimeMessage: UIView!
  @IBOutlet weak var lblTimeMessage: UILabel!
  @IBOutlet weak var imgBackgroundMessage: UIImageView!
  @IBOutlet weak var viewMessage: UIView!
  @IBOutlet weak var consBottomViewMessageToContentView: NSLayoutConstraint!
  @IBOutlet weak var imgBackgroundAvatar: UIImageView!
  @IBOutlet weak var consTopImgBackgroundAvatarToContentView: NSLayoutConstraint!
  @IBOutlet weak var imgForward: UIImageView!

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    imgAvatar.layer.cornerRadius = imgAvatar.frame.size.height / 2
    imgAvatar.clipsToBounds = true
    viewYourMessage.layer.cornerRadius = 8
    lblMessage.text = "Have a great weekend! Proin quis tortor orci. Etiam at risus et justo dignissim congue. Donec. Proin quis tortor orci. Have a great weekend! "
    selectionStyle = UITableViewCellSelectionStyle.None
    lblMessage.enabledTextCheckingTypes = NSTextCheckingType.Link.rawValue
    lblMessage.linkAttributes = [NSForegroundColorAttributeName: UIColor(red: 74/255, green: 144/255, blue: 226/255, alpha: 1)]
    lblMessage.userInteractionEnabled = true
    lblMessage.delegate = self
  }

}

// MARK: - TTTAttributedLabelDelegate
extension YourMessageCell: TTTAttributedLabelDelegate {
  func attributedLabel(label: TTTAttributedLabel!, didLongPressLinkWithURL url: NSURL!, atPoint point: CGPoint) {
    UIApplication.sharedApplication().openURL(url)
  }
}
