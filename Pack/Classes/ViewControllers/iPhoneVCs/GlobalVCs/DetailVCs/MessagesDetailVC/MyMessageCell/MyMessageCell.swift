//
//  MyMessageCell.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/8/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class MyMessageCell: UITableViewCell {

  // MARK: - Outlets
  @IBOutlet weak var lblMessage: TTTAttributedLabel!
  @IBOutlet weak var viewMyMessage: UIView!
  @IBOutlet weak var consTopArrowViewMessage: NSLayoutConstraint!
  @IBOutlet weak var viewTimeMessage: UIView!
  @IBOutlet weak var lblTimeMessage: UILabel!
  @IBOutlet weak var imgBackgroundMessage: UIImageView!
  @IBOutlet weak var lblTimeRead: UILabel!
  @IBOutlet weak var consBottomLblTimeReadToContentView: NSLayoutConstraint!
  @IBOutlet weak var consTopLblTimeReadToViewMessage: NSLayoutConstraint!
  @IBOutlet weak var viewMessage: UIView!
  @IBOutlet weak var consTopViewMessageToContentView: NSLayoutConstraint!

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    viewMyMessage.layer.cornerRadius = 8
    lblMessage.text = "Have a great weekend! Proin quis tortor orci. Etiam at risus et justo dignissim congue."
    selectionStyle = UITableViewCellSelectionStyle.None
    lblMessage.enabledTextCheckingTypes = NSTextCheckingType.Link.rawValue
    lblMessage.linkAttributes = [NSForegroundColorAttributeName: UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)]
    lblMessage.userInteractionEnabled = true
    lblMessage.delegate = self
    consTopArrowViewMessage.constant = 0
  }

}

// MARK: - TTTAttributedLabelDelegate
extension MyMessageCell: TTTAttributedLabelDelegate {
  func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithURL url: NSURL!) {
    
  }

  func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithTextCheckingResult result: NSTextCheckingResult!) {
    print(result.URL!)
  }

  func attributedLabel(label: TTTAttributedLabel!, didLongPressLinkWithURL url: NSURL!, atPoint point: CGPoint) {
    UIApplication.sharedApplication().openURL(url)
  }
}
