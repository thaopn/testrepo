//
//  YourImageMessageCell.swift
//  Pack
//
//  Created by iOS on 5/18/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class YourImageMessageCell: UITableViewCell {

  // MARK: - Outlet
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var viewYourMessage: UIView!
  @IBOutlet weak var consTopImgToViewCell: NSLayoutConstraint!
  @IBOutlet weak var viewTimeMessage: UIView!
  @IBOutlet weak var lblTimeMessage: UILabel!
  @IBOutlet weak var imgBackgroundMessage: UIImageView!
  @IBOutlet weak var viewMessage: UIView!
  @IBOutlet weak var imgBackgroundAvatar: UIImageView!
  @IBOutlet weak var consTopImgBackgroundAvatarToContentView: NSLayoutConstraint!
  @IBOutlet weak var imgForward: UIImageView!
  @IBOutlet weak var consBottomViewMessageToContentView: NSLayoutConstraint!
  @IBOutlet weak var imgMessage: UIImageView!
  @IBOutlet weak var consHeightImgMessage: NSLayoutConstraint!

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    imgAvatar.layer.cornerRadius = imgAvatar.frame.size.height / 2
    imgAvatar.clipsToBounds = true
    selectionStyle = UITableViewCellSelectionStyle.None
    viewMessage.layer.cornerRadius = 8
    imgMessage.layer.cornerRadius = 8
  }

}
