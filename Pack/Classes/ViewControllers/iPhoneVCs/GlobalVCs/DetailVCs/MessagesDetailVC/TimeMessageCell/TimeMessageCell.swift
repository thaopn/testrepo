//
//  TimeMessageCell.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/8/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class TimeMessageCell: UITableViewCell {

  // MARK: - Outlets
  @IBOutlet weak var lblTimeMessage: UILabel!

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = UITableViewCellSelectionStyle.None
  }
  
}
