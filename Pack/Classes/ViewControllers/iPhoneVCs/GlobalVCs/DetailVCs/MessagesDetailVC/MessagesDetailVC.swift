//
//  MessagesDetailVC.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/6/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import LayerKit
import Async
import Localytics

class MessagesDetailVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet var viewHeader: UIView!
  @IBOutlet var viewFooter: UIView!
  @IBOutlet weak var lblHeader: UILabel!
  @IBOutlet weak var lblPlaceholderTv: UILabel!
  @IBOutlet weak var tvMessage: UITextView!
  @IBOutlet weak var viewFakeMessage: UIView!
  @IBOutlet weak var btnSendMessage: UIButton!
  @IBOutlet weak var consBottomViewFooterToSelfView: NSLayoutConstraint!
  @IBOutlet weak var consHeightViewFooter: NSLayoutConstraint!
  @IBOutlet weak var consHeightTvMessage: NSLayoutConstraint!
  @IBOutlet weak var consHeightLblHeader: NSLayoutConstraint!

  // MARK: - Variables
  var heightKeyboard: CGFloat = 0
  var arrMessage: [Messages] = []
  var layerClient: LYRClient = AppDelegate.shareInstance().layerClient!
  var conversation: LYRConversation?
  var queryController: LYRQueryController?
  var dateFormatter: NSDateFormatter = NSDateFormatter()
  var sheetCurrentPhoto: UIAlertController!
  var sendingImage: Bool = false
  var photo: UIImage?
  var isLoadingMore: Bool = false
  var isLoading: Bool = false
  var isNewConversation: Bool = true
  var user = User()
  var formatTime: String = "HH:mm"
  var formatDate: String = "yyyy-MM-dd"
  var formatDateTime: String = "yyyy-MM-dd HH:mm:ss"
  var type: LYRQueryControllerChangeType?
  var identifier: String = ""
  var isNeedUpdateStatus: Bool = false
  var matchId: String = ""
  let api = MessageManageAPI()
  var isTapToSendMessage: Bool = false
  var isDidLoseConnection: Bool = false
  var isShowAlertLoseConnection: Bool = false

  // MARK: - Lifecycle
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "MessagesDetailVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.Back, centerType:CenterItems.Title, rightType:RightItems.Flag)
  }

  convenience init(user: User, isNewConversation: Bool, conversation: LYRConversation?) {
    self.init(leftType:LeftItems.Back, centerType:CenterItems.Title, rightType:RightItems.Flag)
    self.user = user
    self.isNewConversation = isNewConversation
    self.conversation = conversation
  }

  convenience init(user: User, isNewConversation: Bool, identifier: String) {
    self.init(leftType:LeftItems.Back, centerType:CenterItems.Title, rightType:RightItems.Flag)
    self.user = user
    self.isNewConversation = isNewConversation
    self.identifier = identifier
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    if isNewConversation {
      setTitleForVC(user.getUserName())
    } else {
      setTitleForVC(user.username)
    }
    if user.getPrimaryImageUrl() != "" {
      user.primaryImage = user.getPrimaryImageUrl()
    }
    configViewDidLoad()
    configNotification()
    configureSheetChoosePhoto()
    DHIndicator.show()
    layerClient.delegate = self
    checkConnect()
  }

  override func tapToBack(sender: UIButton!) {
    NSNotificationCenter.defaultCenter().postNotificationName(NotificationUpdateMessage, object: nil)
    navigationController?.popViewControllerAnimated(true)
  }

  // MARK: - Public method
  func hideKeyboard() {
    tvMessage.resignFirstResponder()
    consBottomViewFooterToSelfView.constant = 0
  }

  func keyboardWillShow(notification: NSNotification) {
    UIView.beginAnimations(nil, context: nil)
    UIView.setAnimationDuration(notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double)
    UIView.setAnimationCurve(UIViewAnimationCurve(rawValue:notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! Int)!)
    UIView.setAnimationBeginsFromCurrentState(true)
    tableView.scrollEnabled = false
    tableView.decelerationRate = UIScrollViewDecelerationRateFast
    consBottomViewFooterToSelfView.constant = heightKeyboard
    view.layoutIfNeeded()
    UIView.commitAnimations()
  }

  func keyboardDidShown(notification: NSNotification) -> Void {
    updateNext()
  }

  func keyboardWillHide(notification: NSNotification) {
    UIView.beginAnimations(nil, context: nil)
    UIView.setAnimationDuration(notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double)
    UIView.setAnimationCurve(UIViewAnimationCurve(rawValue:notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! Int)!)
    UIView.setAnimationBeginsFromCurrentState(true)
    tableView.scrollEnabled = false
    tableView.decelerationRate = UIScrollViewDecelerationRateFast
    consBottomViewFooterToSelfView.constant = 0
    self.view.layoutIfNeeded()
    UIView.commitAnimations()
  }

  func keyboardDidHide(notification: NSNotification) -> Void {
    updateNext()
  }

  func languageChanged(notification: NSNotification) -> Void {
    let sizeKeyboard = notification.userInfo![UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size
    self.heightKeyboard = (sizeKeyboard?.height > sizeKeyboard?.width ? sizeKeyboard?.width:sizeKeyboard?.height)!
  }

  func checkConnect() {
    if layerClient.isConnected && !layerClient.isConnecting {
      if layerClient.authenticatedUser != nil && layerClient.authenticatedUser?.userID == AccountFlowManager.currentUser.id {
        getListMessage()
      } else {
        AppDelegate.shareInstance().authenticateLayerWithUserID(AccountFlowManager.currentUser.id, completion: { [weak self] (isSuccess, error) in
          if isSuccess {
            self?.getListMessage()
          } else {
            DHIndicator.hide()
            self?.showAlertWithError(error?.localizedDescription)
          }

          })
      }
    } else {
      connectLayerMessage()
    }
  }

  func updateNext() {
    self.tableView.scrollEnabled = true
    self.tableView.decelerationRate = UIScrollViewDecelerationRateNormal
    self.tableView.setNeedsLayout()
    self.tableView.layoutIfNeeded()
    if self.tableView.contentSize.height - self.tableView.frame.size.height > 0 {
      UIView.animateWithDuration(0.5, animations: { () -> Void in
        self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: Int(self.queryController!.numberOfObjectsInSection(UInt(0))) - 1, inSection: 0), atScrollPosition: .None, animated: false)
      })
    }
    self.tableView.setNeedsLayout()
    self.tableView.layoutIfNeeded()
  }

  func updateMessage(notification: NSNotification) {

  }

  func updateListMatched(notification: NSNotification) {

  }

  // MARK: - Private method
  private func configViewDidLoad() {
    layerClient.delegate = self
    tableView.registerNib(UINib(nibName: "YourMessageCell", bundle: nil), forCellReuseIdentifier: "YourMessageCell")
    tableView.registerNib(UINib(nibName: "MyMessageCell", bundle: nil), forCellReuseIdentifier: "MyMessageCell")
    tableView.registerNib(UINib(nibName: "ImageMessageCell", bundle: nil), forCellReuseIdentifier: "ImageMessageCell")
    tableView.registerNib(UINib(nibName: "YourImageMessageCell", bundle: nil), forCellReuseIdentifier: "YourImageMessageCell")
    tableView.separatorColor = UIColor.clearColor()
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 160.0
    tvMessage.layer.cornerRadius = 2
    tvMessage.textContainerInset = UIEdgeInsets(top: 12, left: 11, bottom: 11, right: 85)
    tvMessage.delegate = self
    viewFakeMessage.layer.cornerRadius = 2
    viewFakeMessage.layer.shadowOffset = CGSize(width: 0, height: 0)
    viewFakeMessage.layer.shadowRadius = 4.0
    viewFakeMessage.layer.shadowOpacity = 0.5
    viewFakeMessage.layer.shadowColor = UIColor(red: 188/255, green: 188/255, blue: 188/255, alpha: 1).CGColor
    let dateformatter = Common.getDateFormat()
    var dateMatch = ""
    if isNewConversation {
      let userName = user.getUserName()
      if user.dateMatch != "" {
        dateMatch = user.dateMatch
        print("DateMatch: \(dateMatch)")
      } else {
        dateMatch = dateformatter.stringFromDate(NSDate())
        user.dateMatch = dateMatch
        print("DateMatch: \(dateMatch)")
      }
      lblHeader.text = "You matched with \(userName) on \(Common.formatDateMatch(dateMatch)). Tell \(userName) about what you are looking for in a roommate"
    } else {
      dateMatch = dateformatter.stringFromDate(NSDate())
      lblHeader.text = "You matched with \(user.username) on \(Common.formatDateMatch(dateMatch)). Tell \(user.username) about what you are looking for in a roommate"
    }
    fixSizeHeader()
    let tapRec = UITapGestureRecognizer()
    tapRec.addTarget(self, action: #selector(hideKeyboard))
    self.view.addGestureRecognizer(tapRec)
  }

  private func fixSizeHeader() {
    viewHeader.setNeedsDisplay()
    viewHeader.frame = CGRectMake(viewHeader.frame.origin.x, viewHeader.frame.origin.y, viewHeader.frame.size.width, lblHeader.frame.size.height + 20)
    viewHeader.setNeedsDisplay()
    tableView.tableHeaderView = viewHeader
  }

  private func fixSizeFooter() {
    let newSize = tvMessage.sizeThatFits(CGSize(width: tvMessage.frame.size.width, height: CGFloat.max))
    if newSize.height >= 70 {
      tvMessage.scrollEnabled = true
      tvMessage.frame.size.height = 70
      consHeightTvMessage.constant = 70
    } else {
      tvMessage.scrollEnabled = false
      viewFooter.setNeedsDisplay()
      viewFooter.frame = CGRectMake(viewFooter.frame.origin.x, viewFooter.frame.origin.y, viewFooter.frame.size.width, newSize.height + 19)
    }
  }

  private func configNotification() {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(keyboardDidShown(_:)), name: UIKeyboardDidShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(keyboardDidHide(_:)), name: UIKeyboardDidHideNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(languageChanged(_:)), name: UIKeyboardWillChangeFrameNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateMessage(_:)), name: NotificationUpdateMessage, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateListMatched(_:)), name: NotificationUpdateListMatched, object: nil)
  }

  private func fetchLayerConversation() {
    let metadata = [
        "participant1": [
            "userID" : user.id,
            "displayname" : user.getUserName(),
            "avatarImageUrl" : user.getPrimaryImageUrl()
        ],
        "participant2": [
            "userID" : AccountFlowManager.currentUser.id,
            "displayname" : AccountFlowManager.currentUser.getUserName(),
            "avatarImageUrl" : AccountFlowManager.currentUser.getPrimaryImageUrl()
        ],
        "dateMatch": user.dateMatch
    ]
    let query: LYRQuery = LYRQuery(queryableClass: LYRConversation.self)
    query.predicate = LYRPredicate(property: "participants", predicateOperator: LYRPredicateOperator.IsEqualTo, value: [AccountFlowManager.currentUser.id, user.id])
    query.sortDescriptors = [NSSortDescriptor(key: "createdAt", ascending: false)]
    var error: NSError? = nil
    var conversations: NSOrderedSet?
    do {
      conversations = try layerClient.executeQuery(query)
    } catch let err as NSError {
      error = err
      conversations = nil
    }
    if error == nil {
      print("\(conversations?.count) conversations with participants \(user.id)")
    } else {
      DHIndicator.hide()
      print(error?.localizedDescription)
    }
    if conversations == nil || conversations!.count <= 0 {
      var convError: NSError? = nil
      do {
        conversation = try layerClient.newConversationWithParticipants(NSSet(array: [user.id]) as! Set<String>, options: nil)
        conversation!.setValuesForMetadataKeyPathsWithDictionary(metadata as! [String : AnyObject], merge: false)
      } catch let error as NSError {
        convError = error
        conversation = nil
      }
      DHIndicator.hide()
      if conversation == nil {
        print(convError?.localizedDescription)
      }
    }
    if conversations != nil && conversations!.count > 0 {
      conversation = conversations!.lastObject as? LYRConversation
      if conversation?.metadata == nil {
        conversation!.setValuesForMetadataKeyPathsWithDictionary(metadata as! [String : AnyObject], merge: false)
      }
      if queryController == nil {
        setUpQuerryController()
      }
    }
  }

  private func fetchLayerConversationWithidentifier(identifier: String) {
    DHIndicator.show()
    let query: LYRQuery = LYRQuery(queryableClass: LYRConversation.self)
    query.predicate = LYRPredicate(property: "identifier", predicateOperator: LYRPredicateOperator.IsEqualTo, value: identifier)
    var error: NSError? = nil
    var conversations: NSOrderedSet?
    do {
      conversations = try layerClient.executeQuery(query)
    } catch let err as NSError {
      error = err
      conversations = nil
    }
    if error == nil {
      print("\(conversations?.count) conversations with participants \(user.id)")
    } else {
      DHIndicator.hide()
      print(error?.localizedDescription)
    }
    if conversations != nil && conversations!.count > 0 {
      conversation = conversations!.firstObject as? LYRConversation
      if queryController == nil {
        setUpQuerryController()
      }
    }
  }

  private func setUpQuerryController() {
    if identifier.characters.count > 0 {
      if let metadata = conversation?.metadata {
        if let participant1 = metadata["participant1"] {
          if let userId = participant1["userID"] as? String {
            if AccountFlowManager.currentUser.id == userId {
              if let imageUrl = participant1["avatarImageUrl"] as? String {
                if imageUrl != AccountFlowManager.currentUser.getPrimaryImageUrl() {
                  conversation?.setValue(AccountFlowManager.currentUser.getPrimaryImageUrl(), forMetadataAtKeyPath: "participant1.avatarImageUrl")
                } else {

                }
              }
              if let participant2 = metadata["participant2"] {
                if let name = participant2["displayname"] as? String {
                  user.username = name
                }
                if let imageUrl = participant2["avatarImageUrl"] as? String {
                  user.primaryImage = imageUrl
                }
              }
            } else {

              if let participant2 = metadata["participant2"] {
                if let imageUrl = participant2["avatarImageUrl"] as? String {
                  if imageUrl != AccountFlowManager.currentUser.getPrimaryImageUrl() {
                    conversation?.setValue(AccountFlowManager.currentUser.getPrimaryImageUrl(), forMetadataAtKeyPath: "participant2.avatarImageUrl")
                  } else {

                  }
                }
              }
              if let name = participant1["displayname"] as? String {
                user.username = name
              }

              if let imageUrl = participant1["avatarImageUrl"] as? String {
                user.primaryImage = imageUrl
              }
            }
          }
        }
      }
      setTitleForVC(user.username)
    } else {
      if let metadata = conversation?.metadata {
        if let participant1 = metadata["participant1"] {
          if let userId = participant1["userID"] as? String {
            if AccountFlowManager.currentUser.id == userId {
              if let imageUrl = participant1["avatarImageUrl"] as? String {
                if imageUrl != AccountFlowManager.currentUser.getPrimaryImageUrl() {
                  conversation?.setValue(AccountFlowManager.currentUser.getPrimaryImageUrl(), forMetadataAtKeyPath: "participant1.avatarImageUrl")
                } else {

                }
              }

              if let participant2 = metadata["participant2"] {
                if let imageUrl = participant2["avatarImageUrl"] as? String {
                  if imageUrl != user.primaryImage {
                    conversation?.setValue(user.primaryImage, forMetadataAtKeyPath: "participant2.avatarImageUrl")
                  } else {

                  }
                }
              }
            } else {

              if let imageUrl = participant1["avatarImageUrl"] as? String {
                if imageUrl != user.primaryImage {
                  conversation?.setValue(user.primaryImage, forMetadataAtKeyPath: "participant1.avatarImageUrl")
                } else {

                }
              }

              if let participant2 = metadata["participant2"] {
                if let imageUrl = participant2["avatarImageUrl"] as? String {
                  if imageUrl != AccountFlowManager.currentUser.getPrimaryImageUrl() {
                    conversation?.setValue(AccountFlowManager.currentUser.getPrimaryImageUrl(), forMetadataAtKeyPath: "participant2.avatarImageUrl")
                  } else {

                  }
                }
              }

            }
          }
        }
      }
    }

    var dateMatch: String = ""
    if conversation?.metadata == nil || conversation?.metadata!["dateMatch"] == nil {
      let dateformatter = Common.getDateFormat()
      dateMatch = dateformatter.stringFromDate(NSDate())
    } else {
      dateMatch = (conversation?.metadata!["dateMatch"])! as! String
    }
    if !isNewConversation {
      lblHeader.text = "You matched with \(user.username) on \(Common.formatDateMatch(dateMatch)). Tell \(user.username) about what you are looking for in a roommate"
      fixSizeHeader()
    }
    let query: LYRQuery = LYRQuery(queryableClass: LYRMessage.self)
    query.predicate = LYRPredicate(property: "conversation", predicateOperator: LYRPredicateOperator.IsEqualTo, value: conversation)
    query.sortDescriptors = [NSSortDescriptor(key: "position", ascending: true)]
    queryController = try? layerClient.queryControllerWithQuery(query)
    var error: NSError?
    var isSuccess: Bool = true
    do {
      if let queryController = queryController {
        try queryController.execute()
        isSuccess = true
      } else {
        isSuccess = false
      }
    } catch let err as NSError {
      error = err
      isSuccess = false
    }
    if isSuccess {
      print("Query fetched \(queryController?.numberOfObjectsInSection(0)) messages")
      if let queryController = queryController {
        queryController.delegate = self
      }
      do {
        if let conversation = conversation {
          try conversation.markAllMessagesAsRead()
        }
      } catch let error as NSError {
        print(error)
      }
      tableView.reloadData()
      scrollToBottom()
      DHIndicator.hide()
    } else {
      DHIndicator.hide()
      print(error?.localizedDescription)
    }
  }

  private func getNumberOfMessages() -> Int {
    let message: LYRQuery = LYRQuery(queryableClass: LYRMessage.self)
    let listMessage: NSOrderedSet?
    do {
      listMessage = try layerClient.executeQuery(message)
    } catch _ {
      listMessage = nil
    }
    return listMessage != nil ? listMessage!.count : 0
  }

  private func scrollToBottom() {
    let messages: Int = getNumberOfMessages()
    if self.conversation != nil && messages > 0 {
      let numberOfRowsInSection = tableView.numberOfRowsInSection(0)
      if numberOfRowsInSection > 0 {
        let indexPath: NSIndexPath = NSIndexPath(forRow: numberOfRowsInSection - 1, inSection: 0)
        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.None, animated: false)
      }
    }
  }

  private func sendMessage(textMessage: String) {
    var messagePart: LYRMessagePart?
    var msg: String = ""
    guard let conversation = conversation else {
      return
    }
    if sendingImage {
      let image: UIImage = self.photo!
      let imageData: NSData = UIImagePNGRepresentation(image)!
      messagePart = LYRMessagePart(MIMEType: "image/png", data: imageData)
      sendingImage = false
      msg = "(Attached an image)"
    } else {
      messagePart = LYRMessagePart(text: textMessage)
      msg = textMessage
    }
    let defaultConfiguration = LYRPushNotificationConfiguration()
    defaultConfiguration.alert = "\(AccountFlowManager.currentUser.getUserName()): \(msg)"
    defaultConfiguration.sound = "layerbell.caf"
    let messageOptions = [LYRMessageOptionsPushNotificationConfigurationKey:defaultConfiguration]
    guard let messageData = messagePart else {
      return
    }
    guard let message: LYRMessage = try? layerClient.newMessageWithParts([messageData], options: messageOptions) else {
      return
    }
    var error: NSError?
    var success: Bool = true
    do {
      if layerClient.isConnected {
        if AccountFlowManager.shareInstance.checkIdentityUser() {
          try conversation.sendMessage(message)
          success = true
        } else {
          success = false
        }
      } else {
        success = false
      }

    } catch let error1 as NSError {
      error = error1
      success = false
    }
    if success {
      tvMessage.scrollEnabled = false
      tvMessage.text = ""
      lblPlaceholderTv.alpha = 1
      btnSendMessage.setTitleColor(UIColor(red: 165/255, green: 165/255, blue: 184/255, alpha: 1), forState: UIControlState.Normal)
      btnSendMessage.setImage(UIImage(named: "ic_button_send_message_disable.png"), forState: UIControlState.Normal)
      NSNotificationCenter.defaultCenter().postNotificationName(NotificationUpdateMessage, object: nil)
      if isNeedUpdateStatus {
        if matchId.characters.count > 0 {
          api.updateStatusMatched(matchId)
        }
        NSNotificationCenter.defaultCenter().postNotificationName(NotificationUpdateListMatched, object: nil)
        isNeedUpdateStatus = false
        matchId = ""
      }
    } else {
      Common.showToastWithMessage("Fail to send message")
      print(error?.localizedDescription)
    }
    if queryController == nil {
      setUpQuerryController()
    }
    isTapToSendMessage = false
  }

  private func configureSheetChoosePhoto() {
    sheetCurrentPhoto = UIAlertController(title: "SELECT PHOTO", message:nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
    let libButton: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .Default) {[weak self] action -> Void in
      if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
        let imag = UIImagePickerController()
        imag.navigationBar.barTintColor = UIColor(red: 81/255.0, green: 156/255.0, blue: 255/255.0, alpha: 1.0)
        imag.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        imag.navigationBar.tintColor = UIColor.whiteColor()
        imag.navigationBar.translucent = false
        imag.delegate = self
        imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imag.allowsEditing = true
        self!.presentViewController(imag, animated: true, completion: nil)
      } else {

      }
    }
    let takeButton: UIAlertAction = UIAlertAction(title: "Take A Photo", style: .Default) {[weak self] action -> Void in
      if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
        let imag = UIImagePickerController()
        imag.navigationBar.barTintColor = UIColor(red: 81/255.0, green: 156/255.0, blue: 255/255.0, alpha: 1.0)
        imag.navigationBar.tintColor = UIColor.whiteColor()
        imag.navigationBar.translucent = false
        imag.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        imag.delegate = self
        imag.sourceType = UIImagePickerControllerSourceType.Camera
        imag.allowsEditing = true
        self!.presentViewController(imag, animated: true, completion: nil)
      } else {

      }
    }
    let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in

    }
    sheetCurrentPhoto?.addAction(libButton)
    sheetCurrentPhoto?.addAction(takeButton)
    sheetCurrentPhoto?.addAction(cancelAction)
  }

  private func resizeImage(image: UIImage, size: CGSize) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    image.drawInRect(CGRectMake(0, 0, size.width, size.height))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage
  }

  private func showAlertWithError(message: String!) {
    let alertView: UIAlertController = UIAlertController(title: appName, message: message, preferredStyle:UIAlertControllerStyle.Alert)
    let action = UIAlertAction(title: "Retry", style: UIAlertActionStyle.Default) { _ in
      self.checkConnect()
    }
    let actionCancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { _ in

    }
    alertView.addAction(action)
    alertView.addAction(actionCancel)
    self.presentViewController(alertView, animated: true, completion: nil)
  }

  private func connectLayerMessage() {
    DHIndicator.show()
    layerClient.connectWithCompletion({ [weak self] (isSuccess, error) in
      if isSuccess {
        AppDelegate.shareInstance().authenticateLayerWithUserID(AccountFlowManager.currentUser.id, completion: { [weak self]
          (isSuccess, error) in
          if isSuccess {
            print("Connect success")
            if self!.isNewConversation {
              Async.main(after: 0.5, block: {
                self?.fetchLayerConversation()
              })
            } else {
              if self!.identifier == "" {
                Async.main(after: 0.5, block: {
                  self?.setUpQuerryController()
                })
              } else {
                Async.main(after: 0.5, block: {
                  self?.fetchLayerConversationWithidentifier(self!.identifier)
                })
              }
            }
          } else {
            DHIndicator.hide()
            self?.showAlertWithError(error?.localizedDescription)
          }
        })
      } else {
        DHIndicator.hide()
        if error?.code != 6000 && error?.code != 6002 {
          self?.showAlertWithError(error?.localizedDescription)
        }
      }
      })
  }

  private func getTimeForFirstMessage(message: LYRMessage?) -> String {
    var timeMessage: String = ""
    var dateWithTime: NSDate?
    dateFormatter.dateFormat = formatDateTime
    if let sentAt = message?.sentAt {
      dateWithTime = dateFormatter.dateFromString(dateFormatter.stringFromDate(sentAt))
    } else {
      dateWithTime = dateFormatter.dateFromString(dateFormatter.stringFromDate(DHCalendarData.getCurrentDate()))
    }
    dateFormatter.dateFormat = formatDate
    if let dateWithTime = dateWithTime {
      if let date = dateFormatter.dateFromString(dateFormatter.stringFromDate(dateWithTime)) {
        if DHCalendarData.equalToDate(date, ToCompare: DHCalendarData.getCurrentDate()) {
          dateFormatter.dateFormat = formatTime
          let time = Common.formatTime(dateFormatter.stringFromDate(dateWithTime))
          timeMessage = "TODAY \(time)"
        } else if DHCalendarData.checkDateInWeekCurrent(date) {
          var day = ""
          if DHCalendarData.equalToDate(date, ToCompare: DHCalendarData.prevDateFrom(DHCalendarData.getCurrentDate())) {
            day = "YESTERDAY"
          } else {
            day = DHCalendarData.nameDayWithNumberDay(DHCalendarData.getWeekDayWithDate(date))
          }
          dateFormatter.dateFormat = formatTime
          let time = Common.formatTime(dateFormatter.stringFromDate(dateWithTime))
          timeMessage = "\(day) \(time)"
        } else {
          dateFormatter.dateFormat = formatTime
          let time = dateFormatter.stringFromDate((message?.sentAt)!)
          timeMessage = "\(Common.formatDate(message!.sentAt!)) \(Common.formatTime(time))"
        }
      }
    }
    return timeMessage
  }

  private func getTimeForMessage(date: NSDate?, dateMessage: NSDate?) -> String {
    var timeMessage: String = ""
    if DHCalendarData.equalToDate(date!, ToCompare: DHCalendarData.getCurrentDate()) {
      dateFormatter.dateFormat = formatTime
      let time = Common.formatTime(dateFormatter.stringFromDate(dateMessage!))
      timeMessage = "TODAY \(time)"
    } else if DHCalendarData.checkDateInWeekCurrent(date!) {
      var day = ""
      if DHCalendarData.equalToDate(date!, ToCompare: DHCalendarData.prevDateFrom(DHCalendarData.getCurrentDate())) {
        day = "YESTERDAY"
      } else {
        day = DHCalendarData.nameDayWithNumberDay(DHCalendarData.getWeekDayWithDate(date!))
      }
      dateFormatter.dateFormat = formatTime
      let time = Common.formatTime(dateFormatter.stringFromDate(dateMessage!))
      timeMessage = "\(day) \(time)"
    } else {
      dateFormatter.dateFormat = formatTime
      let time = dateFormatter.stringFromDate(dateMessage!)
      timeMessage = "\(Common.formatDate(dateMessage!)) \(Common.formatTime(time))"
    }
    return timeMessage
  }

  private func getDefaultCell() -> UITableViewCell {
    var cell = tableView.dequeueReusableCellWithIdentifier("cell")
    if cell == nil {
      cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
    }
    return cell!
  }

  private func getStateMessage(message: LYRMessage?) -> String {
    var state: String = ""
    switch message!.recipientStatusForUserID(user.id) {
    case LYRRecipientStatus.Sent:
      dateFormatter.dateFormat = "HH:mm"
      state = "Sent \(dateFormatter.stringFromDate((message?.sentAt)!))"
      break
    case LYRRecipientStatus.Delivered:
      dateFormatter.dateFormat = "HH:mm"
      state = "Delivered"
      break
    case LYRRecipientStatus.Read:
      dateFormatter.dateFormat = "HH:mm"
      state = "Read \(dateFormatter.stringFromDate((message?.receivedAt)!))"
      break
    case LYRRecipientStatus.Invalid:
      state = ""
      break
    case LYRRecipientStatus.Pending:
      state = "Pending"
      break
    }
    return state
  }

  private func getListMessage() {
    if isNewConversation {
      Async.main(after: 0.5, block: {
        self.fetchLayerConversation()
      })
    } else {
      if identifier == "" {
        Async.main(after: 0.5, block: {
          self.setUpQuerryController()
        })
      } else {
        Async.main(after: 0.5, block: {
          self.fetchLayerConversationWithidentifier(self.identifier)
        })
      }
    }
  }

  private func localyticsSendMessage(targetUser: User, message: String) {
    let extraAttributes = ["Action": "SendMessage",
                           "OwnerUserID": AccountFlowManager.currentUser.id,
                           "OwnerUserFirstName": AccountFlowManager.currentUser.firstName,
                           "OwnerUserLastName": AccountFlowManager.currentUser.lastName,
                           "OwnerUserFullName": AccountFlowManager.currentUser.fullName,
                           "TargetUserID": targetUser.id,
                           "TargetUserFirstName": targetUser.firstName,
                           "TargetUserLastName": targetUser.lastName,
                           "TargetUserFullName": targetUser.fullName,
                           "Content": message]
    Localytics.tagEvent("SendMessage", attributes: extraAttributes)
  }

  private func reportUser() {
    DHIndicator.show()
    AccountFlowManager.shareInstance.reportUser(user.id) { (error) in
      DHIndicator.hide()
      if let e = error {
        Common.showToastWithMessage(e.errMessage)
      } else {
        JLToast.makeText(textReportSuccess, duration: 5).show()
      }
    }
  }

  // MARK: - Actions
  override func tapToFlag(sender: UIButton!) {
    let alertView = UIAlertController(title: appName, message: textConfirmReport, preferredStyle:UIAlertControllerStyle.Alert)
    let actionYes = UIAlertAction(title: "Yes", style: .Default) { [weak self] (action) in
      self?.reportUser()
    }
    alertView.addAction(actionYes)
    let actionNo = UIAlertAction(title: "No", style: .Default) { (action) in

    }
    alertView.addAction(actionNo)
    presentViewController(alertView, animated: true, completion: nil)
  }

  @IBAction func tapToSendMessage(sender: AnyObject) {
    var message = tvMessage.text
    if message.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != "" {
      message = message.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
      isTapToSendMessage = true
      if layerClient.isConnected {
        localyticsSendMessage(user, message: message)
        sendMessage(message)
      } else {
        showAlertWithError(messageNotConnected)
      }
    }
  }

  @IBAction func tapToSendImage(sender: AnyObject) {
    self.presentViewController(sheetCurrentPhoto, animated: true, completion: nil)
  }

}

// MARK: - UITableView DataSource
extension MessagesDetailVC: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if queryController == nil {
      return 0
    } else {
      return Int(queryController!.numberOfObjectsInSection(UInt(0)))
    }
  }
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    guard let query = queryController else {
      return getDefaultCell()
    }
    guard let message = query.objectAtIndexPath(indexPath) as? LYRMessage else {
      return getDefaultCell()
    }
    guard let messagePart: LYRMessagePart = message.parts[0] else {
      return getDefaultCell()
    }
    if message.sender.userID == AccountFlowManager.currentUser.id {
      if messagePart.MIMEType == "image/png" {
        let cell = tableView.dequeueReusableCellWithIdentifier("ImageMessageCell", forIndexPath: indexPath) as! ImageMessageCell
        if let data = messagePart.data {
          if let image = UIImage(data: data) {
            let ratioImage = image.size.height / image.size.width
            let height = 200 * ratioImage
            cell.consHeightImgMessage.constant = height
            cell.imgMessage!.image = resizeImage(image, size: CGSize(width: 200, height: height))
          }
          if indexPath.row == 0 {
            cell.lblTimeMessage.text = getTimeForFirstMessage(message)
            cell.viewTimeMessage.alpha = 1
            cell.imgBackgroundMessage.alpha = 1
            cell.consTopViewMessageToContentView.constant = 40
          } else {
            let preIndexPath = NSIndexPath(forRow: indexPath.row - 1, inSection: indexPath.section)
            let preMessage = query.objectAtIndexPath(preIndexPath) as? LYRMessage
            var dateMessage: NSDate?
            var datePreMessage: NSDate?
            dateFormatter.dateFormat = formatDateTime
            if let preMessage = preMessage {
              if let preSentAt = preMessage.sentAt {
                datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(preSentAt))
              } else {
                datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
              }
            } else {
              datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
            }
            if let sentAt = message.sentAt {
              dateMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(sentAt))
            } else {
              dateMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(DHCalendarData.getCurrentDate()))
            }
            dateFormatter.dateFormat = formatDate
            if dateMessage != nil  && datePreMessage != nil {
              let date = dateFormatter.dateFromString(dateFormatter.stringFromDate(dateMessage!))
              let preDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(datePreMessage!))
              if date != nil && preDate != nil {
                if DHCalendarData.equalToDate(date!, ToCompare: preDate!) {
                  dateFormatter.dateFormat = formatDateTime
                  if DHCalendarData.compareTwoMoment(dateFormatter.stringFromDate(datePreMessage!), toDate: dateFormatter.stringFromDate(dateMessage!)) {
                    cell.lblTimeMessage.text = getTimeForMessage(date, dateMessage: dateMessage)
                    cell.viewTimeMessage.alpha = 1
                    cell.imgBackgroundMessage.alpha = 1
                    cell.consTopViewMessageToContentView.constant = 40
                  } else {
                    cell.viewTimeMessage.alpha = 0
                    if preMessage!.sender.userID == AccountFlowManager.currentUser.id {
                      cell.imgBackgroundMessage.alpha = 0
                      cell.consTopViewMessageToContentView.constant = 0
                    } else {
                      cell.imgBackgroundMessage.alpha = 1
                      cell.consTopViewMessageToContentView.constant = 4
                    }
                  }
                } else {
                  cell.lblTimeMessage.text = getTimeForMessage(date, dateMessage: dateMessage)
                  cell.viewTimeMessage.alpha = 1
                  cell.imgBackgroundMessage.alpha = 1
                  cell.consTopViewMessageToContentView.constant = 40
                }
              }
            }
          }
          if indexPath.row == tableView.numberOfRowsInSection(0) - 1 {
            cell.lblTimeRead.text = getStateMessage(message)
            cell.consTopLblTimeReadToViewMessage.constant = 7
            cell.consBottomLblTimeReadToContentView.constant = 8
          } else {
            cell.lblTimeRead.text = ""
            cell.consTopLblTimeReadToViewMessage.constant = 0
            cell.consBottomLblTimeReadToContentView.constant = 6
          }
        }
        return cell
      } else {
        let cell = tableView.dequeueReusableCellWithIdentifier("MyMessageCell", forIndexPath: indexPath) as! MyMessageCell
        if indexPath.row == 0 {
          cell.lblTimeMessage.text = getTimeForFirstMessage(message)
          cell.viewTimeMessage.alpha = 1
          cell.imgBackgroundMessage.alpha = 1
          cell.consTopViewMessageToContentView.constant = 40
        } else {
          let preIndexPath = NSIndexPath(forRow: indexPath.row - 1, inSection: indexPath.section)
          let preMessage = query.objectAtIndexPath(preIndexPath) as? LYRMessage
          var dateMessage: NSDate?
          var datePreMessage: NSDate?
          dateFormatter.dateFormat = formatDateTime
          if let preMessage = preMessage {
            if let preSentAt = preMessage.sentAt {
              datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(preSentAt))
            } else {
              datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
            }
          } else {
            datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
          }
          if let sentAt = message.sentAt {
            dateMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(sentAt))
          } else {
            dateMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(DHCalendarData.getCurrentDate()))
          }
          dateFormatter.dateFormat = formatDate
          if dateMessage != nil  && datePreMessage != nil {
            let date = dateFormatter.dateFromString(dateFormatter.stringFromDate(dateMessage!))
            let preDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(datePreMessage!))
            if date != nil && preDate != nil {
              if DHCalendarData.equalToDate(date!, ToCompare: preDate!) {
                dateFormatter.dateFormat = formatDateTime
                if DHCalendarData.compareTwoMoment(dateFormatter.stringFromDate(datePreMessage!), toDate: dateFormatter.stringFromDate(dateMessage!)) {
                  cell.lblTimeMessage.text = getTimeForMessage(date, dateMessage: dateMessage)
                  cell.viewTimeMessage.alpha = 1
                  cell.imgBackgroundMessage.alpha = 1
                  cell.consTopViewMessageToContentView.constant = 40
                } else {
                  cell.viewTimeMessage.alpha = 0
                  if preMessage!.sender.userID == AccountFlowManager.currentUser.id {
                    cell.imgBackgroundMessage.alpha = 0
                    cell.consTopViewMessageToContentView.constant = 0
                  } else {
                    cell.imgBackgroundMessage.alpha = 1
                    cell.consTopViewMessageToContentView.constant = 4
                  }
                }
              } else {
                cell.lblTimeMessage.text = getTimeForMessage(date, dateMessage: dateMessage)
                cell.viewTimeMessage.alpha = 1
                cell.imgBackgroundMessage.alpha = 1
                cell.consTopViewMessageToContentView.constant = 40
              }
            }
          }
        }

        if let data = messagePart.data {
          cell.lblMessage.text = NSString(data: data, encoding: NSUTF8StringEncoding) as? String
        }
        if indexPath.row == tableView.numberOfRowsInSection(0) - 1 {
          cell.lblTimeRead.text = getStateMessage(message)
          cell.consTopLblTimeReadToViewMessage.constant = 7
          cell.consBottomLblTimeReadToContentView.constant = 8
        } else {
          cell.lblTimeRead.text = ""
          cell.consTopLblTimeReadToViewMessage.constant = 0
          cell.consBottomLblTimeReadToContentView.constant = 6
        }
        return cell
      }
    } else {
      if messagePart.MIMEType == "image/png" {
        let cell = tableView.dequeueReusableCellWithIdentifier("YourImageMessageCell", forIndexPath: indexPath) as! YourImageMessageCell
        if let data = messagePart.data {
          if let image = UIImage(data: data) {
            let ratioImage = image.size.height / image.size.width
            let height = 200 * ratioImage
            cell.consHeightImgMessage.constant = height
            cell.imgMessage!.image = resizeImage(image, size: CGSize(width: 200, height: height))
          }
          let array = user.primaryImage.componentsSeparatedByString("/")
          if array.count > 3 {

          } else {
            user.primaryImage = apiHost + user.primaryImage
          }
          if let url = NSURL(string: user.primaryImage) {
            cell.imgAvatar.kf_setImageWithURL(url)
          }
          if indexPath.row == 0 {
            cell.lblTimeMessage.text = getTimeForFirstMessage(message)
            cell.viewTimeMessage.alpha = 1
            cell.imgBackgroundMessage.alpha = 1
            cell.imgAvatar.alpha = 1
            cell.imgBackgroundAvatar.alpha = 1
            cell.imgForward.alpha = 1
            cell.consTopImgBackgroundAvatarToContentView.constant = 38
          } else {
            let preIndexPath = NSIndexPath(forRow: indexPath.row - 1, inSection: indexPath.section)
            let preMessage = query.objectAtIndexPath(preIndexPath) as? LYRMessage
            var dateMessage: NSDate?
            var datePreMessage: NSDate?
            dateFormatter.dateFormat = formatDateTime
            if let preMessage = preMessage {
              if let preSentAt = preMessage.sentAt {
                datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(preSentAt))
              } else {
                datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
              }
            } else {
              datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
            }
            if let sentAt = message.sentAt {
              dateMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(sentAt))
            } else {
              dateMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(DHCalendarData.getCurrentDate()))
            }
            dateFormatter.dateFormat = formatDate
            if dateMessage != nil  && datePreMessage != nil {
              let date = dateFormatter.dateFromString(dateFormatter.stringFromDate(dateMessage!))
              let preDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(datePreMessage!))
              if date != nil && preDate != nil {
                if DHCalendarData.equalToDate(date!, ToCompare: preDate!) {
                  dateFormatter.dateFormat = formatDateTime
                  if DHCalendarData.compareTwoMoment(dateFormatter.stringFromDate(datePreMessage!), toDate: dateFormatter.stringFromDate(dateMessage!)) {
                    cell.lblTimeMessage.text = getTimeForMessage(date, dateMessage: dateMessage)
                    cell.viewTimeMessage.alpha = 1
                    cell.imgBackgroundMessage.alpha = 1
                    cell.imgAvatar.alpha = 1
                    cell.imgBackgroundAvatar.alpha = 1
                    cell.imgForward.alpha = 1
                    cell.consTopImgBackgroundAvatarToContentView.constant = 38
                  } else {
                    cell.viewTimeMessage.alpha = 0
                    if preMessage!.sender.userID == AccountFlowManager.currentUser.id {
                      cell.imgBackgroundMessage.alpha = 1
                      cell.imgAvatar.alpha = 1
                      cell.imgBackgroundAvatar.alpha = 1
                      cell.imgForward.alpha = 1
                      cell.consTopImgBackgroundAvatarToContentView.constant = 4
                    } else {
                      cell.imgBackgroundMessage.alpha = 0
                      cell.imgAvatar.alpha = 0
                      cell.imgBackgroundAvatar.alpha = 0
                      cell.imgForward.alpha = 0
                      cell.consTopImgBackgroundAvatarToContentView.constant = 0
                    }
                  }
                } else {
                  cell.lblTimeMessage.text = getTimeForMessage(date, dateMessage: dateMessage)
                  cell.viewTimeMessage.alpha = 1
                  cell.imgBackgroundMessage.alpha = 1
                  cell.imgAvatar.alpha = 1
                  cell.imgBackgroundAvatar.alpha = 1
                  cell.imgForward.alpha = 1
                  cell.consTopImgBackgroundAvatarToContentView.constant = 38
                }
              }
            }
          }
          do {
            try message.markAsRead()
          } catch _ {

          }
        }
        return cell
      } else {
        let cell = tableView.dequeueReusableCellWithIdentifier("YourMessageCell", forIndexPath: indexPath) as! YourMessageCell
        let array = user.primaryImage.componentsSeparatedByString("/")
        if array.count > 3 {

        } else {
          user.primaryImage = apiHost + user.primaryImage
        }
        if let url = NSURL(string: user.primaryImage) {
          cell.imgAvatar.kf_setImageWithURL(url)
        }
        if indexPath.row == 0 {
          cell.lblTimeMessage.text = getTimeForFirstMessage(message)
          cell.viewTimeMessage.alpha = 1
          cell.imgBackgroundMessage.alpha = 1
          cell.imgAvatar.alpha = 1
          cell.imgBackgroundAvatar.alpha = 1
          cell.imgForward.alpha = 1
          cell.consTopImgBackgroundAvatarToContentView.constant = 38
        } else {
          let preIndexPath = NSIndexPath(forRow: indexPath.row - 1, inSection: indexPath.section)
          let preMessage = query.objectAtIndexPath(preIndexPath) as? LYRMessage
          var dateMessage: NSDate?
          var datePreMessage: NSDate?
          dateFormatter.dateFormat = formatDateTime
          if let preMessage = preMessage {
            if let preSentAt = preMessage.sentAt {
              datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(preSentAt))
            } else {
              datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
            }
          } else {
            datePreMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
          }
          if let sentAt = message.sentAt {
            dateMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(sentAt))
          } else {
            dateMessage = dateFormatter.dateFromString(dateFormatter.stringFromDate(DHCalendarData.getCurrentDate()))
          }
          dateFormatter.dateFormat = formatDate
          if dateMessage != nil  && datePreMessage != nil {
            let date = dateFormatter.dateFromString(dateFormatter.stringFromDate(dateMessage!))
            let preDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(datePreMessage!))
            if date != nil && preDate != nil {
              if DHCalendarData.equalToDate(date!, ToCompare: preDate!) {
                dateFormatter.dateFormat = formatDateTime
                if DHCalendarData.compareTwoMoment(dateFormatter.stringFromDate(datePreMessage!), toDate: dateFormatter.stringFromDate(dateMessage!)) {
                  cell.lblTimeMessage.text = getTimeForMessage(date, dateMessage: dateMessage)
                  cell.viewTimeMessage.alpha = 1
                  cell.imgBackgroundMessage.alpha = 1
                  cell.imgAvatar.alpha = 1
                  cell.imgBackgroundAvatar.alpha = 1
                  cell.imgForward.alpha = 1
                  cell.consTopImgBackgroundAvatarToContentView.constant = 38
                } else {
                  cell.viewTimeMessage.alpha = 0
                  if preMessage!.sender.userID == AccountFlowManager.currentUser.id {
                    cell.imgBackgroundMessage.alpha = 1
                    cell.imgAvatar.alpha = 1
                    cell.imgBackgroundAvatar.alpha = 1
                    cell.imgForward.alpha = 1
                    cell.consTopImgBackgroundAvatarToContentView.constant = 4
                  } else {
                    cell.imgBackgroundMessage.alpha = 0
                    cell.imgAvatar.alpha = 0
                    cell.imgBackgroundAvatar.alpha = 0
                    cell.imgForward.alpha = 0
                    cell.consTopImgBackgroundAvatarToContentView.constant = 0
                  }
                }
              } else {
                cell.lblTimeMessage.text = getTimeForMessage(date, dateMessage: dateMessage)
                cell.viewTimeMessage.alpha = 1
                cell.imgBackgroundMessage.alpha = 1
                cell.imgAvatar.alpha = 1
                cell.imgBackgroundAvatar.alpha = 1
                cell.imgForward.alpha = 1
                cell.consTopImgBackgroundAvatarToContentView.constant = 38
              }
            }
          }
        }
        if let data = messagePart.data {
          cell.lblMessage.text = NSString(data: data, encoding: NSUTF8StringEncoding) as? String
        }
        do {
          try message.markAsRead()
        } catch _ {

        }
        return cell
      }
    }
  }
}

// MARK: - UITextField Delegate
extension MessagesDetailVC: UITextViewDelegate {
  func textViewDidChange(textView: UITextView) {
    if textView.hasText() {
      lblPlaceholderTv.alpha = 0
      tvMessage.scrollEnabled = true
      btnSendMessage.userInteractionEnabled = true
      btnSendMessage.setTitleColor(UIColor(red: 81/255, green: 156/255, blue: 255/255, alpha: 1), forState: UIControlState.Normal)
      btnSendMessage.setImage(UIImage(named: "ic_button_send_message.png"), forState: UIControlState.Normal)
    } else {
      lblPlaceholderTv.alpha = 1
      btnSendMessage.userInteractionEnabled = false
      tvMessage.scrollEnabled = false
      tvMessage.frame.size.height = 40
      consHeightTvMessage.constant = 40
      btnSendMessage.setTitleColor(UIColor(red: 165/255, green: 165/255, blue: 184/255, alpha: 1), forState: UIControlState.Normal)
      btnSendMessage.setImage(UIImage(named: "ic_button_send_message_disable.png"), forState: UIControlState.Normal)
    }
    fixSizeFooter()
  }
}

// MARK: - LYRQueryControllerDelegate
extension MessagesDetailVC: LYRQueryControllerDelegate {
  func queryControllerWillChangeContent(queryController: LYRQueryController) {

  }
  func queryController(controller: LYRQueryController, didChangeObject object: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: LYRQueryControllerChangeType, newIndexPath: NSIndexPath?) {
    tableView.reloadData()
  }
  func queryControllerDidChangeContent(controller: LYRQueryController) {
    scrollToBottom()
  }
}

// MARK: - UIImagePickerControllerDelegate
extension MessagesDetailVC: UIImagePickerControllerDelegate {
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
    let image = info[UIImagePickerControllerEditedImage] as! UIImage
    sendingImage = true
    photo = image
    isTapToSendMessage = true
    sendMessage("")
    picker.dismissViewControllerAnimated(true, completion: nil)
  }
}

// MARK: - LYRClientDelegate
extension MessagesDetailVC: LYRClientDelegate {
  func layerClient(client: LYRClient, didReceiveAuthenticationChallengeWithNonce nonce: String) {
    AppDelegate.shareInstance().requestIdentityTokenForUserID(AccountFlowManager.currentUser.id, nonce: nonce) { (isSuccess, error) in
      if isSuccess {
        if self.isTapToSendMessage {
          DHIndicator.hide()
        } else {
          self.getListMessage()
        }
      } else {
        DHIndicator.hide()
        print("Request identity token for user id with error: \(error?.localizedDescription)")
      }
    }
  }

  func layerClientDidConnect(client: LYRClient) {
    if isDidLoseConnection {
      DHIndicator.show()
      checkConnect()
    }
  }

  func layerClientDidDisconnect(client: LYRClient) {
    DHIndicator.hide()
    isDidLoseConnection = true
  }

  func layerClient(client: LYRClient, objectsDidChange changes: [LYRObjectChange]) {
    DHIndicator.hide()
  }

  func layerClient(client: LYRClient, didLoseConnectionWithError error: NSError) {
    isDidLoseConnection = true
  }

  func layerClient(client: LYRClient, willAttemptToConnect attemptNumber: UInt, afterDelay delayInterval: NSTimeInterval, maximumNumberOfAttempts attemptLimit: UInt) {
    if attemptNumber > 1 {
      isDidLoseConnection = true
    } else {
      isDidLoseConnection = false
    }
    if isDidLoseConnection {
      DHIndicator.hide()
    } else {

    }
  }
}

extension MessagesDetailVC: UINavigationControllerDelegate {

}
