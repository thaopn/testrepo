//
//  CreateProfileVC2.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/12/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

protocol ChooseCollegeDelegate: NSObjectProtocol {
  func didSelectedCollege(currentCollege: String)
}

class CreateProfileVC2: GGParentVC {
  
  // MARK: Outlets
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: Variables
  weak var delegate: ChooseCollegeDelegate?
  var isNewYorkUniver: Bool = false
  var isHavardUniver: Bool = false
  var isNewYorkSchool: Bool = false
  var currentCollege: String = ""

  private var dicSchool = [String: [SchoolObj]]()
  private var listHeader = [String]()

  // MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.

    setTitleForVC("Choose College")

    tableView.registerNib(UINib(nibName: "SchoolCell", bundle: nil ), forCellReuseIdentifier: "SchoolCell")
    tableView.contentInset.top = 64
    tableView.separatorInset.left = 10000
    buildListSchool()
  }

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "CreateProfileVC2", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.Back, centerType:CenterItems.Title, rightType:RightItems.None)
  }

  // MARK: - Privates
  private func buildListSchool() {
    var didChoose = false

    var dic = [String: [SchoolObj]]()
    for obj in User.getListSchoolObject(currentCollege) {
      if obj.sChoose == "co" {
        didChoose = true
      }
      if var value = dic[obj.sType] {
        value.append(obj)
        dic[obj.sType] = value
      } else {
        let value = [obj]
        dic[obj.sType] = value
      }
    }

    let noneObj = SchoolObj()
    noneObj.sName = textNone
    if didChoose == false {
      noneObj.sChoose = "co"
    }
    dic[textChooseCollegeNoticeNone] = [noneObj]

    dicSchool = dic
  }

  private func resetStatus() {
    for key in dicSchool.keys {
      if let list = dicSchool[key] {
        for obj in list {
          obj.sChoose = "0"
        }
      }
    }
  }

  private func getJsonStringFromDic() -> String {
    var listObject = [SchoolObj]()
    for key in dicSchool.keys {
      if let list = dicSchool[key] {
        for obj in list {
          listObject.append(obj)
        }
      }
    }

    if listObject.count > 0 {
      return User.convertEducationToJsonString(listObject)
    }
    return ""
  }

  // MARK: - Actions
  override func tapToBack(sender: UIButton!) {
    navigationController?.popViewControllerAnimated(true)

    dicSchool.removeValueForKey(textChooseCollegeNoticeNone)
    currentCollege = getJsonStringFromDic()

    delegate?.didSelectedCollege(currentCollege)
  }

}

// MARK: - Tableview Datasource
extension CreateProfileVC2: UITableViewDataSource {

  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    listHeader = Array(dicSchool.keys)
    return listHeader.count
  }

  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let key = listHeader[section]
    if let list = dicSchool[key] {
      return list.count
    }
    return 0
  }

  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("SchoolCell") as! SchoolCell
    if let list = dicSchool[listHeader[indexPath.section]] {
      let obj = list[indexPath.row]
      cell.lblSchoolName.text = obj.sName
      if obj.sChoose == "co" {
        cell.imvStatus.image = UIImage(named: "img_checked_update_profile.png")
        cell.lblSchoolName.textColor = UIColor(red: 63/255, green: 63/255, blue: 66/255, alpha: 1)
      } else {
        cell.imvStatus.image = UIImage(named: "img_uncheck_update_profile.png")
        cell.lblSchoolName.textColor = UIColor(red: 165/255, green: 165/255, blue: 184/255, alpha: 1)
      }
    }
    cell.preservesSuperviewLayoutMargins = false
    cell.layoutMargins = UIEdgeInsetsZero
    cell.separatorInset = UIEdgeInsetsZero
    return cell
  }

}

// MARK: - Tableview Delegate
extension CreateProfileVC2: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    let lbl = UILabel(frame: CGRect(x: 19, y: 20, width: screenWidth, height: 20))
    lbl.text = listHeader[section]
    lbl.font = getRobotoMediumWith(14)
    lbl.textColor = UIColor(red: 63/255, green: 63/255, blue: 63/255, alpha: 1)
    lbl.numberOfLines = 0
    let size = lbl.sizeThatFits(CGSize(width: screenWidth - 40, height: 1000))
    lbl.frame.size = size


    return size.height + 24
  }

  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let lbl = UILabel(frame: CGRect(x: 19, y: 20, width: screenWidth, height: 20))
    lbl.text = listHeader[section]
    lbl.font = getRobotoMediumWith(14)
    lbl.textColor = UIColor(red: 63/255, green: 63/255, blue: 63/255, alpha: 1)
    lbl.numberOfLines = 0
    let size = lbl.sizeThatFits(CGSize(width: screenWidth - 40, height: 1000))
    lbl.frame.size = size

    let view = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: CGRectGetMaxY(lbl.frame) + 10))
    view.backgroundColor = UIColor(red: 237/255, green: 237/255, blue: 249/255, alpha: 1)


    view.addSubview(lbl)

    return view
  }

  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 44
  }

  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: false)

    resetStatus()

    if let list = dicSchool[listHeader[indexPath.section]] {
      let obj = list[indexPath.row]
        obj.sChoose = "co"
    }
    tableView.reloadData()
  }
}
