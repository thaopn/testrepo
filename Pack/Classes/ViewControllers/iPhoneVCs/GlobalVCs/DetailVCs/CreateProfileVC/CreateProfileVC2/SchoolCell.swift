//
//  SchoolCell.swift
//  Pack
//
//  Created by admin on 5/23/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class SchoolCell: UITableViewCell {
  
  // MARK: - Outlets
  @IBOutlet weak var lblSchoolName: UILabel!
  @IBOutlet weak var imvStatus: UIImageView!

  override func awakeFromNib() {

  }

}
