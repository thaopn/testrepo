//
//  CreateProfileVC.swift
//  DebugView
//
//  Created by Pham Ngoc Thao on 5/7/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper
import Async

class CreateProfileVC: GGParentVC {
  
  // MARK: - Outlets
  @IBOutlet weak var viewContent: UIView!
  @IBOutlet weak var viewPhoto: UIView!
  @IBOutlet weak var lblLocationContent: UILabel!
  @IBOutlet weak var lblLocationTitle: UILabel!
  @IBOutlet weak var viewAbout: UIView!
  @IBOutlet weak var lblAboutTitle: UILabel!
  @IBOutlet weak var lblAboutContent: UILabel!
  @IBOutlet weak var lblGenderTitle: UILabel!
  @IBOutlet weak var lblGenderContent: UILabel!
  @IBOutlet weak var lblAgeTitle: UILabel!
  @IBOutlet weak var lblAgeContent: UILabel!
  @IBOutlet weak var lblBudgetTitle: UILabel!
  @IBOutlet weak var lblCompanyTitle: UILabel!
  @IBOutlet weak var lblCompanyContent: UILabel!
  @IBOutlet weak var lblSchoolTitle: UILabel!
  @IBOutlet weak var lblSchoolContent: UILabel!
  @IBOutlet weak var viewPersonalities: UIView!
  @IBOutlet weak var consHeightPersonalities: NSLayoutConstraint!
  @IBOutlet weak var lblPersonalitiesTitle: UILabel!
  @IBOutlet weak var lblInstagramTitle: UILabel!
  @IBOutlet weak var lblLinkedinTitle: UILabel!
  @IBOutlet weak var btnConnectInstagram: UIButton!
  @IBOutlet weak var imvConnectInstagram: UIImageView!
  @IBOutlet weak var btnConnectLinkedin: UIButton!
  @IBOutlet weak var imvConnectLinkedin: UIImageView!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var lblMoveInDateContent: UILabel!
  @IBOutlet weak var lblMoveInDateTitle: UILabel!
  @IBOutlet weak var sliderBudget: NMRangeSlider!
  @IBOutlet weak var consLblBudgetMax: NSLayoutConstraint!
  @IBOutlet weak var consLblBudgetMin: NSLayoutConstraint!
  @IBOutlet weak var lblBudgetMin: UILabel!
  @IBOutlet weak var lblBudgetMax: UILabel!
  @IBOutlet weak var viewGender: UIView!
  @IBOutlet weak var tvAbout: UITextView!
  @IBOutlet weak var lblConnectInstagram: UILabel!
  @IBOutlet weak var lblConnectLinkedin: UILabel!
  @IBOutlet weak var imvSyncCompany: UIImageView!
  @IBOutlet weak var consLeftImvSync: NSLayoutConstraint!
  @IBOutlet weak var lblCharacterAbout: UILabel!
  @IBOutlet weak var consHeightViewAbout: NSLayoutConstraint!
  @IBOutlet weak var consTopShareToViewSchool: NSLayoutConstraint!
  @IBOutlet weak var consTopViewIstaToViewPersonal: NSLayoutConstraint!
  
  // MARK: - Variables
  
  var pickerView = NSPickerView()
  var budgetMin: Float = 0
  var budgetMax: Float = 5000
  var location: ResourceObject?
  
  private var activeView: UIView?
  private var profilePhotos: ProfilePhotoView?
  private var listPersonal: [Personalities] = []
  private var personalityView: PersonalitiesView = PersonalitiesView()
  private var page: Int = 1
  private var timePicker: NMTimePickerView = NMTimePickerView(false)
  private var timeMoveInDatePicker = NMTimePickerView(true)
  private var dob: NSDate = NSDate()
  private var moveInDate: String?
  private var isChangeDob: Bool = false
  private var isActionDone: Bool = false
  private var isChangeInfo: Bool = false
  private var isChangeTextAbout: Bool = false
  private var countUpload: Int = 0
  private var urlPrimaryImage: String = AccountFlowManager.currentUser.primaryImage
  private var currentCollege = ""
  private var companyAndRole = ""
  private let colorDark = UIColor(red: 165/255, green: 165/255, blue: 184/255, alpha: 1)
  private let colorBlue = UIColor(red: 81/255, green: 156/255, blue: 255/255, alpha: 1)
  private var didConnectLinked = false
  private var isFirstLogin: Bool = false
  private var arrImageInsta: [UIImage] = []
  private var arrIdImg: [String] = []
  private var isHavePhotoFacebook: Bool = false
  private var isUploadPhotoDone: Bool = false
  private var isChangeImage: Bool = false
  private var isDeletePhoto: Bool = false
  private var isChangeLocation: Bool = false
  private var oldLocation: ResourceObject?
  private var tokenLinkedin: String?
  private var moveDate: NSDate?
  
  //MARK: - Publics
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "CreateProfileVC", bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  convenience init(update: Bool) {
    if update {
      self.init(leftType:LeftItems.None, centerType:CenterItems.Title, rightType:RightItems.Done)
      isFirstLogin = true
    } else {
      self.init(leftType:LeftItems.Back, centerType:CenterItems.Title, rightType:RightItems.Save)
      isFirstLogin = false
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setTitleForVC("Update profile")
    scrollView.contentInset.top = 64
    
    // Do any additional setup after loading the view.
    navigationController?.navigationBar.barTintColor = UIColor(red: 81/255.0, green: 156/255.0, blue: 255/255.0, alpha: 1.0)
    navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    navigationController?.navigationBar.translucent = false
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    
    configureFont()
    
    for image in AccountFlowManager.currentUser.images {
      arrIdImg.append(image.imgId)
    }
    if AccountFlowManager.currentUser.isLoginWithInstagram {
      if isFirstLogin {
        getPhotoInstagram()
      } else {
        configureProfilePhotoView()
      }
    } else {
      if isFirstLogin {
        getPhotoFacebook()
      } else {
        configureProfilePhotoView()
      }
    }
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWasShown(_:)), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWasHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    
    var image = UIImage(named: "ic_filter_budget")
    image = image?.imageWithAlignmentRectInsets(UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 6))
    
    sliderBudget.minimumValue = 0
    sliderBudget.maximumValue = 5000
    sliderBudget.lowerValue = 0
    sliderBudget.upperValue = 5000
    sliderBudget.minimumRange = 500
    sliderBudget.stepValue = 100.0
    sliderBudget.stepValueContinuously = true
    
    lblLocationContent.text = AccountFlowManager.currentUser.uLocation
    
    var sliderBudgetTrackImage = UIImage(named: "slider-default-track")
    sliderBudgetTrackImage = sliderBudgetTrackImage?.resizableImageWithCapInsets(UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0))
    sliderBudget.trackImage = sliderBudgetTrackImage
    
    sliderBudget.lowerHandleImageNormal = image
    sliderBudget.upperHandleImageNormal = image
    
    sliderBudget.lowerHandleImageHighlighted = image
    sliderBudget.upperHandleImageHighlighted = image
    
    if Common.isIPhone5or4() {
      consLblBudgetMin.constant = 88.5
      consLblBudgetMax.constant = 260.5
    } else if Common.isIPhone6() {
      consLblBudgetMin.constant = 88.5
      consLblBudgetMax.constant = 315.5
    } else {
      consLblBudgetMin.constant = 88.6667
      consLblBudgetMax.constant = 354.333
    }
    
    pickerView.delegate = self
    var arrGender: [[String:String]] = []
    arrGender.append(["name":"Choose Gender"])
    arrGender.append(["name":"Female"])
    arrGender.append(["name":"Male"])
    pickerView.arrayDataSource = NSMutableArray(array: arrGender, copyItems: true)
    
    configDatePicker()
    configDateMoveInDate()
    configSetDataUser()
    setupPersonalities(AccountFlowManager.shareInstance.listPersonal)
    
    tvAbout.alpha = 0
    tvAbout.delegate = self
    tvAbout.textContainerInset = UIEdgeInsets(top: 10, left: 13, bottom: 30, right: 31)
    lblCharacterAbout.text = "\(500)"
    lblCharacterAbout.alpha = 0
    
    let tapGestureScrollView = UITapGestureRecognizer(target: self, action: #selector(CreateProfileVC.hideKeyboard))
    scrollView.addGestureRecognizer(tapGestureScrollView)
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    fixLocationForLabelBudget()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    print("touch")
  }
  
  // MARK: - Public method
  func hideKeyboard() {
    view.endEditing(true)
    if isChangeTextAbout {
      lblAboutContent.text = tvAbout.text
    } else {
      
    }
    activeView = nil
    lblAboutContent.alpha = 1
    tvAbout.alpha = 0
    lblCharacterAbout.alpha = 0
  }
  
  func keyboardWasShown(notification: NSNotification) {
    if let _ = activeView, keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
      let contentInsets = UIEdgeInsets(top: 64.0, left: 0.0, bottom: keyboardSize.height - 40, right: 0.0)
      scrollView.contentInset = contentInsets
      var aRect = self.view.frame
      aRect.size.height -= keyboardSize.size.height
      
      if (!CGRectContainsPoint(aRect, CGPoint(x: 0, y: CGRectGetMaxY(viewAbout.frame)))) {
        scrollView.scrollRectToVisible(CGRect(x: 0, y: CGRectGetMaxY(viewAbout.frame), width: 100, height: 100), animated: true)
        fixSizeTextView()
      } else {
        
      }
    }
  }
  
  func keyboardWasHide(notification: NSNotification) {
    let contentInsets = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
    scrollView.contentInset = contentInsets
    
  }
  
  // MARK: - Privates
  func setupPersonalities(listPersonal: [Personalities]) {
    if listPersonal.count > 0 {
      for personal in listPersonal {
        self.listPersonal.append(personal)
      }
    }
    if self.listPersonal.count == 0 {
      viewPersonalities.alpha = 0
      lblPersonalitiesTitle.alpha = 0
      consHeightPersonalities.constant = 0
      consTopShareToViewSchool.priority = 999
      consTopViewIstaToViewPersonal.priority = 500
      consTopShareToViewSchool.constant = 42
    } else {
      consTopShareToViewSchool.priority = 500
      consTopViewIstaToViewPersonal.priority = 999
      viewPersonalities.alpha = 1
      lblPersonalitiesTitle.alpha = 1
    }
    configurePersonalitiesView()
  }
  
  
  private func updateListIdImg() {
    isChangeImage = false
    print(arrIdImg)
    let param = ["imageids": arrIdImg]
    print(AccountFlowManager.currentUser.authToken)
    print(AccountFlowManager.currentUser.id)
    let headers = [
      Authorization   : "Bearer " + AccountFlowManager.currentUser.authToken,
      "Content-Type"  : "application/json"
    ]
    
    let request = NSMutableURLRequest(URL: NSURL(string: getApiUpdateInfoUser(AccountFlowManager.currentUser.id))!,
                                      cachePolicy: .ReloadIgnoringCacheData,
                                      timeoutInterval: 10.0)
    request.HTTPMethod = "PATCH"
    request.allHTTPHeaderFields = headers
    request.HTTPBody = Common.jsonToNSData(param)
    let session = NSURLSession.sharedSession()
    let dataTask = session.dataTaskWithRequest(request, completionHandler: { [weak self] (data, response, error) -> Void in
      if let e = error {
        Async.main(block: { () -> Void in
          print(e)
          Common.showAlert(e.localizedDescription)
        })
      } else {
        if let data = data {
          print("update list photo\(Common.nsdataToJSON(data))")
          let result = Common.nsdataToJSON(data)
          if let err: Error = Mapper<Error>().map(result) {
            if err.status == "ERR" {
              print("Update id Image failed")
            } else {
              
            }
          }
        }
      }
      self?.countUpload = (self?.countUpload)! + 1
      self?.getUserInfoAfterUpdate()
      })
    dataTask.resume()
  }
  
  private func getPhotoFacebook() {
    var arrImgFacebook = [UIImage]()
    var countImg = 0
    DHIndicator.show()
    DHSocial.getMePhoto { [weak self] (photos, error) in
      if let _ = error {
        self?.configureProfilePhotoView()
        DHIndicator.hide()
      } else {
        if photos.count != 0 {
          AccountFlowManager.currentUser.images.removeAll()
          for image in photos {
            if let imgObj = image.images.first, url = NSURL(string: imgObj.url), dataImage = NSData(contentsOfURL: url), img = UIImage(data: dataImage) {
              arrImgFacebook.append(img)
            }
          }
          for image in arrImgFacebook {
            let api = CreateProfileManageAPI()
            if let dataImage = UIImageJPEGRepresentation(image, 1.0) {
              api.uploadPhoto(AccountFlowManager.currentUser.id, dataImage: dataImage, indexImage: 0, completed: { (indexImg, result, success, error) in
                if let error = error {
                  Common.showAlert(error.error_msg)
                } else {
                  print("Upload Photo Success")
                  if let id = result?.id {
                    self?.arrIdImg.append(id)
                  }
                  countImg = countImg + 1
                  if countImg == arrImgFacebook.count {
                    self?.uploadArrIdImg((self?.arrIdImg)!)
                  }
                }
              })
            }
          }
        } else {
          self?.uploadArrIdImg((self?.arrIdImg)!)
          DHIndicator.hide()
        }
      }
    }
  }
  
  private func getPhotoInstagram() {
    DHIndicator.show()
    var countImg = 0
    var arrImgInsta: [UIImage] = []
    let userDefault = NSUserDefaults.standardUserDefaults()
    if let token = userDefault.stringForKey(userInstaToken) {
      LoginWithInstagramVC.getPhotos(AccountFlowManager.currentUser.uInstaId, accessToken: token) { [weak self] (result) in
        print(result)
        if let result = result {
          if result.count <= 4 {
            for objInsta in result {
              if objInsta.objType == "image" {
                if let objImage = objInsta.imageInstagram {
                  if let strImg = objImage.standardResolution?.url {
                    if let urlImg = NSURL(string: strImg) {
                      if let dataImg = NSData(contentsOfURL: urlImg) {
                        if let image = UIImage(data: dataImg) {
                          arrImgInsta.append(image)
                        }
                      }
                    }
                  }
                }
              }
            }
          } else {
            for i in 0..<result.count {
              if i <= 3 {
                let objInsta = result[i]
                if let objImage = objInsta.imageInstagram {
                  if let strImg = objImage.standardResolution?.url {
                    if let urlImg = NSURL(string: strImg) {
                      if let dataImg = NSData(contentsOfURL: urlImg) {
                        if let image = UIImage(data: dataImg) {
                          arrImgInsta.append(image)
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          self?.arrImageInsta = arrImgInsta
          print(arrImgInsta.count)
          if arrImgInsta.count != 0 {
            for image in arrImgInsta {
              let api = CreateProfileManageAPI()
              if let dataImage = UIImageJPEGRepresentation(image, 1.0) {
                api.uploadPhoto(AccountFlowManager.currentUser.id, dataImage: dataImage, indexImage: 0, completed: { (indexImg, result, success, error) in
                  if let error = error {
                    Common.showToastWithMessage(error.error_msg)
                  } else {
                    print("Upload Photo Success")
                    if let id = result?.id {
                      self?.arrIdImg.append(id)
                    }
                    countImg = countImg + 1
                    if countImg == arrImgInsta.count {
                      self?.uploadArrIdImg((self?.arrIdImg)!)
                    }
                  }
                })
              }
            }
          } else {
            self?.uploadArrIdImg((self?.arrIdImg)!)
            DHIndicator.hide()
          }
        } else {
          print("result nil")
          self?.configureProfilePhotoView()
        }
      }
    }
  }
  
  private func getUserInfoAfterUpdate() {
    print("countUpload: \(countUpload)")
    if isFirstLogin {
      GGWebservice.callWebServiceWithRequest(.GET, urlString: ApiGetUserInfo(AccountFlowManager.currentUser.id), param: nil, accessToken: nil, isAuthorization: false) { [weak self] (value, statusCode, error) in
        if statusCode == 200 || statusCode == 201 {
          let user: User = Mapper<User>().map(value)!
          print(user)
          AccountFlowManager.currentUser = user
          AccountFlowManager.currentUser.authToken = (NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant))!
          AccountFlowManager.currentUser.isLoginWithFacebook = NSUserDefaults.standardUserDefaults().boolForKey(userIsLoginWithFacebook)
          AccountFlowManager.currentUser.isLoginWithInstagram = NSUserDefaults.standardUserDefaults().boolForKey(userIsLoginWithInstagram)
          DHIndicator.hide()
          Common.showToastWithMessage(textUploadInfoUserSuccess)
        } else {
          let result = Mapper<Error>().map(value)
          DHIndicator.hide()
          Common.showAlert(result?.errMessage)
        }
        if let done = self?.isActionDone {
          if done {
            AccountFlowManager.shareInstance.goLoadingPage()
            self?.isFirstLogin = false
          }
        }
      }
    } else {
      if countUpload == 2 {
        GGWebservice.callWebServiceWithRequest(.GET, urlString: ApiGetUserInfo(AccountFlowManager.currentUser.id), param: nil, accessToken: nil, isAuthorization: false) { [weak self] (value, statusCode, error) in
          print(value)
          if statusCode == 200 || statusCode == 201 {
            let user: User = Mapper<User>().map(value)!
            print(user)
            AccountFlowManager.currentUser = user
            AccountFlowManager.currentUser.authToken = (NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant))!
            AccountFlowManager.currentUser.isLoginWithFacebook = NSUserDefaults.standardUserDefaults().boolForKey(userIsLoginWithFacebook)
            AccountFlowManager.currentUser.isLoginWithInstagram = NSUserDefaults.standardUserDefaults().boolForKey(userIsLoginWithInstagram)
            NSNotificationCenter.defaultCenter().postNotificationName(NotificationGetInfoAfterUpdateSuccess, object: nil)
            DHIndicator.hide()
            Common.showToastWithMessage(textUploadInfoUserSuccess)
            if let changeLoc = self?.isChangeLocation {
              if changeLoc {
                AccountFlowManager.shareInstance.goLoadingPage()
              } else {
                self?.navigationController?.popViewControllerAnimated(true)
              }
            }
          } else {
            let result = Mapper<Error>().map(value)
            DHIndicator.hide()
            Common.showAlert(result?.errMessage)
          }
        }
      } else {
        
      }
    }
    
    if didConnectLinked == false {
      CreateProfileManageAPI.updateProfession(AccountFlowManager.currentUser)
    }
  }
  
  private func uploadArrIdImg(arrId: [String]) {
    let param = ["imageids": arrId]
    
    let headers = [
      Authorization   : "Bearer " + AccountFlowManager.currentUser.authToken,
      "Content-Type"  : "application/json"
    ]
    
    let request = NSMutableURLRequest(URL: NSURL(string: getApiUpdateInfoUser(AccountFlowManager.currentUser.id))!,
                                      cachePolicy: .ReloadIgnoringCacheData,
                                      timeoutInterval: 10.0)
    request.HTTPMethod = "PATCH"
    request.allHTTPHeaderFields = headers
    request.HTTPBody = Common.jsonToNSData(param)
    
    let session = NSURLSession.sharedSession()
    let dataTask = session.dataTaskWithRequest(request, completionHandler: { [weak self] (data, response, error) -> Void in
      if let e = error {
        Async.main(block: { () -> Void in
          print(e)
          Common.showToastWithMessage(e.localizedDescription)
        })
      } else {
        if let data = data {
          print(Common.nsdataToJSON(data))
          let value = Common.nsdataToJSON(data)
          if let value: Error = Mapper<Error>().map(value) {
            if value.status == "ERR" {
              Common.showToastWithMessage(value.errMessage)
            } else {
              print("Update Image Id Success")
              GGWebservice.callWebServiceWithRequest(.GET, urlString: ApiGetUserInfo(AccountFlowManager.currentUser.id), param: nil, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
                if statusCode == 200 || statusCode == 201 {
                  let user: User = Mapper<User>().map(value)!
                  AccountFlowManager.currentUser = user
                  AccountFlowManager.currentUser.authToken = (NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant))!
                  AccountFlowManager.currentUser.isLoginWithFacebook = NSUserDefaults.standardUserDefaults().boolForKey(userIsLoginWithFacebook)
                  AccountFlowManager.currentUser.isLoginWithInstagram = NSUserDefaults.standardUserDefaults().boolForKey(userIsLoginWithInstagram)
                  self?.configureProfilePhotoView()
                  DHIndicator.hide()
                } else {
                  let result = Mapper<Error>().map(value)
                  DHIndicator.hide()
                  Common.showAlert(result?.errMessage)
                }
              }
            }
          }
        }
      }
      DHIndicator.hide()
      })
    dataTask.resume()
  }
  
  private func configSetDataUser() {
    if let currentUser = AccountFlowManager.currentUser {
      if currentUser.isLoginWithInstagram {
        lblConnectInstagram.textColor = UIColor(red: 157/255, green: 157/255, blue: 157/255, alpha: 1)
        btnConnectInstagram.userInteractionEnabled = false
        imvConnectInstagram.image = UIImage(named: "ic_updateprofile_synchronizationarrows_disable.png")
      } else {
        if currentUser.uInstaUsername != "" || currentUser.uInstaId != "" {
          lblConnectInstagram.text = "Disconnect Instagram"
        } else {
          lblConnectInstagram.textColor = UIColor(red: 81/255, green: 156/255, blue: 255/255, alpha: 1)
          btnConnectInstagram.userInteractionEnabled = true
          imvConnectInstagram.image = UIImage(named: "ic_updateprofile_synchronizationarrows.png")
        }
      }
      
      lblLocationContent.text = currentUser.uLocation
      if currentUser.uLocation.characters.count > 0 {
        if let lat = currentUser.uLatitude, lon = currentUser.uLongtitude {
          let obj = ResourceObject()
          obj.title = currentUser.uLocation
          obj.resourceLat = lat
          obj.resourceLon = lon
          location = obj
        }
      }
      
      lblAboutContent.text = currentUser.about
      
      if currentUser.gender == "male" {
        lblGenderContent.text = "Male"
      } else if currentUser.gender == "female" {
        lblGenderContent.text = "Female"
      } else {
        lblGenderContent.text = currentUser.gender
      }
      if currentUser.uEducation == "\"\"" {
        lblSchoolContent.text = ""
      } else {
        lblSchoolContent.text = currentUser.getEducation()
        currentCollege = currentUser.uEducation
      }
      
      if lblSchoolContent.text?.characters.count == 0 {
        lblSchoolContent.text = textNone
      }
      
      companyAndRole = AccountFlowManager.currentUser.workHistory
      setStatusBtnLinked()
      
      let format = Common.getDateFormat()
      if let date = format.dateFromString(currentUser.dob) {
        dob = date
      }
      
      let ageUser = Common.caculateAge(currentUser.dob)
      if ageUser >= 10 {
        lblAgeContent.text = String(ageUser)
      } else {
        lblAgeContent.text = ""
      }
      
      sliderBudget.lowerValue = currentUser.budgetMin
      sliderBudget.upperValue = currentUser.budgetMax
      let budgetMin = Int(currentUser.budgetMin)
      let budgetMax = Int(currentUser.budgetMax)
      lblBudgetMax.text = "$\(Common.formatNumber(budgetMax))"
      lblBudgetMin.text = "$\(Common.formatNumber(budgetMin))"
      
      if let moveDay = format.dateFromString(currentUser.moveInDate) {
        moveDate = moveDay
      }
      
      if Common.checkDateInPast(currentUser.moveInDate) {
        lblMoveInDateContent.text = "MM/dd/YYYY"
      } else {
        let arrDMY = currentUser.moveInDate.componentsSeparatedByString("-")
        
        if arrDMY.count >= 3 {
          let arrDay = arrDMY[2].componentsSeparatedByString("T")
          let strMoveDate = arrDMY[1] + "/" + arrDay[0] + "/" + arrDMY[0]
          
          lblMoveInDateContent.text = strMoveDate
        }
      }
    }
  }
  
  private func configDatePicker() {
    timePicker.delegate = self
    let dateFormatter = Common.getDateFormat()
    if let date: NSDate = dateFormatter.dateFromString(AccountFlowManager.currentUser.dob) {
      timePicker.datePicker.date = date
      timePicker.datePicker.locale = NSLocale(localeIdentifier: "en_US")
      timePicker.strCancel = "Cancel"
      timePicker.strDone = "Done"
    }
  }
  
  private func configDateMoveInDate() {
    timeMoveInDatePicker.delegate = self
    timeMoveInDatePicker.datePicker.datePickerMode = UIDatePickerMode.Date
    let dateFormatter = Common.getDateFormat()
    
    timeMoveInDatePicker.datePicker.minimumDate = NSDate()
    timeMoveInDatePicker.datePicker.locale = NSLocale(localeIdentifier: "en_US")
    timeMoveInDatePicker.strCancel = "Cancel"
    timeMoveInDatePicker.strDone = "Done"
    
    if let date: NSDate = dateFormatter.dateFromString(AccountFlowManager.currentUser.moveInDate) {
      timeMoveInDatePicker.datePicker.date = date
    }
  }
  
  private func requestPersonal() {
    let api = CreateProfileManageAPI()
    api.delegate = self
    api.getPersonalities(page)
  }
  
  private func configureFont() {
    lblAboutContent.font = getRobotoRegularWith(14)
    lblAboutTitle.font = getRobotoRegularWith(14)
    
    lblLocationContent.font = getRobotoRegularWith(14)
    lblLocationTitle.font = getRobotoRegularWith(14)
    
    lblGenderContent.font = getRobotoRegularWith(14)
    lblGenderTitle.font = getRobotoRegularWith(14)
    
    lblAgeContent.font = getRobotoRegularWith(14)
    lblAgeTitle.font = getRobotoRegularWith(14)
    
    lblBudgetTitle.font = getRobotoRegularWith(14)
    
    lblCompanyContent.font = getRobotoRegularWith(14)
    lblCompanyTitle.font = getRobotoRegularWith(14)
    
    lblSchoolTitle.font = getRobotoRegularWith(14)
    lblSchoolContent.font = getRobotoRegularWith(14)
    
    lblPersonalitiesTitle.font = getRobotoMediumWith(14)
    lblInstagramTitle.font = getRobotoMediumWith(14)
    lblLinkedinTitle.font = getRobotoMediumWith(14)
    
    tvAbout.font = getRobotoRegularWith(14)
    lblCharacterAbout.font = getRobotoRegularWith(14)
    
    btnConnectInstagram.titleLabel?.font = getRobotoRegularWith(14)
    btnConnectLinkedin.titleLabel?.font = getRobotoRegularWith(14)
  }
  
  private func configureProfilePhotoView() {
    let profilePhoto = ProfilePhotoView(frame: viewPhoto.bounds)
    profilePhoto.delegate = self
    viewPhoto.addSubview(profilePhoto)
    profilePhotos = profilePhoto
    var images = AccountFlowManager.currentUser.images
    let listIdImage = AccountFlowManager.currentUser.listIdImage
    if listIdImage.count < AccountFlowManager.currentUser.images.count {
      images.removeAll()
      for i in 0..<listIdImage.count {
        let image = AccountFlowManager.currentUser.images[i]
        images.append(image)
      }
    }
    profilePhoto.setImage(images)
  }
  
  private func configurePersonalitiesView() {
    personalityView = PersonalitiesView(frame: viewPersonalities.bounds)
    personalityView.heightSuperView = consHeightPersonalities
    personalityView.listPersonalities = listPersonal
    personalityView.delegate = self
    personalityView.setDataForPersonal()
    viewPersonalities.addSubview(personalityView)
    let arrPer = AccountFlowManager.currentUser.interests.componentsSeparatedByString("|")
    var listPersonalSelected = [Personalities]()
    for namePer in arrPer {
      for personal in listPersonal {
        if personal.namePersonalities == namePer {
          listPersonalSelected.append(personal)
        }
      }
    }
    
    personalityView.setStatusPersonalities(listPersonalSelected)
  }
  
  private func updateListImageUser() {
    GGWebservice.callWebServiceWithRequest(.GET, urlString: apiGetImagesWithUserId(AccountFlowManager.currentUser.id), param: nil, accessToken: nil, isAuthorization: false, success: { (value, statusCode, error) in
      if let listImage: ListImage = Mapper<ListImage>().map(value) {
        AccountFlowManager.currentUser.images = listImage.data
      }
    })
  }
  
  private func updateInfo() {
    let api = CreateProfileManageAPI()
    
    var strPer = ""
    for personal in personalityView.getPersonalities(listPersonal) {
      if strPer == "" {
        strPer = strPer + personal.namePersonalities
      } else {
        strPer = strPer + "|" + personal.namePersonalities
      }
    }
    
    var gender = ""
    if lblGenderContent.text == "Female" {
      gender = "female"
    } else if lblGenderContent.text == "Male" {
      gender = "male"
    }
    
    if lblLocationContent.text == nil {
      lblLocationContent.text = ""
    }
    
    if lblAboutContent.text == nil {
      lblAboutContent.text = ""
    }
    
    if let _ = moveInDate {
      
    } else {
      if AccountFlowManager.currentUser.moveInDate == "" {
        let strDateMove = Common.getDateCurrentFirstMonth()
        let arr = strDateMove.componentsSeparatedByString("+")
        moveInDate = arr[0] + "Z"
      } else {
        print(AccountFlowManager.currentUser.moveInDate)
        moveInDate = AccountFlowManager.currentUser.moveInDate
      }
    }
    
    var prefGender = ""
    var prefProfessional = ""
    var prefAgeMin = 0
    var prefAgeMax = 0
    var prefBudgetMin = 0
    var prefBudgetMax = 0
    var prefShowDiscovery = ""
    var prefNotifiMessage = ""
    var prefMatch = ""
    
    if isActionDone {
      prefGender = ""
      prefProfessional = ""
      prefAgeMin = 18
      prefAgeMax = 200
      prefBudgetMin = 0
      prefBudgetMax = 5000
      prefShowDiscovery = "true"
      prefNotifiMessage = "true"
      prefMatch = "true"
    } else {
      let userCurrent = AccountFlowManager.currentUser
      prefGender = userCurrent.prefGender
      prefProfessional = userCurrent.preProfession
      prefAgeMin = Int(userCurrent.prefAgeMin)
      prefAgeMax = Int(userCurrent.prefAgeMax)
      prefBudgetMin = Int(userCurrent.prefBudgetMin)
      prefBudgetMax = Int(userCurrent.prefBudgetMax)
      prefShowDiscovery = userCurrent.prefShowInDiscovery == "" ? "true" : userCurrent.prefShowInDiscovery
      prefNotifiMessage = userCurrent.prefMessage == "" ? "true" : userCurrent.prefMessage
      prefMatch = userCurrent.prefNewMatch == "" ? "true" : userCurrent.prefNewMatch
    }
    
    // ------------------------Upload Info User
    if let logitude = location?.resourceLon, latitude = location?.resourceLat {
      api.delegate = self
      api.updateInfoUser(AccountFlowManager.currentUser.id, budgetMin: Int(sliderBudget.lowerValue), budgetMax: Int(sliderBudget.upperValue), locale: lblLocationContent.text!, profession: "", aboutMe: lblAboutContent.text!, workHistory: companyAndRole, dob: AccountFlowManager.currentUser.dob, education: currentCollege, gender: gender, interests: strPer, logitude: logitude, latitude: latitude, primaryImage: urlPrimaryImage, moveInDate: moveInDate!, prefBudgetMin: prefBudgetMin, prefBudgetMax: prefBudgetMax, prefProfession: prefProfessional, prefGender: prefGender, prefAgeMin: prefAgeMin, prefAgeMax: prefAgeMax, prefNotifiMessage: prefNotifiMessage, prefNotifiNewMatch: prefMatch, prefShowDiscovery: prefShowDiscovery)
    } else {
      api.delegate = self
      api.updateInfoUser(AccountFlowManager.currentUser.id, budgetMin: Int(sliderBudget.lowerValue), budgetMax: Int(sliderBudget.upperValue), locale: lblLocationContent.text!, profession: "", aboutMe: lblAboutContent.text!, workHistory: companyAndRole, dob: AccountFlowManager.currentUser.dob, education: currentCollege, gender: gender, interests: strPer, logitude: 0.0, latitude: 0.0, primaryImage: urlPrimaryImage, moveInDate: moveInDate!, prefBudgetMin: prefBudgetMin, prefBudgetMax: prefBudgetMax, prefProfession: prefProfessional, prefGender: prefGender, prefAgeMin: prefAgeMin, prefAgeMax: prefAgeMax, prefNotifiMessage: prefNotifiMessage, prefNotifiNewMatch: prefMatch, prefShowDiscovery: prefShowDiscovery)
    }
    // -------------------------
  }
  
  private func fixSizeTextView() {
    let widthTvReview = tvAbout.frame.size.width
    let newSize = tvAbout.sizeThatFits(CGSize(width: widthTvReview, height: CGFloat.max))
    var newFrame = tvAbout.frame
    newFrame.size = CGSize(width: widthTvReview, height: newSize.height)
    tvAbout.frame = newFrame
    
    if newFrame.size.height <= 69 {
      tvAbout.frame.size.height = 69
      consHeightViewAbout.constant = 69
    } else {
      tvAbout.frame.size.height = newFrame.size.height
      consHeightViewAbout.constant = newFrame.size.height
    }
    
    scrollView.scrollRectToVisible(CGRect(x: 0, y: CGRectGetMaxY(viewAbout.frame), width: 100, height: 100), animated: false)
  }
  
  private func fixLocationForLabelBudget() {
    consLblBudgetMin.constant = sliderBudget.lowerCenter.x + sliderBudget.frame.origin.x - (lblBudgetMin.frame.size.width / 2)
    
    consLblBudgetMax.constant = sliderBudget.upperCenter.x + sliderBudget.frame.origin.x - (lblBudgetMax.frame.size.width / 2)
    
    if ((consLblBudgetMin.constant + CGRectGetWidth(lblBudgetMin.frame)) - consLblBudgetMax.constant) > -5 {
      consLblBudgetMax.constant = consLblBudgetMax.constant + 10
      consLblBudgetMin.constant = consLblBudgetMin.constant - 10
    }
  }
  
  private func setStatusBtnLinked() {
    if let _ = NSUserDefaults.standardUserDefaults().stringForKey(userLinkedInToken) {
      lblCompanyContent.textColor = colorDark
      lblCompanyContent.text = companyAndRole
      imvSyncCompany.hidden = true
      consLeftImvSync.constant = -4
      lblConnectLinkedin.text = textDisconnectLinkedin
    } else {
      if companyAndRole.characters.count > 0 {
        lblCompanyContent.text = companyAndRole
        lblCompanyContent.textColor = colorDark
        lblCompanyContent.text = companyAndRole
        imvSyncCompany.hidden = true
        consLeftImvSync.constant = -4
        lblConnectLinkedin.text = textConnectLinkedin
      } else {
        lblCompanyContent.text = textConnectLinkedin
        lblCompanyContent.textColor = colorBlue
        imvSyncCompany.hidden = false
        consLeftImvSync.constant = 19
        lblConnectLinkedin.text = textConnectLinkedin
      }
    }
  }
  
  private func getUserAfterConnectInstagram() {
    GGWebservice.callWebServiceWithRequest(.GET, urlString: ApiGetUserInfo(AccountFlowManager.currentUser.id), param: nil, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
      print(value)
      if statusCode == 200 || statusCode == 201 {
        let user: User = Mapper<User>().map(value)!
        AccountFlowManager.currentUser = user
        AccountFlowManager.currentUser.authToken = (NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant))!
        AccountFlowManager.currentUser.isLoginWithFacebook = NSUserDefaults.standardUserDefaults().boolForKey(userIsLoginWithFacebook)
        AccountFlowManager.currentUser.isLoginWithInstagram = NSUserDefaults.standardUserDefaults().boolForKey(userIsLoginWithInstagram)
      } else {
        let result = Mapper<Error>().map(value)
        DHIndicator.hide()
        Common.showAlert(result?.errMessage)
      }
    }
  }
  
  private func callWS() {
    DHIndicator.show()
    let strDate = AccountFlowManager.currentUser.dob
    let dateFormatter = Common.getDateFormat()
    var dateCurrent: String = ""
    
    if isChangeDob {
      dateCurrent = dateFormatter.stringFromDate(dob)
    } else {
      if let date = dateFormatter.dateFromString(strDate) {
        dateCurrent = dateFormatter.stringFromDate(date)
      } else {
        dateCurrent = dateFormatter.stringFromDate(NSDate())
      }
    }
    let array = dateCurrent.componentsSeparatedByString("+")
    let ageUser = Common.caculateAge(array[0] + "Z")
    if ageUser >= 18 {
      AccountFlowManager.currentUser.dob = array[0] + "Z"
      if lblGenderContent.text == "Male" || lblGenderContent.text == "Female" {
        updateInfo()
      } else {
        DHIndicator.hide()
        Common.showAlert(textDontEmptyGender)
      }
    } else {
      DHIndicator.hide()
      Common.showAlert(textAgeMuchOlderThan18)
      countUpload = 0
    }
  }
  
  // MARK: - Actions
  override func tapToDone(sender: UIButton!) {
    DHIndicator.show()
    isActionDone = true
    if isChangeTextAbout {
      lblAboutContent.text = tvAbout.text
    } else {
      
    }
    let strDate = AccountFlowManager.currentUser.dob
    let dateFormatter = Common.getDateFormat()
    var dateCurrent: String = ""
    
    if isChangeDob {
      dateCurrent = dateFormatter.stringFromDate(dob)
    } else {
      if let date = dateFormatter.dateFromString(strDate) {
        dateCurrent = dateFormatter.stringFromDate(date)
      } else {
        dateCurrent = dateFormatter.stringFromDate(NSDate())
      }
    }
    let array = dateCurrent.componentsSeparatedByString("+")
    let ageUser = Common.caculateAge(array[0] + "Z")
    if ageUser >= 18 {
      AccountFlowManager.currentUser.dob = array[0] + "Z"
      if lblGenderContent.text == "Male" || lblGenderContent.text == "Female" {
        updateInfo()
      } else {
        DHIndicator.hide()
        Common.showAlert(textDontEmptyGender)
      }
    } else {
      DHIndicator.hide()
      Common.showAlert(textAgeMuchOlderThan18)
    }
  }
  
  override func tapToSave(sender: UIButton!) {
    isActionDone = false
    if isChangeTextAbout {
      lblAboutContent.text = tvAbout.text
    } else {
      
    }
    if isChangeImage {
      if isDeletePhoto {
        if isChangeInfo {
          callWS()
        } else {
          countUpload = countUpload + 1
          if let arrId = profilePhotos?.getListIdImage() {
            arrIdImg = arrId
            updateListIdImg()
          }
        }
      } else {
        if isUploadPhotoDone {
          if isChangeInfo {
            callWS()
          } else {
            countUpload = countUpload + 1
            if let arrId = profilePhotos?.getListIdImage() {
              arrIdImg = arrId
              updateListIdImg()
            }
          }
        } else {
          Common.showAlert(textUploadingPhoto)
        }
      }
    } else {
      if isChangeInfo {
        countUpload = countUpload + 1
        callWS()
      } else {
        navigationController?.popViewControllerAnimated(true)
      }
    }
  }
  
  @IBAction func locationTapped(sender: AnyObject) {
    let vc = MapLocationVC()
    vc.delegate = self
    if let loc = location {
      oldLocation = loc
      vc.isInit = true
      vc.currentLocation = loc
    }
    let navi = GGNavigationController(rootViewController: vc)
    presentViewController(navi, animated: true, completion: nil)
  }
  
  @IBAction func genderTapped(sender: AnyObject) {
    hideKeyboard()
    isChangeInfo = true
    if lblGenderContent.text == "Male" {
      pickerView.setValueWhenReShow(2)
    } else if lblGenderContent.text == "Female" {
      pickerView.setValueWhenReShow(1)
    } else {
      pickerView.setValueWhenReShow(0)
    }
    pickerView.showActionSheetInView(view)
  }
  
  @IBAction func ageTapped(sender: AnyObject) {
    hideKeyboard()
    isChangeInfo = true
    timePicker.datePicker.date = dob
    timePicker.showActionSheetInView(view)
  }
  
  @IBAction func companyTapped(sender: AnyObject) {
    if companyAndRole.characters.count > 0 {
      return
    }
    connectLinkedinTapped(sender)
  }
  
  @IBAction func schoolTapped(sender: AnyObject) {
    isChangeInfo = true
    let vc = CreateProfileVC2()
    vc.currentCollege = currentCollege
    vc.delegate = self
    navigationController?.pushViewController(vc, animated: true)
  }
  
  @IBAction func connectInstagramTapped(sender: AnyObject) {
    if lblConnectInstagram.text == "Connect Instagram" {
      let loginVC = LoginWithInstagramVC()
      loginVC.delegate = self
      let vc = GGNavigationController(rootViewController: loginVC)
      presentViewController(vc, animated: true, completion: nil)
    } else {
      let param: [String:AnyObject] = ["insta_id": "",
                                       "insta_username": ""]
      GGWebservice.callWebServiceWithRequest(.PATCH, urlString: getApiUpdateInfoUser(AccountFlowManager.currentUser.id), param: param, accessToken: nil, isAuthorization: false) { [weak self] (value, statusCode, error) in
        
        if let result = Mapper<Error>().map(value) {
          if result.status == "ERR" {
            Common.showToastWithMessage(textDisconnectInstagramFailed)
          } else if result.status == "OK" {
            Common.showToastWithMessage(textDisconnectInstagramSuccess)
            self?.lblConnectInstagram.text = "Connect Instagram"
            self?.getUserAfterConnectInstagram()
          }
        }
      }
      CreateProfileManageAPI.updateInstagramPhoto(AccountFlowManager.currentUser.id, instagramPhotos: [])
    }
  }
  
  @IBAction func connectLinkedinTapped(sender: AnyObject) {
    if let _ = NSUserDefaults.standardUserDefaults().stringForKey(userLinkedInToken) {
      let alertView: UIAlertController = UIAlertController(title: appName, message: textConfirmDisconnectLinkedin, preferredStyle:UIAlertControllerStyle.Alert)
      let cancel = UIAlertAction(title: "No", style: .Default) { _ in
        
      }
      alertView.addAction(cancel)
      let action = UIAlertAction(title: "Yes", style: .Default) { [weak self] (_) in
        self?.didConnectLinked = false
        self?.companyAndRole = ""
        NSUserDefaults.standardUserDefaults().removeObjectForKey(userLinkedInToken)
        NSUserDefaults.standardUserDefaults().synchronize()
        self?.setStatusBtnLinked()
        CreateProfileManageAPI.updateInfoUser(AccountFlowManager.currentUser.id, workHistory: "")
      }
      alertView.addAction(action)
      
      presentViewController(alertView, animated: true, completion: nil)
    } else {
      let vc = LoginWithLinkedIn()
      vc.delegate = self
      let navi = GGNavigationController(rootViewController: vc)
      presentViewController(navi, animated: true, completion: nil)
    }
  }
  
  @IBAction func budgetChanged(sender: AnyObject) {
    hideKeyboard()
    isChangeInfo = true
    
    lblBudgetMin.text = "$\(Common.formatNumber(Int(sliderBudget.lowerValue)))"
    lblBudgetMax.text = "$\(Common.formatNumber(Int(sliderBudget.upperValue)))"
    
    fixLocationForLabelBudget()
  }
  
  @IBAction func tapToMoveInDate(sender: AnyObject) {
    hideKeyboard()
    isChangeInfo = true
    if let date = moveDate {
      timeMoveInDatePicker.datePicker.date = date
    } else {
      timeMoveInDatePicker.datePicker.date = NSDate()
    }
    timeMoveInDatePicker.showActionSheetInView(view)
  }
  
  @IBAction func tapToEditAbout(sender: AnyObject) {
    if tvAbout.alpha == 0 {
      activeView = tvAbout
      tvAbout.becomeFirstResponder()
      tvAbout.text = lblAboutContent.text
      tvAbout.editable = true
      lblCharacterAbout.text = "\(500 - tvAbout.text.characters.count)"
      fixSizeTextView()
      lblAboutContent.alpha = 0
      tvAbout.alpha = 1
      lblCharacterAbout.alpha = 1
    } else {
      consHeightViewAbout.priority = 500
      hideKeyboard()
    }
  }
  
  @IBAction func tapToDoneEditAbout(sender: AnyObject) {
    
  }
  
}

// MARK: - ProfilePhotoViewDelegate
extension CreateProfileVC: ProfilePhotoViewDelegate {
  func didNotDeletePhoto() {
    isDeletePhoto = false
  }
  
  func didChangePhoto() {
    isChangeImage = true
  }
  
  func didUploadPhotoDone() {
    isUploadPhotoDone = true
  }
  
  func didDeletePhoto() {
    isDeletePhoto = true
  }
  
  func didUploadingPhoto() {
    isUploadPhotoDone = false
  }
}

// MARK: - MapLocationDelegate
extension CreateProfileVC: MapLocationDelegate {
  func mapLocationDidChoose(location: ResourceObject) {
    if let oldLoc = oldLocation {
      if oldLoc.resourceLat != location.resourceLat || oldLoc.resourceLon != location.resourceLon {
        isChangeInfo = true
        isChangeLocation = true
      } else {
        isChangeLocation = false
      }
    }
    if location.title.characters.count > 0 {
      lblLocationContent.text = location.title
      self.location = location
    } else {
      lblLocationContent.text = textPleaseChooseLocation
    }
  }
}

// MARK: NSPickerView Delegate
extension CreateProfileVC: NSPickerViewDelegate {
  func picker(picker: NSPickerView!, dismissWithButtonDone done: Bool, withDic dic: [NSObject : AnyObject]!) {
    if done {
      lblGenderContent.text = "\(dic["name"]!)"
    } else {
      
    }
  }
}

// MARK: ChooseCollegeDelegate
extension CreateProfileVC: ChooseCollegeDelegate {
  func didSelectedCollege(currentCollege: String) {
    self.currentCollege = currentCollege
    lblSchoolContent.text = User.getEducationFromJsonString(currentCollege)
  }
}

// MARK: - Personalities Delegate
extension CreateProfileVC: PersonalitiesViewDelegate {
  func didChangePersonal() {
    isChangeInfo = true
  }
}

// MARK: - CreateProfile Delegate
extension CreateProfileVC: CreateProfileDelegate {
  func didUpdateInfoFailed() {
    countUpload = 0
  }
  
  func didUpdateInfoSuccess() {
    if isChangeImage {
      if let arrId = profilePhotos?.getListIdImage() {
        arrIdImg = arrId
        updateListIdImg()
      }
    }
    countUpload = countUpload + 1
    getUserInfoAfterUpdate()
  }
  
  func didRequestPersonality(listPersonal: [Personalities]) {
    if listPersonal.count != 0 {
      for personal in listPersonal {
        self.listPersonal.append(personal)
      }
      
      page = page + 1
      requestPersonal()
    } else {
      
    }
    
    if self.listPersonal.count == 0 {
      viewPersonalities.alpha = 0
      lblPersonalitiesTitle.alpha = 0
      consHeightPersonalities.constant = 0
      consTopShareToViewSchool.priority = 999
      consTopViewIstaToViewPersonal.priority = 500
      consTopShareToViewSchool.constant = 42
    } else {
      consTopShareToViewSchool.priority = 500
      consTopViewIstaToViewPersonal.priority = 999
      viewPersonalities.alpha = 1
      lblPersonalitiesTitle.alpha = 1
    }
    configurePersonalitiesView()
  }
}

// MARK: - PickerDate Delegate
extension CreateProfileVC: NMTimePickerViewDelegate {
  func datePicker(picker: NMTimePickerView!, dismissWithButtonDone done: Bool) {
    if done {
      if picker == timePicker {
        isChangeDob = true
        dob = picker.datePicker.date
        let str = Common.stringWithDate(dob)
        lblAgeContent.text = "\(Common.caculateAge(str))"
      } else {
        let formater: NSDateFormatter = NSDateFormatter()
        formater.dateFormat = "MM-dd-YYYY'T'HH:mm:ssZ"
        formater.timeZone = NSTimeZone(name: "UTC")
        var date = picker.datePicker.date
        moveDate = picker.datePicker.date
        if picker.datePicker.date.compare(NSDate()) == .OrderedAscending {
          date = NSDate()
        } else {
          
        }
        let strMoveInDate: String = formater.stringFromDate(date)
        
        let formatDatePreUp = Common.getDateFormat()
        let strDatePreUp = formatDatePreUp.stringFromDate(date)
        let arrMoveInDatePreUp = strDatePreUp.componentsSeparatedByString("+")
        moveInDate = arrMoveInDatePreUp[0] + "Z"
        let arrStrMoveInDate = strMoveInDate.componentsSeparatedByString("T")
        let arrDate = arrStrMoveInDate[0].componentsSeparatedByString("-")
        lblMoveInDateContent.text = "\(arrDate[0])/\(arrDate[1])/\(arrDate[2])"
      }
    }
  }
}

// MARK: TextView Delegate
extension CreateProfileVC: UITextViewDelegate {
  func textViewDidChange(textView: UITextView) {
    isChangeInfo = true
    isChangeTextAbout = true
    consHeightViewAbout.priority = 999
    fixSizeTextView()
    if textView.text.characters.count >= 500 {
      tvAbout.editable = false
      var str = textView.text
      let start = str.startIndex.advancedBy(500)
      let end = str.startIndex.advancedBy(str.characters.count)
      str.removeRange(start...end)
      tvAbout.text = str
    }
    lblCharacterAbout.text = "\(500 - textView.text.characters.count)"
  }
}

// MARK: - LinkedIn Delegate
extension CreateProfileVC: LoginWithLinkedInDelegate {
  func loginWithLinkedInCompleted(companyAndRole: String, token: String, error: Error?) {
    if let e = error {
      print(e.errMessage)
    } else {
      self.companyAndRole = companyAndRole
      CreateProfileManageAPI.updateInfoUser(AccountFlowManager.currentUser.id, workHistory: companyAndRole)
      
      NSUserDefaults.standardUserDefaults().setValue(companyAndRole, forKey: userLinkedInToken)
      NSUserDefaults.standardUserDefaults().synchronize()
      
      setStatusBtnLinked()
    }
  }
  
  func loginWithLinkedInProfile(profile: LinkedinProfile?, error: Error?) {
    if let _ = error {
      
    } else {
      if let pro = profile {
        didConnectLinked = true
        CreateProfileManageAPI.updateProfession(AccountFlowManager.currentUser, linkedProfile: pro)
      } else {
        
      }
    }
  }
}

// MARK: - LoginInstagram Delegate
extension CreateProfileVC: LoginWithInstagramDelegate {
  func loginWithInstagramCompleted(userId: String, userName: String, instaToken: String, error: NSError?) {
    let param: [String:AnyObject] = ["insta_id": userId,
                                     "insta_username": userName]
    
    GGWebservice.callWebServiceWithRequest(.PATCH, urlString: getApiUpdateInfoUser(AccountFlowManager.currentUser.id), param: param, accessToken: nil, isAuthorization: false) { [weak self] (value, statusCode, error) in
      if let result = Mapper<Error>().map(value) {
        if result.status == "ERR" {
          if statusCode == 422 {
            Common.showToastWithMessage(textConnectInstagramExisted)
          } else {
            Common.showToastWithMessage(textConnectInstagramFailed)
          }
        } else if result.status == "OK" {
          Common.showToastWithMessage(textConnectInstagramSuccess)
          self?.lblConnectInstagram.text = "Disconnect Instagram"
          self?.getUserAfterConnectInstagram()
          LoginWithInstagramVC.getPhotos(userId, accessToken: instaToken) { (result) in
            if let result = result {
              print(result.count)
              CreateProfileManageAPI.updateInstagramPhoto(AccountFlowManager.currentUser.id, instagramPhotos: result)
            } else {
              
            }
          }
        }
      }
    }
  }
}
