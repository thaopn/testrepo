//
//  WellcomeVC.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/4/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import CoreLocation

class WellcomeVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var btnContinue: UIButton!
  @IBOutlet weak var slider: NMRangeSlider!
  @IBOutlet weak var lblBudgetMax: UILabel!
  @IBOutlet weak var lblBudgetMin: UILabel!
  @IBOutlet weak var lblLocation: UILabel!
  @IBOutlet weak var consLeftLblLowerToSlider: NSLayoutConstraint!
  @IBOutlet weak var consLeftLblUpperToView: NSLayoutConstraint!
  @IBOutlet weak var consHeightViewContent: NSLayoutConstraint!

  // MARK: - Variables
  private var currentLocation: ResourceObject!

  // MARK: - Lifecycle
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "WellcomeVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.None, centerType:CenterItems.None, rightType:RightItems.None)
    self.setTitleForVC("")
  }


  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)

    self.setHidenNaviBar(true)
  }

  override func viewDidLayoutSubviews() {
    fixLocationForLabelBudget()
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.

    configViewDidload()
  }

  // MARK: - Private method
  private func fixLocationForLabelBudget() {
    consLeftLblLowerToSlider.constant = slider.lowerCenter.x + slider.frame.origin.x - (lblBudgetMin.frame.size.width / 2)

    consLeftLblUpperToView.constant = slider.upperCenter.x + slider.frame.origin.x - (lblBudgetMax.frame.size.width / 2)

    if ((consLeftLblLowerToSlider.constant + CGRectGetWidth(lblBudgetMin.frame)) - consLeftLblUpperToView.constant) > -8 {
      consLeftLblUpperToView.constant = consLeftLblUpperToView.constant + 12
      consLeftLblLowerToSlider.constant = consLeftLblLowerToSlider.constant - 12
    }
  }

  private func configViewDidload() {
    btnContinue.layer.cornerRadius = CGRectGetHeight(btnContinue.frame)/2

    slider.minimumValue = 0
    slider.maximumValue = 5000

    slider.lowerValue = 0
    slider.upperValue = 5000

    slider.minimumRange = 500

    slider.stepValue = 100.0
    slider.stepValueContinuously = true

    var sliderTrackImage = UIImage(named: "slider-default-track")
    sliderTrackImage = sliderTrackImage?.resizableImageWithCapInsets(UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0))
    slider.trackImage = sliderTrackImage

    var image = UIImage(named: "ic_filter_budget")
    image = image?.imageWithAlignmentRectInsets(UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 6))
    slider.lowerHandleImageNormal = image
    slider.upperHandleImageNormal = image

    slider.lowerHandleImageHighlighted = image
    slider.upperHandleImageHighlighted = image

    lblLocation.text = textPleaseChooseLocation

    if Common.isIPhone5or4() {
      consLeftLblLowerToSlider.constant = 28
      consLeftLblUpperToView.constant = 258.75
    } else if Common.isIPhone6plus() {
      consLeftLblLowerToSlider.constant = 75
      consLeftLblUpperToView.constant = 305.833
      consHeightViewContent.constant = screenHeight
    } else {
      consLeftLblLowerToSlider.constant = 55.5
      consLeftLblUpperToView.constant = 287.75
    }
  }

  // MARK: - Actions
  @IBAction func updateBudget(sender: AnyObject) {
    lblBudgetMin.text = "$\(Common.formatNumber(Int(slider.lowerValue)))"
    lblBudgetMax.text = "$\(Common.formatNumber(Int(slider.upperValue)))"
    fixLocationForLabelBudget()
  }

  @IBAction func tapToSelectLocation(sender: AnyObject) {
    let vc = MapLocationVC()
    if let loc = currentLocation {
      vc.isInit = true
      vc.currentLocation = loc
    }
    vc.delegate = self
    let navi = GGNavigationController(rootViewController: vc)
    presentViewController(navi, animated: true, completion: nil)
  }

  @IBAction func tapToContinue(sender: AnyObject) {
    guard let location = currentLocation else {
      lblLocation.text = textPleaseChooseLocation
      Common.showAlert(textPleaseChooseLocation)
      return
    }

    AccountFlowManager.currentUser.uLocation = location.title
    AccountFlowManager.currentUser.uLatitude = location.resourceLat
    AccountFlowManager.currentUser.uLongtitude = location.resourceLon
    AccountFlowManager.currentUser.budgetMax = slider.upperValue
    AccountFlowManager.currentUser.budgetMin = slider.lowerValue

    let vc = CreateProfileVC(update: true)
    vc.budgetMin = slider.lowerValue
    vc.budgetMax = slider.upperValue
    vc.location = currentLocation
    let navi = GGNavigationController(rootViewController: vc)
    AppDelegate.shareInstance().window?.rootViewController = navi
  }
}

// MARK: - MapLocationDelegate
extension WellcomeVC: MapLocationDelegate {
  func mapLocationDidChoose(location: ResourceObject) {
    if location.title.characters.count > 0 {
      lblLocation.text = location.title
      currentLocation = location
    } else {
      lblLocation.text = textPleaseChooseLocation
    }
  }
}
