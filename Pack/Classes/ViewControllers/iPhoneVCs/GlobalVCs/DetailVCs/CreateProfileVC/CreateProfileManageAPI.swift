//
//  CreateProfileManageAPI.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/16/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol CreateProfileDelegate: NSObjectProtocol {
  func didRequestPersonality(listPersonal: [Personalities])
  optional func didUpdateInfoSuccess()
  optional func didUpdateInfoFailed()
}

class CreateProfileManageAPI: NSObject {

  // MARK: - Variables
  weak var delegate: CreateProfileDelegate?

  // MARK - Public method
  func updateInfoUser(userId: String, budgetMin: Int = 0, budgetMax: Int = 0, locale: String = "", profession: String = "", aboutMe: String = "", workHistory: String = "", dob: String = "", education: String = "", gender: String = "", interests: String = "", logitude: Double, latitude: Double, primaryImage: String, moveInDate: String = "", prefBudgetMin: Int = 0, prefBudgetMax: Int = 0, prefProfession: String = "", prefGender: String = "", prefAgeMin: Int = 0, prefAgeMax: Int = 0, prefNotifiMessage: String = "", prefNotifiNewMatch: String = "", prefShowDiscovery: String = "") {
    let url = getApiUpdateInfoUser(userId)

    let param: [String:AnyObject] = ["budget_min"              : budgetMin,
                                     "location"               : locale,
                                     "about_me"               : aboutMe,
                                     "dob"                    : dob,
                                     "education"              : education,
                                     "interests"              : interests,
                                     "gender"                 : gender,
                                     "budget_max"             : budgetMax,
                                     "latitude"               : latitude,
                                     "longtitude"             : logitude,
                                     "primary_image"          : primaryImage,
                                     "movein_date"            : moveInDate,
                                     "pref_age_min"           : prefAgeMin,
                                     "pref_age_max"           : prefAgeMax,
                                     "pref_profession"        : prefProfession,
                                     "pref_budget_min"        : prefBudgetMin,
                                     "pref_budget_max"        : prefBudgetMax,
                                     "pref_notify_message"    : prefNotifiMessage,
                                     "pref_gender"            : prefGender,
                                     "pref_notify_new_match"  : prefNotifiNewMatch,
                                     "pref_show_in_discovery" : prefShowDiscovery]

    let header = GetHeader()

    GGWebservice.callWebServiceWithHeader(.PATCH, urlString: url, header: header, param: param) { (value, statusCode, error) in
      if let error = error {
        DHIndicator.hide()
        Common.showAlert(error.error_msg)
        self.delegate?.didUpdateInfoFailed!()
      } else {
        if let result = Mapper<Error>().map(value) {
          if result.status == "ERR" {
            DHIndicator.hide()
            Common.showToastWithMessage(textUploadInfoUserFailed)
            self.delegate?.didUpdateInfoFailed!()
          } else if result.status == "OK" {
            if statusCode == 200 || statusCode == 201 {
              self.delegate?.didUpdateInfoSuccess!()
            } else {
              DHIndicator.hide()
              Common.showToastWithMessage(textUploadInfoUserFailed)
              self.delegate?.didUpdateInfoFailed!()
            }
          }
        }
      }
    }
  }

  typealias upLoadPhotoCompleted = (indexImg: Int, result: Error?, success: Bool, error: GGWSError?) -> Void

  func uploadPhoto(userId: String, dataImage: NSData, indexImage: Int, completed: upLoadPhotoCompleted) {
    let accessToken: String = NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != nil ?NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant)!:""
    GGWebservice.uploadObjectWithStringURL(apiUpImage(userId), dataFile: dataImage, field: "image", info: nil, accessToken: accessToken, isAuthen: false, success: { (value, statusCode, error) -> Void in
      if let e = error {
        if statusCode == 413 {
          let err = GGWSError()
          err.error_id = 413
          err.error_msg = textSizeFileTooBig
          completed(indexImg: indexImage, result: nil, success: false, error: err)
        } else {
          completed(indexImg: indexImage, result: nil, success: false, error: e)
        }
      } else {
        if let result: Error = Mapper<Error>().map(value) {
          if result.status == "ERR" {
            print(result.errMessage)
          } else if result.status == "OK" {
            completed(indexImg: indexImage, result: result, success: true, error: nil)
          }
        }
      }
    }) { (value) -> Void in
      print(value)
    }
  }

  typealias deletePhotoCompleted = (success: Bool, error: GGWSError?) -> Void

  func deleteImage(imageId: String, success: deletePhotoCompleted) {
    GGWebservice.callWebServiceWithRequest(.DELETE, urlString: apiGetImageWithId(imageId), param: nil, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
      print(value)
      if statusCode == 200 || statusCode == 204 {
        success(success: true, error: nil)
      } else {
        success(success: false, error: error)
      }
    }
  }

  func getPersonalities(page: Int) {
    GGWebservice.callWebServiceWithRequest(.GET, urlString: apiGetPersonalities(page), param: nil, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
      if let _ = error {
        self.delegate?.didRequestPersonality([])
      } else {
        if statusCode == 200 {
          if let listPersonality = Mapper<ListPersonalities>().map(value) {
            self.delegate?.didRequestPersonality(listPersonality.personalities)
          } else {
            self.delegate?.didRequestPersonality([])
          }
        } else {
          if let result: Error = Mapper<Error>().map(value) {
            print(result.errMessage)
          }
        }
      }
    }

  }

  class func updateProfession(user: User) {
    let pro = DetectProfesional.detectWithUser(user)
    if pro.rawValue.characters.count > 0 {
      print("professional: \(pro.rawValue)")
      updateInfoUser(user.id, profession: pro.rawValue)
    }
  }

  class func updateProfession(user: User, linkedProfile: LinkedinProfile) {
    let pro = DetectProfesional.detectWithLinkedinProfile(user, lProfile: linkedProfile)
    if pro.rawValue.characters.count > 0 {
      print("professional: \(pro.rawValue)")
      updateInfoUser(user.id, profession: pro.rawValue)
    }
  }

  class private func updateInfoUser(userId: String, profession: String = "") {
    let url = getApiUpdateInfoUser(userId)
    print(profession)
    let param: [String:AnyObject] = ["profession"             : profession]

    let header = GetHeader()

    GGWebservice.callWebServiceWithHeader(.PATCH, urlString: url, header: header, param: param) { (value, statusCode, error) in
      print(value)
      if let error = error {
        print("Error profession: \(error.error_msg)")
      } else {
        if let result = Mapper<Error>().map(value) {
          if result.status == "ERR" {
            print("Error profession: \(result.errMessage)")
          } else if result.status == "OK" {
            if statusCode == 200 || statusCode == 201 {
              print("update professional success!")
            }
          }
        }
      }
    }
  }

  class func updateInstagramPhoto(userId: String, instagramPhotos: [ObjInstagram]) {
    let url = getApiUpdateInfoUser(userId)
    var arrImageUrl = [String]()

    for obj in instagramPhotos {
      if let instaPhoto = obj.imageInstagram, standard = instaPhoto.standardResolution {
        arrImageUrl.append(standard.url)
      }
    }
    let strPhotosUrl = arrImageUrl.joinWithSeparator("|")
    let param: [String:AnyObject] = ["insta_photos"             : strPhotosUrl]
    let header = GetHeader()

    GGWebservice.callWebServiceWithHeader(.PATCH, urlString: url, header: header, param: param) { (value, statusCode, error) in
      print(value)
      if let error = error {
        DHIndicator.hide()
        print("Error insta_photos: \(error.error_msg)")
      } else {
        if let result = Mapper<Error>().map(value) {
          if result.status == "ERR" {
            print("Error insta_photos: \(result.errMessage)")
          } else if result.status == "OK" {
            if statusCode == 200 || statusCode == 201 {
              print("update insta_photos success!")
            }
          }
        }
      }
    }
  }

  class func updateInfoUser(userId: String, workHistory: String) {
    let url = getApiUpdateInfoUser(userId)
    let param: [String:AnyObject] = ["work_history"             : workHistory]
    let header = GetHeader()

    GGWebservice.callWebServiceWithHeader(.PATCH, urlString: url, header: header, param: param) { (value, statusCode, error) in
      print(value)
      if let error = error {
        print("Error work_history: \(error.error_msg)")
      } else {
        if let result = Mapper<Error>().map(value) {
          if result.status == "ERR" {
            print("Error work_history: \(result.errMessage)")
          } else if result.status == "OK" {
            if statusCode == 200 || statusCode == 201 {
              AccountFlowManager.currentUser.workHistory = workHistory
              NSNotificationCenter.defaultCenter().postNotificationName(NotificationUpdateWorkHistorySuccess, object: nil)
            }
          }
        }
      }
    }
  }
}
