//
//  SettingVC.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/5/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class SettingVC: GGParentVC {


  // MARK: - Outlets
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet var contentView: UIView!
  @IBOutlet weak var ageSlider: NMRangeSlider!
  @IBOutlet weak var budgetSlider: NMRangeSlider!
  @IBOutlet weak var lblGender: UILabel!
  @IBOutlet weak var lblStudent: UILabel!
  @IBOutlet weak var lblMaxAge: UILabel!
  @IBOutlet weak var lblMinAge: UILabel!
  @IBOutlet weak var lblMinBudget: UILabel!
  @IBOutlet weak var lblMaxBudget: UILabel!
  @IBOutlet weak var switchMessage: UISwitch!
  @IBOutlet weak var switchMatches: UISwitch!
  @IBOutlet weak var switchShowMe: UISwitch!
  @IBOutlet weak var lblSwitchShowMe: UILabel!
  @IBOutlet weak var lblSwitchMatches: UILabel!
  @IBOutlet weak var lblSwitchMessage: UILabel!
  @IBOutlet weak var consLeftLblLowerAgeToView: NSLayoutConstraint!
  @IBOutlet weak var consLeftLblUpperAgeToView: NSLayoutConstraint!
  @IBOutlet weak var consLeftLblLowerBudgetToView: NSLayoutConstraint!
  @IBOutlet weak var consLeftLblUpperBudgetToView: NSLayoutConstraint!
  @IBOutlet weak var consTopViewVersionToHelpSupport: NSLayoutConstraint!
  @IBOutlet weak var btnSharePack: UIButton!
  @IBOutlet weak var viewRatePack: UIView!
  @IBOutlet weak var viewLegal: UIView!
  @IBOutlet weak var lblTitleLegal: UILabel!
  @IBOutlet weak var btnHelpAndSupport: UIButton!
  @IBOutlet weak var lblContactUs: UILabel!

  // MARK: - Variables
  private var pickerViewGender = NSPickerView()
  private var pickerViewStudent = NSPickerView()
  private var isChangeInfo: Bool = false

  // MARK: - Lifecycle
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "SettingVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.Back, centerType:CenterItems.Title, rightType:RightItems.None)
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.setTitleForVC("Settings and Preferences")
  }

  override func viewDidAppear(animated: Bool) {
    lblMinAge.alpha = 1
    lblMaxAge.alpha = 1

    lblMinBudget.alpha = 1
    lblMaxBudget.alpha = 1
    fixLocationForLabelBudget()
    fixLocationForLabelAge()
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    configViewDidLoad()
    configSlider()
    getInfoSetting()
    configSwitchControl()
  }

  override func tapToBack(sender: UIButton!) {
    if isChangeInfo {
      DHIndicator.show()
      updateSettingUser()
    } else {
      navigationController?.popViewControllerAnimated(true)
    }
  }

  // MARK: - Private method
  private func configViewDidLoad() {
    tableView.tableHeaderView = contentView

    btnSharePack.alpha = 0
    viewRatePack.alpha = 0
    lblTitleLegal.alpha = 0
    viewLegal.alpha = 0
    btnHelpAndSupport.alpha = 0
    lblContactUs.alpha = 0
    consTopViewVersionToHelpSupport.constant = 16

    fixSizeHeaderAfterHideField()

    pickerViewGender.delegate = self
    var arrGender: [[String:String]] = []
    arrGender.append(["name":"Choose Gender"])
    arrGender.append(["name":"All"])
    arrGender.append(["name":"Female"])
    arrGender.append(["name":"Male"])
    pickerViewGender.arrayDataSource = NSMutableArray(array: arrGender, copyItems: true)

    pickerViewStudent.delegate = self
    var arrStudent: [[String:String]] = []
    arrStudent.append(["name":"Student/Professional"])
    arrStudent.append(["name":"All"])
    arrStudent.append(["name":"Student"])
    arrStudent.append(["name":"Professional"])
    pickerViewStudent.arrayDataSource = NSMutableArray(array: arrStudent, copyItems: true)

    lblMinAge.alpha = 0
    lblMaxAge.alpha = 0

    lblMinBudget.alpha = 0
    lblMaxBudget.alpha = 0
  }

  private func getInfoSetting() {
    let currentUser = AccountFlowManager.currentUser
    if currentUser.prefGender == "male" {
      lblGender.text = "Male"
    } else if currentUser.prefGender == "female" {
      lblGender.text = "Female"
    } else {
      lblGender.text = "All"
    }
    if currentUser.preProfession == "student" {
      lblStudent.text = "Student"
    } else if currentUser.preProfession == "professional" {
      lblStudent.text = "Professional"
    } else {
      lblStudent.text = "All"
    }

    budgetSlider.lowerValue = currentUser.prefBudgetMin
    budgetSlider.upperValue = currentUser.prefBudgetMax

    let budgetMin = Int(currentUser.prefBudgetMin)
    let budgetMax = Int(currentUser.prefBudgetMax)
    lblMaxBudget.text = "$\(Common.formatNumber(budgetMax))"
    lblMinBudget.text = "$\(Common.formatNumber(budgetMin))"

    let ageMin = Int(currentUser.prefAgeMin)
    var ageMax = Int(currentUser.prefAgeMax)
    ageSlider.lowerValue = currentUser.prefAgeMin
    if currentUser.prefAgeMax > 60 {
      ageSlider.upperValue = 61
      ageMax = 61
    } else {
      ageSlider.upperValue = currentUser.prefAgeMax
    }

    if ageMax == 61 {
      lblMaxAge.text = "60+"
    } else {
      lblMaxAge.text = "\(ageMax)"
    }
    lblMinAge.text = "\(ageMin)"

    if currentUser.prefMessage == "true" {
      switchMessage.on = true
    } else if currentUser.prefMessage == "false" {
      switchMessage.on = false
    }
    if currentUser.prefNewMatch == "true" {
      switchMatches.on = true
    } else if currentUser.prefNewMatch == "false" {
      switchMatches.on = false
    }
    if currentUser.prefShowInDiscovery == "true" {
      switchShowMe.on = true
    } else {
      switchShowMe.on = false
    }
  }

  private func fixSizeHeaderAfterHideField() {
    contentView.frame.size.height = contentView.frame.size.height - 383
    tableView.tableHeaderView = contentView
  }

  private func fixLocationForLabelBudget() {
    consLeftLblLowerBudgetToView.constant = budgetSlider.lowerCenter.x + budgetSlider.frame.origin.x - (lblMinBudget.frame.size.width / 2)

    consLeftLblUpperBudgetToView.constant = budgetSlider.upperCenter.x + budgetSlider.frame.origin.x - (lblMaxBudget.frame.size.width / 2)

    if (consLeftLblUpperBudgetToView.constant - consLeftLblLowerBudgetToView.constant) <= 42.25 {
      consLeftLblUpperBudgetToView.constant = consLeftLblUpperBudgetToView.constant + (lblMaxBudget.frame.size.width / 3)
      consLeftLblLowerBudgetToView.constant = consLeftLblLowerBudgetToView.constant - (lblMinBudget.frame.size.width / 3)
    }
  }

  private func fixLocationForLabelAge() {
    consLeftLblLowerAgeToView.constant = ageSlider.lowerCenter.x + ageSlider.frame.origin.x - (lblMinAge.frame.size.width / 2)

    consLeftLblUpperAgeToView.constant = ageSlider.upperCenter.x + ageSlider.frame.origin.x - (lblMaxAge.frame.size.width / 2.5)

    if ((consLeftLblLowerAgeToView.constant + CGRectGetWidth(lblMinAge.frame)) - consLeftLblUpperAgeToView.constant) > -5 {
      consLeftLblUpperAgeToView.constant = consLeftLblUpperAgeToView.constant + 10
      consLeftLblLowerAgeToView.constant = consLeftLblLowerAgeToView.constant - 10
    }
  }

  private func updateSettingUser() {
    let api = SettingManageAPI()
    let prefBudgetMin = Int(budgetSlider.lowerValue)
    let prefBudgetMax = Int(budgetSlider.upperValue)
    let prefAgeMin = Int(ageSlider.lowerValue)
    var prefAgeMax = 0
    if Int(ageSlider.upperValue) == 61 {
      prefAgeMax = 200
    } else {
      prefAgeMax = Int(ageSlider.upperValue)
    }
    var prefNotiMessage: String = ""
    if switchMessage.on {
      prefNotiMessage = "true"
    } else {
      prefNotiMessage = "false"
    }
    var prefNewMatch: String = ""
    if switchMatches.on {
      prefNewMatch = "true"
    } else {
      prefNewMatch = "false"
    }
    var prefDiscovery: String = ""
    if switchShowMe.on {
      prefDiscovery = "true"
    } else {
      prefDiscovery = "false"
    }

    var prefGender = ""
    if lblGender.text == "Female" {
      prefGender = "female"
    } else if lblGender.text == "Male" {
      prefGender = "male"
    } else {
      prefGender = ""
    }

    var prefProfession = ""
    if lblStudent.text == "Student" {
      prefProfession = "student"
    } else if lblStudent.text == "Professional" {
      prefProfession = "professional"
    } else {
      prefProfession = ""
    }

    api.updateSetting(AccountFlowManager.currentUser.id, prefBudgetMin: prefBudgetMin, prefBudgetMax: prefBudgetMax, prefProfession: prefProfession, prefGender: prefGender, prefAgeMin: prefAgeMin, prefAgeMax: prefAgeMax, prefNotifiMessage: prefNotiMessage, prefNotifiNewMatch: prefNewMatch, prefShowDiscovery: prefDiscovery) { (success, error) in
      DHIndicator.hide()
      if let _ = error {
        Common.showToastWithMessage(textUpdateSettingFailed)
      } else {
        if AccountFlowManager.currentUser.prefMessage == "true" {
          if let deviceToken = NSUserDefaults.standardUserDefaults().objectForKey(userKeyDataDeviceToken) as? NSData {
            AccountFlowManager.shareInstance.updateRemoteNotificationDeviceToken(deviceToken)
          }
        } else {
          AccountFlowManager.shareInstance.updateRemoteNotificationDeviceToken(nil)
        }
        Common.showToastWithMessage(textUpdateSettingSuccess)
        AccountFlowManager.shareInstance.goLoadingPage()
        self.navigationController?.popViewControllerAnimated(true)
      }
    }
  }

  private func configSlider() {
    ageSlider.minimumValue = 18
    ageSlider.maximumValue = 61

    ageSlider.upperValue = 61
    ageSlider.lowerValue = 18

    ageSlider.minimumRange = 5

    var sliderAgeTrackImage = UIImage(named: "slider-default-track")
    sliderAgeTrackImage = sliderAgeTrackImage?.resizableImageWithCapInsets(UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0))
    ageSlider.trackImage = sliderAgeTrackImage

    var image = UIImage(named: "ic_filter_budget")
    image = image?.imageWithAlignmentRectInsets(UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 6))

    ageSlider.lowerHandleImageNormal = image
    ageSlider.upperHandleImageNormal = image

    ageSlider.lowerHandleImageHighlighted = image
    ageSlider.upperHandleImageHighlighted = image

    budgetSlider.minimumValue = 0
    budgetSlider.maximumValue = 5000
    budgetSlider.lowerValue = 0
    budgetSlider.upperValue = 5000

    budgetSlider.minimumRange = 500

    budgetSlider.stepValue = 100.0
    budgetSlider.stepValueContinuously = true

    var sliderBudgetTrackImage = UIImage(named: "slider-default-track")
    sliderBudgetTrackImage = sliderBudgetTrackImage?.resizableImageWithCapInsets(UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0))
    budgetSlider.trackImage = sliderBudgetTrackImage

    budgetSlider.lowerHandleImageNormal = image
    budgetSlider.upperHandleImageNormal = image

    budgetSlider.lowerHandleImageHighlighted = image
    budgetSlider.upperHandleImageHighlighted = image
  }

  private func configSwitchControl() {
    if switchShowMe.on {
      lblSwitchShowMe.text = "ON"
    } else {
      lblSwitchShowMe.text = "OFF"
    }

    if switchMatches.on {
      lblSwitchMatches.text = "ON"
    } else {
      lblSwitchMatches.text = "OFF"
    }

    if switchMessage.on {
      lblSwitchMessage.text = "ON"
    } else {
      lblSwitchMessage.text = "OFF"
    }
  }

  // MARK: - Actions
  @IBAction func tapToLincenses(sender: AnyObject) {
    print("Go to Lincenses")
  }

  @IBAction func tapToTerm(sender: AnyObject) {
    print("Go to term")
  }

  @IBAction func tapToPolicy(sender: AnyObject) {
    print("Go to policy")
  }

  @IBAction func tapRatePack(sender: AnyObject) {
    print("Rate Pack")
  }

  @IBAction func tapToHelpSupport(sender: AnyObject) {
    print("Go Help")
    if let url = NSURL(string: urlHelp) {
      UIApplication.sharedApplication().openURL(url)
    }
  }

  @IBAction func tapToSharePack(sender: AnyObject) {
    print("Share Pack")
  }

  @IBAction func tapToSelectGender(sender: AnyObject) {
    isChangeInfo = true
    if lblGender.text == "Male" {
      pickerViewGender.setValueWhenReShow(3)
    } else if lblGender.text == "Female" {
      pickerViewGender.setValueWhenReShow(2)
    } else {
      pickerViewGender.setValueWhenReShow(1)
    }
    pickerViewGender.showActionSheetInView(self.view)
  }

  @IBAction func tapToStudent(sender: AnyObject) {
    isChangeInfo = true
    if lblStudent.text == "Professional" {
      pickerViewStudent.setValueWhenReShow(3)
    } else if lblStudent.text == "Student" {
      pickerViewStudent.setValueWhenReShow(2)
    } else {
      pickerViewStudent.setValueWhenReShow(1)
    }
    pickerViewStudent.showActionSheetInView(self.view)
  }

  @IBAction func tapChangeAgeSlider(sender: AnyObject) {
    isChangeInfo = true
    lblMinAge.text = "\(Int(ageSlider.lowerValue))"

    if Int(ageSlider.upperValue) == 61 {
      lblMaxAge.text = "60+"
    } else {
      lblMaxAge.text = "\(Int(ageSlider.upperValue))"
    }
    fixLocationForLabelAge()
  }

  @IBAction func tapChangeBudgetSlider(sender: AnyObject) {
    isChangeInfo = true

    lblMinBudget.text = "$\(Common.formatNumber(Int(budgetSlider.lowerValue)))"
    lblMaxBudget.text = "$\(Common.formatNumber(Int(budgetSlider.upperValue)))"

    fixLocationForLabelBudget()
  }

  @IBAction func changeShowMe(sender: AnyObject) {
    isChangeInfo = true
    if switchShowMe.on {
      lblSwitchShowMe.text = "ON"
    } else {
      lblSwitchShowMe.text = "OFF"
    }
  }

  @IBAction func changeMatches(sender: AnyObject) {
    isChangeInfo = true
    if switchMatches.on {
      lblSwitchMatches.text = "ON"
    } else {
      lblSwitchMatches.text = "OFF"
    }
  }

  @IBAction func changeMessage(sender: AnyObject) {
    isChangeInfo = true
    if switchMessage.on {
      lblSwitchMessage.text = "ON"
    } else {
      lblSwitchMessage.text = "OFF"
    }
  }

  @IBAction func tapToLogout(sender: AnyObject) {
    if AccountFlowManager.currentUser.isLoginWithFacebook {
      AccountFlowManager.shareInstance.logoutWithFB()
    } else {
      AccountFlowManager.shareInstance.logoutWithInstagram()
    }
  }
}

// MARK: - NSPickerView Delegate
extension SettingVC: NSPickerViewDelegate {
  func picker(picker: NSPickerView!, dismissWithButtonDone done: Bool, withDic dic: [NSObject : AnyObject]!) {
    if done {
      if picker == pickerViewGender {
        lblGender.text = "\(dic["name"]!)"
      } else if picker == pickerViewStudent {
        lblStudent.text = "\(dic["name"]!)"
      }
    } else {

    }
  }
}
