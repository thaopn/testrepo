//
//  SettingManageAPI.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/25/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

class SettingManageAPI: NSObject {

  typealias updateSettingCompleted = (success: Bool, error: Error?) -> Void

  func updateSetting(userId: String, prefBudgetMin: Int = 0, prefBudgetMax: Int = 0, prefProfession: String = "", prefGender: String = "", prefAgeMin: Int
    = 0, prefAgeMax: Int = 0, prefNotifiMessage: String = "", prefNotifiNewMatch: String = "", prefShowDiscovery: String = "", completed: updateSettingCompleted) {
    let url = getApiUpdateInfoUser(userId)

    let param: [String:AnyObject] = ["pref_age_min"             : prefAgeMin,
                                     "pref_age_max"             : prefAgeMax,
                                     "pref_profession"          : prefProfession,
                                     "pref_budget_min"          : prefBudgetMin,
                                     "pref_budget_max"          : prefBudgetMax,
                                     "pref_notify_message"      : prefNotifiMessage,
                                     "pref_gender"              : prefGender,
                                     "pref_notify_new_match"    : prefNotifiNewMatch,
                                     "pref_show_in_discovery"   : prefShowDiscovery]

    let accessToken = NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant) != nil ?NSUserDefaults.standardUserDefaults().stringForKey(userAccesstokenKeyConstant)!:""
    let header = [
      Authorization : "Bearer " + accessToken
    ]

    GGWebservice.callWebServiceWithHeader(.PATCH, urlString: url, header: header, param: param) { (value, statusCode, error) in
      print(value)
      if let error = error {
        DHIndicator.hide()
        Common.showAlert(error.error_msg)
      } else {
        if let result = Mapper<Error>().map(value) {
          if result.status == "ERR" {
            completed(success: false, error: result)
          } else if result.status == "OK" {
            if statusCode == 200 || statusCode == 201 {
              let currentUser = AccountFlowManager.currentUser
              currentUser.prefAgeMin = Float(prefAgeMin)
              currentUser.prefAgeMax = Float(prefAgeMax)
              currentUser.preProfession = prefProfession
              currentUser.prefBudgetMin = Float(prefBudgetMin)
              currentUser.prefBudgetMax = Float(prefBudgetMax)
              currentUser.prefMessage = prefNotifiMessage
              currentUser.prefGender = prefGender
              currentUser.prefNewMatch = prefNotifiNewMatch
              currentUser.prefShowInDiscovery = prefShowDiscovery
            }
            completed(success: true, error: nil)
          }
        }
      }
    }
  }
}
