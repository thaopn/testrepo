//
//  MapLocationAPIManager.swift
//  DemoMapSuggest
//
//  Created by admin on 5/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import ObjectMapper

protocol MapLocationAPIDelegate: NSObjectProtocol {
  func mapLocationAPIGetCompleted(error: GGWSError?, list: [PlaceObject], nextToken: String?)
  func mapLocationAPIGetLocationDetailsCompleted(error: GGWSError?, details: [PlaceDetailObject])
}

let API_LOCATION_KEY = "AIzaSyDAvy12wUXxyqT0FoJ185jd3WbL89bXI3Q"

class MapLocationAPIManager: NSObject {

  weak var delegate: MapLocationAPIDelegate?

  //MARK: - API
  func API_GET_SUGGESTION_PLACE(latitude: Double, longitude: Double, key: String, pageToken: String) -> String {
    return "https://maps.googleapis.com/maps/api/place/search/json?location=\(latitude),\(longitude)&radius=1000&rankBy=distance&sensor=true&key=\(key)&pagetoken=\(pageToken)"
  }

  func API_GET_SEARCH_PLACE(input: String, key: String, pageToken: String) -> String {
    return "https://maps.googleapis.com/maps/api/place/textsearch/json?type=name&query=\(input)&key=\(key)&pagetoken=\(pageToken)"
  }

  func API_GET_ADDRESS(latitude: Double, longtitude: Double) -> String {
    return "https://maps.googleapis.com/maps/api/geocode/json?&latlng=\(latitude),\(longtitude)&key=\(API_LOCATION_KEY)"
  }

  func getListSearch(strSearch: String, nextPageToken: String) {
    let url = API_GET_SEARCH_PLACE(strSearch.encoding(), key: API_LOCATION_KEY, pageToken: nextPageToken)
    print("google: \(url)")
    GGWebservice.callAPIExceptParseMeta(.GET, urlString: url, param: nil, accessToken: nil) {[weak self] (value, error) -> Void in
      if let e = error {
        self?.delegate?.mapLocationAPIGetCompleted(e, list: [], nextToken: nil)
      } else {
        let list = Mapper<ListPlace>().map(value)

        if let l = list {
          self?.delegate?.mapLocationAPIGetCompleted(nil, list: l.listPlace, nextToken: l.nextPageToken)
        } else {
          self?.delegate?.mapLocationAPIGetCompleted(GGWSError.errorWithMessage("Can't mapping"), list: [], nextToken: nil)
        }
      }
    }
  }

  func getLocationDetails(lat: Double, lng: Double) {
    let url = API_GET_ADDRESS(lat, longtitude: lng)
    print("API_GET_ADDRESS: \(url)")

    GGWebservice.callAPIExceptParseMeta(.GET, urlString: url, param: nil, accessToken: nil) {[weak self] (value, error) -> Void in
      if let e = error {
        self?.delegate?.mapLocationAPIGetLocationDetailsCompleted(e, details: [])
      } else {
        let list = Mapper<PlaceDetailObjects>().map(value)

        if let l = list {
          self?.delegate?.mapLocationAPIGetLocationDetailsCompleted(nil, details: l.list)
        } else {
          self?.delegate?.mapLocationAPIGetLocationDetailsCompleted(GGWSError.errorWithMessage("Can't mapping"), details: [])
        }
      }
    }
  }
}
