//
//  MapLocationVC.swift
//  DemoMapSuggest
//
//  Created by admin on 5/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import CoreLocation

let kCenterLocationLatitude: String  = "kCenterLocationLatitude"
let kCenterLocationLongitude: String = "kCenterLocationLongitude"
let kCenterLocationZoom: String      = "kCenterLocationZoom"

protocol MapLocationDelegate: NSObjectProtocol {
  func mapLocationDidChoose(location: ResourceObject)
}

class MapLocationVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var mapView: GGMapView!
  @IBOutlet weak var btnDone: UIButton!
  @IBOutlet weak var viewContentTextField: UIView!
  @IBOutlet weak var txfSearch: UITextField!
  @IBOutlet weak var btnLocation: UIButton!

  // MARK: - Variables
  weak var delegate: MapLocationDelegate?

  var currentLocation: ResourceObject!
  var isInit = false

  private var listLocation = [PlaceObject]()
  private var locationManager = CLLocationManager()
  private let api = MapLocationAPIManager()
  private var isRequest = false
  private var nextPageToken = ""
  private var timer: NSTimer!
  private var currentText = ""
  private let apiLocationDetail = MapLocationAPIManager()

  // MARK: - Publics
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "MapLocationVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.Back, centerType:CenterItems.Title, rightType:RightItems.None)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    mapView.delegate = self
    btnDone.enabled = false

    apiLocationDetail.delegate = self
    configureView()
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    setTitleForVC("Where are you moving to?")
  }

  override func tapToBack(sender: UIButton!) {
    view.endEditing(true)
    if let _ = presentingViewController {
      dismissViewControllerAnimated(true, completion: nil)
    } else {
      navigationController?.popViewControllerAnimated(true)
    }
  }

  func textFieldDidChanged(textField: UITextField) {
    currentText = textField.text!

    if currentText.characters.count == 0 {
      btnLocation.hidden = false
      emptyAll()
    } else {
      btnLocation.hidden = true
      api.delegate = self
      timer?.invalidate()
      timer = nil
      timer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: #selector(tick), userInfo: nil, repeats: false)
    }
  }

  func tick() {
    nextPageToken = ""
    requestWithText(currentText)
  }

  // MARK: - Privates
  private func configureView() {
    txfSearch.layer.cornerRadius = CGRectGetHeight(txfSearch.frame)/2
    txfSearch.layer.masksToBounds = true
    txfSearch.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 21, height: 21))
    txfSearch.leftViewMode = UITextFieldViewMode.Always
    txfSearch.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 21))
    txfSearch.rightViewMode = UITextFieldViewMode.UnlessEditing
    txfSearch.addTarget(self, action: #selector(textFieldDidChanged(_:)), forControlEvents: UIControlEvents.EditingChanged)

    viewContentTextField.layer.masksToBounds = false
    viewContentTextField.layer.shadowOffset = CGSize(width: 0, height: 2)
    viewContentTextField.layer.shadowRadius = 1
    viewContentTextField.layer.shadowOpacity = 0.1

    tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
  }

  private func requestWithText(text: String) {
    api.getListSearch(text, nextPageToken: nextPageToken)
  }

  private func pinMapMarker() {
    txfSearch.text = currentLocation.title
    mapView.addDataOnMapWithObject(currentLocation)
    mapView.setCameraWithLocation(CLLocationCoordinate2DMake(currentLocation.resourceLat, currentLocation.resourceLon))
    btnDone.enabled = true
  }

  private func requestPermission() {
    let alertView = UIAlertController(title: "Pack", message: textRequestPermissionLocation, preferredStyle: .Alert)
    let cancelAction = UIAlertAction(title: "Cancel", style: .Default) { (action) in

    }
    alertView.addAction(cancelAction)

    let okAction = UIAlertAction(title: "Setting", style: .Default) { (action) in
      if let url = NSURL(string: UIApplicationOpenSettingsURLString) {
        UIApplication.sharedApplication().openURL(url)
      }
    }
    alertView.addAction(okAction)

    presentViewController(alertView, animated: true, completion: nil)
  }

  private func emptyAll() {
    txfSearch.text = ""
    currentText = ""
    nextPageToken = ""
    listLocation = []
    tableView.reloadData()
  }

  @IBAction func btnLocationTapped(sender: AnyObject) {
    txfSearch.resignFirstResponder()
    emptyAll()

    self.locationManager.requestAlwaysAuthorization()

    // For use in foreground
    self.locationManager.requestWhenInUseAuthorization()

    if CLLocationManager.locationServicesEnabled() {
      let status = CLLocationManager.authorizationStatus()
      switch status {
      case .NotDetermined:
        print("NotDetermined")
      case .Denied:
        print("Denied")
        requestPermission()
        return
      case .Restricted:
        print("Restricted")
      case .AuthorizedWhenInUse:
        print("AuthorizedWhenInUse")
      case .AuthorizedAlways:
        print("AuthorizedAlways")
      }

      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locationManager.startUpdatingLocation()
    }
  }

  @IBAction func btnDoneTapped(sender: AnyObject) {
    if let loc = currentLocation {
      delegate?.mapLocationDidChoose(loc)
    }
    tapToBack(sender as! UIButton)
  }
}

extension MapLocationVC: GGMapViewDelegate {
  func ggmapviewIdle() {
    if isInit == true {
      isInit = false
      if let _ = currentLocation {
        pinMapMarker()
      }
    }
  }

  func ggmapviewTapOnLocation(location: CLLocationCoordinate2D) {
    DHIndicator.show()
    apiLocationDetail.getLocationDetails(location.latitude, lng: location.longitude)
  }

  func ggmapviewTapOnInfoWindow() {
    btnDoneTapped(btnDone)
  }
}

// MARK: - UITableViewDataSource
extension MapLocationVC: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if listLocation.count > 0 {
      tableView.alpha = 1
    } else {
      tableView.alpha = 0
    }

    return listLocation.count
  }

  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell!
    listLocation[indexPath.row].printDescription()

    cell.textLabel?.text = listLocation[indexPath.row].placeAddress

    return cell!
  }
}

// MARK: - UITableViewDelegate
extension MapLocationVC: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)

    api.delegate = nil

    currentLocation = ResourceObject()
    currentLocation.resourceLat = listLocation[indexPath.row].lat
    currentLocation.resourceLon = listLocation[indexPath.row].lon
    currentLocation.title = listLocation[indexPath.row].placeAddress
    listLocation = []
    tableView.reloadData()
    txfSearch.resignFirstResponder()
    pinMapMarker()
  }
}

// MARK: - UITextFieldDelegate
extension MapLocationVC: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }

  func textFieldDidEndEditing(textField: UITextField) {
    btnLocation.hidden = false
  }

  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {

    if textField.text?.characters.count > 0 {
      btnLocation.hidden = true
    } else {
      btnLocation.hidden = false
    }
    return true
  }
}

// MARK: - MapLocationAPIDelegate
extension MapLocationVC: MapLocationAPIDelegate {
  func mapLocationAPIGetCompleted(error: GGWSError?, list: [PlaceObject], nextToken: String?) {
    if let e = error {
      print("error: \(e.error_msg)")
      Common.showToastWithMessage(e.error_msg)
    } else {
      if nextPageToken.characters.count == 0 {
        listLocation = []
      }
      listLocation += list
      if let next = nextToken {
        nextPageToken = next
      } else {
        nextPageToken = ""
      }
    }
    tableView.reloadData()
  }

  func mapLocationAPIGetLocationDetailsCompleted(error: GGWSError?, details: [PlaceDetailObject]) {
    DHIndicator.hide()
    if let e = error {
      print("error: \(e.error_msg)")
      Common.showToastWithMessage(e.error_msg)
    } else {
      if let topLocation = details.first, lat = topLocation.lat, lng = topLocation.lng {
        let obj = ResourceObject()
        obj.resourceLat = lat
        obj.resourceLon = lng
        var strs = [String]()
        for component in topLocation.addressComponents {
          if component.isCanNotUse() {

          } else {
            strs.append(component.longName)
          }
        }
        var title = ""
        if strs.count > 0 {
          title = strs.joinWithSeparator(", ")
          obj.title = title
        }

        currentLocation = obj
        pinMapMarker()
      }
    }
  }
}

// MARK: - Location Manager Delegate
extension MapLocationVC: CLLocationManagerDelegate {
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let location = manager.location {
      manager.stopUpdatingLocation()
      ggmapviewTapOnLocation(location.coordinate)
    }
  }
}

import ObjectMapper

class PlaceObject: NSObject, Mappable {
  var lat: Double! = 0
  var lon: Double! = 0
  var placeId: String! = ""
  var placeName: String! = ""
  var placeVicinity: String! = ""
  var placeIcon: String! = ""
  var placeAddress: String! = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    lat <- map["geometry.location.lat"]
    lon <- map["geometry.location.lng"]
    placeId <- map["place_id"]
    placeName <- map["name"]
    placeVicinity <- map ["vicinity"]
    placeAddress <- map["formatted_address"]

    if placeAddress != nil && placeAddress != "" {
      let array = placeAddress.componentsSeparatedByString(",")

      if array.count > 1 {
        var new = ""
        for i in 0 ..< array.count - 1 {

          if new == "" {
            new = array[i]
          } else {
            new = new + ", " + array[i]
          }
        }
        placeAddress = new
      }
    }
  }

  func printDescription() {
    print("lat: \(lat) lon: \(lon) placeName: \(placeName) vicinity: \(placeVicinity) address: \(placeAddress)")
  }
}

class ListPlace: NSObject, Mappable {
  var nextPageToken: String?
  var listPlace: [PlaceObject] = []

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {

    nextPageToken <- map["next_page_token"]

    listPlace <- map["results"]
  }
}

class AddressComponent: NSObject, Mappable {
  var longName = ""
  var shortName = ""
  var types = [String]()

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    longName    <- map["long_name"]
    shortName   <- map["short_name"]
    types       <- map["types"]
  }

  func isCountry() -> Bool {
    for type in types {
      if type == "country" {
        return true
      }
    }
    return false
  }

  func isCanNotUse() -> Bool {
    for type in types {
      if type == "country" || type == "postal_code" {
        return true
      }
    }
    return false
  }
}

class PlaceDetailObject: NSObject, Mappable {
  var addressComponents = [AddressComponent]()
  var addressFormatted = ""
  var lat: Double?
  var lng: Double?
  var placeId = ""
  var types = [String]()

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    addressComponents   <- map["address_components"]
    addressFormatted    <- map["formatted_address"]
    lat                 <- map["geometry.location.lat"]
    lng                 <- map["geometry.location.lng"]
    placeId             <- map["place_id"]
    types               <- map["types"]
  }

  func isStreetAddress() -> Bool {
    for type in types {
      if type == "street_address" {
        return true
      }
    }
    return false
  }
}

class PlaceDetailObjects: NSObject, Mappable {
  var list = [PlaceDetailObject]()

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    list <- map["results"]
  }
}
