//
//  GGMapView.swift
//  Youlook
//
//  Created by Đăng Hoà on 10/1/15.
//  Copyright (c) 2015 ThaoPN. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps


@objc protocol GGMapViewDelegate: NSObjectProtocol {
  optional func didTapOnMarkerWithIndex(index: Int)
  optional func ggmapviewTapOnLocation(location: CLLocationCoordinate2D)
  optional func ggmapviewIdle()
  optional func ggmapviewTapOnInfoWindow()
}

class GGMapView: UIView, CLLocationManagerDelegate, GMSMapViewDelegate {

  var arrayItem: NSMutableArray! = NSMutableArray()
  var arrayMarker: NSMutableArray! = NSMutableArray()

  private var btnLocationCurrent: UIButton! = UIButton()
  private var mapView: GMSMapView! = GMSMapView()
  //private var locationManager:CLLocationManager! =  CLLocationManager()

  var topRightLocation: CLLocationCoordinate2D! = CLLocationCoordinate2D()
  var bottomLeftLocation: CLLocationCoordinate2D! = CLLocationCoordinate2D()

  var timerRequest: NSTimer! = NSTimer()
  private var markerCurrentSelected: GMSMarker? = nil
  private var isTapedMarker: Bool! = false

  weak var delegate: GGMapViewDelegate?

  private var currentZoom: Float = 15

  override init(frame: CGRect) {
    super.init(frame: frame)

    commonInit()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)

    commonInit()

  }


  //Private method
  private func commonInit() {
    self.mapView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
    self.mapView.autoresizingMask = [UIViewAutoresizing.FlexibleLeftMargin, UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleRightMargin, UIViewAutoresizing.FlexibleTopMargin, UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleBottomMargin]
    self.mapView.delegate = self

    let camera = GMSCameraPosition.cameraWithTarget(CLLocationCoordinate2D(latitude: 39.6385040334459, longitude: -103.197384662926), zoom: mapView.camera.zoom)

    mapView.camera = camera
    self.addSubview(self.mapView)
//    self.btnLocationCurrent.frame = CGRectMake(12, 12 + 80, IMAGE_CURRENT_LOCATION.size.width, IMAGE_CURRENT_LOCATION.size.height)
//    self.btnLocationCurrent.setImage(IMAGE_CURRENT_LOCATION, forState: UIControlState.Normal)
//    self.btnLocationCurrent.addTarget(self, action: #selector(tapMyLocation(_:)), forControlEvents: UIControlEvents.TouchUpInside)
//    self.addSubview(self.btnLocationCurrent)

//    self.locationManager.requestWhenInUseAuthorization()
//    self.locationManager.requestAlwaysAuthorization()
//    self.locationManager.delegate = self
//    self.locationManager.distanceFilter = 1
//    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
//    self.locationManager.startUpdatingLocation()

    self.mapView.myLocationEnabled = true
    self.mapView.settings.rotateGestures = false
  }


  private func createPinOnMap(object: ResourceObject, index: Int) {
    clearMap()

    let marker: GMSMarker = GMSMarker()

    marker.icon = UIImage(named: "ic_map_marker")
    marker.groundAnchor = CGPoint(x: 0.3, y: 1)

    let dic: NSDictionary = [
      "title": object.title,
      ]

    marker.userData = dic
    marker.position = CLLocationCoordinate2DMake(object.resourceLat, object.resourceLon)
    marker.infoWindowAnchor = CGPoint(x: 0.3, y: -0.3)
    marker.appearAnimation = kGMSMarkerAnimationPop


    self.arrayMarker = [marker]

    NSLog("category %f , %f", marker.position.latitude, marker.position.longitude)


    marker.map = self.mapView

    mapView.selectedMarker = marker
  }

  func setCameraWithLocation(location: CLLocationCoordinate2D) {

    let camera = GMSCameraPosition.cameraWithLatitude(location.latitude, longitude: location.longitude, zoom: mapView.camera.zoom)

    CATransaction.begin()
    CATransaction.setValue(NSNumber(float: 0.5), forKey: kCATransactionAnimationDuration)
    self.mapView.animateToCameraPosition(camera)
    CATransaction.commit()
  }

  private func getLocationTopLeftBottomRightOnMap() {

    let topRightPoint: CGPoint = CGPointMake(self.mapView.frame.size.width, 0)
    let topRightLocation: CLLocationCoordinate2D = self.mapView.projection.coordinateForPoint(topRightPoint)
    let bottomLeftPoint: CGPoint = CGPointMake(0, self.mapView.frame.size.height)
    let bottomLeftLocation: CLLocationCoordinate2D = self.mapView.projection.coordinateForPoint(bottomLeftPoint)
    self.topRightLocation = topRightLocation
    self.bottomLeftLocation = bottomLeftLocation
  }

  private func getLocationCenter() -> CLLocationCoordinate2D {
    let centerPoint: CGPoint = CGPointMake(self.mapView.frame.size.width/2, self.mapView.frame.size.height/2)
    let centerLocation: CLLocationCoordinate2D = self.mapView.projection.coordinateForPoint(centerPoint)

    return centerLocation
  }

  //MAKR:Map Delegate
  func mapView(mapView: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
    print(coordinate.latitude)
    print(coordinate.longitude)

    if let d = delegate where d.respondsToSelector(#selector(GGMapViewDelegate.ggmapviewTapOnLocation(_:))) {
      d.ggmapviewTapOnLocation!(coordinate)
    }
  }

  func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {

    currentZoom = mapView.camera.zoom

    self.delegate?.ggmapviewIdle!()

    if self.isTapedMarker == true {
      self.isTapedMarker = false
    } else {
      if self.timerRequest != nil {
        self.timerRequest.invalidate()
        self.timerRequest = nil
      }


      self.timerRequest = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector:#selector(didIdleAtCameraPosition), userInfo:nil, repeats: false)
    }

  }

  func didIdleAtCameraPosition() -> Void {
    self.getLocationTopLeftBottomRightOnMap()

    //self.delegate?.getListFeedExploreWhenIdleMap!()
  }


  func mapView(mapView: GMSMapView, willMove gesture: Bool) {
    if self.timerRequest != nil {
      self.timerRequest.invalidate()
      self.timerRequest = nil
    }
  }

  func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {


    self.isTapedMarker = true

    if self.markerCurrentSelected != nil {

    } else {
      //
    }


    self.markerCurrentSelected = marker

    self.setCameraWithLocation(marker.position)

    //self.delegate?.didTapOnMarkerWithIndex!(Int((self.markerCurrentSelected?.userData as! NSDictionary)["index"] as! String)!)

    //self.delegate?.returnCenterLocationMap!(self.getLocationCenter())

    return true
  }

  func mapView(mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {

    let title = (marker.userData as! NSDictionary).stringForKey("title")
    let desc = "Use this location"

    var size = Common.sizeOfString(title, inFont: getRobotoMediumWith(14), andMaxHeight: 15)
    size.width += 60
    if size.width > screenWidth - 40 {
      size.width = screenWidth - 40
    }
    if size.width < 165 {
      size.width = 185
    }

    let view = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: 65))

    let imvBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 8))
    imvBackground.image = UIImage(named: "img_infowindow_bgrd")
    view.addSubview(imvBackground)

    let imvArrow = UIImageView(image: UIImage(named: "ic_down_arrow_blue"))
    imvArrow.frame.origin = CGPoint(x: CGRectGetWidth(view.bounds)/2 - imvArrow.frame.width/2, y: CGRectGetMaxY(view.bounds) - imvArrow.frame.height)
    view.addSubview(imvArrow)

    let imvArrowR = UIImageView(image: UIImage(named: "ic_right_arrow_white"))
    imvArrowR.frame.origin = CGPoint(x: CGRectGetWidth(view.bounds) - 20 - imvArrowR.frame.width, y: view.bounds.height/2 - imvArrowR.frame.height/2 - 8)
    view.addSubview(imvArrowR)

    let lblTitle = UILabel(frame: CGRect(x: 20, y: 10, width: size.width - 60, height: 15))
    lblTitle.text = title
    lblTitle.font = getRobotoMediumWith(14)
    lblTitle.textColor = UIColor.whiteColor()
    view.addSubview(lblTitle)

    let lblDesc = UILabel(frame: CGRect(x: 20, y: CGRectGetMaxY(lblTitle.frame) + 4, width: size.width - 60, height: 15))
    lblDesc.text = desc
    lblDesc.font = getRobotoRegularWith(14)
    lblDesc.textColor = UIColor.whiteColor()
    view.addSubview(lblDesc)

    return view
  }

  func mapView(mapView: GMSMapView, didTapInfoWindowOfMarker marker: GMSMarker) {
    print("Tap On InfoWindow")
    delegate?.ggmapviewTapOnInfoWindow!()
  }

  func tapMyLocation(sender: UIButton!) {

    //self.setCameraWithLocation(self.locationManager.location!.coordinate, zoom: currentZoom)
  }

  //MAKR: Public Method
  internal func setFrameCurrentLocationButton(frame: CGRect) -> Void {
    self.btnLocationCurrent.frame = frame
  }

  func getNumberZoomCenter() -> Float {

    return self.mapView.camera.zoom
  }

  func clearMap() -> Void {
    self.arrayItem.removeAllObjects()
    self.arrayMarker.removeAllObjects()
    self.markerCurrentSelected = nil
    self.mapView.clear()
  }

//  internal func addDataOnMapWithObjects(objects:NSMutableArray)->Void{
//    var i = self.arrayItem.count
//    self.arrayItem.addObjectsFromArray(objects as [AnyObject])
//
//    for object in objects {
//
//      let resource = (object as! FeedObject).feedObject!
//      resource.resourceLat = (object as! FeedObject).feedLat!
//      resource.resourceLon = (object as! FeedObject).feedLon!
//      self.createPinOnMap(resource, index:i)
//      i++
//    }
//  }

  internal func addDataOnMapWithObject(object: ResourceObject) {
    createPinOnMap(object, index: 0)
  }

  internal func setMarkerCurrentAtIndex(index: Int) -> Void {

    if self.timerRequest != nil {
      self.timerRequest.invalidate()
      self.timerRequest = nil
    }
    if index < self.arrayMarker.count && index >= 0 {

      self.isTapedMarker = true

//      if self.markerCurrentSelected != nil {
//
//        if (self.markerCurrentSelected?.userData as! NSDictionary).stringForKey("type") == ResourceType.Topic{
//          self.markerCurrentSelected!.icon = IMAGE_PIN_TOPIC
//        }else if (self.markerCurrentSelected?.userData as! NSDictionary).stringForKey("type") == ResourceType.Article{
//          self.markerCurrentSelected!.icon = IMAGE_PIN_ARTICLE
//        }else if (self.markerCurrentSelected?.userData as! NSDictionary).stringForKey("type") == ResourceType.Video{
//          self.markerCurrentSelected!.icon = IMAGE_PIN_VIDEO
//        }else{
//          self.markerCurrentSelected!.icon = IMAGE_PIN_PHOTO
//        }

//      }else{
//        //
//      }

      let marker = self.arrayMarker.objectAtIndex(index) as! GMSMarker

//      if (marker.userData as! NSDictionary).stringForKey("type") == ResourceType.Topic{
//        marker.icon = IMAGE_PIN_ACTIVE_TOPIC
//      }else if (marker.userData as! NSDictionary).stringForKey("type") == ResourceType.Article{
//        marker.icon = IMAGE_PIN_ACTIVE_ARTICLE
//      }else if (marker.userData  as! NSDictionary).stringForKey("type") == ResourceType.Video{
//        marker.icon = IMAGE_PIN_ACTIVE_VIDEO
//      }else{
//        marker.icon = IMAGE_PIN_ACTIVE_PHOTO
//      }

      self.markerCurrentSelected = marker

      self.setCameraWithLocation(marker.position)

      //self.delegate?.returnCenterLocationMap!(self.getLocationCenter())

      //            marker.map = nil
      //            marker.map = self.mapView
    }

  }



}

class ResourceObject: NSObject {
  var title: String = ""
  var resourceLon: Double = 0
  var resourceLat: Double = 0
}
