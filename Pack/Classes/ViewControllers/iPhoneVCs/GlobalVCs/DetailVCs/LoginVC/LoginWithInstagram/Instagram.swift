//
//  Instagram.swift
//  PhotoBrowser
//
//  Created by Zhouqi Mo on 12/22/14.
//  Copyright (c) 2014 Zhouqi Mo. All rights reserved.
//

import Alamofire
import UIKit

struct Instagram {

  enum Router: URLRequestConvertible {
    static let baseURLString = "https://api.instagram.com"
    static let clientID = "3dd781bb7a22479ea013628211f65355"
    static let redirectURI = "http://greenglobal.vn/"
    static let clientSecret = "c5e519244c784827b2affedb11cedde2"

    case PopularPhotos(String, String)
    case requestOauthCode

    static func requestAccessTokenURLStringAndParms(code: String) -> (URLString: String, Params: [String: AnyObject]) {
      let params = ["client_id": Router.clientID, "client_secret": Router.clientSecret, "grant_type": "authorization_code", "redirect_uri": Router.redirectURI, "code": code]
      let pathString = "/oauth/access_token"
      let urlString = Instagram.Router.baseURLString + pathString
      return (urlString, params)
    }

    // MARK: - URLRequestConvertible
    var URLRequest: NSMutableURLRequest {
      let result: (path: String, parameters: [String: AnyObject]?) = {
        switch self {
        case .PopularPhotos (let userID, let accessToken):
          let params = ["access_token": accessToken]
          let pathString = "/v1/users/" + userID + "/media/recent"
          return (pathString, params)

        case .requestOauthCode:
          let pathString = "/oauth/authorize/?client_id=" + Router.clientID + "&redirect_uri=" + Router.redirectURI + "&response_type=code"
          return (pathString, nil)
        }
      }()

      let BaeseURL = NSURL(string: Router.baseURLString)!
      let URLRequest = NSURLRequest(URL: BaeseURL.URLByAppendingPathComponent(result.path))
      let encoding = Alamofire.ParameterEncoding.URL
      return encoding.encode(URLRequest, parameters: result.parameters).0
    }
  }
}
