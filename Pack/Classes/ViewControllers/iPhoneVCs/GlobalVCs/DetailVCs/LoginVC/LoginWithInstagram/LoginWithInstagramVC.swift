//
//  LoginWithInstagramVC.swift
//  Pack
//
//  Created by admin on 5/22/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

protocol LoginWithInstagramDelegate: NSObjectProtocol {
  func loginWithInstagramCompleted(userId: String, userName: String, instaToken: String, error: NSError?)
}

class LoginWithInstagramVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var webView: UIWebView!

  // MARK: - Variables
  weak var delegate: LoginWithInstagramDelegate?

  // MARK: - Public
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "LoginWithInstagramVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType: LeftItems.Cancel, centerType: CenterItems.Title, rightType: RightItems.None)
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    setTitleForVC("Pack")

    webView.hidden = true
    NSURLCache.sharedURLCache().removeAllCachedResponses()
    if let cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies {
      for cookie in cookies {
        NSHTTPCookieStorage.sharedHTTPCookieStorage().deleteCookie(cookie)
      }
    }

    print(Instagram.Router.requestOauthCode.URLRequest.URL)
    var url = Instagram.Router.requestOauthCode.URLRequest.URL!.absoluteString
    url = url.stringByReplacingOccurrencesOfString("%3F", withString: "?")
    print(url)

    let request = NSURLRequest(URL: NSURL(string: url)!, cachePolicy: .ReloadIgnoringLocalAndRemoteCacheData, timeoutInterval: Double(GGWSRequestTimeOutDefault))
    self.webView.loadRequest(request)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    webView.scrollView.contentInset.top = 64
  }

  typealias requestPhotosInstagramSuccess = (result: [ObjInstagram]?) -> Void

  class func getPhotos(userId: String, accessToken: String, completed: requestPhotosInstagramSuccess) {
    let request = Instagram.Router.PopularPhotos(userId, accessToken)
    Alamofire.request(request).responseJSON() {
       response in

      switch response.result {
      case .Success(let jsonObject):
        //debugPrint(jsonObject)
        let json = JSON(jsonObject)
        print("Instagram photos: \(json)")

        if let listObjInsta = Mapper<ListObjInstagram>().map(jsonObject) {
          print(listObjInsta.data.count)
          completed(result: listObjInsta.data)
        } else {
          completed(result: nil)
        }
      case .Failure:
        completed(result: nil)
        break
      }
    }
  }
}

extension LoginWithInstagramVC: UIWebViewDelegate {
  func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
    DHIndicator.show()
    debugPrint(request.URLString)
    let urlString = request.URLString
    if let range = urlString.rangeOfString(Instagram.Router.redirectURI + "?code=") {

      let location = range.endIndex
      let code = urlString.substringFromIndex(location)
      debugPrint(code)
      requestAccessToken(code)
      return false
    }
    return true
  }

  func requestAccessToken(code: String) {
    let request = Instagram.Router.requestAccessTokenURLStringAndParms(code)

    Alamofire.request(.POST, request.URLString, parameters: request.Params)
      .responseJSON {
        response in
        switch response.result {
        case .Success(let jsonObject):
          let json = JSON(jsonObject)
          print("Instagram json: \(json)")
          if let accessToken = json["access_token"].string,
            userID = json["user"]["id"].string,
            userName = json["user"]["username"].string {

            self.delegate?.loginWithInstagramCompleted(userID, userName: userName, instaToken: accessToken, error: nil)
            self.dismissViewControllerAnimated(true, completion: nil)
          }
        case .Failure:
          self.delegate?.loginWithInstagramCompleted("", userName: "", instaToken: "", error: response.result.error)
          self.dismissViewControllerAnimated(true, completion: nil)
          break
        }
    }
  }

  func webViewDidFinishLoad(webView: UIWebView) {
    webView.hidden = false
    DHIndicator.hide()
  }

  func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
    DHIndicator.hide()
    print(error?.localizedDescription)
    if let e = error {
      print(e.localizedDescription)
      if e.localizedDescription == "The URL can’t be shown" {
        dismissViewControllerAnimated(true, completion: nil)
        Common.showToastWithMessage(textInstaAuthen)
      }
    }
  }
}

class InstagramPhoto: NSObject, Mappable {
  var iLowResolutionLink  = ""
  var iThumbnail          = ""
  var iStandardResolution = ""
  var iType               = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    iLowResolutionLink    <- map["images.low_resolution.url"]
    iThumbnail            <- map["images.thumbnail.url"]
    iStandardResolution   <- map["images.standard_resolution.url"]
    iType                 <- map["type"]
  }
}

class ListInstagramPhotos: NSObject, Mappable {
  var list = [InstagramPhoto]()
  var nextLink = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    list        <- map["data"]
  }
}
