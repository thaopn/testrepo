//
//  LoginWithLinkedIn.swift
//  Pack
//
//  Created by admin on 5/26/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import ObjectMapper

protocol LoginWithLinkedInDelegate: NSObjectProtocol {
  func loginWithLinkedInCompleted(companyAndRole: String, token: String, error: Error?)
  func loginWithLinkedInProfile(profile: LinkedinProfile?, error: Error?)
}

class LoginWithLinkedIn: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var webView: UIWebView!

  // MARK: - Variables
  weak var delegate: LoginWithLinkedInDelegate?

  // MARK: - Constants
  let linkedInKey = "751ft4pg8bs4cd"
  let linkedInSecret = "pfAjqgIsZ05gJRo2"
  let authorizationEndPoint = "https://www.linkedin.com/uas/oauth2/authorization"
  let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"

  // MARK: - Publics
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "LoginWithLinkedIn", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType: LeftItems.Cancel, centerType: CenterItems.Title, rightType: RightItems.None)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    webView.scrollView.contentInset.top = 64
    webView.delegate = self
    webView.hidden = true

    startAuthorization()
  }

  // MARK: - Privates
  private func startAuthorization() {
    // Specify the response type which should always be "code".
    let responseType = "code"

    // Set the redirect URL. Adding the percent escape characthers is necessary.
    let redirectURL = "https://com.greenglobal.linkedin.oauth/oauth".stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())!

    // Create a random string based on the time intervale (it will be in the form linkedin12345679).
    let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"

    // Set preferred scope.
    let scope = "r_basicprofile%20rw_company_admin"

    // Create the authorization URL string.
    var authorizationURL = "\(authorizationEndPoint)?"
    authorizationURL += "response_type=\(responseType)&"
    authorizationURL += "client_id=\(linkedInKey)&"
    authorizationURL += "redirect_uri=\(redirectURL)&"
    authorizationURL += "state=\(state)&"
    authorizationURL += "scope=\(scope)"

    print(authorizationURL)

    // Create a URL request and load it in the web view.
    let request = NSURLRequest(URL: NSURL(string: authorizationURL)!)
    webView.loadRequest(request)
  }


  private func requestForAccessToken(authorizationCode: String) {
    let grantType = "authorization_code"

    let redirectURL = "https://com.greenglobal.linkedin.oauth/oauth".stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())!

    // Set the POST parameters.
    var postParams = "grant_type=\(grantType)&"
    postParams += "code=\(authorizationCode)&"
    postParams += "redirect_uri=\(redirectURL)&"
    postParams += "client_id=\(linkedInKey)&"
    postParams += "client_secret=\(linkedInSecret)"

    // Convert the POST parameters into a NSData object.
    let postData = postParams.dataUsingEncoding(NSUTF8StringEncoding)


    // Initialize a mutable URL request object using the access token endpoint URL string.
    let request = NSMutableURLRequest(URL: NSURL(string: accessTokenEndPoint)!)

    // Indicate that we're about to make a POST request.
    request.HTTPMethod = "POST"

    // Set the HTTP body using the postData object created above.
    request.HTTPBody = postData

    // Add the required HTTP header field.
    request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")

    // Initialize a NSURLSession object.
    let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    DHIndicator.show()
    // Make the request.
    let task: NSURLSessionDataTask = session.dataTaskWithRequest(request) { (data, response, error) -> Void in

      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        DHIndicator.hide()
      })
      // Get the HTTP status code of the request.
      let statusCode = (response as! NSHTTPURLResponse).statusCode

      if statusCode == 200 {
        // Convert the received JSON data into a dictionary.
        do {
          let dataDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if let accessToken = dataDictionary["access_token"] as? String {
              DHIndicator.show()
              self.getCompanyAndRole(accessToken)
            } else {
              let e = Error()
              e.errMessage = "No Data"
              self.delegate?.loginWithLinkedInCompleted("", token: "", error: e)
            }
          })
        } catch {
          print("Could not convert JSON data into a dictionary.")
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let e = Error()
            e.errMessage = "Could not convert JSON data into a dictionary."
            self.delegate?.loginWithLinkedInCompleted("", token: "", error: e)
          })
        }
      }
    }

    task.resume()
  }

  func getCompanyAndRole(accessToken: String) {
    print("Accesstoken Linkedin: \(accessToken)")
    // Specify the URL string that we'll get the profile info from.
    let targetURLString = "https://api.linkedin.com/v1/people/~:(id,num-connections,picture-url,public-profile-url,headline,positions)?format=json"

    // Initialize a mutable URL request object.
    let request = NSMutableURLRequest(URL: NSURL(string: targetURLString)!)

    // Indicate that this is a GET request.
    request.HTTPMethod = "GET"

    // Add the access token as an HTTP header field.
    request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")

    // Initialize a NSURLSession object.
    let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    DHIndicator.show()
    // Make the request.
    let task: NSURLSessionDataTask = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        DHIndicator.hide()
      })

      // Get the HTTP status code of the request.
      let statusCode = (response as! NSHTTPURLResponse).statusCode

      if statusCode == 200 {
        // Convert the received JSON data into a dictionary.
        do {
          let dataDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)

          print(dataDictionary)

          let profile = Mapper<LinkedinProfile>().map(dataDictionary)

          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if let pro = profile {
              self.delegate?.loginWithLinkedInCompleted(pro.lHeadline, token: accessToken, error: nil)
              self.delegate?.loginWithLinkedInProfile(pro, error: nil)
            } else {
              let e = Error()
              e.errMessage = "No Data"
              self.delegate?.loginWithLinkedInCompleted("", token: "", error: e)
              self.delegate?.loginWithLinkedInProfile(nil, error: e)
            }
          })
        } catch {
          print("Could not convert JSON data into a dictionary.")

          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let e = Error()
            e.errMessage = "Could not convert JSON data into a dictionary."
            self.delegate?.loginWithLinkedInCompleted("", token: "", error: e)
            self.delegate?.loginWithLinkedInProfile(nil, error: e)
          })
        }
      }
      self.dismissViewControllerAnimated(true, completion: nil)
    }

    task.resume()
  }
}


// MARK: - UIWebViewDelegate Functions
extension LoginWithLinkedIn: UIWebViewDelegate {
  func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
    DHIndicator.show()
    let url = request.URL!
    print(url)

    if url.host == "com.greenglobal.linkedin.oauth" {
      if url.absoluteString.rangeOfString("code") != nil {
        let urlParts = url.absoluteString.componentsSeparatedByString("?")
        let code = urlParts[1].componentsSeparatedByString("=")[1]

        requestForAccessToken(code)
      }
    }
    return true
  }

  func webViewDidFinishLoad(webView: UIWebView) {
    DHIndicator.hide()
    webView.hidden = false
  }

  func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
    DHIndicator.hide()
    print(error?.localizedDescription)
  }
}

class Company: NSObject, Mappable {
  var name = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    name <- map["name"]
  }
}

class Position: NSObject, Mappable {
  var company: Company?
  var role = ""

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    company <- map["company"]
    role    <- map["title"]
  }
}

class Positions: NSObject, Mappable {
  var value = 0
  var list = [Position]()

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    value   <- map["_total"]
    list    <- map["values"]
  }
}

class LinkedinProfile: NSObject, Mappable {
  var lHeadline = ""
  var lNumConnections = 0
  var lPositions: Positions?

  required convenience init?(_ map: Map) {
    self.init()
  }

  func mapping(map: Map) {
    lHeadline       <- map["headline"]
    lNumConnections <- map["numConnections"]
    lPositions      <- map["positions"]
  }
}
