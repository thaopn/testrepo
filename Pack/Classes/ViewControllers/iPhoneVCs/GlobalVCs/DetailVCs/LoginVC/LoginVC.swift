//
//  LoginVC.swift
//  Pack
//
//  Created by Pham Ngoc Thao on 5/3/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class LoginVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var pageControl: UIPageControl!
  @IBOutlet weak var lblNote: UILabel!
  @IBOutlet weak var btnLoginInstagram: UIButton!
  @IBOutlet weak var btnLoginFacebook: UIButton!
  @IBOutlet weak var scrMain: UIScrollView!
  @IBOutlet weak var viewContent: UIView!
  @IBOutlet weak var lblBySigning: LabelTextkit!
  @IBOutlet weak var lblDescription: UILabel!
  @IBOutlet weak var consWidthViewContent: NSLayoutConstraint!

  // MARK: - Variables
  private let listImage = ["img_login_1.png", "img_login_2.png", "img_login_3.png"]
  private let listDesc = [textLoginVCSlideOne, textLoginVCSlideTwo, textLoginVCSlideThree]
  private var currentPage = 0

  // MARK: - Public Func
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
  }

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "LoginVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.None, centerType:CenterItems.None, rightType:RightItems.None)
  }

  // MARK: - Private Func
  private func configureView() {
    scrMain.showsVerticalScrollIndicator = false
    scrMain.showsHorizontalScrollIndicator = false

    btnLoginFacebook.layer.cornerRadius = CGRectGetHeight(btnLoginFacebook.frame)/2
    btnLoginFacebook.titleLabel?.font = getRobotoMediumWith(16)

    btnLoginInstagram.layer.cornerRadius = CGRectGetHeight(btnLoginInstagram.frame)/2
    btnLoginFacebook.titleLabel?.font = getRobotoMediumWith(16)

    lblNote.text = textLoginVCNote
    lblNote.font = getRobotoRegularWith(12)

    lblDescription.text = listDesc[0]
    lblDescription.font = getRobotoMediumWith(16)

    collectionView.registerNib(UINib(nibName: "LoginCollectionCell", bundle: nil), forCellWithReuseIdentifier: "LoginCollectionCell")

    let bySigning = NSStringAttributesObject()
    bySigning.string = textLoginVCTermNote
    bySigning.font = getRobotoRegularWith(12)
    bySigning.isSelect = false
    bySigning.color = UIColor.whiteColor()

    let terms = NSStringAttributesObject()
    terms.string = textTermOfService
    terms.font = getRobotoRegularWith(12)
    terms.isSelect = true
    terms.underline = true
    terms.color = UIColor.whiteColor()

    lblBySigning.setAttributesTextWithArrayText([bySigning, terms], labelHighlightType: LabelHighlightTypeBackground, withColor: UIColor.grayColor()) { (idx, obj) -> Void in
      self.tapToTermsOfService()
    }
  }

  private func logInInsta(instaToken: String) {
    AccountFlowManager.shareInstance.logInWithInstagram(instaToken)
  }

  // MARK: - Actions
  @IBAction func tapToLoginFacebook(sender: AnyObject) {
    DHSocial.getProfileFBWithComplete(self) { (userInfo, isCancelSignIn, error) in
      if let error = error {
        print(error.localizedDescription)
      } else {
        if let isCancelSignIn = isCancelSignIn {
          if isCancelSignIn {
            print("SignIn Canceled")
          } else {
            print(userInfo?.fbToken)
            AccountFlowManager.shareInstance.loginWithFB(userInfo)
          }
        }
      }
    }
  }

  @IBAction func tapToLoginInstagram(sender: AnyObject) {
    let loginVC = LoginWithInstagramVC()
    loginVC.delegate = self
    let vc = GGNavigationController(rootViewController: loginVC)
    presentViewController(vc, animated: true, completion: nil)
  }

  func tapToTermsOfService() {
    print("Terms of Service")
    let vc = WebViewVC()
    let navi = GGNavigationController(rootViewController: vc)
    presentViewController(navi, animated: true, completion: nil)
  }

}

// MARK: - UICollectionViewDataSource
extension LoginVC: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return listImage.count
  }

  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("LoginCollectionCell", forIndexPath: indexPath) as! LoginCollectionCell

    cell.imvIntroduce.image = UIImage(named: listImage[indexPath.row])

    return cell
  }
}

// MARK: - UICollectionViewDelegate
extension LoginVC: UICollectionViewDelegate {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return collectionView.frame.size
  }

  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    if scrollView == self.collectionView {
      let pageWidth  = self.collectionView.frame.size.width
      let page = Int(self.collectionView.contentOffset.x/pageWidth)

      pageControl.currentPage = page
      if page >= 0 && page < 3 {

        if page != currentPage {
          currentPage = page
          UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.lblDescription.alpha = 0
            }, completion: { (finished) -> Void in
              self.lblDescription.text = self.listDesc[page]
              UIView.animateWithDuration(0.2, animations: { () -> Void in
                self.lblDescription.alpha = 1
              })
          })
        }
      }
    }
  }
}

// MARK: - UICollectionViewDelegate
extension LoginVC: LoginWithInstagramDelegate {
  func loginWithInstagramCompleted(userId: String, userName: String, instaToken: String, error: NSError?) {
    NSUserDefaults.standardUserDefaults().setObject(instaToken, forKey: userInstaToken)
    NSUserDefaults.standardUserDefaults().synchronize()
    print(instaToken)
    logInInsta(instaToken)
  }
}
