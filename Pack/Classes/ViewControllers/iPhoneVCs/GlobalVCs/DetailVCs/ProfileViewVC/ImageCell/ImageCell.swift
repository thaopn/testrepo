//
//  ImageCell.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/6/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {

  // MARK: - Outlet
  @IBOutlet weak var imgCollection: UIImageView!

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code

    imgCollection.layer.cornerRadius = 3
    imgCollection.clipsToBounds = true
  }

  // MARK: - Public method
  func setImageForCell(imgStringLeft: String) {
    imgCollection.image = nil
    if let urlLeft = NSURL(string: imgStringLeft) {
      imgCollection.kf_setImageWithURL(urlLeft)
    }
  }

}
