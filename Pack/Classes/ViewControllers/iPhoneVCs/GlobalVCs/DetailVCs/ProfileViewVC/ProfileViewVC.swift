//
//  ProfileViewVC.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/5/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit


protocol ProfileViewDelegate: NSObjectProtocol {
  func profileViewLike()
  func profileViewDislike()
}

class ProfileViewVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet var headerView: UIView!
  @IBOutlet weak var headerContentView: UIView!
  @IBOutlet weak var imgCover: UIImageView!
  @IBOutlet weak var lblUserName: UILabel!
  @IBOutlet weak var lblJob: UILabel!
  @IBOutlet weak var lblSchool: UILabel!
  @IBOutlet weak var lblBudget: UILabel!
  @IBOutlet weak var lblShareInstagram: UILabel!
  @IBOutlet weak var btnConnectInstagram: UIButton!
  @IBOutlet weak var btnCancel: UIButton!
  @IBOutlet weak var btnOk: UIButton!
  @IBOutlet weak var lblDescriptionTitle: UILabel!
  @IBOutlet weak var lblInfoUser: UILabel!
  @IBOutlet weak var collectionViewImage: UICollectionView!
  @IBOutlet weak var pageControlCollection: UIPageControl!
  @IBOutlet weak var viewJobAndBudget: UIView!
  @IBOutlet weak var consHeightCollectionView: NSLayoutConstraint!
  @IBOutlet weak var consWidthCollectionView: NSLayoutConstraint!
  @IBOutlet weak var viewInstagram: UIView!
  @IBOutlet weak var consHeightViewInstagram: NSLayoutConstraint!
  @IBOutlet weak var consBottomLblJob: NSLayoutConstraint!
  @IBOutlet weak var consBottomLblSchool: NSLayoutConstraint!
  @IBOutlet weak var consTopLblJob: NSLayoutConstraint!
  @IBOutlet weak var consBottomLblBudget: NSLayoutConstraint!
  @IBOutlet weak var coverCollectionView: UICollectionView!
  @IBOutlet weak var coverPagingControl: UIPageControl!
  @IBOutlet weak var viewDescription: UIView!
  @IBOutlet weak var viewLineDescription: UIView!
  @IBOutlet weak var viewPersonalities: ProfileViewPersonalitiesView!
  @IBOutlet weak var consPersonalitiesViewHeight: NSLayoutConstraint!
  @IBOutlet weak var lblTitleShareInsta: UILabel!
  @IBOutlet weak var consTopLblDescriptionToPersonalitiesView: NSLayoutConstraint!
  @IBOutlet weak var consTopLblAboutToBottomContainer: NSLayoutConstraint!
  @IBOutlet weak var consBottomLblDescriptionToLblAbout: NSLayoutConstraint!

  // MARK: - Variables
  weak var delegate: ProfileViewDelegate?
  private var arrImageInstagram = [String]()
  private var user: User!
  private var widthCell: CGFloat = 0.0

  // MARK: - Lifecycle
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "ProfileViewVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.Close, centerType:CenterItems.None, rightType:RightItems.None)
  }


  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    setHidenNaviBar(true)
  }

  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    fixSizeHeader()
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.

    calculateCellSize()
    configViewDidLoad()
    setupView()
    getPhotoInstagram()

  }

  internal func setupViewWith(user: User) {
    self.user = user
  }

  // MARK: - Private method
  private func configViewDidLoad() {
    btnConnectInstagram.layer.cornerRadius = 3
    tableView.tableHeaderView = headerView

    collectionViewImage.dataSource = self
    collectionViewImage.delegate = self
    collectionViewImage.contentSize = CGSize(width: 327, height: collectionViewImage.frame.size.height)
    collectionViewImage.registerNib(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "ImageCell")

    coverCollectionView.delegate = self
    coverCollectionView.dataSource = self
    coverCollectionView.contentSize = CGSize(width: screenWidth, height: 320)
    coverCollectionView.registerNib(UINib(nibName: "CoverImageCell", bundle: nil), forCellWithReuseIdentifier: "CoverImageCell")

    lblShareInstagram.text = "Instagram Photos (\(arrImageInstagram.count))"

    consHeightCollectionView.constant = screenWidth / 3.64

    lblShareInstagram.text = "Instagram Photos (\(arrImageInstagram.count))"
  }

  private func setupView() {
    if let _ = user {
      lblUserName.attributedText = CustomSwipeCardView.getNameAndAge(user)
      lblSchool.text = user.getEducation()
      if lblSchool.text?.characters.count == 0 {
        consBottomLblSchool.constant = 0
      } else {
        consBottomLblSchool.constant = 8
      }

      lblJob.text = user.getWorkHistory()
      if lblJob.text?.characters.count == 0 {
        consBottomLblJob.constant = 0
      } else {
        consBottomLblJob.constant = 8
      }

      if lblJob.text?.characters.count == 0 && lblSchool.text?.characters.count == 0 && lblBudget.text?.characters.count == 0 {
        consTopLblJob.constant = 0
        consBottomLblBudget.constant = 0
      } else {
        consTopLblJob.constant = 20
        consBottomLblBudget.constant = 20
      }

      imgCover.image = nil
      if user.getPrimaryImageUrl() == "" {
        imgCover.image = nil

      } else {
        if let urlImage = NSURL(string: (user.getPrimaryImageUrl())) {
          imgCover.kf_setImageWithURL(urlImage)
        } else {
          imgCover.image = nil
        }
      }

      let strBudgetMin = Common.formatNumber(Int(user.budgetMin))
      let strBudgetMax = Common.formatNumber(Int(user.budgetMax))
      lblBudget.text = "Budget: " + "$\(strBudgetMin) - " + "$\(strBudgetMax)"

      if user.interests.characters.count == 0 {
        consPersonalitiesViewHeight.constant = 0
      } else {
        viewPersonalities.personalString = user.interests
        viewPersonalities.delegate = self
      }

      if user.about == "" {
        lblInfoUser.text = ""
        lblDescriptionTitle.text = ""
        viewLineDescription.alpha = 0
        consTopLblAboutToBottomContainer.constant = 0
        consBottomLblDescriptionToLblAbout.constant = 0
        consTopLblDescriptionToPersonalitiesView.constant = 0
      } else {
        lblInfoUser.text = user.about
      }

      if user.uInstaPhotos.characters.count > 0 {
        lblTitleShareInsta.alpha = 0
        btnConnectInstagram.alpha = 0
        consHeightViewInstagram.constant = 258
        viewInstagram.hidden = false
      } else {
        consHeightViewInstagram.constant = 100
        viewInstagram.hidden = true
      }
    }

    fixSizeHeader()
  }

  private func calculateCellSize() {
    widthCell = (screenWidth - 20 - 55) / 3
  }

  private func fixSizeHeader() {
    headerView.setNeedsLayout()
    headerView.layoutIfNeeded()

    var newFrame = headerView.frame
    newFrame.size.height = headerContentView.frame.height
    headerView.frame = newFrame

    tableView.tableHeaderView = headerView
    tableView.reloadData()
  }

  private func getPhotoInstagram() {
    if let u = user where u.uInstaPhotos.characters.count > 0 && u.uInstaPhotos.containsString("|") {
      let arrPhotos = u.uInstaPhotos.componentsSeparatedByString("|")
      var arrInstagramPhotos = [String]()
      var dem = 0
      for str in arrPhotos {
        if str.characters.count > 0 {
          dem += 1
          arrInstagramPhotos.append(str)
          if dem == 30 {
            break
          }
        }
      }
      arrImageInstagram = arrInstagramPhotos
      collectionViewImage.reloadData()
      lblShareInstagram.text = "Instagram Photos (\(arrImageInstagram.count))"
      pageControlCollection.numberOfPages = Common.getPageCollectionView(self.arrImageInstagram.count)
    }
  }

  private func reportUser() {
    DHIndicator.show()
    AccountFlowManager.shareInstance.reportUser(user.id) { (error) in
      DHIndicator.hide()
      if let e = error {
        Common.showToastWithMessage(e.errMessage)
      } else {
        JLToast.makeText(textReportSuccess, duration: 5).show()
      }
    }
  }

  // MARK: - Actions
  @IBAction func tapToReport(sender: AnyObject) {
    let alertView = UIAlertController(title: appName, message: textConfirmReport, preferredStyle:UIAlertControllerStyle.Alert)
    let actionYes = UIAlertAction(title: "Yes", style: .Default) { [weak self] (action) in
      self?.reportUser()
    }
    alertView.addAction(actionYes)

    let actionNo = UIAlertAction(title: "No", style: .Default) { (action) in

    }
    alertView.addAction(actionNo)
    presentViewController(alertView, animated: true, completion: nil)
  }

  @IBAction func tapClose(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
  }

  @IBAction func tapCancel(sender: AnyObject) {
    delegate?.profileViewDislike()
    dismissViewControllerAnimated(true, completion: nil)
  }

  @IBAction func tapToOk(sender: AnyObject) {
    delegate?.profileViewLike()
    dismissViewControllerAnimated(true, completion: nil)
  }

  @IBAction func tapToConnectInstagram(sender: AnyObject) {
    print("Connect")
  }

}

// MARK: - UICollectionView DataSource
extension ProfileViewVC: UICollectionViewDataSource {

  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if let _ = user where collectionView == coverCollectionView {
      var count = user.images.count
      if count > 5 {
        count = 5
      }

      if count == 0 {
        coverCollectionView.hidden = true
        coverPagingControl.hidden = true
      } else if count == 1 {
        coverCollectionView.hidden = true
        coverPagingControl.hidden = true
      } else {
        coverCollectionView.hidden = false
        coverPagingControl.hidden = false
        coverPagingControl.numberOfPages = count
      }
      return count
    }

    let tmp = arrImageInstagram.count % 3
    if tmp == 0 {
      return arrImageInstagram.count
    } else if tmp == 1 {
      return arrImageInstagram.count + 2
    } else if tmp == 2 {
      return arrImageInstagram.count + 1
    } else {
      return 0
    }
  }

  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    if let u = user {
    if collectionView == coverCollectionView {
      let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CoverImageCell", forIndexPath: indexPath) as! CoverImageCell
      let image = u.images[indexPath.row]
      print(u.images.count)
      cell.setImage(image.urlImg)

      return cell
    }
    }


    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageCell", forIndexPath: indexPath) as! ImageCell
    if indexPath.item >= arrImageInstagram.count {
      cell.imgCollection.image = nil
    } else {
      cell.setImageForCell(arrImageInstagram[indexPath.item])
    }
    return cell
  }
}

// MARK: - UICollectionView Delegate
extension ProfileViewVC: UICollectionViewDelegate {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    if collectionView == coverCollectionView {
      return CGSize(width: screenWidth, height: 320)
    }
    return CGSize(width: widthCell, height: widthCell)
  }

  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    collectionView.deselectItemAtIndexPath(indexPath, animated: false)
  }

}

// MARK: - UIScrollView Delegate
extension ProfileViewVC: UIScrollViewDelegate {
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    if scrollView == coverCollectionView {
      let page = Int(coverCollectionView.contentOffset.x / screenWidth)
      coverPagingControl.currentPage = page
    }

    if scrollView == collectionViewImage {

      let pageWidth  = collectionViewImage.frame.size.width
      let page = Int(round(collectionViewImage.contentOffset.x / (pageWidth)))

      pageControlCollection.currentPage = page
    }
  }
}

// MARK: - ProfileViewPersonalitiesViewDelegate
extension ProfileViewVC: ProfileViewPersonalitiesViewDelegate {
  func profileViewPersonalitiesViewDidLayout(height: CGFloat) {
    print("profileViewPersonalitiesViewDidLayout: \(height)")
    consPersonalitiesViewHeight.constant = height
    print(viewPersonalities.frame)
  }
}
