//
//  CoverImageCell.swift
//  Pack
//
//  Created by admin on 5/27/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class CoverImageCell: UICollectionViewCell {
  
  // MARK: - Outlets
  @IBOutlet weak var imvCover: UIImageView!
  @IBOutlet weak var indicatorView: UIActivityIndicatorView!

  // MARK: - Override methods
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    indicatorView.startAnimating()
  }

  func setImage(urlString: String) {
    indicatorView.startAnimating()
    var fullUrl = ""
    if urlString.containsString("http") {
      fullUrl = urlString
      if fullUrl.containsString("facebook") {
        fullUrl += "?type=large"
      }
    } else if urlString.containsString("https") {
      fullUrl = urlString
    } else {
      fullUrl = apiHost + urlString
    }

    if let url = NSURL(string: fullUrl) {
      imvCover.image = nil
      imvCover.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: {[weak self] (image, error, cacheType, imageURL) in
        self?.indicatorView.stopAnimating()
        if let e = error {
          print("imvCover.kf_setImageWithURL: \(e.localizedDescription)")
          self?.imvCover.image = defaultAvatar
        } else {
          self?.imvCover.image = image
        }
      })
    }
  }
}
