//
//  WebViewVC.swift
//  Pack
//
//  Created by admin on 6/16/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class WebViewVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var webView: UIWebView!

  // MARK: - Public
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "WebViewVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType: LeftItems.Close, centerType: CenterItems.Title, rightType: RightItems.None)
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    setTitleForVC("Terms of Service")

    webView.hidden = true
    NSURLCache.sharedURLCache().removeAllCachedResponses()
    if let cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies {
      for cookie in cookies {
        NSHTTPCookieStorage.sharedHTTPCookieStorage().deleteCookie(cookie)
      }
    }
    guard let url = NSURL(string: urlTermsAndConditions) else {
      dismissViewControllerAnimated(true, completion: nil)
      return
    }
    DHIndicator.show()
    let request = NSURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalAndRemoteCacheData, timeoutInterval: Double(GGWSRequestTimeOutDefault))
    self.webView.loadRequest(request)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    webView.scrollView.contentInset.top = 64
  }
}


extension WebViewVC: UIWebViewDelegate {
  func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
    print(request.URL)

    if !request.URLString.containsString("terms") {
      return false
    }

    return true
  }

  func webViewDidFinishLoad(webView: UIWebView) {
    webView.hidden = false
    DHIndicator.hide()
  }

  func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
    DHIndicator.hide()
    print(error?.localizedDescription)
  }
}
