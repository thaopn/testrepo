//
//  LoadingVC.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/6/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class LoadingVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var imgBackgroundBig: UIImageView!
  @IBOutlet weak var imgBackgroundNormal: UIImageView!
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var imgIconBall1: UIImageView!
  @IBOutlet weak var imgIconBall2: UIImageView!
  @IBOutlet weak var imgIconBall3: UIImageView!
  @IBOutlet weak var lblMatching: UILabel!

  //MARK: - Variables
  private let api = MainScreenManageAPI()

  // MARK: - Lifecycle
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "LoadingVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.Profile, centerType:CenterItems.Title, rightType:RightItems.Message)
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    setTitleForVC("Pack")
    imgIconBall1.alpha = 1
    imgIconBall2.alpha = 1
    imgIconBall3.alpha = 1
    addAnimationForImageAvatar(true)
    addAnimationForIconBall(true)
    configViewDidLoad()
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
  }

  override func viewDidAppear(animated: Bool) {
    imgIconBall1.alpha = 0
    imgIconBall2.alpha = 0
    imgIconBall3.alpha = 0
    super.viewDidAppear(animated)
    api.delegate = self
    api.getListMatchingUser()
    if AppDelegate.shareInstance().isAppTerminal {
      AppDelegate.shareInstance().isLoadingApp = true
      AppDelegate.shareInstance().isAppTerminal = false
      ReceiverNotiControl.shareInstance().receiveNoti(AppDelegate.shareInstance().notiInfo)
    } else {
      AppDelegate.shareInstance().isLoadingApp = false
    }
  }

  // MARK: - Private method
  private func configViewDidLoad() {
    imgAvatar.layer.cornerRadius = imgAvatar.frame.size.width / 2
    imgAvatar.image = nil
    imgAvatar.layer.borderWidth = 1
    imgAvatar.layer.borderColor = UIColor(red: 223/255, green: 223/255, blue: 223/255, alpha: 1).CGColor
    imgAvatar.clipsToBounds = true

    imgIconBall1.layer.cornerRadius = imgIconBall1.frame.size.width / 2
    imgIconBall2.layer.cornerRadius = imgIconBall2.frame.size.width / 2
    imgIconBall3.layer.cornerRadius = imgIconBall3.frame.size.width / 2

    if var strAvatar = AccountFlowManager.currentUser.images.first?.urlImg {
      if strAvatar.containsString("http") || strAvatar.containsString("https") {
        if let url = NSURL(string: strAvatar + "?type=large") {
          imgAvatar.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            if let e = error {
              print("imgAvatar.kf_setImageWithURL: \(e.localizedDescription)")
              self.imgAvatar.image = defaultAvatar
            } else {
              self.imgAvatar.image = image
            }
          })
        }
      } else {
        strAvatar = apiHost + strAvatar + "?type=large"
        if let url = NSURL(string: strAvatar) {
          imgAvatar.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            if let e = error {
              print("imgAvatar.kf_setImageWithURL: \(e.localizedDescription)")
              self.imgAvatar.image = defaultAvatar
            } else {
              self.imgAvatar.image = image
            }
          })
        }
      }
    } else {
      imgAvatar.image = UIImage(named: "img_avatar_default.jpg")
    }
    lblMatching.text = textMatchingLoading
  }

  private func addAnimationForImageAvatar(isAnimation: Bool) {
    let pulseAnimationBig = CABasicAnimation(keyPath: "opacity")
    pulseAnimationBig.duration = 0.8
    pulseAnimationBig.fromValue = 0
    pulseAnimationBig.toValue = 1
    pulseAnimationBig.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationBig.autoreverses = true
    pulseAnimationBig.repeatCount = FLT_MAX

    let pulseAnimationNormal = CABasicAnimation(keyPath: "opacity")
    pulseAnimationNormal.duration = 1
    pulseAnimationNormal.fromValue = 0
    pulseAnimationNormal.toValue = 1
    pulseAnimationNormal.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationNormal.autoreverses = true
    pulseAnimationNormal.repeatCount = FLT_MAX

    if isAnimation {
      imgBackgroundBig.layer.addAnimation(pulseAnimationBig, forKey: nil)
      imgBackgroundNormal.layer.addAnimation(pulseAnimationNormal, forKey: nil)
    } else {
      imgBackgroundBig.layer.removeAllAnimations()
      imgBackgroundNormal.layer.removeAllAnimations()
    }
  }

  private func addAnimationForIconBall(isAnimation: Bool) {
    let pulseAnimationBall1 = CABasicAnimation(keyPath: "opacity")
    pulseAnimationBall1.duration = 0.3
    pulseAnimationBall1.fromValue = 0
    pulseAnimationBall1.toValue = 1
    pulseAnimationBall1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationBall1.autoreverses = true
    pulseAnimationBall1.repeatCount = FLT_MAX

    let pulseAnimationBall2 = CABasicAnimation(keyPath: "opacity")
    pulseAnimationBall2.duration = 0.5
    pulseAnimationBall2.fromValue = 0
    pulseAnimationBall2.toValue = 1
    pulseAnimationBall2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationBall2.autoreverses = true
    pulseAnimationBall2.repeatCount = FLT_MAX

    let pulseAnimationBall3 = CABasicAnimation(keyPath: "opacity")
    pulseAnimationBall3.duration = 0.8
    pulseAnimationBall3.fromValue = 0
    pulseAnimationBall3.toValue = 1
    pulseAnimationBall3.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    pulseAnimationBall3.autoreverses = true
    pulseAnimationBall3.repeatCount = FLT_MAX

    if isAnimation {
      imgIconBall1.layer.addAnimation(pulseAnimationBall1, forKey: nil)
      imgIconBall2.layer.addAnimation(pulseAnimationBall2, forKey: nil)
      imgIconBall3.layer.addAnimation(pulseAnimationBall3, forKey: nil)
    } else {
      imgIconBall1.layer.removeAllAnimations()
      imgIconBall2.layer.removeAllAnimations()
      imgIconBall3.layer.removeAllAnimations()
    }

  }

  private func showInfoNoUser() {
    addAnimationForImageAvatar(false)
    addAnimationForIconBall(false)
    imgIconBall1.alpha = 0
    imgIconBall2.alpha = 0
    imgIconBall3.alpha = 0
    lblMatching.text = textMatchingNoBody
  }

}

// MARK: - MainScreenAPIDelegate
extension LoadingVC: MainScreenAPIDelegate {
  func didRequestListUser(listUser: ListUser!, error: Error?) {

    if let _ = error {
      self.showInfoNoUser()
      AppDelegate.shareInstance().isNotFoundUserMatching = true
    } else {
      if let list = listUser {
        if list.listUser.count > 0 {
          let vc = MainScreenVC()
          vc.listUser = list.listUser
          navigationController?.viewControllers.insert(vc, atIndex: 1)

          AppDelegate.shareInstance().isNotFoundUserMatching = false
        } else {
          self.showInfoNoUser()
          AppDelegate.shareInstance().isNotFoundUserMatching = true
        }
      }
    }
  }

  func likeUser(target: String, matchId: String, error: Error?) {
    
  }
  
  func dislikeUser(error: Error?) {
    
  }
  
  func getStatusLikedCompleted(status: LikeStatusObject?, error: Error?) {
    
  }
}
