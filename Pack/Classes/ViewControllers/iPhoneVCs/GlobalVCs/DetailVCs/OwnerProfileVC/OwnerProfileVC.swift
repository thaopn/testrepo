//
//  OwnerProfileVC.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/5/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import SwiftyJSON

class OwnerProfileVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var imgBackgroundAvatar: UIImageView!
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var lblUserName: UILabel!
  @IBOutlet weak var lblJob: UILabel!
  @IBOutlet weak var lblPlaceWork: UILabel!
  @IBOutlet weak var btnEditProfile: UIButton!
  @IBOutlet weak var btnSetting: UIButton!
  @IBOutlet weak var consHeightContentView: NSLayoutConstraint!

  // MARK: - Lifecycle
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "OwnerProfileVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.None, centerType:CenterItems.Title, rightType:RightItems.Home)
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.setTitleForVC("Profile")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.

    configViewDidLoad()

    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateInfoSuccess), name: NotificationGetInfoAfterUpdateSuccess, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateWorkHistory), name: NotificationUpdateWorkHistorySuccess, object: nil)
  }

  func updateWorkHistory() {
    lblJob.text = AccountFlowManager.currentUser.getWorkHistoryDidTrim()
  }

  func updateInfoSuccess() {
    if var strAvatar = AccountFlowManager.currentUser.images.first?.urlImg {
      if strAvatar.containsString("http") || strAvatar.containsString("https") {
        if let url = NSURL(string: strAvatar + "?type=large") {
          imgAvatar.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            if let e = error {
              print("imgAvatar.kf_setImageWithURL: \(e.localizedDescription)")
              self.imgAvatar.image = defaultAvatar
            } else {
              self.imgAvatar.image = image
            }
          })
        }
      } else {
        strAvatar = apiHost + strAvatar + "?type=large"
        if let url = NSURL(string: strAvatar) {
          imgAvatar.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            if let e = error {
              print("imgAvatar.kf_setImageWithURL: \(e.localizedDescription)")
              self.imgAvatar.image = defaultAvatar
            } else {
              self.imgAvatar.image = image
            }
          })
        }
      }
    } else {
      imgAvatar.image = UIImage(named: "img_avatar_default.jpg")
    }

    lblUserName.text = AccountFlowManager.currentUser.getFirstName()
    lblJob.text = AccountFlowManager.currentUser.getWorkHistoryDidTrim()
    lblPlaceWork.text = AccountFlowManager.currentUser.getEducation()

  }

  // MARK: - Private method
  private func configViewDidLoad() {
    imgBackgroundAvatar.layer.cornerRadius = imgBackgroundAvatar.frame.size.width / 2
    imgAvatar.layer.cornerRadius = imgAvatar.frame.size.width / 2
    imgAvatar.image = nil
    imgAvatar.layer.borderWidth = 1
    imgAvatar.layer.borderColor = UIColor(red: 223/255, green: 223/255, blue: 223/255, alpha: 1).CGColor
    imgAvatar.clipsToBounds = true

    btnEditProfile.layer.cornerRadius = btnEditProfile.frame.size.height / 2
    btnEditProfile.layer.borderWidth = 1
    btnEditProfile.layer.borderColor = UIColor(red: 81/255, green: 156/255, blue: 255/255, alpha: 1).CGColor

    btnSetting.layer.cornerRadius = btnSetting.frame.size.height / 2
    btnSetting.layer.borderWidth = 1
    btnSetting.layer.borderColor = UIColor(red: 81/255, green: 156/255, blue: 255/255, alpha: 1).CGColor

    if var strAvatar = AccountFlowManager.currentUser.images.first?.urlImg {
      if strAvatar.containsString("http") || strAvatar.containsString("https") {
        if let url = NSURL(string: strAvatar + "?type=large") {
          imgAvatar.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            if let e = error {
              print("imgAvatar.kf_setImageWithURL: \(e.localizedDescription)")
              self.imgAvatar.image = defaultAvatar
            } else {
              self.imgAvatar.image = image
            }
          })
        }
      } else {
        strAvatar = apiHost + strAvatar + "?type=large"
        if let url = NSURL(string: strAvatar) {
          imgAvatar.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            if let e = error {
              print("imgAvatar.kf_setImageWithURL: \(e.localizedDescription)")
              self.imgAvatar.image = defaultAvatar
            } else {
              self.imgAvatar.image = image
            }
          })
        }
      }
    } else {
      imgAvatar.image = UIImage(named: "img_avatar_default.jpg")
    }

    lblUserName.text = AccountFlowManager.currentUser.getFirstName()
    lblJob.text = AccountFlowManager.currentUser.getWorkHistoryDidTrim()
    lblPlaceWork.text = AccountFlowManager.currentUser.getEducation()
  }

  // MARK: - Action
  @IBAction func tapToEditProfile(sender: AnyObject) {
    let vc = CreateProfileVC(update: false)
    navigationController?.pushViewController(vc, animated: true)
  }

  @IBAction func tapToSetting(sender: AnyObject) {
    let vc = SettingVC()
    navigationController?.pushViewController(vc, animated: true)
  }

  override func tapToHome(sender: UIButton!) {
    navigationController?.popViewControllerAnimated(true)
  }

}
