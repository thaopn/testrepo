//
//  MatchVC.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/5/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

protocol MatchDelegate: NSObjectProtocol {
  func gotoMessage(user: User)
}

class MatchVC: GGParentVC {

  // MARK: - Outlets
  @IBOutlet weak var lblTitleMatch: UILabel!
  @IBOutlet weak var imgAvatarLeft: UIImageView!
  @IBOutlet weak var imgAvatarRight: UIImageView!
  @IBOutlet weak var btnSendMessage: UIButton!
  @IBOutlet weak var btnKeepExploring: UIButton!
  @IBOutlet weak var indicatorRight: UIActivityIndicatorView!
  @IBOutlet weak var indicatorLeft: UIActivityIndicatorView!

  //MARK: - Variables
  weak var delegate: MatchDelegate?
  var targetUser: User?
  var matchId: String = ""

  // MARK: - Lifecycle
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "MatchVC", bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init() {
    self.init(leftType:LeftItems.Close, centerType:CenterItems.None, rightType:RightItems.None)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.

    configViewDidLoad()
  }


  // MARK: - Private method
  private func configViewDidLoad() {
    indicatorLeft.startAnimating()
    indicatorRight.startAnimating()
    imgAvatarLeft.layer.cornerRadius = imgAvatarLeft.frame.size.width / 2
    imgAvatarLeft.image = nil
    imgAvatarRight.layer.cornerRadius = imgAvatarRight.frame.size.width / 2
    imgAvatarRight.image = nil

    btnSendMessage.layer.cornerRadius = btnSendMessage.frame.size.height / 2
    btnKeepExploring.layer.cornerRadius = btnKeepExploring.frame.size.height / 2
    btnKeepExploring.layer.borderWidth = 1
    btnKeepExploring.layer.borderColor = UIColor(red: 134/255, green: 186/255, blue: 255/255, alpha: 1).CGColor

    if let target = targetUser {

      if target.images.count == 0 && target.primaryImage.characters.count > 0 {
        let newImage = Image()
        newImage.urlImg = target.primaryImage
        target.images.append(newImage)
      }

      if let url = NSURL(string: target.getPrimaryImageUrl()) {
        imgAvatarRight.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { [weak self] (image, error, cacheType, imageURL) in
          self?.indicatorRight.stopAnimating()
          if let _ = error {

          } else {
            self?.imgAvatarRight.image = image
          }
        })
      }

      if let url = NSURL(string: AccountFlowManager.currentUser.getPrimaryImageUrl()) {
        imgAvatarLeft.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { [weak self] (image, error, cacheType, imageURL) in
          self?.indicatorLeft.stopAnimating()
          if let _ = error {

          } else {
            self?.imgAvatarLeft.image = image
          }
        })
      }

      let strName = "\(target.getUserName())"
      lblTitleMatch.text = "You and \(strName) liked each other Start chatting now!"
    }
  }

  // MARK: - Actions
  @IBAction func tapToSendMessage(sender: AnyObject) {
    delegate?.gotoMessage(targetUser!)
    dismissViewControllerAnimated(true, completion: nil)
  }

  @IBAction func tapToKeepExploring(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
  }

  @IBAction func tapToClosed(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}
