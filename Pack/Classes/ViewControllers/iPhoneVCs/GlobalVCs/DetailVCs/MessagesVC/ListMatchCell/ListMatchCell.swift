//
//  ListMatchCell.swift
//  Pack
//
//  Created by iOS on 5/27/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

protocol ListMatchCellDelegate {
  func didChooseListMatchItem(matchId: String, currentIndexPath: NSIndexPath?, user: User) -> Void
  func didShowLoadMore() -> Void
}

class ListMatchCell: UITableViewCell {

  // MARK: - Outlet
  @IBOutlet weak var collectionView: UICollectionView!

  // MARK: - Variable
  var listMatched: [UserMatched] = []
  var user: User = User()
  var delegate: ListMatchCellDelegate?

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = UITableViewCellSelectionStyle.None
    collectionView.registerNib(UINib(nibName: "UserMatchCell", bundle: nil), forCellWithReuseIdentifier: "UserMatchCell")
    collectionView.dataSource = self
    collectionView.delegate = self
  }
  
}

// MARK: - UICollectionViewDelegate
extension ListMatchCell: UICollectionViewDelegate {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return CGSize(width: 84, height: 130)
  }

  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    let userMatched = listMatched[indexPath.row]
    user.id = userMatched.userId
    user.firstName = userMatched.firstName
    user.lastName = userMatched.lastName
    user.fullName = userMatched.fullName
    user.images = userMatched.images
    user.dateMatch = userMatched.dateMatch

    if delegate?.didChooseListMatchItem(userMatched.matchedId, currentIndexPath: indexPath, user: user) != nil {
      
    }
  }
}

extension ListMatchCell: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return listMatched.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("UserMatchCell", forIndexPath: indexPath) as! UserMatchCell
    let match = listMatched[indexPath.row]
    cell.lblName.text = match.getUserName()
    if let url = NSURL(string: match.getPrimaryImageUrl()) {
      cell.imgAvatar.kf_setImageWithURL(url)
    } else {
      cell.imgAvatar.image = defaultAvatar
    }
    return cell
  }
}
