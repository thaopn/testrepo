//
//  UserMatchCell.swift
//  Pack
//
//  Created by iOS on 5/27/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class UserMatchCell: UICollectionViewCell {

  // MARK: - Outlet
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var lblName: UILabel!

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    imgAvatar.layer.cornerRadius = imgAvatar.frame.size.height / 2
    imgAvatar.clipsToBounds = true
  }

}
