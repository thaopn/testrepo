//
//  MessageCell.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/7/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

  // MARK: - Outlets
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var lblUserName: UILabel!
  @IBOutlet weak var lblDayMatched: UILabel!
  @IBOutlet weak var lblTimeMessage: UILabel!
  @IBOutlet weak var viewActive: UIView!

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    viewActive.layer.cornerRadius = viewActive.frame.size.height / 2
    selectionStyle = UITableViewCellSelectionStyle.None
    imgAvatar.layer.cornerRadius = imgAvatar.frame.size.height / 2
    imgAvatar.clipsToBounds = true
  }

}
