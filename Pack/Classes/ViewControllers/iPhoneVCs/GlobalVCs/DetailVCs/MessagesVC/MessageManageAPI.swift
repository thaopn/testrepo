//
//  MessageFlowManager.swift
//  Pack
//
//  Created by iOS on 5/27/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import Async
import ObjectMapper

@objc protocol MessageManageAPIDelegate {
  optional func didGetListMatched(listMatched: ListMatched, error: Error?) -> Void
  optional func didGetListMatchedSearch(listMatched: ListMatched, error: Error?) -> Void
}

class MessageManageAPI: NSObject {
  
  weak var delegate: MessageManageAPIDelegate?

  func getListUserMatched(page: Int) {
    let url = apiGetListUserMatched(AccountFlowManager.currentUser.id, page: page)
    GGWebservice.callWebServiceWithRequest(.GET, urlString: url, param: nil, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
      if let value = value {
        if statusCode == 200 {
          let data: ListMatched = Mapper<ListMatched>().map(value)!
          if self.delegate?.didGetListMatched!(data, error: nil) != nil {

          }
        } else {
          let error = Mapper<Error>().map(value)!
          if self.delegate?.didGetListMatched!(ListMatched(), error: error) != nil {

          }
        }
      } else {
        if self.delegate?.didGetListMatched!(ListMatched(), error: Error()) != nil {

        }
      }
    }
  }

  func updateStatusMatched(matchedId: String) {
    let url = getApiUpdateStatusMatch(matchedId)
    let param = [
      "chats" : "1"
    ]
    GGWebservice.callWebServiceWithRequest(.PATCH, urlString: url, param: param, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
      if statusCode == 200 {
        print("Update status chat success")
      } else {
        print("Update status chat error")
      }
    }
  }

  func searchListMatched(userId: String, text: String) {
    let url = apiSearchListMatched(userId, textSearch: text.encoding())
    GGWebservice.callWebServiceWithRequest(.GET, urlString: url, param: nil, accessToken: nil, isAuthorization: false) { (value, statusCode, error) in
      if statusCode == 200 {
        if let value = value {
          let data: ListMatched = Mapper<ListMatched>().map(value)!
          if self.delegate?.didGetListMatchedSearch!(data, error: nil) != nil {

          }
        } else {
          if self.delegate?.didGetListMatchedSearch!(ListMatched(), error: nil) != nil {

          }
        }
      } else {
        if let value = value {
          let error = Mapper<Error>().map(value)!
          if self.delegate?.didGetListMatchedSearch!(ListMatched(), error: error) != nil {

          }
        } else {
          if self.delegate?.didGetListMatchedSearch!(ListMatched(), error: Error()) != nil {

          }
        }
      }
    }
  }
  
}
