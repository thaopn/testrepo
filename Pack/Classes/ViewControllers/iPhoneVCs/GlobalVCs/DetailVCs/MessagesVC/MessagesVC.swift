//
//  MessagesVC.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/7/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit
import LayerKit
import Async

class MessagesVC: GGParentVC {
  
  // MARK: - Outlet
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var tfSearch: UITextField!
  @IBOutlet weak var btnSearch: UIButton!
  @IBOutlet weak var consLeftBtnSearchToView: NSLayoutConstraint!
  @IBOutlet weak var lblNoMessage: UILabel!
  
  // MARK: - Variable
  let api = MessageManageAPI()
  var layerClient = AppDelegate.shareInstance().layerClient
  var conversations: NSOrderedSet?
  var queryController: LYRQueryController?
  var userName: String = ""
  var avatar: String = ""
  var dateFormatter: NSDateFormatter = NSDateFormatter()
  var countUnread: Int = 0
  var isSearching: Bool = false
  var searchConversations: [LYRConversation] = []
  var countError: Int = 0
  var refreshHeader: GGRefreshHeader!
  var isLoading: Bool = false
  var listMatched: [UserMatched] = []
  var isLoadingNewMatches: Bool = true
  var isLoadingMessage: Bool = true
  var page: Int = 1
  var currentIndexPath: NSIndexPath?
  var currentMatchedId: String = ""
  var isLoadingMoreMatches: Bool = false
  var isObjectDidChange: Bool = false
  var timer: NSTimer!
  var keyWord: String = ""
  var listMatchSearch: [UserMatched] = []
  var isDidLoseConnection: Bool = false
  var isShowAlertLoseConnection: Bool = false
  
  // MARK: - Lifecycle
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: "MessagesVC", bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  convenience init() {
    self.init(leftType:LeftItems.Home, centerType:CenterItems.Title, rightType:RightItems.None)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setTitleForVC("Messages")
    configViewDidLoad()
    configNotification()
    layerClient.delegate = self
    api.delegate = self
    DHIndicator.show()
    checkConnect()
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    self.view.endEditing(true)
  }
  
  // MARK: - Public method
  func textDidChange() {
    if tfSearch.hasText() {
      btnSearch.alpha = 0
    } else {
      btnSearch.alpha = 1
    }
    keyWord = tfSearch.text!
    let textNoEmpty = tfSearch.text?.characters.count > 0
    isSearching = textNoEmpty
    guard let listConversation = conversations else {
      return
    }
    if listMatched.count > 0 || listConversation.count > 0 {
      if isSearching {
        if let _ = timer {
          timer.invalidate()
          timer = nil
        }
        if keyWord.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != "" {
          keyWord = keyWord.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
          timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(MessagesVC.getListMatchSearch), userInfo: nil, repeats: false)
        } else {
     
        }
      } else {
        isSearching = false
        lblNoMessage.alpha = 0
        tableView.reloadData()
      }
    } else {
      tableView.reloadData()
    }
  }
  
  func updateMessage(notification: NSNotification) {
    countUnread = 0
    fetchLayerConversation()
  }
  
  func updateListMatched(notification: NSNotification) {
    if currentMatchedId.characters.count > 0 {
      api.updateStatusMatched(currentMatchedId)
      guard let indexPath = currentIndexPath else {return}
      if !isSearching {
        if listMatched.count > 0 {
          listMatched.removeAtIndex(indexPath.row)
          tableView.reloadData()
        }
      }
      currentMatchedId = ""
    }
  }
  
  func getListMatchSearch() {
    if keyWord.characters.count > 0 {
      api.searchListMatched(AccountFlowManager.currentUser.id, text: keyWord)
    }
  }
  
  func getCellMessageWithIndexPath(indexPath: NSIndexPath) -> MessageCell {
    var conversation: LYRConversation?
    if let arrayConversation = conversations?.array {
      conversation = arrayConversation[indexPath.row - 1] as? LYRConversation
    }
    let cell = tableView.dequeueReusableCellWithIdentifier("MessageCell", forIndexPath: indexPath) as! MessageCell
    print(conversation?.metadata)
    if let metadata = conversation?.metadata {
      if let participant1 = metadata["participant1"] {
        if let userId = participant1["userID"] as? String {
          if AccountFlowManager.currentUser.id == userId {
            if let participant2 = metadata["participant2"] {
              if let name = participant2["displayname"] as? String {
                userName = name
              } else {
                userName = "No user name"
              }
              if let imageUrl = participant2["avatarImageUrl"] as? String {
                avatar = imageUrl
              } else {
                avatar = "http://i.imgur.com/FvBtAEV.png"
              }
            }
          } else {
            if let name = participant1["displayname"] as? String {
              userName = name
            } else {
              userName = "No user name"
            }
            if let imageUrl = participant1["avatarImageUrl"] as? String {
              avatar = imageUrl
            } else {
              avatar = "http://i.imgur.com/FvBtAEV.png"
            }
          }
        } else {
          userName = "No user name"
          avatar = "http://i.imgur.com/FvBtAEV.png"
        }
      }
    } else {
      userName = "No user name"
      avatar = "http://i.imgur.com/FvBtAEV.png"
    }
    let array = avatar.componentsSeparatedByString("/")
    if array.count > 3 {
    } else {
      avatar = apiHost + avatar
    }
    cell.lblUserName.text = userName
    if let url = NSURL(string: avatar) {
      cell.imgAvatar.kf_setImageWithURL(url)
    }
    if let lastMessage = conversation?.lastMessage {
      if lastMessage.parts.count > 0 {
        if let messagePart: LYRMessagePart = lastMessage.parts[0] {
          if messagePart.MIMEType == "image/png" {
            cell.lblDayMatched.text = "(Attached an image)"
          } else {
            cell.lblDayMatched.text = NSString(data: messagePart.data!, encoding: NSUTF8StringEncoding) as? String
          }
          var dateSent = NSDate()
          dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          if let sentAt = conversation?.lastMessage?.sentAt {
            dateSent = dateFormatter.dateFromString(dateFormatter.stringFromDate(sentAt))!
          } else {
            dateSent = dateFormatter.dateFromString(dateFormatter.stringFromDate(dateSent))!
          }
          dateFormatter.dateFormat = "HH:mm"
          let time = dateFormatter.stringFromDate(dateSent)
          dateFormatter.dateFormat = "yyyy-MM-dd"
          let date = dateFormatter.dateFromString(dateFormatter.stringFromDate(dateSent))
          if DHCalendarData.equalToDate(date!, ToCompare: DHCalendarData.getCurrentDate()) {
            cell.lblTimeMessage.text = Common.formatTime(time)
          } else if DHCalendarData.checkDateInWeekCurrent(date!) {
            var day = ""
            if DHCalendarData.equalToDate(date!, ToCompare: DHCalendarData.prevDateFrom(DHCalendarData.getCurrentDate())) {
              day = "YESTERDAY"
            } else {
              day = DHCalendarData.nameDayWithNumberDay(DHCalendarData.getWeekDayWithDate(date!))
            }
            cell.lblTimeMessage.text = day
          } else {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            cell.lblTimeMessage.text = dateFormatter.stringFromDate(date!)
          }
        } else {
          cell.lblDayMatched.text = "No message"
        }
      } else {
        cell.lblDayMatched.text = "No message"
      }
    }
    return cell
  }
  
  func getCellNewMatchesWithIndexPath(indexPath: NSIndexPath) -> ListMatchCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("ListMatchCell", forIndexPath: indexPath) as! ListMatchCell
    if isSearching {
      cell.listMatched = listMatchSearch
    } else {
      cell.listMatched = listMatched
    }
    cell.delegate = self
    cell.collectionView.reloadData()
    return cell
  }
  
  func getCellHeaderMatchWithIndexPath(indexPath: NSIndexPath, title: String) -> HeaderMessageCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("HeaderMessageCell", forIndexPath: indexPath) as! HeaderMessageCell
    cell.viewNumberMessage.alpha = 0
    cell.lblTitle.text = title
    return cell
  }
  
  func getCellHeaderMessageWithIndexPath(indexPath: NSIndexPath) -> HeaderMessageCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("HeaderMessageCell", forIndexPath: indexPath) as! HeaderMessageCell
    cell.lblTitle.text = "MESSAGES"
    if countUnread == 0 {
      cell.viewNumberMessage.alpha = 0
    } else {
      cell.viewNumberMessage.alpha = 1
      cell.lblNumberMessage.text = "\(countUnread)"
    }
    return cell
  }
  
  func checkConnect() {
    if layerClient.isConnected && !layerClient.isConnecting {
      if layerClient.authenticatedUser != nil && layerClient.authenticatedUser?.userID == AccountFlowManager.currentUser.id {
        api.getListUserMatched(page)
      } else {
        AppDelegate.shareInstance().authenticateLayerWithUserID(AccountFlowManager.currentUser.id, completion: { [weak self] (isSuccess, error) in
          if isSuccess {
            self?.api.getListUserMatched(self!.page)
          } else {
            DHIndicator.hide()
            self?.refreshHeader.endRefreshing()
            self?.showAlertWithError(error?.localizedDescription)
          }
          
          })
      }
    } else {
      connectLayerMessage()
    }
  }
  
  // MARK: - Private method
  private func configViewDidLoad() {
    tableView.registerNib(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: "MessageCell")
    tableView.registerNib(UINib(nibName: "HeaderMessageCell", bundle: nil), forCellReuseIdentifier: "HeaderMessageCell")
    tableView.registerNib(UINib(nibName: "ListMatchCell", bundle: nil), forCellReuseIdentifier: "ListMatchCell")
    tableView.separatorColor = UIColor.clearColor()
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 160.0
    consLeftBtnSearchToView.constant = (screenWidth / 2) - 33
    tfSearch.addTarget(self, action: #selector(textDidChange), forControlEvents: UIControlEvents.EditingChanged)
    let paddingTfSearch = UIView(frame: CGRect(x: 0, y: 0, width: 22, height: tfSearch.frame.size.height))
    tfSearch.layer.cornerRadius = tfSearch.frame.size.height / 2
    tfSearch.delegate = self
    tfSearch.leftView = paddingTfSearch
    tfSearch.leftViewMode = .Always
    refreshHeader = GGRefreshHeader(scrollView: tableView)
    refreshHeader.beginRefreshingBlock = {[weak self]() in
      if self?.isLoading == false && self?.isSearching == false && self?.isDidLoseConnection == false {
        self?.isLoading = true
        self?.isLoadingMoreMatches = false
        self?.lblNoMessage.alpha = 0
        self?.checkConnect()
      }
    }
  }
  
  private func fetchLayerConversation() {
    isLoading = false
    refreshHeader.enableRefresh = true
    let query: LYRQuery = LYRQuery(queryableClass: LYRConversation.self)
    query.sortDescriptors = [NSSortDescriptor(key: "lastMessage.receivedAt", ascending: false)]
    layerClient!.executeQuery(query) {[weak self] (conversations, error) -> Void in
      if error == nil {
        self?.conversations = conversations
        DHIndicator.hide()
        self?.refreshHeader.endRefreshing()
        if conversations?.count == 0 {
          if self?.listMatched.count > 0 {
            self?.lblNoMessage.alpha = 0
          } else {
            self?.lblNoMessage.alpha = 1
          }
        } else {
          self?.lblNoMessage.alpha = 0
        }
        print("Conversation: ", conversations?.count)
        if conversations != nil {
          for conversation in conversations! {
            if let lastMessage = conversation.lastMessage {
              if let isUnread = lastMessage?.isUnread {
                if isUnread {
                  self?.countUnread = (self?.countUnread)! + 1
                }
              }
            }
          }
        }
        self?.tableView.reloadData()
        print("Unread: \(self?.countUnread)")
      } else {
        DHIndicator.hide()
        self?.refreshHeader.endRefreshing()
        self?.tableView.reloadData()
        print(error?.localizedDescription)
      }
    }
    isObjectDidChange = false
  }
  
  private func configNotification() {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateMessage(_:)), name: NotificationUpdateMessage, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateListMatched(_:)), name: NotificationUpdateListMatched, object: nil)
  }
  
  private func showAlertWithError(message: String!) {
    let alertView: UIAlertController = UIAlertController(title: appName, message: message, preferredStyle:UIAlertControllerStyle.Alert)
    let action = UIAlertAction(title: "Retry", style: UIAlertActionStyle.Default) { _ in
      self.checkConnect()
    }
    let actionCancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { _ in
    }
    alertView.addAction(action)
    alertView.addAction(actionCancel)
    self.presentViewController(alertView, animated: true, completion: nil)
  }
  
  private func connectLayerMessage() {
    layerClient.connectWithCompletion({ [weak self] (isSuccess, error) in
      if isSuccess {
        AppDelegate.shareInstance().authenticateLayerWithUserID(AccountFlowManager.currentUser.id, completion: {
          (isSuccess, error) in
          if isSuccess {
            print("Connect success")
            Async.main(after: 5, block: {
              self?.api.getListUserMatched((self?.page)!)
            })
          } else {
            DHIndicator.hide()
            self?.refreshHeader.endRefreshing()
            self?.showAlertWithError(error?.localizedDescription)
          }
        })
      } else {
        DHIndicator.hide()
        self?.refreshHeader.endRefreshing()
        if error?.code != 6000 && error?.code != 6002 {
          self?.showAlertWithError(error?.localizedDescription)
        }
      }
      })
  }
  
  // MARK: - Actions
  @IBAction func tapToSearch(sender: AnyObject) {
    consLeftBtnSearchToView.constant = 26
    tfSearch.becomeFirstResponder()
  }
  override func tapToHome(sender: UIButton!) {
    navigationController?.popViewControllerAnimated(true)
  }
}

// MARK: - UITextField Delegate
extension MessagesVC: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if !textField.hasText() {
      consLeftBtnSearchToView.constant = (screenWidth / 2) - 33
    }
    textField.resignFirstResponder()
    return true
  }
  
  func textFieldDidBeginEditing(textField: UITextField) {
    consLeftBtnSearchToView.constant = 26
  }
}

// MARK: - UITableView DataSource
extension MessagesVC: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    if isSearching {
      return 1
    } else {
      guard let listConversation = conversations else {
        return 1
      }
      if listConversation.count > 0 && listMatched.count > 0 {
        return 2
      } else {
        return 1
      }
    }
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if isSearching {
      if listMatchSearch.count > 0 {
        return 2
      } else {
        return 0
      }
    } else {
      guard let listConversation = conversations else {
        if listMatched.count > 0 {
          return 2
        } else {
          return 0
        }
      }
      if listConversation.count > 0 && listMatched.count > 0 {
        if section == 0 {
          return 2
        } else {
          return listConversation.count + 1
        }
      } else if listConversation.count > 0 && listMatched.count == 0 {
        return listConversation.count + 1
      } else if listConversation.count == 0 && listMatched.count > 0 {
        return 2
      } else {
        return 0
      }
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if isSearching {
      if indexPath.row == 0 {
        return getCellHeaderMatchWithIndexPath(indexPath, title: "MATCHES")
      } else {
        return getCellNewMatchesWithIndexPath(indexPath)
      }
    } else {
      if conversations?.count > 0 && listMatched.count > 0 {
        if indexPath.section == 0 {
          if indexPath.row == 0 {
            return getCellHeaderMatchWithIndexPath(indexPath, title: "NEW MATCHES")
          } else {
            return getCellNewMatchesWithIndexPath(indexPath)
          }
        } else {
          if indexPath.row == 0 {
            return getCellHeaderMessageWithIndexPath(indexPath)
          } else {
            return getCellMessageWithIndexPath(indexPath)
          }
        }
      } else if conversations?.count > 0 && listMatched.count == 0 {
        if indexPath.row == 0 {
          return getCellHeaderMessageWithIndexPath(indexPath)
        } else {
          return getCellMessageWithIndexPath(indexPath)
        }
      } else if conversations?.count == 0 || conversations == nil && listMatched.count > 0 {
        if indexPath.row == 0 {
          return getCellHeaderMatchWithIndexPath(indexPath, title: "NEW MATCHES")
        } else {
          return getCellNewMatchesWithIndexPath(indexPath)
        }
      } else {
        let cell = tableView.dequeueReusableCellWithIdentifier("HeaderMessageCell", forIndexPath: indexPath) as! HeaderMessageCell
        return cell
      }
    }
  }
}

// MARK: - UITableView Delegate
extension MessagesVC: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.row == 0 {
      return
    }
    let user = User()
    var conversation: LYRConversation?
    guard let conversations = conversations else {
      return
    }
    conversation = conversations.array[indexPath.row - 1] as? LYRConversation
    if let metadata = conversation?.metadata {
      if let participant1 = metadata["participant1"] {
        if let userId = participant1["userID"] as? String {
          if AccountFlowManager.currentUser.id == userId {
            if let participant2 = metadata["participant2"] {
              if let id = participant2["userID"] as? String {
                user.id = id
              }
              if let name = participant2["displayname"] as? String {
                user.username = name
              }
              if let imageUrl = participant2["avatarImageUrl"] as? String {
                user.primaryImage = imageUrl
              }
            }
          } else {
            if let id = participant1["userID"] as? String {
              user.id = id
            }
            if let name = participant1["displayname"] as? String {
              user.username = name
            }
            if let imageUrl = participant1["avatarImageUrl"] as? String {
              user.primaryImage = imageUrl
            }
          }
        }
      }
      let vc = MessagesDetailVC(user: user, isNewConversation: false, conversation: conversation!)
      navigationController?.pushViewController(vc, animated: true)
    } else {
      Common.showToastWithMessage(messageDataWrong)
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if isSearching {
      if indexPath.row == 0 {
        return 50
      } else {
        return 130
      }
    } else {
      if conversations?.count > 0 && listMatched.count > 0 {
        if indexPath.section == 0 {
          if indexPath.row == 0 {
            return 50
          } else {
            return 130
          }
        } else {
          if indexPath.row == 0 {
            return 50
          } else {
            return UITableViewAutomaticDimension
          }
        }
      } else if conversations?.count > 0 && listMatched.count == 0 {
        if indexPath.row == 0 {
          return 50
        } else {
          return UITableViewAutomaticDimension
        }
      } else if conversations?.count == 0 || conversations == nil && listMatched.count > 0 {
        if indexPath.row == 0 {
          return 50
        } else {
          return 130
        }
      } else {
        return UITableViewAutomaticDimension
      }
    }
  }
}

// MARK: - LYRClientDelegate
extension MessagesVC: LYRClientDelegate {
  func layerClient(client: LYRClient, didReceiveAuthenticationChallengeWithNonce nonce: String) {
    print("Layer client did receive authentication challenge with nonce: \(nonce)")
    AppDelegate.shareInstance().requestIdentityTokenForUserID(AccountFlowManager.currentUser.id, nonce: nonce) { [weak self](isSuccess, error) in
      if isSuccess {
        self?.fetchLayerConversation()
      } else {
        DHIndicator.hide()
        print(error?.localizedDescription)
      }
    }
  }
  
  func layerClientDidConnect(client: LYRClient) {
    if isDidLoseConnection {
      DHIndicator.show()
      checkConnect()
    }
  }
  
  func layerClientDidDisconnect(client: LYRClient) {
    DHIndicator.hide()
    refreshHeader.endRefreshing()
    isDidLoseConnection = true
  }
  
  func layerClient(client: LYRClient, objectsDidChange changes: [LYRObjectChange]) {
    if !isObjectDidChange {
      isObjectDidChange = true
    }
    tableView.reloadData()
  }
  
  func layerClient(client: LYRClient, didLoseConnectionWithError error: NSError) {
    isDidLoseConnection = true
  }
  
  func layerClient(client: LYRClient, willAttemptToConnect attemptNumber: UInt, afterDelay delayInterval: NSTimeInterval, maximumNumberOfAttempts attemptLimit: UInt) {
    if attemptNumber > 1 {
      isDidLoseConnection = true
    } else {
      isDidLoseConnection = false
    }
    if isDidLoseConnection {
      DHIndicator.hide()
      refreshHeader.endRefreshing()
    } else {
      
    }
  }
}

// MARK: - MessageManageAPIDelegate
extension MessagesVC: MessageManageAPIDelegate {
  func didGetListMatched(listMatched: ListMatched, error: Error?) {
    refreshHeader.enableRefresh = true
    if let e = error {
      Common.showToastWithMessage(e.errMessage)
    } else {
      if isLoadingMoreMatches {
        for matched in listMatched.matches {
          if matched.userId != "" {
            self.listMatched.append(matched)
          }
        }
      } else {
        self.listMatched.removeAll()
        for matched in listMatched.matches {
          if matched.userId != "" {
            self.listMatched.append(matched)
          }
        }
      }
    }
    countUnread = 0
    fetchLayerConversation()
  }
  
  func didGetListMatchedSearch(listMatched: ListMatched, error: Error?) {
    if let e = error {
      Common.showToastWithMessage(e.errMessage)
    } else {
      listMatchSearch = listMatched.matches
    }
    if listMatchSearch.count > 0 {
      lblNoMessage.alpha = 0
    } else {
      lblNoMessage.alpha = 1
    }
    tableView.reloadData()
  }
}

// MARK: - ListMatchCellDelegate
extension MessagesVC: ListMatchCellDelegate {
  func didChooseListMatchItem(matchId: String, currentIndexPath: NSIndexPath?, user: User) {
    self.currentIndexPath = currentIndexPath
    currentMatchedId = matchId
    let vc = MessagesDetailVC(user: user, isNewConversation: true, conversation: nil)
    vc.isNeedUpdateStatus = true
    navigationController?.pushViewController(vc, animated: true)
  }
  
  func didShowLoadMore() {
    
  }
  
}
