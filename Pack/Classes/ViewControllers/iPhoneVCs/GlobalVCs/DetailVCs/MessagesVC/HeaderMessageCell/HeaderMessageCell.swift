//
//  HeaderMessageCell.swift
//  Pack
//
//  Created by Le Kim Tuan on 5/7/16.
//  Copyright © 2016 ThaoPN. All rights reserved.
//

import UIKit

class HeaderMessageCell: UITableViewCell {

  // MARK: - Outlet
  @IBOutlet weak var lblNumberMessage: UILabel!
  @IBOutlet weak var viewNumberMessage: UIView!
  @IBOutlet weak var lblTitle: UILabel!

  // MARK: - Override method
  override func awakeFromNib() {
    super.awakeFromNib()
    viewNumberMessage.layer.cornerRadius = viewNumberMessage.frame.size.height / 2
    lblNumberMessage.text = "0"
    selectionStyle = UITableViewCellSelectionStyle.None
  }

}
